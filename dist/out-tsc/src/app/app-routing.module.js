var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
//routes
var routes = [
    { path: '', loadChildren: './pages/accueil/accueil.module#AccueilPageModule' },
    { path: 'login', loadChildren: './pages/login/login.module#LoginPageModule' },
    { path: 'register', loadChildren: './pages/register/register.module#RegisterPageModule' },
    { path: 'about', loadChildren: './pages/about/about.module#AboutPageModule' },
    { path: 'settings', loadChildren: './pages/settings/settings.module#SettingsPageModule' },
    { path: 'edit-profile', loadChildren: './pages/edit-profile/edit-profile.module#EditProfilePageModule' },
    { path: 'home-results', loadChildren: './pages/home-results/home-results.module#HomeResultsPageModule' },
    { path: 'accueiltwo', loadChildren: './pages/accueiltwo/accueiltwo.module#AccueiltwoPageModule' },
    { path: 'identification', loadChildren: './pages/identification/identification.module#IdentificationPageModule' },
    { path: 'contact', loadChildren: './pages/contact/contact.module#ContactPageModule' },
    { path: 'uploaddoc', loadChildren: './pages/uploaddoc/uploaddoc.module#UploaddocPageModule' },
    { path: 'mycar', loadChildren: './pages/mycar/mycar.module#MycarPageModule' },
    { path: 'myprofile', loadChildren: './pages/myprofile/myprofile.module#MyprofilePageModule' },
    { path: 'choiceoption', loadChildren: './pages/choiceoption/choiceoption.module#ChoiceoptionPageModule' },
    { path: 'infocar', loadChildren: './pages/infocar/infocar.module#InfocarPageModule' },
    { path: 'comptecredit', loadChildren: './pages/comptecredit/comptecredit.module#ComptecreditPageModule' },
    { path: 'resultatssimulation', loadChildren: './pages/resultatssimulation/resultatssimulation.module#ResultatssimulationPageModule' },
    { path: 'mesobjetsassures', loadChildren: './pages/mesobjetsassures/mesobjetsassures.module#MesobjetsassuresPageModule' },
    { path: 'mesassurances', loadChildren: './pages/mesassurances/mesassurances.module#MesassurancesPageModule' },
    { path: 'myprofilecom', loadChildren: './pages/myprofilecom/myprofilecom.module#MyprofilecomPageModule' },
    { path: 'contrat', loadChildren: './pages/contrat/contrat.module#ContratPageModule' },
    { path: 'choicestepone', loadChildren: './pages/choicestepone/choicestepone.module#ChoicesteponePageModule' },
    { path: 'ccac', loadChildren: './pages/ccac/ccac.module#CcacPageModule' },
    { path: 'pageaccueilcom', loadChildren: './pages/pageaccueilcom/pageaccueilcom.module#PageaccueilcomPageModule' },
    { path: 'simplepageaccueil', loadChildren: './pages/simplepageaccueil/simplepageaccueil.module#SimplepageaccueilPageModule' },
    { path: 'epocno', loadChildren: './pages/epocno/epocno.module#EpocnoPageModule' },
    { path: 'selection-profil', loadChildren: './pages/selection-profil/selection-profil.module#SelectionProfilPageModule' },
    { path: 'assurinfove', loadChildren: './pages/assurinfove/assurinfove.module#AssurinfovePageModule' },
];
var AppRoutingModule = /** @class */ (function () {
    function AppRoutingModule() {
    }
    AppRoutingModule = __decorate([
        NgModule({
            imports: [RouterModule.forRoot(routes)],
            exports: [RouterModule]
        })
    ], AppRoutingModule);
    return AppRoutingModule;
}());
export { AppRoutingModule };
//# sourceMappingURL=app-routing.module.js.map
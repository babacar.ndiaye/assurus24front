var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { IonicModule } from '@ionic/angular';
import { ContratPage } from './contrat.page';
import { AccordionListComponent } from './../../components/accordion-list/accordion-list.component';
//import { AccordionListComponentModule } from './../../components/accordion-list/accordion-list.component.module';
import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
var routes = [
    {
        path: '',
        component: ContratPage
    }
];
var ContratPageModule = /** @class */ (function () {
    function ContratPageModule() {
    }
    ContratPageModule = __decorate([
        NgModule({
            imports: [
                CommonModule,
                FormsModule,
                IonicModule,
                RouterModule.forChild(routes),
            ],
            declarations: [ContratPage, AccordionListComponent],
            schemas: [CUSTOM_ELEMENTS_SCHEMA]
        })
    ], ContratPageModule);
    return ContratPageModule;
}());
export { ContratPageModule };
//# sourceMappingURL=contrat.module.js.map
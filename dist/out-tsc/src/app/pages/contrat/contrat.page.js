var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Component } from '@angular/core';
var ContratPage = /** @class */ (function () {
    function ContratPage() {
        this.technologies = [
            {
                name: 'Contrat d\'assurance',
                description: '<table><tr><td><B>Compagnie d\'assurance: </B> Axa</td><td><B>Catégorie:</B> Véhicule</td></tr><tr><td><B>Branche: Auto</B> </td><td><B>Avenant:</B> 00002-00158</td></tr><tr><td><B>N police:</B>501/21-012356</td></tr></table>',
                image: '../assets/img/axa.jpg'
            },
            {
                name: 'Client',
                description: 'Latest cutting edge front-end development framework - can be enabled as an option for Ionic development',
                image: '/assets/images/vuejs-logo.png'
            },
            {
                name: 'Objet Assuré',
                description: 'Popular front-end development framework from Facebook- can be enabled as an option for Ionic development',
                image: 'assets/images/react-logo.png'
            },
            {
                name: 'Risques couverts',
                description: 'Superset of JavaScript that provides class based object oriented programming and strict data typing',
                image: 'assets/images/typescript-logo.png'
            },
            {
                name: 'Conditions spécials',
                description: 'Apache Cordova compatible plugins that allow native device API\'s to be utilised',
                image: 'assets/images/ionic-native-logo.png'
            },
            {
                name: 'Période de validité',
                description: 'Plugins for Progressive Web App and hybrid app development',
                image: 'assets/images/capacitor-logo.png'
            },
            {
                name: 'Prime',
                description: 'Custom web component development framework',
                image: 'assets/images/stencil-logo.png'
            },
            {
                name: 'Déclaration sinistre',
                description: 'CSS pre-processor development library',
                image: 'assets/images/sass-logo.png'
            },
            {
                name: 'Conditions et dispositions légales',
                description: 'Markup language and front-end API support',
                image: 'assets/images/html5-logo.png'
            },
            {
                name: 'Messages',
                description: 'Markup language and front-end API support',
                image: 'assets/images/html5-logo.png'
            },
            {
                name: 'Carte grise',
                description: 'Markup language and front-end API support',
                image: 'assets/images/html5-logo.png'
            },
            {
                name: 'Control Technique',
                description: 'Markup language and front-end API support',
                image: 'assets/images/html5-logo.png'
            }
        ];
    }
    /**
     * Captures and console logs the value emitted from the user depressing the accordion component's <ion-button> element
     * @public
     * @method captureName
     * @param {any}		event 				The captured event
     * @returns {none}
     */
    ContratPage.prototype.captureName = function (event) {
        console.log("Captured name by event value: " + event);
    };
    ContratPage = __decorate([
        Component({
            selector: 'app-contrat',
            templateUrl: './contrat.page.html',
            styleUrls: ['./contrat.page.scss'],
        }),
        __metadata("design:paramtypes", [])
    ], ContratPage);
    return ContratPage;
}());
export { ContratPage };
//# sourceMappingURL=contrat.page.js.map
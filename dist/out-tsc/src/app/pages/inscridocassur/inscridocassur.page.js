var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Component, ViewChild } from '@angular/core';
import { IonInput } from '@ionic/angular';
var InscridocassurPage = /** @class */ (function () {
    function InscridocassurPage() {
    }
    InscridocassurPage.prototype.ngOnInit = function () {
    };
    InscridocassurPage.prototype.ouvrirGestionnaireFichier = function () {
    };
    __decorate([
        ViewChild('ion-input'),
        __metadata("design:type", IonInput)
    ], InscridocassurPage.prototype, "input", void 0);
    InscridocassurPage = __decorate([
        Component({
            selector: 'app-inscridocassur',
            templateUrl: './inscridocassur.page.html',
            styleUrls: ['./inscridocassur.page.scss'],
        }),
        __metadata("design:paramtypes", [])
    ], InscridocassurPage);
    return InscridocassurPage;
}());
export { InscridocassurPage };
//# sourceMappingURL=inscridocassur.page.js.map
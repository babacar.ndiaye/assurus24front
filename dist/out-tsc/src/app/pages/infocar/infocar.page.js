var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Component } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { NavController } from '@ionic/angular';
var InfocarPage = /** @class */ (function () {
    function InfocarPage(formBuilder, navCtrl) {
        this.formBuilder = formBuilder;
        this.navCtrl = navCtrl;
        this.puissancefiscale = 1;
    }
    InfocarPage.prototype.ngOnInit = function () {
        this.onInfocarForm = this.formBuilder.group({
            'numimmat': [null, Validators.compose([
                    Validators.required
                ])],
            'numcartegrise': [null, Validators.compose([
                    Validators.required
                ])],
            'marque': [null, Validators.compose([
                    Validators.required
                ])],
            'model': [null, Validators.compose([
                    Validators.required
                ])],
            'carburant': [null, Validators.compose([
                    Validators.required
                ])],
            'age': [null, Validators.compose([
                    Validators.required
                ])],
            'puissancefiscale': [null, Validators.compose([
                    Validators.required
                ])],
            'nombredeplace': [null, Validators.compose([
                    Validators.required
                ])],
            'valeurdeclarative': [null, Validators.compose([
                    Validators.required
                ])],
            'valeurinitiale': [null, Validators.compose([
                    Validators.required
                ])],
            'datedevalidite': [null, Validators.compose([
                    Validators.required
                ])],
        });
    };
    InfocarPage.prototype.goToHome = function () {
        this.navCtrl.navigateRoot('/home-results');
    };
    InfocarPage = __decorate([
        Component({
            selector: 'app-infocar',
            templateUrl: './infocar.page.html',
            styleUrls: ['./infocar.page.scss'],
        }),
        __metadata("design:paramtypes", [FormBuilder, NavController])
    ], InfocarPage);
    return InfocarPage;
}());
export { InfocarPage };
//# sourceMappingURL=infocar.page.js.map
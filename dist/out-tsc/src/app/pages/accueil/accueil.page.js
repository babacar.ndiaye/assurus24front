var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Component } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { NavController } from '@ionic/angular';
import { FingerprintAIO } from '@ionic-native/fingerprint-aio/ngx';
var AccueilPage = /** @class */ (function () {
    function AccueilPage(formBuilder, navCtrl, fingerAuth) {
        this.formBuilder = formBuilder;
        this.navCtrl = navCtrl;
        this.fingerAuth = fingerAuth;
    }
    AccueilPage.prototype.ngOnInit = function () {
        this.accueilForm = this.formBuilder.group({
            'lieu': [null, Validators.compose([
                    Validators.required
                ])],
            'langue': [null, Validators.compose([
                    Validators.required
                ])]
        });
    };
    AccueilPage.prototype.goToHome = function () {
        this.navCtrl.navigateRoot('/home-results');
    };
    AccueilPage.prototype.goToLogin = function () {
        this.navCtrl.navigateRoot('/login');
    };
    AccueilPage.prototype.showFingerprintAuthDlg = function () {
        var _this = this;
        this.fingerprintOptions = {
            clientId: 'fingerprint-Demo',
            clientSecret: 'password',
            disableBackup: true //Only for Android(optional)
        };
        this.fingerAuth.isAvailable().then(function (result) {
            if (result === "OK") {
                _this.fingerAuth.show(_this.fingerprintOptions)
                    .then(function (result) { return console.log(result); })
                    .catch(function (error) { return console.log(error); });
            }
        });
    };
    AccueilPage = __decorate([
        Component({
            selector: 'app-accueil',
            templateUrl: './accueil.page.html',
            styleUrls: ['./accueil.page.scss'],
        }),
        __metadata("design:paramtypes", [FormBuilder, NavController, FingerprintAIO])
    ], AccueilPage);
    return AccueilPage;
}());
export { AccueilPage };
//# sourceMappingURL=accueil.page.js.map
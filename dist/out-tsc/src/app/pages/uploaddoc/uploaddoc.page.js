var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Component } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { NavController } from '@ionic/angular';
var UploaddocPage = /** @class */ (function () {
    function UploaddocPage(formBuilder, navCtrl) {
        this.formBuilder = formBuilder;
        this.navCtrl = navCtrl;
    }
    UploaddocPage.prototype.ngOnInit = function () {
        this.onuploaddocForm = this.formBuilder.group({
            'cni': [null, Validators.compose([
                    Validators.required
                ])],
            'cg': [null, Validators.compose([
                    Validators.required
                ])],
            'ct': [null, Validators.compose([
                    Validators.required
                ])]
        });
    };
    UploaddocPage.prototype.goToHome = function () {
        this.navCtrl.navigateRoot('/home-results');
    };
    UploaddocPage = __decorate([
        Component({
            selector: 'app-uploaddoc',
            templateUrl: './uploaddoc.page.html',
            styleUrls: ['./uploaddoc.page.scss'],
        }),
        __metadata("design:paramtypes", [FormBuilder, NavController])
    ], UploaddocPage);
    return UploaddocPage;
}());
export { UploaddocPage };
//# sourceMappingURL=uploaddoc.page.js.map
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { IonicModule } from '@ionic/angular';
import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { ChoicesteponePage } from './choicestepone.page';
import { AccordionchecklistComponent } from './../../components/accordionchecklist/accordionchecklist.component';
var routes = [
    {
        path: '',
        component: ChoicesteponePage
    }
];
var ChoicesteponePageModule = /** @class */ (function () {
    function ChoicesteponePageModule() {
    }
    ChoicesteponePageModule = __decorate([
        NgModule({
            imports: [
                CommonModule,
                FormsModule,
                ReactiveFormsModule,
                IonicModule,
                RouterModule.forChild(routes)
            ],
            declarations: [ChoicesteponePage, AccordionchecklistComponent],
            schemas: [CUSTOM_ELEMENTS_SCHEMA]
        })
    ], ChoicesteponePageModule);
    return ChoicesteponePageModule;
}());
export { ChoicesteponePageModule };
//# sourceMappingURL=choicestepone.module.js.map
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Component } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
var ChoicesteponePage = /** @class */ (function () {
    function ChoicesteponePage(formBuilder) {
        this.formBuilder = formBuilder;
        this.technologies = [
            {
                name: 'Responsabilité Civile',
                description: '<table><tr><td><B>Compagnie d\'assurance: </B> Axa</td><td><B>Catégorie:</B> Véhicule</td></tr><tr><td><B>Branche: Auto</B> </td><td><B>Avenant:</B> 00002-00158</td></tr><tr><td><B>N police:</B>501/21-012356</td></tr></table>',
                isChecked: false
            },
            {
                name: 'Défense / Recours',
                description: 'Latest cutting edge front-end development framework - can be enabled as an option for Ionic development',
                isChecked: false
            },
            {
                name: 'Recours anticipé',
                description: 'Popular front-end development framework from Facebook- can be enabled as an option for Ionic development',
                isChecked: false
            },
            {
                name: 'Individuel accident',
                description: 'Superset of JavaScript that provides class based object oriented programming and strict data typing',
                isChecked: false
            },
            {
                name: 'Assistance',
                description: 'Apache Cordova compatible plugins that allow native device API\'s to be utilised',
                isChecked: false
            },
            {
                name: 'Bris de glace',
                description: 'Plugins for Progressive Web App and hybrid app development',
                isChecked: false
            },
            {
                name: 'Incendie',
                description: 'Custom web component development framework',
                isChecked: false
            },
            {
                name: 'vol',
                description: 'CSS pre-processor development library',
                isChecked: false
            },
            {
                name: 'Assurance tous risques',
                description: 'Markup language and front-end API support',
                isChecked: false
            },
            {
                name: 'Messages',
                description: 'Markup language and front-end API support',
                isChecked: false
            },
            {
                name: 'Carte grise',
                description: 'Markup language and front-end API support',
                isChecked: false
            },
            {
                name: 'Control Technique',
                description: 'Markup language and front-end API support',
                isChecked: false
            }
        ];
    }
    /**
     * Captures and console logs the value emitted from the user depressing the accordion component's <ion-button> element
     * @public
     * @method captureName
     * @param {any}		event 				The captured event
     * @returns {none}
     */
    ChoicesteponePage.prototype.captureName = function (event) {
        console.log("Captured name by event value: " + event);
    };
    ChoicesteponePage.prototype.ngOnInit = function () {
        this.onSimulationForm = this.formBuilder.group({
            'rs': [null, Validators.compose([
                    Validators.required
                ])],
            'dr': [null, Validators.compose([
                    Validators.required
                ])],
        });
    };
    ChoicesteponePage = __decorate([
        Component({
            selector: 'app-choicestepone',
            templateUrl: './choicestepone.page.html',
            styleUrls: ['./choicestepone.page.scss'],
        }),
        __metadata("design:paramtypes", [FormBuilder])
    ], ChoicesteponePage);
    return ChoicesteponePage;
}());
export { ChoicesteponePage };
//# sourceMappingURL=choicestepone.page.js.map
import { async, TestBed } from '@angular/core/testing';
import { AccordionchecklistComponent } from './accordionchecklist.component';
describe('AccordionchecklistComponent', function () {
    var component;
    var fixture;
    beforeEach(async(function () {
        TestBed.configureTestingModule({
            declarations: [AccordionchecklistComponent]
        })
            .compileComponents();
    }));
    beforeEach(function () {
        fixture = TestBed.createComponent(AccordionchecklistComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });
    it('should create', function () {
        expect(component).toBeTruthy();
    });
});
//# sourceMappingURL=accordionchecklist.component.spec.js.map
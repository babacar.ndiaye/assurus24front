var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Component, Input, EventEmitter, Output } from '@angular/core';
var AccordionListComponent = /** @class */ (function () {
    function AccordionListComponent() {
        /**
         * The change event that will be broadcast to the parent component when the user interacts with the component's
         * <ion-button> element
         * @public
         * @property change
         * @type {EventEmitter}
         */
        this.change = new EventEmitter();
        /**
         * Determines and stores the accordion state (I.e. opened or closed)
         * @public
         * @property isMenuOpen
         * @type {boolean}
         */
        this.isMenuOpen = false;
    }
    AccordionListComponent.prototype.ngOnInit = function () {
    };
    /**
     * Allows the accordion state to be toggled (I.e. opened/closed)
     * @public
     * @method toggleAccordion
     * @returns {none}
     */
    AccordionListComponent.prototype.toggleAccordion = function () {
        this.isMenuOpen = !this.isMenuOpen;
    };
    /**
     * Allows the value for the <ion-button> element to be broadcast to the parent component
     * @public
     * @method broadcastName
     * @returns {none}
     */
    AccordionListComponent.prototype.broadcastName = function (name) {
        this.change.emit(name);
    };
    __decorate([
        Input(),
        __metadata("design:type", String)
    ], AccordionListComponent.prototype, "name", void 0);
    __decorate([
        Input(),
        __metadata("design:type", String)
    ], AccordionListComponent.prototype, "description", void 0);
    __decorate([
        Input(),
        __metadata("design:type", String)
    ], AccordionListComponent.prototype, "image", void 0);
    __decorate([
        Output(),
        __metadata("design:type", EventEmitter)
    ], AccordionListComponent.prototype, "change", void 0);
    AccordionListComponent = __decorate([
        Component({
            selector: 'app-accordion-list',
            templateUrl: './accordion-list.component.html',
            styleUrls: ['./accordion-list.component.scss']
        }),
        __metadata("design:paramtypes", [])
    ], AccordionListComponent);
    return AccordionListComponent;
}());
export { AccordionListComponent };
//# sourceMappingURL=accordion-list.component.js.map
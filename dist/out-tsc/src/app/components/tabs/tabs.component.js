var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Component } from '@angular/core';
import { Router } from '@angular/router';
var TabsComponent = /** @class */ (function () {
    function TabsComponent(router) {
        this.router = router;
    }
    TabsComponent.prototype.ngOnInit = function () {
    };
    /*navigate1(){
        this.router.navigate(['/accueiltwo']);
      }
      navigate2(){
        this.router.navigate(['/accueiltwo']);
      }
      navigate3(){
        this.router.navigate(['/accueiltwo']);
      }*/
    TabsComponent.prototype.navigate4 = function () {
        this.router.navigate(['/accueiltwo']);
    };
    TabsComponent = __decorate([
        Component({
            selector: 'app-tabs',
            templateUrl: './tabs.component.html',
            styleUrls: ['./tabs.component.scss']
        }),
        __metadata("design:paramtypes", [Router])
    ], TabsComponent);
    return TabsComponent;
}());
export { TabsComponent };
//# sourceMappingURL=tabs.component.js.map
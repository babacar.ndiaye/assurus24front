import { Client } from './../models/client';
import { ContactClient } from './../models/contact-client';

import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Compte } from '../models/compte';
import { Observable } from 'rxjs';
import { UrlApi} from 'src/environments/environment';
import { MailLien } from '../models/mail-lien';

import { Platform, AlertController, LoadingController } from '@ionic/angular'

import { FileTransfer, FileTransferObject } from '@ionic-native/file-transfer/ngx';

import { File, Entry } from "@ionic-native/file/ngx";
import { AndroidPermissions } from '@ionic-native/android-permissions/ngx';
import { FileOpener } from '@ionic-native/file-opener/ngx';
import { UtilsService } from './utils.service';
import { DateFormatMobile } from '../pojos/date-format-mobile';
import { Reseau } from '../models/reseau';
import { ClientParticulier } from '../models/client-particulier';





@Injectable({
  providedIn: 'root'
})
export class CompteService {
  copyFile: Entry = null;
  shouldMove = false;

  private fileTransfer: FileTransferObject;
  directories = [];
  folder = '';
  constructor(private httpClient : HttpClient,private transfer: FileTransfer,
    private file: File,
  //  private platform: Platform,
    private utilsService :UtilsService,
  //  private androidPermissions: AndroidPermissions,
    private fileOpener :FileOpener,
  private loadingController: LoadingController,
    private alertCtrl: AlertController) {
      this.copyFile = null;
      this.shouldMove = false;


  }

 postCompte(Compte : Compte,clientParticulier : ClientParticulier): Observable<Compte> {

   Compte.client = clientParticulier ;
   return this.httpClient.post<Compte>(UrlApi + "/comptes/add",Compte).pipe();
 }
 getComptes():Observable<Compte[]>
 {

   return this.httpClient.get<Compte[]>(UrlApi+"/comptes/getAll").pipe();
 }

 getCompte(id :number):Observable<Compte>
 {
   return this.httpClient.get<Compte>(UrlApi+'/comptes/getOne/'+id).pipe();
 }

 deleteCompte(id : number)
 {
   return this.httpClient.get(UrlApi+'/comptes/delete/'+id).pipe();
 }

 updateCompte(Compte) : Observable <Compte>
 {
   return this.httpClient.post<Compte>(UrlApi+'/comptes/update', Compte).pipe();
 }
 seConnecter(Compte) : Observable <Compte>
 {
   return this.httpClient.post<Compte>(UrlApi+'/comptes/exist', Compte).pipe();
 }

 envoyerMail(mailLien:MailLien) : Observable <Compte>
 {
   return this.httpClient.post<Compte>(UrlApi+'/email/sendmail',mailLien).pipe();
 }
 envoyerSms(mailLien:MailLien) : Observable <Compte>
 {
   return this.httpClient.post<Compte>(UrlApi+'/sms/sendSms',mailLien).pipe();
 }
 recupererMailByTel(Compte) : Observable <Client>
 {
   return this.httpClient.post<Client>(UrlApi+'/comptes/getEmailByTel', Compte).pipe();
 }
 recupererIdByTel(Compte) : Observable <Compte>
 {
   return this.httpClient.post<Compte>(UrlApi+'/comptes/getIdByTelephone', Compte).pipe();
 }
 //verifier le telephone dans la liste des comptes clients user
 validerTelephone(Compte) : Observable <Boolean>
 {
   return this.httpClient.post<Boolean>(UrlApi+'/comptes/validerTelephone', Compte).pipe();
 }
 //verifier le telephone dans la liste des compte clients dun reseau
 validerTelephoneinReseau(Compte,Reseau) : Observable <Boolean>
 {
   return this.httpClient.post<Boolean>(UrlApi+'/comptes/validerTelephone', Compte).pipe();
 }
 inscription(compte : Compte,contact :ContactClient): Observable<ContactClient> {


  contact.compte = compte ;

  return this.httpClient.post<ContactClient>(UrlApi + "/comptes/inscription",contact).pipe();
}
inscriptionClientReseau(client : Client ,reseau :Reseau): Observable<ContactClient> {


//todo
let contact = new ContactClient() ;
//A changer selon la structure du modele
contact.compte = new Compte() ;
  return this.httpClient.post<ContactClient>(UrlApi + "/comptes/inscription",contact).pipe();
}
updateInscription(compte : Compte,contact :ContactClient): Observable<ContactClient> {


  contact.compte = compte ;

  return this.httpClient.post<ContactClient>(UrlApi + "/comptes/updateInscription",contact).pipe();
}
 //crud contact
 postContactClient(ContactClient : ContactClient): Observable<ContactClient> {


  return this.httpClient.post<ContactClient>(UrlApi + "/contactClient/add",ContactClient).pipe();
}
getContactClients():Observable<ContactClient[]>
{

  return this.httpClient.get<ContactClient[]>(UrlApi+"/contactClient/getAll").pipe();
}

getContactClient(id :number):Observable<ContactClient>
{
  return this.httpClient.get<ContactClient>(UrlApi+'/contactClient/getOne/'+id).pipe();
}
getContactClientByIdCompte(id :number):Observable<ContactClient>
{
  return this.httpClient.get<ContactClient>(UrlApi+'/contactClient/getOneByIdCompte/'+id).pipe();
}



deleteContactClient(id : number)
{
  return this.httpClient.get(UrlApi+'/contactClient/delete/'+id).pipe();
}

updateContactClient(ContactClient) : Observable <ContactClient>
{
  return this.httpClient.post<ContactClient>(UrlApi+'/contactClient/update', ContactClient).pipe();
}
 //fin crud contact

//upload fichier
upload(formData) : Observable <Boolean>
{
  var headers = new HttpHeaders();

  headers.append('Content-Type', 'multipart/form-data' );
  return this.httpClient.post<Boolean>(UrlApi+'/file/uploadCni', formData,{headers:headers}).pipe();
}

getContactClientByCompte(id : number,typeClient):Observable <Client>
{
  return this.httpClient.get<Client>(UrlApi+'/'+typeClient+'/findClientByCompte/'+id).pipe();
}


public async download(fileName,id,content)  {
  let arret = false ;
  let downloadFile : any ;
  let filePath = UrlApi+"/file/"+content+"/"+id ;
  let url = encodeURI(filePath);
  this.fileTransfer = this.transfer.create();

  this.fileTransfer.download(url, this.file.dataDirectory + fileName, true).then((entry) => {
    //here logging our success downloaded file path in mobile.
    if(entry !=null)
    {
      this.loadingController.dismiss();
    downloadFile = entry.toURL()
    this.openFileHandler(downloadFile) ;
     arret =true ;
    }





  }).catch((error) => {
    console.log("Erreur", JSON.stringify(error));

    this.loadingController.dismiss();
    //here logging an error.
    this.loadingController.dismiss();
    //here logging an error.
    this.utilsService.alertNotification("Vous n 'avez pas encore soumis ce fichier ");
    arret=true ;
  });
  await this.delay(15000);
  if (arret==false)
  {
    this.loadingController.dismiss();
    this.utilsService.alertNotification('Erreur connexion veuillez reessayer ulterieurement');
  }


}
openFileHandler(cni):any {
  this.fileOpener.open(cni, '')
    .then(() => {this.utilsService.alertNotification("document ouvert avec succès")

      this.loadDocuments() ;})
    .catch(e => {this.utilsService.alertNotification('Error opening file'+e);});
    return null ;
}








async alertNotification(message) {

  let changeLocation ;


  changeLocation= await this.alertCtrl.create({
    header: 'Notification',
    message: message,


    buttons: [

      {
        text: 'ok',
        handler: data => {

        }

      }
    ]
  });

  changeLocation.present();
}
//fin upload fichier
delay(ms: number) {
  return new Promise( resolve => setTimeout(resolve, ms) );
}
startCopy(file: Entry, moveFile = false) {
  this.copyFile = file;
  this.shouldMove = moveFile;
}
finishCopyFile(file: Entry) {
  let path = this.file.dataDirectory + this.folder;
  let newPath = this.file.dataDirectory + this.folder + '/' + file.name;


      this.file
        .copyFile(path, this.copyFile.name, newPath, this.copyFile.name)
        .then(() => {

        });
    }
    async loadDocuments() {
      let directories = [];
      let folder = '';
      const loader = await this.loadingController.create({
        message: '',
      });
        // Reset for later copy/move operations
        this.copyFile = null;
        this.shouldMove = false;
        // this.file
        // .createDir(
        //   `${this.file.dataDirectory,"baniere"}`,
        //   "baniere2",
        //   true
        // )
        // .then(res=>{
          this.file.listDir(this.file.dataDirectory,"").then(res => {
            this.directories = res;

          }
          ,
          error =>{this.utilsService.alertNotification(error)});
      this.directories.forEach(element => {
        loader.dismiss() ;
        //this.utilsService.alertNotification(element.name)  ;

      });

        }
  //       )

  // }
  postPaiement(): Observable<any> {


    return this.httpClient.post<ContactClient>(UrlApi + "/paiement/demande",null).pipe();
  }


}

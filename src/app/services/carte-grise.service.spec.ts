import { TestBed } from '@angular/core/testing';

import { CarteGriseService } from './carte-grise.service';

describe('CarteGriseService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: CarteGriseService = TestBed.get(CarteGriseService);
    expect(service).toBeTruthy();
  });
});

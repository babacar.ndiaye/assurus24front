import { TestBed } from '@angular/core/testing';

import { CniService } from './cni.service';

describe('CniService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: CniService = TestBed.get(CniService);
    expect(service).toBeTruthy();
  });
});

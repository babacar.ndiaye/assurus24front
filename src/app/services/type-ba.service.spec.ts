import { TestBed } from '@angular/core/testing';

import { TypeBAService } from './type-ba.service';

describe('TypeBAService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: TypeBAService = TestBed.get(TypeBAService);
    expect(service).toBeTruthy();
  });
});

import { TestBed } from '@angular/core/testing';

import { OperateurService } from './operateur.service';

describe('OperateurService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: OperateurService = TestBed.get(OperateurService);
    expect(service).toBeTruthy();
  });
});

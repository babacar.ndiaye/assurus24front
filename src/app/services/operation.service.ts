import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Contrat2 } from '../models/contrat';
import { UrlApi } from 'src/environments/environment';
import { HttpClient } from '@angular/common/http';
import { Operation } from '../models/operation';

@Injectable({
  providedIn: 'root'
})
export class OperationService {

  constructor(private httpClient: HttpClient) {
  }
  postContrat(Contrat: Contrat2): Observable<Contrat2> {


    return this.httpClient.post<Contrat2>(UrlApi + "/operations/add", Contrat).pipe();
  }
  getContrats(): Observable<Contrat2[]> {

    return this.httpClient.get<Contrat2[]>(UrlApi + "/operations/getAll").pipe();
  }

  getContrat(id: number): Observable<Contrat2> {
    return this.httpClient.get<Contrat2>(UrlApi + '/operations/getOne/' + id).pipe();
  }

  deleteContrat(id: number) {
    return this.httpClient.get(UrlApi + '/operations/delete/' + id).pipe();
  }

  updateContrat(Contrat): Observable<Contrat2> {
    return this.httpClient.post<Contrat2>(UrlApi + '/operations/update', Contrat).pipe();
  }
  getOperationByContrat(id: number): Observable<Operation[]> {
    return this.httpClient.get<Operation[]>(UrlApi + '/operations/getOperationByContrat/' + id)
  }
}

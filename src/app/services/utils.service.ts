import { AttributAussureBranche } from './../models/attribut-assure-branche';
import { Injectable } from '@angular/core';
import { AlertController, NavController, Platform } from '@ionic/angular';
import { simulationActifenSession, DescAutos, OriginePageGesSim, OrigineClientNonLogue, OriginePageMonObjet, ListeSimulationEnSession, contratActifenSession, objetActifenSession, operationActifenSession, operationSimActifenSession, UrlApi, typeRenouvellement, typeRoutine, typeRoutineNewContrat, typeRoutineSimulation, flotte, listeSimulationDansFlotte, listeObjetFlotte, listeOperationFlotte, listeContratFlotte, retourFlotte, numeroPivot, statutContratActif, statutContratEnCoursDeValidation, statutContratValideNonActif, statutContratSuspendu, statutContratResilie, statutValideEnCours, statutValidationKo, listeSimulationDansFlotteValide, SessionClient, loginToPage, banniereCom, listeRoutineContratSession, indexRoutineContratEditingSession, LIENLOGIN, SessionCompte, typeRoutineNewObject, LIENASSURINFOVE, typeSousBranche, OriginePageMesObjets, OrigineMesContrats } from 'src/environments/environment';
import { BA } from '../models/ba';
import { TypeGarantie } from '../models/type-garantie';
import { TypeBA, TypeBA2 } from '../models/type-ba';
import { DateFormatMobile } from '../pojos/date-format-mobile';
import { CompteService } from './compte.service';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { AlertOptions } from '@ionic/core';
import { Simulation } from '../models/simulation';
import { ObjetAssures, ObjetAssures2 } from '../models/objet-assures';
import { ContactClient } from '../models/contact-client';
import { FlotteSimulations } from '../models/flotte-simulations';
import { RoutineContrat } from '../pojos/routine-contrat';
import { Client } from '../models/client';
import { Compte } from '../models/compte';
import { ClientParticulier } from '../models/client-particulier';
import { Globalization } from '@ionic-native/globalization/ngx';
import { TranslateService } from '@ngx-translate/core';
import { TypeBas } from '../models/type-bas';
import { AttributAussureBrancheDto } from '../Dto/AttributAussureBrancheDto';
import { SimulationClient } from '../models/simulation-client';
import { AssureSimulation } from '../models/assure-simulation';


@Injectable({
  providedIn: 'root'
})
export class UtilsService {


  language: string;
  brancheParentId;
  constructor(
    private alertCtrl: AlertController,
    private navCtrl: NavController,
    private httpClient: HttpClient,
    private platform: Platform,
    private globalization: Globalization,
    private _translate: TranslateService,
  ) { }
  async alertNotification(message) {

    let changeLocation;


    changeLocation = await this.alertCtrl.create({
      header: 'Notification',
      message: message,


      buttons: [

        {
          text: 'ok',
          handler: data => {

          }

        }
      ]
    });

    changeLocation.present();
  }
  delay(ms: number) {
    return new Promise(resolve => setTimeout(resolve, ms));
  }
  enregistrerSimulation(listeSimulation) {
    let flotteSimulation = new FlotteSimulations();
    flotteSimulation.simulations = listeSimulation;
    //ici on appelera service Flottesimulation.add pour post la simulation

  }
  arreterSimulation(origineRoutine) {

    sessionStorage.removeItem(simulationActifenSession);
    switch (origineRoutine) {
      case OriginePageGesSim: this.navCtrl.navigateRoot("/gestion-mes-simulations"); break;
      case OrigineClientNonLogue: this.navCtrl.navigateRoot("/epocno"); break;
      case OriginePageMonObjet: this.navCtrl.navigateRoot("/monobjet"); break;
      default: this.navCtrl.navigateRoot("/gestion-mes-simulations");
    }

  }
  recupererTypeBranches() {
    let typeG1: TypeGarantie = {
      id: 1,
      libelle: "Responsabilite civile",
      description: "Tout propriétaire d’un véhicule à moteur doit obligatoirement l’assurer avec, au minimum, une garantie responsabilité civile (également appelée « assurance au tiers ». Cette garantie couvre exclusivement les dommages que pourrait causer la voiture assuré à un tiers (passager, autre conducteur, piéton…) ou à un autre véhicule.Tout propriétaire d’un véhicule à moteur doit obligatoirement l’assurer avec, au minimum, une garantie responsabilité civile (également appelée « assurance au tiers ». Cette garantie couvre exclusivement les dommages que pourrait causer la voiture assuré à un tiers (passager, autre conducteur, piéton…) ou à un autre véhicule.Tout propriétaire d’un véhicule à moteur doit obligatoirement l’assurer avec, au minimum, une garantie responsabilité civile (également appelée « assurance au tiers ». Cette garantie couvre exclusivement les dommages que pourrait causer la voiture assuré à un tiers (passager, autre conducteur, piéton…) ou à un autre véhicule.Tout propriétaire d’un véhicule à moteur doit obligatoirement l’assurer avec, au minimum, une garantie responsabilité civile (également appelée « assurance au tiers ». Cette garantie couvre exclusivement les dommages que pourrait causer la voiture assuré à un tiers (passager, autre conducteur, piéton…) ou à un autre véhicule.",
        obligatoire: true,
      code: "05D55",
      datesupp: new Date(),
      slogan: "",
      pack: false,
      typeBA: null,
      attributs: [],
      exclusions: [],
      inPack: false,


    }
    let typeG2: TypeGarantie = {
      id: 2,
      libelle: "Défense/Recours",
      description: "Tout propriétaire d’un véhicule à moteur doit obligatoirement l’assurer avec, au minimum, une garantie responsabilité civile (également appelée « assurance au tiers ». Cette garantie couvre exclusivement les dommages que pourrait causer la voiture assuré à un tiers (passager, autre conducteur, piéton…) ou à un autre véhicule.Tout propriétaire d’un véhicule à moteur doit obligatoirement l’assurer avec, au minimum, une garantie responsabilité civile (également appelée « assurance au tiers ». Cette garantie couvre exclusivement les dommages que pourrait causer la voiture assuré à un tiers (passager, autre conducteur, piéton…) ou à un autre véhicule.Tout propriétaire d’un véhicule à moteur doit obligatoirement l’assurer avec, au minimum, une garantie responsabilité civile (également appelée « assurance au tiers ». Cette garantie couvre exclusivement les dommages que pourrait causer la voiture assuré à un tiers (passager, autre conducteur, piéton…) ou à un autre véhicule.Tout propriétaire d’un véhicule à moteur doit obligatoirement l’assurer avec, au minimum, une garantie responsabilité civile (également appelée « assurance au tiers ». Cette garantie couvre exclusivement les dommages que pourrait causer la voiture assuré à un tiers (passager, autre conducteur, piéton…) ou à un autre véhicule.",
      obligatoire: true,
      code: "05D55",
      datesupp: new Date(),
      slogan: "",
      pack: false,
      typeBA: null,
      attributs: [],
      exclusions: [],
      inPack: false,

    }
    let typeG3: TypeGarantie = {
      id: 3,
      libelle: "Recours anticipé",
      description: "Tout propriétaire d’un véhicule à moteur doit obligatoirement l’assurer avec, au minimum, une garantie responsabilité civile (également appelée « assurance au tiers ». Cette garantie couvre exclusivement les dommages que pourrait causer la voiture assuré à un tiers (passager, autre conducteur, piéton…) ou à un autre véhicule.Tout propriétaire d’un véhicule à moteur doit obligatoirement l’assurer avec, au minimum, une garantie responsabilité civile (également appelée « assurance au tiers ». Cette garantie couvre exclusivement les dommages que pourrait causer la voiture assuré à un tiers (passager, autre conducteur, piéton…) ou à un autre véhicule.Tout propriétaire d’un véhicule à moteur doit obligatoirement l’assurer avec, au minimum, une garantie responsabilité civile (également appelée « assurance au tiers ». Cette garantie couvre exclusivement les dommages que pourrait causer la voiture assuré à un tiers (passager, autre conducteur, piéton…) ou à un autre véhicule.Tout propriétaire d’un véhicule à moteur doit obligatoirement l’assurer avec, au minimum, une garantie responsabilité civile (également appelée « assurance au tiers ». Cette garantie couvre exclusivement les dommages que pourrait causer la voiture assuré à un tiers (passager, autre conducteur, piéton…) ou à un autre véhicule.",
      obligatoire: true,
      code: "05D55",
      datesupp: new Date(),
      slogan: "",
      pack: false,
      typeBA: null,
      attributs: [],
      exclusions: [],
      inPack: false,


    }
    let typeG4: TypeGarantie = {
      id: 4,
      libelle: "Individuelle accident",
      description: "Tout propriétaire d’un véhicule à moteur doit obligatoirement l’assurer avec, au minimum, une garantie responsabilité civile (également appelée « assurance au tiers ». Cette garantie couvre exclusivement les dommages que pourrait causer la voiture assuré à un tiers (passager, autre conducteur, piéton…) ou à un autre véhicule.Tout propriétaire d’un véhicule à moteur doit obligatoirement l’assurer avec, au minimum, une garantie responsabilité civile (également appelée « assurance au tiers ». Cette garantie couvre exclusivement les dommages que pourrait causer la voiture assuré à un tiers (passager, autre conducteur, piéton…) ou à un autre véhicule.Tout propriétaire d’un véhicule à moteur doit obligatoirement l’assurer avec, au minimum, une garantie responsabilité civile (également appelée « assurance au tiers ». Cette garantie couvre exclusivement les dommages que pourrait causer la voiture assuré à un tiers (passager, autre conducteur, piéton…) ou à un autre véhicule.Tout propriétaire d’un véhicule à moteur doit obligatoirement l’assurer avec, au minimum, une garantie responsabilité civile (également appelée « assurance au tiers ». Cette garantie couvre exclusivement les dommages que pourrait causer la voiture assuré à un tiers (passager, autre conducteur, piéton…) ou à un autre véhicule.",
      obligatoire: true,
      code: "05D55",
      datesupp: new Date(),
      slogan: "",
      pack: false,
      typeBA: null,
      attributs: [],
      exclusions: [],
      inPack: false,
    }
    let typeG5: TypeGarantie = {
      id: 5,
      libelle: "Assistance",
      description: "Tout propriétaire d’un véhicule à moteur doit obligatoirement l’assurer avec, au minimum, une garantie responsabilité civile (également appelée « assurance au tiers ». Cette garantie couvre exclusivement les dommages que pourrait causer la voiture assuré à un tiers (passager, autre conducteur, piéton…) ou à un autre véhicule.Tout propriétaire d’un véhicule à moteur doit obligatoirement l’assurer avec, au minimum, une garantie responsabilité civile (également appelée « assurance au tiers ». Cette garantie couvre exclusivement les dommages que pourrait causer la voiture assuré à un tiers (passager, autre conducteur, piéton…) ou à un autre véhicule.Tout propriétaire d’un véhicule à moteur doit obligatoirement l’assurer avec, au minimum, une garantie responsabilité civile (également appelée « assurance au tiers ». Cette garantie couvre exclusivement les dommages que pourrait causer la voiture assuré à un tiers (passager, autre conducteur, piéton…) ou à un autre véhicule.Tout propriétaire d’un véhicule à moteur doit obligatoirement l’assurer avec, au minimum, une garantie responsabilité civile (également appelée « assurance au tiers ». Cette garantie couvre exclusivement les dommages que pourrait causer la voiture assuré à un tiers (passager, autre conducteur, piéton…) ou à un autre véhicule.",
      obligatoire: true,
      code: "05D55",
      datesupp: new Date(),
      slogan: "",
      pack: false,
      typeBA: null,
      attributs: [],
      exclusions: [],
      inPack: false,
    }
    let typeG6: TypeGarantie = {
      id: 6,
      libelle: "Bris de glace",
      description: "Tout propriétaire d’un véhicule à moteur doit obligatoirement l’assurer avec, au minimum, une garantie responsabilité civile (également appelée « assurance au tiers ». Cette garantie couvre exclusivement les dommages que pourrait causer la voiture assuré à un tiers (passager, autre conducteur, piéton…) ou à un autre véhicule.Tout propriétaire d’un véhicule à moteur doit obligatoirement l’assurer avec, au minimum, une garantie responsabilité civile (également appelée « assurance au tiers ». Cette garantie couvre exclusivement les dommages que pourrait causer la voiture assuré à un tiers (passager, autre conducteur, piéton…) ou à un autre véhicule.Tout propriétaire d’un véhicule à moteur doit obligatoirement l’assurer avec, au minimum, une garantie responsabilité civile (également appelée « assurance au tiers ». Cette garantie couvre exclusivement les dommages que pourrait causer la voiture assuré à un tiers (passager, autre conducteur, piéton…) ou à un autre véhicule.Tout propriétaire d’un véhicule à moteur doit obligatoirement l’assurer avec, au minimum, une garantie responsabilité civile (également appelée « assurance au tiers ». Cette garantie couvre exclusivement les dommages que pourrait causer la voiture assuré à un tiers (passager, autre conducteur, piéton…) ou à un autre véhicule.",
      obligatoire: true,
      code: "05D55",
      datesupp: new Date(),
      slogan: "",
      pack: false,
      typeBA: null,
      attributs: [],
      exclusions: [],
      inPack: false,
    }


    let typeBranche1: TypeBA = {
      id: 1,

      nom: "Automobile",


      bas: [],
      description: DescAutos,

      ConditionsDisposition: "<p>Quam ob rem circumspecta cautela observatum est deinceps </p> <p>et cum edita montium petere coeperint grassatores, loci iniquitati milites cedunt. ubi autem in planitie potuerint reperiri, quod contingit adsidue, nec exsertare lacertos nec crispare permissi tela, quae vehunt bina vel terna, pecudum ritu inertium trucidantur.Huic Arabia est conserta, ex alio latere Nabataeis contigua; opima varietate conmerciorum castrisque oppleta validis et castellis, quae ad repellendos gentium vicinarum excursus sollicitudo pervigil veterum per oportunos saltus erexit et cautos. haec quoque civitates habet inter oppida quaedam ingentes Bostram et Gerasam atque Philadelphiam murorum firmitate cautissimas. hanc provinciae inposito nomine rectoreque adtributo obtemperare legibus nostris Traianus conpulit imperator incolarum tumore saepe contunso cum glorioso marte Mediam urgeret et Partho.Utque proeliorum periti rectores primo catervas densas opponunt et fortes, deinde leves armaturas, post iaculatores ultimasque subsidiales acies, si fors adegerit, iuvaturas, ita praepositis urbanae familiae suspensae digerentibus sollicite,</p> <p> quos insignes faciunt virgae dexteris aptatae velut tessera data castrensi iuxta vehiculi frontem omne textrinum incedit: huic atratum coquinae iungitur ministerium, dein totum promiscue servitium cum otiosis plebeiis de vicinitate coniunctis: postrema multitudo spadonum a senibus in pueros desinens, obluridi distortaque lineamentorum conpage deformes, ut quaqua incesserit quisquam cernens mutilorum hominum agmina detestetur memoriam Samiramidis reginae illius veteris, quae teneros mares castravit omnium prima velut vim iniectans naturae, eandemque ab instituto cursu retorquens, quae inter ipsa oriundi crepundia per primigenios seminis fontes tacita quodam modo lege vias propagandae posteritatis ostendit.Qui cum venisset ob haec festinatis itineribus Antiochiam, praestrictis palatii ianuis, contempto Caesare, quem videri decuerat, ad praetorium cum pompa sollemni perrexit morbosque diu causatus nec regiam introiit nec processit in publicum, sed abditus multa in eius moliebatur exitium addens quaedam relationibus supervacua, quas subinde dimittebat ad principem.Cuius acerbitati uxor grave accesserat incentivum, germanitate Augusti turgida supra modum, quam Hannibaliano regi fratris filio antehac Constantinus iunxerat pater, Megaera quaedam mortalis, inflammatrix saevientis adsidua, humani cruoris avida nihil mitius quam maritus; qui paulatim eruditiores facti processu temporis ad nocendum per clandestinos versutosque rumigerulos conpertis leviter addere quaedam male suetos falsa et placentia sibi discentes, adfectati regni vel artium nefandarum calumnias insontibus adfligebant.</p>",
      typeGaranties: [typeG1, typeG2, typeG3, typeG4, typeG5, typeG6]
    }
    let typeBranche2: TypeBA = {
      id: 2,

      nom: "Sante",

      bas: [],
      description: DescAutos,

      ConditionsDisposition: "<p>Quam ob rem circumspecta cautela observatum est deinceps </p> <p>et cum edita montium petere coeperint grassatores, loci iniquitati milites cedunt. ubi autem in planitie potuerint reperiri, quod contingit adsidue, nec exsertare lacertos nec crispare permissi tela, quae vehunt bina vel terna, pecudum ritu inertium trucidantur.Huic Arabia est conserta, ex alio latere Nabataeis contigua; opima varietate conmerciorum castrisque oppleta validis et castellis, quae ad repellendos gentium vicinarum excursus sollicitudo pervigil veterum per oportunos saltus erexit et cautos. haec quoque civitates habet inter oppida quaedam ingentes Bostram et Gerasam atque Philadelphiam murorum firmitate cautissimas. hanc provinciae inposito nomine rectoreque adtributo obtemperare legibus nostris Traianus conpulit imperator incolarum tumore saepe contunso cum glorioso marte Mediam urgeret et Partho.Utque proeliorum periti rectores primo catervas densas opponunt et fortes, deinde leves armaturas, post iaculatores ultimasque subsidiales acies, si fors adegerit, iuvaturas, ita praepositis urbanae familiae suspensae digerentibus sollicite,</p> <p> quos insignes faciunt virgae dexteris aptatae velut tessera data castrensi iuxta vehiculi frontem omne textrinum incedit: huic atratum coquinae iungitur ministerium, dein totum promiscue servitium cum otiosis plebeiis de vicinitate coniunctis: postrema multitudo spadonum a senibus in pueros desinens, obluridi distortaque lineamentorum conpage deformes, ut quaqua incesserit quisquam cernens mutilorum hominum agmina detestetur memoriam Samiramidis reginae illius veteris, quae teneros mares castravit omnium prima velut vim iniectans naturae, eandemque ab instituto cursu retorquens, quae inter ipsa oriundi crepundia per primigenios seminis fontes tacita quodam modo lege vias propagandae posteritatis ostendit.Qui cum venisset ob haec festinatis itineribus Antiochiam, praestrictis palatii ianuis, contempto Caesare, quem videri decuerat, ad praetorium cum pompa sollemni perrexit morbosque diu causatus nec regiam introiit nec processit in publicum, sed abditus multa in eius moliebatur exitium addens quaedam relationibus supervacua, quas subinde dimittebat ad principem.Cuius acerbitati uxor grave accesserat incentivum, germanitate Augusti turgida supra modum, quam Hannibaliano regi fratris filio antehac Constantinus iunxerat pater, Megaera quaedam mortalis, inflammatrix saevientis adsidua, humani cruoris avida nihil mitius quam maritus; qui paulatim eruditiores facti processu temporis ad nocendum per clandestinos versutosque rumigerulos conpertis leviter addere quaedam male suetos falsa et placentia sibi discentes, adfectati regni vel artium nefandarum calumnias insontibus adfligebant.</p>",
      typeGaranties: [typeG1, typeG2, typeG3, typeG4, typeG5, typeG6]
    }
    let typeBranches = new Array<TypeBA>();
    typeBranches.push(typeBranche1, typeBranche2);
    return typeBranches;


  }
  displayDateFormatInput(date: Date): String {
    date = new Date(date);

    let jour, mois;
    let dd = date.getDate();

    let mm = date.getMonth() + 1;
    let yyyy = date.getFullYear();
    if (dd < 10) {
      jour = '0' + dd;
    }
    else
      jour = dd;

    if (mm < 10) {
      mois = '0' + mm;
    }
    else
      mois = mm;

    return yyyy + "-" + mois + "-" + jour;
  }
  displayDateFormatInitialise(date: Date): string {
    //console.log(date) ;
    date = new Date(date);

    let jour, mois;
    let dd = date.getDate();
    //console.log(dd)
    let mm = date.getMonth() + 1;
    let yyyy = date.getFullYear();
    if (dd < 10) {
      jour = '0' + dd;
    }
    else
      jour = dd;

    if (mm < 10) {
      mois = '0' + mm;
    }
    else
      mois = mm;

    return mois + "-" + jour + "-" + yyyy;
  }
  displayDate(date: Date): String {
    date = new Date(date)
    let jour, mois;
    let dd = date.getDate();

    let mm = date.getMonth() + 1;
    let yyyy = date.getFullYear();
    if (dd < 10) {
      jour = '0' + dd;
    }
    else
      jour = dd;

    if (mm < 10) {
      mois = '0' + mm;
    }
    else
      mois = mm;
    return jour + "/" + mois + "/" + yyyy;
  }
  async initialiserSessionRoutine() {
    //  sessionStorage.removeItem(ListeSimulationEnSession)
    sessionStorage.removeItem(simulationActifenSession)
    sessionStorage.removeItem(contratActifenSession)
    sessionStorage.removeItem(objetActifenSession)
    sessionStorage.removeItem(operationActifenSession)
    sessionStorage.removeItem(operationSimActifenSession)
    sessionStorage.removeItem(typeRenouvellement)
    sessionStorage.removeItem(typeRoutine)
    sessionStorage.removeItem(objetActifenSession)
    sessionStorage.removeItem(indexRoutineContratEditingSession)
    //sessionStorage.removeItem(listeSimulationDansFlotte) ;
    sessionStorage.removeItem(listeRoutineContratSession)
    sessionStorage.removeItem(listeObjetFlotte);
    sessionStorage.removeItem(listeOperationFlotte);
    sessionStorage.removeItem(listeContratFlotte);
    sessionStorage.removeItem(numeroPivot);
    sessionStorage.removeItem("typeBrancheId");
    sessionStorage.removeItem(typeSousBranche);
    sessionStorage.setItem(flotte, JSON.stringify(false));
    sessionStorage.setItem(retourFlotte, JSON.stringify(false));


  }

  async initialiserSessionRoutineContrat() {
    sessionStorage.removeItem("typeBrancheId");
    sessionStorage.removeItem(typeSousBranche);

  }


  getDate(): Observable<DateFormatMobile> {
    return this.httpClient.get<DateFormatMobile>(UrlApi + "/file/getDate").pipe();
  }

  getTypeBas(): Observable<TypeBA2[]> {
    return this.httpClient.get<TypeBA2[]>(UrlApi + "/typebas/getAllGeneric").pipe()
  }
  getTypeBasAndSousBrancheAttributs(id): Observable<AttributAussureBranche[]> {
    return this.httpClient.get<AttributAussureBranche[]>(UrlApi + "/attributassurebranches/findByba/"+id).pipe()
  }

  //AttributAussureBrancheDto
  getTypeBasAndSousBrancheAttributsDTO(id): Observable<AttributAussureBrancheDto[]> {
    return this.httpClient.get<AttributAussureBrancheDto[]>(UrlApi + "/attributassurebranches/findByba/"+id).pipe()
  }

  async selectionSousBrancheObjet(origine, data, branche,attributsParents,brancheId) {
    //window.alert(JSON.stringify(data));
    this.initialiserSessionRoutine().then(
      async () => {
        var options: AlertOptions = {
          header: 'SOUS BRANCHE',
          message: 'Veuillez choisir branche sous-branche',
          buttons: [
            {
              text: this._translate.instant('Cancel'),
              role: 'cancel',
              handler: () => {
                //console.log('Cancel clicked');
              }
            },
            {
              text: this._translate.instant('Ok'),
              handler: (data: TypeBA2) => {
                if (data) {
                    console.log("utils",data);
                    data.parent.attributs = attributsParents;
                    if(brancheId)
                     sessionStorage.setItem("typeBrancheId", brancheId.toString());
                    this.getTypeBasAndSousBrancheAttributsDTO(data.id)
                    .subscribe((attributsFils: any) => {
                        if(attributsFils){
                            data.attributs = attributsFils;
                            switch (branche) {
                                case 'AUTOMOBILE':
                                   if(origine == OriginePageMesObjets){
                                       console.log(OriginePageMesObjets);
                                    sessionStorage.setItem(
                                        typeRoutine,
                                        JSON.stringify(typeRoutineNewObject)
                                    );
                                    sessionStorage.setItem(
                                        typeSousBranche,
                                        JSON.stringify(data)
                                    );
                                    this.navCtrl.navigateForward(LIENASSURINFOVE);
                                   }else{
                                    console.log(OrigineMesContrats);
                                       let contratActif = null;
                                        sessionStorage.setItem(
                                            contratActifenSession,
                                            JSON.stringify(contratActif)
                                        );
                                        sessionStorage.setItem(
                                            typeSousBranche,
                                            JSON.stringify(data)
                                        );
                                        sessionStorage.setItem(
                                            typeRoutine,
                                            JSON.stringify(typeRoutineNewContrat)
                                        );
                                        this.navCtrl.navigateRoot("/assurinfove");
                                   }
                                    break;
                                case 'SANTE':

                                    break;

                                default: this.alertNotification(this._translate.instant("branche_non_disponible"))
                            }
                        }
                    })


                //   switch (data.libelle) {
                //     case "AUTOMOBILE":
                //       let simulationActif = new Simulation();
                //       simulationActif.origineRoutine = origine;
                //       simulationActif.objet = new ObjetAssures();
                //       sessionStorage.setItem(typeRoutine, typeRoutineSimulation)
                //       sessionStorage.setItem(simulationActifenSession, JSON.stringify(simulationActif));
                //       sessionStorage.setItem("typeBrancheId", data.id);
                //       this.navCtrl.navigateRoot("/sim-decla-objet"); break;

                //     default: this.alertNotification(this._translate.instant("branche_non_disponible"))
                //   }


                }
                else
                  this.alertNotification("vous n'avez pas choisi de sous-branche")

              }
            }
          ]
        };

        options.inputs = [];

        // Now we add the radio buttons
        let sousBranches: TypeBA2[] = [];
        sousBranches=data;
        console.log('sousBranches',sousBranches)

        /**
         * * Cette boucle permet de charger les branches dans la liste des options
         */
        for (let i = 0; i < sousBranches.length; i++) {

          options.inputs.push({ name: 'sous-branche' + i, value: sousBranches[i], label: sousBranches[i].libelle, type: 'radio' });
        }

        // Create the alert with the options
        let alert = this.alertCtrl.create(options);
        (await alert).present();

      });
  }

  async selectionSousBrancheObjetForRoutineContrat(data, branche,attributsParents,brancheId) {
      console.log("appel");
    this.initialiserSessionRoutineContrat().then(
        async () => {
          console.log("in");
        var options: AlertOptions = {
          header: 'SOUS BRANCHE',
          message: 'Veuillez choisir branche sous-branche',
          buttons: [
            {
              text: this._translate.instant('Cancel'),
              role: 'cancel',
              handler: () => {
                //console.log('Cancel clicked');
              }
            },
            {
              text: this._translate.instant('Ok'),
              handler: (data: TypeBA2) => {
                if (data) {
                    console.log("utils",data);
                    data.parent.attributs = attributsParents;
                    if(brancheId)
                     sessionStorage.setItem("typeBrancheId", brancheId.toString());
                    this.getTypeBasAndSousBrancheAttributsDTO(data.id)
                    .subscribe((attributsFils: any) => {
                        if(attributsFils){
                            data.attributs = attributsFils;
                            switch (branche) {
                                case 'AUTOMOBILE':
                                   sessionStorage.setItem(
                                        typeRoutine,
                                        JSON.stringify(typeRoutineNewContrat)
                                    );
                                    sessionStorage.setItem(
                                        typeSousBranche,
                                        JSON.stringify(data)
                                    );
                                    this.navCtrl.navigateForward(LIENASSURINFOVE);
                                    break;
                                case 'SANTE':

                                    break;

                                default: this.alertNotification(this._translate.instant("branche_non_disponible"))
                            }
                        }
                    })
                }
                else
                  this.alertNotification("vous n'avez pas choisi de sous-branche")

              }
            }
          ]
        };

        options.inputs = [];

        // Now we add the radio buttons
        let sousBranches: TypeBA2[] = [];
        sousBranches=data;
        console.log('sousBranches',sousBranches)

        /**
         * * Cette boucle permet de charger les branches dans la liste des options
         */
        for (let i = 0; i < sousBranches.length; i++) {

          options.inputs.push({ name: 'sous-branche' + i, value: sousBranches[i], label: sousBranches[i].libelle, type: 'radio' });
        }

        // Create the alert with the options
        let alert = this.alertCtrl.create(options);
        (await alert).present();

      }
    );

  }
  async selectionBrancheSimulation2(origine, data) {
    //window.alert(JSON.stringify(data));
    this.initialiserSessionRoutine().then(
      async () => {
        var options: AlertOptions = {
          header: this._translate.instant('branche'),
          message: this._translate.instant('veuiller_choisir_une_branche'),
          buttons: [
            {
              text: this._translate.instant('Cancel'),
              role: 'cancel',
              handler: () => {
                //console.log('Cancel clicked');
              }
            },
            {
              text: this._translate.instant('Ok'),
              handler: data => {
                if (data) {
                  switch (data.libelle) {
                    case "AUTOMOBILE":
                      let simulationActif = new SimulationClient();
                      simulationActif.origineRoutine = origine;
                      sessionStorage.setItem(typeRoutine, typeRoutineSimulation)
                      sessionStorage.setItem(simulationActifenSession, JSON.stringify(simulationActif));
                      sessionStorage.setItem("typeBrancheId", data.id);
                      if(data.id){
                        this.getTypeBasAndSousBrancheAttributsDTO(data.id).subscribe((att) =>{
                            console.log("Attributs parents", att);
                            sessionStorage.setItem("AttributsParent", JSON.stringify(att));
                        })
                      }
                      this.navCtrl.navigateRoot("/sim-decla-objet"); break;

                    default: this.alertNotification(this._translate.instant("branche_non_disponible"))
                  }


                }
                else
                  this.alertNotification("vous n'avez pas choisi de branche")

              }
            }
          ]
        };

        options.inputs = [];

        // Now we add the radio buttons
        let branches: TypeBA2[];
        branches = data;

        /**
         * * Cette boucle permet de charger les branches dans la liste des options
         */
        for (let i = 0; i < branches.length; i++) {
          options.inputs.push({ name: 'branche' + i, value: branches[i], label: this._translate.instant(branches[i].libelle), type: 'radio' });
        }

        // Create the alert with the options
        let alert = this.alertCtrl.create(options);
        (await alert).present();

      });
  }

  async selectionBrancheSimulation(origine) {
    this.initialiserSessionRoutine().then(
      async () => {
        var options: AlertOptions = {
          header: 'Branche',
          message: 'Veuillez choisir une branche?',
          buttons: [
            {
              text: 'Cancel',
              role: 'cancel',
              handler: () => {
                //console.log('Cancel clicked');
              }
            },
            {
              text: 'Ok',
              handler: data => {
                //console.log(data)
                if (data) {
                  switch (data) {
                    case "Automobile":
                      let simulationActif = new SimulationClient();
                      simulationActif.origineRoutine = origine;
                      sessionStorage.setItem(typeRoutine, typeRoutineSimulation)
                      sessionStorage.setItem(simulationActifenSession, JSON.stringify(simulationActif));
                      this.navCtrl.navigateRoot("/sim-decla-objet"); break;

                    default: this.alertNotification("Message d'alerte pour dire que la branche n'est pas encore disponible")
                  }


                }
                else
                  this.alertNotification("vous n'avez pas choisi de branche")

              }
            }
          ]
        };

        options.inputs = [];

        // Now we add the radio buttons
        let branches = new Array<TypeBA>();
        branches = this.recupererTypeBranches();
        for (let i = 0; i < branches.length; i++) {
          options.inputs.push({ name: 'branche' + i, value: branches[i].nom, label: branches[i].nom, type: 'radio' });
        }

        // Create the alert with the options
        let alert = this.alertCtrl.create(options);
        (await alert).present();

      });

  }
  diffdate(d1, d2, u) {
    //console.log("diffDate") ;
    //console.log("date"+d2)
    d1 = new Date(this.displayDateFormatInitialise(d1))
    d2 = new Date(this.displayDateFormatInitialise(d2))
    //console.log(d1);
    //console.log(d2) ;
    let div = 1
    switch (u) {
      case 'seconde': div = 1000;
        break;
      case 'minute': div = 1000 * 60
        break;
      case 'heure': div = 1000 * 60 * 60
        break;
      case 'jour': div = 1000 * 60 * 60 * 24
        break;
    }

    var Diff = d1.getTime() - d2.getTime() + 1;
    ////console.log(Diff/div+ "jours")

    return Math.ceil((Diff / div))
  }
  /**
   * * Cette fonction permet de lancer la simulation
   * * auto en lui passant comme paramètre l'id
   * * de la branche pour la garder en session
   * @param idBranche
   */
  lancerAuto(idBranche) {
    this.initialiserSessionRoutine().then(
      () => {
        let simulationActif = new SimulationClient();
        simulationActif.origineRoutine = OrigineClientNonLogue;
        sessionStorage.setItem(typeRoutine, typeRoutineSimulation)
        sessionStorage.setItem(simulationActifenSession, JSON.stringify(simulationActif));
        sessionStorage.setItem("typeBrancheId", idBranche);
        this.navCtrl.navigateRoot('/sim-decla-objet')

      }
    )

  }




  isinferior(d1, d2) {
    d1 = new Date(d1);
    d2 = new Date(d2);
    if (d1.getFullYear() != d2.getFullYear())
      return d1.getFullYear() < d2.getFullYear()
    else
      if (d1.getMonth() != d2.getMonth())
        return d1.getMonth() < d2.getMonth()
      else
        if (d1.getDate() != d2.getDate())
          return d1.getDate() < d2.getDate()
        else
          return false;



  }
  /**
   *
   * @param contrats
   * cette fonction nous permet de filtrer les contrats disponibles
   */
  //  filtrerContrat(contrats){
  //    let contratsAfterFiltre =[];
  //    if(contrats != null)
  //    {
  //     contrats.forEach(contrat => {
  //       if(contrat.statut == statutContratActif || contrat.statut == statutContratEnCoursDeValidation ||  contrat.statut==statutContratValideNonActif)
  //       {
  //         contratsAfterFiltre.push(contrat) ;
  //       }

  //     });

  //    }

  //    return contratsAfterFiltre ;

  //  }
  testOperationActif(operation): Boolean {
    if (operation.statut == statutContratActif || operation.statut == statutContratEnCoursDeValidation || operation.statut == statutContratValideNonActif) {
      return true
    }
    else
      return false;

  }
  checkIfObjetHadContratStopped(contrats): boolean {
    contrats.forEach(contrat => {
      contrat.operations.forEach(operation => {
        if (operation.statut == statutContratSuspendu)
          return true;
      });


    });
    return false;

  }

  //checkObjet
  /**
   *
   * @param dateFormatMobile
   * @param dateDebut
   * @param dateFin
   * @param objet
   * Cette fonction nous permet de trouver les intervalles disponibles pour un objer
   */
  async objetEligiblePeriode(dateFormatMobile, dateDebut, dateFin, objet): Promise<boolean> {
    //////console.log("debutFonction")
    //recupere la liste des contrats delobjet
    let dateDuLendemain: Date;

    let dateDebutMinimum = new Date(null);
    let dateFinMaximumBeforeOther = new Date(null);
    // let contrats = this.filtrerContrat(objet.contrats)
    let contrats = objet.contrats
    let resultat



    let dat = new Date(dateFormatMobile.date);
    dat.setDate(dat.getDate() + 1)

    dateDuLendemain = new Date(dat);

    //
    if (contrats == null || contrats.length == 0) {


      dateDebutMinimum = dat;
      dateFinMaximumBeforeOther = null;
      //  //console.log("date minimum"+this.displayDate(dateFinMaximumBeforeOther ))
      //console.log(this.verifierDateOneObject(objet,dateDebut,dateFin,dateDebutMinimum,dateFinMaximumBeforeOther,dateDuLendemain))
      await this.verifierDateOneObject(objet, dateDebut, dateFin, dateDebutMinimum, dateFinMaximumBeforeOther, dateDuLendemain).then(
        res => {
          console.log(res)
          resultat = res

        },
        () => {
          return false
        }
      )
        ;
    }
    else {
      //console.log(contrats[0])
      contrats[0].operations[0].dateEffet = new Date(contrats[0].operations[0].dateEffet)
      let datBeforeOther = new Date(contrats[0].operations[0].dateEffet);
      //console.log("date minimum"+this.displayDate(datBeforeOther))

      contrats.forEach(contrat => {
        //boucle sur operation

        contrat.operations.forEach(operation => {
          operation.dateCloture = new Date(operation.dateCloture);
          if (this.isinferior(dat, operation.dateCloture)) {
            if (operation.dateCloture != null && this.isinferior(dat, operation.dateCloture)) {
              dat = new Date(operation.dateCloture);

            }
            operation.dateEffet = new Date(operation.dateEffet)
            if (operation.dateEffet != null && this.isinferior(operation.dateEffet, datBeforeOther)) {
              datBeforeOther = new Date(operation.dateEffet);
              //console.log("date minimum"+this.displayDate(datBeforeOther))

            }


          }

        });

      });
      if (dat != null) {

        dateDebutMinimum = new Date(dat);
        dateDebutMinimum.setDate(dateDebutMinimum.getDate() + 1)
      }
      if (datBeforeOther != null) {
        dateFinMaximumBeforeOther = new Date(datBeforeOther);
        dateFinMaximumBeforeOther.setDate(dateFinMaximumBeforeOther.getDate() - 1)
        //console.log("date minimum"+this.displayDate(dateFinMaximumBeforeOther ))
      }

      await this.verifierDateOneObject(objet, dateDebut, dateFin, dateDebutMinimum, dateFinMaximumBeforeOther, dateDuLendemain).then(
        res => {
          console.log(res)
          resultat = res

        }
      )

    }



    return resultat


  }


  //return true if correct
  //false in the contraty
  /**
   *
   * @param objet
   * @param dateDebut
   * @param dateFin
   * @param dateDebutMinimum
   * @param dateFinMaximumBeforeOther
   * @param dateDuLendemain
   * cette fonction nous permet de faire les test de verifications avec les différentes limites trouvées
   */
  async verifierDateOneObject(objet, dateDebut, dateFin, dateDebutMinimum, dateFinMaximumBeforeOther, dateDuLendemain): Promise<boolean> {

    if (dateDebut != null) {
      dateDebut = new Date(dateDebut);
      //////console.log("dateDebutVerif "+this.displayDate(dateDebut))



    }
    else {
      return false
    }

    if (dateFin != null) {
      dateFin = new Date(dateFin);

      //////console.log("dateFintVerif "+this.displayDate(dateFin))
    }
    else {
      return false
    }


    //////console.log("dateMinmimumVerif "+this.displayDate(dateDebutMinimum))


    if (dateDebut != null && dateFin != null) {
      // this.trouverdateDebutMinimum() ;
      dateDebut.setHours(0, 0, 0, 0);
      dateFin.setHours(0, 0, 0, 0);
      dateDebutMinimum.setHours(0, 0, 0, 0);

      if (this.isinferior(dateFin, dateDebut)) {

        //////console.log("date debut > fin")
        dateFin = null;
        dateDebut = null;


        this.alertNotification(objet.matricule + ": " + "la date de fin ne doit pas être inférieure à la date de début");
        return false;
      }


      //console.log(this.verifierIfInFirstIntervalle(objet,dateDebut,dateFin,dateDebutMinimum,dateFinMaximumBeforeOther,dateDuLendemain))
      //console.log(this.verifierIfInSecondIntervalle(objet,dateDebut,dateFin,dateDebutMinimum,dateFinMaximumBeforeOther,dateDuLendemain))
      if (!this.verifierIfInFirstIntervalle(objet, dateDebut, dateFin, dateDebutMinimum, dateFinMaximumBeforeOther, dateDuLendemain)) {
        //console.log("2 false")
        if (!this.verifierIfInSecondIntervalle(objet, dateDebut, dateFin, dateDebutMinimum, dateFinMaximumBeforeOther, dateDuLendemain)) {
          return false;
        }
        else {
          //console.log("ici")
          return true;
        }


      }
      else return true;



    }
  }
  verifierIfInFirstIntervalle(objet, dateDebut, dateFin, dateDebutMinimum, dateFinMaximumBeforeOther, dateDuLendemain) {
    var p1;
    //console.log(dateDebut )
    if (dateFinMaximumBeforeOther != null) {


      //console.log("date Debut"+this.displayDate(dateDebut))
      //console.log("date Lendemain"+this.displayDate(dateDuLendemain))
      //console.log("date Fin"+this.displayDate(dateFin))
      //console.log("date debut minimum"+this.displayDate(dateDebutMinimum))
      //console.log("date fin maximumBefore other"+this.displayDate(dateFinMaximumBeforeOther))
      //console.log(!this.isinferior(dateDebut,dateDuLendemain)) ;
      //console.log(!this.isinferior(dateFinMaximumBeforeOther,dateFin)) ;
      if ((!this.isinferior(dateDebut, dateDuLendemain) && !this.isinferior(dateFinMaximumBeforeOther, dateFin))) {


        //console.log("in da building")
        //  this.dateFin = null ;
        //  this.dateDebut =null ;
        //  this.dateDebutError =true ;
        //  this.alertNotification("le contrat doit finir au plus tard le le "+this.displayDate(dateFinMaximumBeforeOther)) ;
        return true
      }
      else
        return false


    }
    else return false
  }

  verifierIfInSecondIntervalle(objet, dateDebut, dateFin, dateDebutMinimum, dateFinMaximumBeforeOther, dateDuLendemain) {
    //console.log("not in first")
    if (this.isinferior(dateDebut, dateDebutMinimum)) {


      //console.log("date fin"+this.displayDate(dateFin))
      //////console.log("date debut < minimum")
      dateFin = null;
      dateDebut = null;

      if (dateFinMaximumBeforeOther != null)
        this.alertNotification(objet.matricule + ": " +
          "le contrat peut soit commencer apres le " + this.displayDate(dateDebutMinimum) +
          "\n ou soit debuter apres le " + this.displayDate(dateDuLendemain) + " se terminer avant le" + this.displayDate(dateFinMaximumBeforeOther));
      else
        this.alertNotification("le contrat ne peut pas commencer avant le " + this.displayDate(dateDebutMinimum));
      return false;
    }
    else
      return true;

  }
  //fin check objet


  async getObjets() {
    let op11 = {
      id: 1,

      intitule: "renouvellement",

      codePin: 5412544,

      dateEffet: new Date("08-11-2020"),
      dateCloture: new Date("08-01-2025"),
      contrat: null,

      paiement: null, statut: statutContratActif,

      garanties: [],

      sinistres: [],
    }
    let op12 = {
      id: 2,

      intitule: "renouvellement",

      codePin: 5412544,

      dateEffet: new Date("08-12-2020"),
      dateCloture: new Date("08-12-2021"),
      contrat: null,

      paiement: null, statut: statutContratActif,

      garanties: [],

      sinistres: [],
    }
    let op21 = {
      id: 3,

      intitule: "renouvellement",

      codePin: 5412544,

      dateEffet: new Date("08-05-2020"),
      dateCloture: new Date("08-22-2021"),
      contrat: null,

      paiement: null, statut: statutContratActif,

      garanties: [],

      sinistres: [],
    }
    let op22 = {
      id: 4,

      intitule: "renouvellement",

      codePin: 5412544,

      dateEffet: new Date("08-21-2020"),
      dateCloture: new Date("08-27-2021"),
      contrat: null,

      paiement: null, statut: statutContratActif,

      garanties: [],

      sinistres: [],
    }
    let p1 = {
      id: 1,
      contact: {
        id: 1,

        nom: "Ngom",
        prenom: "Pape Ousseynou",
        email: null,
        adresse: null,
        dateDeNaissance: null,

        civilite: "mr",
        compte: null


      },
      objetAssure: null
    }
    let typeBranche1 = {
      id: 1,

      nom: "Automobile",
      bas: [],
      description: DescAutos,
      typeGaranties: [],
      ConditionsDisposition: "<p>Quam ob rem circumspecta cautela observatum est deinceps </p> <p>et cum edita montium petere coeperint grassatores, loci iniquitati milites cedunt. ubi autem in planitie potuerint reperiri, quod contingit adsidue, nec exsertare lacertos nec crispare permissi tela, quae vehunt bina vel terna, pecudum ritu inertium trucidantur.Huic Arabia est conserta, ex alio latere Nabataeis contigua; opima varietate conmerciorum castrisque oppleta validis et castellis, quae ad repellendos gentium vicinarum excursus sollicitudo pervigil veterum per oportunos saltus erexit et cautos. haec quoque civitates habet inter oppida quaedam ingentes Bostram et Gerasam atque Philadelphiam murorum firmitate cautissimas. hanc provinciae inposito nomine rectoreque adtributo obtemperare legibus nostris Traianus conpulit imperator incolarum tumore saepe contunso cum glorioso marte Mediam urgeret et Partho.Utque proeliorum periti rectores primo catervas densas opponunt et fortes, deinde leves armaturas, post iaculatores ultimasque subsidiales acies, si fors adegerit, iuvaturas, ita praepositis urbanae familiae suspensae digerentibus sollicite,</p> <p> quos insignes faciunt virgae dexteris aptatae velut tessera data castrensi iuxta vehiculi frontem omne textrinum incedit: huic atratum coquinae iungitur ministerium, dein totum promiscue servitium cum otiosis plebeiis de vicinitate coniunctis: postrema multitudo spadonum a senibus in pueros desinens, obluridi distortaque lineamentorum conpage deformes, ut quaqua incesserit quisquam cernens mutilorum hominum agmina detestetur memoriam Samiramidis reginae illius veteris, quae teneros mares castravit omnium prima velut vim iniectans naturae, eandemque ab instituto cursu retorquens, quae inter ipsa oriundi crepundia per primigenios seminis fontes tacita quodam modo lege vias propagandae posteritatis ostendit.Qui cum venisset ob haec festinatis itineribus Antiochiam, praestrictis palatii ianuis, contempto Caesare, quem videri decuerat, ad praetorium cum pompa sollemni perrexit morbosque diu causatus nec regiam introiit nec processit in publicum, sed abditus multa in eius moliebatur exitium addens quaedam relationibus supervacua, quas subinde dimittebat ad principem.Cuius acerbitati uxor grave accesserat incentivum, germanitate Augusti turgida supra modum, quam Hannibaliano regi fratris filio antehac Constantinus iunxerat pater, Megaera quaedam mortalis, inflammatrix saevientis adsidua, humani cruoris avida nihil mitius quam maritus; qui paulatim eruditiores facti processu temporis ad nocendum per clandestinos versutosque rumigerulos conpertis leviter addere quaedam male suetos falsa et placentia sibi discentes, adfectati regni vel artium nefandarum calumnias insontibus adfligebant.</p>",


    }
    let compagnie1 = {
      id: 1,

      nom: "Axa",

      registreCommerce: "hfhfh",

      compte: null,

      communicationCommerciales: [],

      bas: [],

      listeNoire: null,

      reseaux: [],


      compteCredits: []
    }
    let contrat1 = {
      id: 1,
      police: "488-4445-552",
      etat: statutContratValideNonActif, franchise: 25000, capitauxGarantis: 250000,
      objetAssures: [],
      client: null,
      reseau: null,

      ca: compagnie1,
      operations: [op12],
      dateEffet: new Date("10-02-2020"),
      dateCloture: new Date("01-05-2030"),

    }
    let contrat2 = {
      id: 2,
      police: "488-4445-552",
      etat: statutContratActif, franchise: 25000, capitauxGarantis: 250000,
      objetAssures: [],
      client: null,
      reseau: null,

      ca: compagnie1,
      operations: [op21, op22],
      dateEffet: new Date("09-02-2020"),
      dateCloture: new Date("01-05-2020"),

    }
    let contrat3 = {
      id: 3,
      police: "488-4445-552",
      etat: statutContratResilie, franchise: 25000, capitauxGarantis: 250000,
      objetAssures: [],
      client: null,
      reseau: null,

      ca: compagnie1,
      operations: [op11],
      dateEffet: new Date("08-25-2020"),
      dateCloture: new Date("01-05-2030"),

    }

    let controleTechnique1 = {
      id: 1,

      objet: null,
      dateValidite: new Date("12-10-2021")
    }
    let controleTechnique2 = {
      id: 2,

      objet: null,
      dateValidite: new Date("05-02-2020")
    }
    let objetsEssais = [
    {
      id: 1,
      libelle: "Toyota Tacoma",
      contrats: [contrat1, contrat3, contrat2],
      matricule: "DK-1000-AB",
      numCarteGrise: "44445",
      statut: statutValideEnCours,
      marque: "Toyota",
      modele: "Auris",
      carburant: "gasoil",
      annee: "2015",
      puissanceFiscale: 5445544,
      usage: "particulier",
      nombrePlaces: 7,
      valeurDeclarative: 2500000,
      valeurInitiale: 1750000,
      typeBA: typeBranche1,
      controleTechnique: controleTechnique2
    },
    {
      id: 3,
      libelle: "Toyota Tacoma",
      contrats: [contrat3, contrat1],
      matricule: "DK-3000-AB",
      numCarteGrise: "44445",
      statut: statutValideEnCours,
      marque: "Toyota",
      modele: "Auris",
      carburant: "gasoil",
      annee: "2015",
      puissanceFiscale: 5445544,
      usage: "particulier",
      nombrePlaces: 7,
      valeurDeclarative: 2500000,
      valeurInitiale: 1750000,
      typeBA: typeBranche1,
      proprietaire: p1,
      controleTechnique: controleTechnique1
    },
    {
      id: 2,
      libelle: "Peugeot",
      contrats: [],
      matricule: "DK-2222-AB",
      statut: statutValidationKo,
      numCarteGrise: "44445",
      marque: "Peugeot",
      modele: "4007",
      carburant: "essence",
      annee: "2015",
      puissanceFiscale: "18 cv ",
      usage: "particulier",
      nombrePlaces: 9,
      valeurDeclarative: 2500000,
      valeurInitiale: 1750000,
      typeBA: typeBranche1,
      controleTechnique: controleTechnique1
    }];
    return objetsEssais;



  }
  /**
   * permet de verifier l'unicité dans une flotte
   */
  async verifierUniciteDansFlotte(liste) {
    let ifUnique = true
    for (let i = 0; i < liste.length - 1; i++) {

      for (let j = i + 1; j < liste.length; j++) {
        if (liste[i].objet.intitule == liste[j].objet.intitule) {
          ifUnique = false;
          break;
        }

      }

    }
    return ifUnique;

  }


  /**
   * Cette fonction permet de recuperer les intervalles de periodes  qui  font déja l'objet d'un contrat
   * param objet
   * return tableau de tableau associatif debut fin
   */
  async recupererIntervalleNonDisponibleObjet(contrats) {
    let dateDuLendemain: Date;

    let intervalleIndisponible: Array<Array<Date>> = [];

    //console.log("date minimum"+this.displayDate(datBeforeOther))
    if (contrats != null) {
      contrats.forEach(contrat => {
        //boucle sur operation

        contrat.operations.forEach(operation => {
          if (this.testOperationActif(operation)) {
            operation.dateCloture = new Date(operation.dateCloture);

            operation.dateEffet = new Date(operation.dateEffet)
            intervalleIndisponible.push([operation.dateEffet, operation.dateCloture])


          }


        }
        );
      }
      );

    }

    return intervalleIndisponible;





  }
  /**
   * Cette fonction nous permet de verifier l'objet se trouve dans un intervalle disponible
   * on verifie jour par jour sur tous les intervalles
   * @param intervalle = tableau qui comporte deux elements la date de début et la date de fin
   */
  async testerSiNewContratIntervalleDisponible(objet, dateDebut, dateFin, intervalleIndisponible) {
    let resultatGeneral = true;
    let dateCourant = new Date(dateDebut)
    dateFin = new Date(dateFin);
    dateDebut = new Date(dateDebut)
    console.log("test court")
    console.log(this.displayDate(dateCourant))
    console.log(this.displayDate(dateFin))
    while (!this.isinferior(dateFin, dateCourant)) {
      console.log(this.displayDate(dateCourant))
      intervalleIndisponible.forEach(intervalle => {
        if (!this.isinferior(dateCourant, dateDebut) && !this.isinferior(dateDebut, dateCourant))
          console.log(this.displayDate(dateCourant) + " " + " born inf " + this.displayDate(intervalle[0]) + " born sup "
            + this.displayDate(intervalle[1]) + " resultat "
            + this.testerSiDayIsInIntervalle(dateCourant, intervalle))
        if (this.testerSiDayIsInIntervalle(dateCourant, intervalle)) {

          resultatGeneral = false
        }

      });

      //incremente date courant
      dateCourant.setDate(dateCourant.getDate() + 1)
    }
    if (!resultatGeneral) {
      let message = "<p>le vehicule " + objet.matricule + "a deja une operation pour les periodes suivantes:</p><ul> \n"
      intervalleIndisponible.forEach(intervalle => {
        let sousMessage = "<li> du " + this.displayDate(intervalle[0]) + " au " + this.displayDate(intervalle[1]) + "</li>"
        message = message.concat(sousMessage);

      });
      message.concat("</ul>")
      this.alertNotification(message)
    }
    return resultatGeneral;



  }
  /**
   * cette fonction permet de tester si un jour se trouve dans un intervalle
   * return true if date is in intervalle
   */
  testerSiDayIsInIntervalle(date, intervalle): boolean {
    if (!this.isinferior(new Date(date), new Date(intervalle[0])) && !this.isinferior(new Date(intervalle[1]), new Date(date)))
      return true;
    else
      return false;



  }
  /**
   *
   * @param simulationActif
   * cette fonction nous permet de remplacer les informations d'un objet avant de revenir sur la page de recap flotte
   */
  async remplacerSimulationFlotteBeforeRecap(simulationActif) {
    let listeSimFlotte = JSON.parse(sessionStorage.getItem(listeSimulationDansFlotteValide));
    for (let index = 0; index < listeSimFlotte.length; index++) {


      if (listeSimFlotte[index].numeroDansFlotte == simulationActif.numeroDansFlotte) {
        //simulation=this.simulationActif ;

        listeSimFlotte[index] = simulationActif
        sessionStorage.setItem(listeSimulationDansFlotteValide, JSON.stringify(listeSimFlotte));
      }


    };


  }

  async recupererCompte(pageOrigine): Promise<Compte> {
    let compte: Compte
    compte = JSON.parse(window.sessionStorage.getItem(SessionCompte));
    if (compte == null) {
      let message = "veuillez vous connecter pour continuer";
      this.alertNotification(message);
      sessionStorage.setItem(loginToPage, JSON.stringify(pageOrigine))
      this.navCtrl.navigateRoot(LIENLOGIN);
      compte = JSON.parse(window.sessionStorage.getItem(SessionCompte));
    }
    return compte

  }

  async recupererCompte2(pageOrigine) {
    let compte: Compte
    let promise = new Promise<Compte>((resolve, reject) => {
        let message = "veuillez vous connecter pour continuer";
        this.alertNotification(message);
        sessionStorage.setItem(loginToPage, JSON.stringify(pageOrigine))
        this.navCtrl.navigateRoot(LIENLOGIN);
        compte = JSON.parse(window.sessionStorage.getItem(SessionCompte));
        if(compte.client.id != 0){
            resolve(compte)
        }else{
            reject("Client not exist")
        }
    });

    return promise;

  }

  async recupererBanieresCom() {
    let liste: Array<banniereCom> = [
      {
        id: 1

      },
      {
        id: 2

      },

    ]
    return liste;



  }

  definirNombreAleatoire() {
    let nombre = "";

    for (let i = 0; i < 5; i++) {

      nombre = nombre.concat(this.entierAleatoire(1, 15));

    }
    return Number(nombre);
  }

  entierAleatoire(min, max) {
    return Math.floor(Math.random() * (max - min + 1)) + min;
  }
  //gestion sms

  /**
   * Cette fonction permet de recuperer la durée à partir de la date de début et de la date de fin
   */
  recupererDuree(dureeEnJour): string {


    let dat = new Date(dureeEnJour * 24 * 60 * 60 * 1000)
    let datRef = new Date(null);
    dureeEnJour = dat.getDate() - datRef.getDate() - 1;
    let dureeMois = dat.getMonth() - datRef.getMonth();

    let dureeAnnee = dat.getFullYear() - datRef.getFullYear();
    console.log(dureeEnJour + " " + dureeMois + " " + dureeAnnee)
    if (dureeMois == 0 && dureeAnnee == 0)
      return "15"
    else
      if (dureeAnnee == 0) {
        return dureeMois + "";



      }

      else {
        return "12";


      }

  }
  /**
       *
       * @param newRoutine
       * cette fonction nous permet en cas de modification de la routine d'un objet d'une flotte
       * de rendre effectif cette modification au niveau de la listeRoutineContrat
       * fonction appelé au niveau de page assurinfove et choiceoption lors de la modification lors des routines de creation ou de renouvellement de contrat
       *
       *        */
  async remplacerRoutineContrat(newRoutine) {

    let listeRoutineContrat: Array<RoutineContrat> = JSON.parse(sessionStorage.getItem(listeRoutineContratSession))
    if (listeRoutineContrat == null) {
      listeRoutineContrat = [];
      listeRoutineContrat.push(newRoutine);

    }
    else {
      let indexRoutineEditing = JSON.parse(sessionStorage.getItem(indexRoutineContratEditingSession))
      if (indexRoutineEditing == null || indexRoutineEditing < 0) {
        listeRoutineContrat.push(newRoutine);
        console.log(listeRoutineContrat)

      }
      else {


        listeRoutineContrat[indexRoutineEditing] = newRoutine;
        console.log(listeRoutineContrat)

      }
    }
    sessionStorage.removeItem(indexRoutineContratEditingSession);
    console.log(listeRoutineContrat)
    sessionStorage.setItem(listeRoutineContratSession, JSON.stringify(listeRoutineContrat))



  }
  /**
   * recuperer les clients d'un réseau à changer apres communication du front ou back
   */
  async recupererClientsReseaux() {
    let clients = []
    let contactClient1 = new ContactClient();
    contactClient1.id = 1
    contactClient1.nom = "ngom"
    contactClient1.prenom = "papa"
    contactClient1.dateDeNaissance = new Date("12-05-1995")

    let contactClient2 = new ContactClient();
    contactClient2.id = 2
    contactClient2.nom = "ndiaye"
    contactClient2.prenom = "samba"
    contactClient2.dateDeNaissance = new Date("12-05-1995")
    let contactClient3 = new ContactClient();
    contactClient3.id = 2
    contactClient3.nom = "faye"
    contactClient3.prenom = "samba"
    contactClient3.dateDeNaissance = new Date("12-05-1995")

    let client1 = new Client();
    let client2 = new Client();
    let client3 = new Client();
    client1.id = 1;
    client2.id = 2;
    client3.id = 3;
    client1.compte.contact = contactClient1;
    client2.compte.contact = contactClient2;
    client3.compte.contact = contactClient3;
    //   client3.contact = contactClient3 ;


    clients.push(client1, client3, client2)
    return clients;
  }
  /**GESTION LOGIQUE TRANSLATION DES LANGUES */
  ionViewDidEnter(): void {
    this.getDeviceLanguage()
  }

  public changeLanguage(): void {
    this._translateLanguage();
  }

  _translateLanguage(): void {
    this._translate.use(this.language);
  }

  _initTranslate(language) {
    // Set the default language for translation strings, and the current language.
    this._translate.setDefaultLang('fr-FR');
    if (language) {
      this.language = language;
    }
    else {
      // Set your language here
      this.language = 'fr-FR';
    }
    this._translateLanguage();
  }

  getDeviceLanguage() {
    if (!this.platform.is('android') || !this.platform.is('ios')) {
      //alert('Navigateur')
      console.log('Navigateur langage', navigator.language)
      this._initTranslate(navigator.language);
    } else {
      this.globalization.getPreferredLanguage()
        .then(res => {
          this._initTranslate(res.value)
        })
        .catch(e => { console.log(e); });
    }
  }
  /**END TRANSLATION */
}

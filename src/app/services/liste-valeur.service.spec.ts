import { TestBed } from '@angular/core/testing';

import { ListeValeurService } from './liste-valeur.service';

describe('ListeValeurService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ListeValeurService = TestBed.get(ListeValeurService);
    expect(service).toBeTruthy();
  });
});

import { PackTypeGarantie } from './../models/PackTypeGarantie';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { UrlApi } from 'src/environments/environment';
import { TypeGarantie } from '../models/type-garantie';
import { ExclusionTypeGarantie } from '../models/ExclusionTypeGarantie';

@Injectable({
  providedIn: 'root'
})
export class TypeGarantieService {

  constructor( private httpClient : HttpClient) {
  }
    postTypeGarantie(TypeGarantie : TypeGarantie): Observable<TypeGarantie> {


      return this.httpClient.post<TypeGarantie>(UrlApi + "/typeGaranties/add",TypeGarantie).pipe();
    }
    getTypeGaranties():Observable<TypeGarantie[]>
    {

      return this.httpClient.get<TypeGarantie[]>(UrlApi+"/typeGaranties/getAll").pipe();
    }

    getTypeGarantie(id :number):Observable<TypeGarantie>
    {
      return this.httpClient.get<TypeGarantie>(UrlApi+'/typeGaranties/getOne/'+id).pipe();
    }

    getTypeGarantieByUsageAndBranche(idUsage, idBranche): Observable<TypeGarantie[]> {
        return this.httpClient.get<TypeGarantie[]>(UrlApi+"/typegaranties/getAllByBa/"+idUsage+'/'+idBranche);
      }

    deleteTypeGarantie(id : number)
    {
      return this.httpClient.get(UrlApi+'/typeGaranties/delete/'+id).pipe();
    }

    updateTypeGarantie(TypeGarantie) : Observable <TypeGarantie>
    {
      return this.httpClient.post<TypeGarantie>(UrlApi+'/typeGaranties/update', TypeGarantie).pipe();
    }

    getPackTypeGarantie(idTypeGarantie: number):Observable<PackTypeGarantie[]>
    {
      return this.httpClient.get<PackTypeGarantie[]>(UrlApi+"/typepacks/getPackbyGarantiePrincipale/"+idTypeGarantie);
    }

    getEclusionbyTypeGarantiePrincipale(idTypeGarantie: number):Observable<ExclusionTypeGarantie[]>
    {
      return this.httpClient.get<ExclusionTypeGarantie[]>(UrlApi+"/exclusionstg/getEclusionbyTypeGarantiePrincipale/"+idTypeGarantie);
    }
}

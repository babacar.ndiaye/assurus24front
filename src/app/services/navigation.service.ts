import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class NavigationService {
  public appPages = [
    {
      title: 'Partages',
      url: '/app/shared',
      tab: 'shared',
      icon: 'share'
    },
    {
      title: 'Recherches',
      url: '/app/wanted',
      tab: 'wanted',
      icon: 'search'
    },
    {
      title: 'Messages',
      url: '/app/accueil',
      tab: 'chats',
      icon: 'chatbubbles'
    },
    {
      title: 'Inventaire',
      url: '/app/home-menu',
      tab: 'boxes',
      icon: 'cube'
    },
    {
      title: 'Moi',
      url: '/app/me',
      tab: 'me',
      icon: 'contact'
    },
    {
      title: 'Modérer',
      url: '/app/moderate',
      tab: 'moderate',
      icon: 'construct'
    }
];
  constructor() { }
}

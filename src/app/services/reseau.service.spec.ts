import { TestBed } from '@angular/core/testing';

import { ReseauService } from './reseau.service';

describe('ReseauService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ReseauService = TestBed.get(ReseauService);
    expect(service).toBeTruthy();
  });
});

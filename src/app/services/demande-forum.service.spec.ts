import { TestBed } from '@angular/core/testing';

import { DemandeForumService } from './demande-forum.service';

describe('DemandeForumService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: DemandeForumService = TestBed.get(DemandeForumService);
    expect(service).toBeTruthy();
  });
});

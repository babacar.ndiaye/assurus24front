import { TestBed } from '@angular/core/testing';

import { OffrePromoService } from './offre-promo.service';

describe('OffrePromoService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: OffrePromoService = TestBed.get(OffrePromoService);
    expect(service).toBeTruthy();
  });
});

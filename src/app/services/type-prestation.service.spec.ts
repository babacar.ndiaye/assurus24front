import { TestBed } from '@angular/core/testing';

import { TypePrestationService } from './type-prestation.service';

describe('TypePrestationService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: TypePrestationService = TestBed.get(TypePrestationService);
    expect(service).toBeTruthy();
  });
});

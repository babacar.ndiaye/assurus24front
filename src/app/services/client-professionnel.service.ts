import { Injectable } from '@angular/core';
import { ClientProfessionnel } from '../models/client-professionnel';
import { HttpClient} from '@angular/common/http';
import { Observable } from 'rxjs';
import { UrlApi } from 'src/environments/environment';
import { Compte } from '../models/compte';


@Injectable({
  providedIn: 'root'
})
//
export class ClientProfessionnelService {

  constructor(private httpClient : HttpClient) { }
  inscription(compte : Compte,clientProfessionnel :ClientProfessionnel): Observable<ClientProfessionnel> {
    console.log("inscription client professionnel")
    clientProfessionnel.compte = compte
    clientProfessionnel.telephone=clientProfessionnel.compte.telephone;
    console.log("clientPro avant requete:",clientProfessionnel);
    
  return this.httpClient.post<ClientProfessionnel>(UrlApi + "/clients-professionnels/add",clientProfessionnel).pipe();
}
updateClient(clientProfessionnel :ClientProfessionnel) : Observable <Compte>
  {
    // clientProfessionnel.telephone=clientProfessionnel.compte.telephone;

    return this.httpClient.post<Compte>(UrlApi+'/clients-professionnels/update', clientProfessionnel).pipe();
  }
}

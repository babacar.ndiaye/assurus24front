import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { ListeValeur } from '../models/liste-valeur';
import { UrlApi } from 'src/environments/environment';
import { TypeBas } from '../models/type-bas';
import { TypeBA2 } from '../models/type-ba';


@Injectable({
  providedIn: 'root'
})
export class ListeValeurService {
  constructor(private httpClient: HttpClient) {
  }
  postListeValeur(ListeValeur: ListeValeur): Observable<ListeValeur> {


    return this.httpClient.post<ListeValeur>(UrlApi + "/listeValeurs/add", ListeValeur).pipe();
  }
  getListeValeurs(): Observable<ListeValeur[]> {

    return this.httpClient.get<ListeValeur[]>(UrlApi + "/listevaleurs/getAll").pipe();
  }

  getOneListeValeur(id: number): Observable<ListeValeur> {
    return this.httpClient.get<ListeValeur>(UrlApi + '/listevaleurs/getOne/' + id).pipe();
  }

  getAllListeValeur(codeListeValeur: string) {
    return this.httpClient.get<ListeValeur[]>(UrlApi + '/listes-valeur/getListValByCode/' + codeListeValeur)
  }

  deleteListeValeur(id: number) {
    return this.httpClient.get(UrlApi + '/listevaleurs/delete/' + id).pipe();
  }

  updateListeValeur(ListeValeur): Observable<ListeValeur> {
    return this.httpClient.post<ListeValeur>(UrlApi + '/listevaleurs/update', ListeValeur).pipe();
  }

  getListeCivilites(): Observable<ListeValeur[]> {
    return this.httpClient.get<ListeValeur[]>(UrlApi + "/listes-valeur/getAllCivilite").pipe();
  }

  getUsages(id): Observable<TypeBA2[]> {
    return this.httpClient.get<TypeBA2[]>(UrlApi + "/typebas/getAllByBa/" + id);
  }

  getAllEnergie(): Observable<ListeValeur[]> {
    return this.httpClient.get<ListeValeur[]>(UrlApi + "/listes-valeur/getAllEnergie");
  }

  getAllCarosserie(): Observable<ListeValeur[]> {
    return this.httpClient.get<ListeValeur[]>(UrlApi + "/listes-valeur/getAllCarosserie");
  }

  getAllCategorieMoto(): Observable<ListeValeur[]> {
    return this.httpClient.get<ListeValeur[]>(UrlApi + "/listes-valeur/getAllCategorieMoto")
  }

  getAllTypeVehiculeMoto(): Observable<ListeValeur[]> {
    return this.httpClient.get<ListeValeur[]>(UrlApi + "/listes-valeur/getAllTypeVehiculeMoto")
  }
}

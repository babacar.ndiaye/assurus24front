import { Contrat2 } from './../models/contrat';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { UrlApi } from 'src/environments/environment';
import { Operation } from '../models/operation';

@Injectable({
  providedIn: 'root'
})
export class ContratService {

  constructor(private httpClient : HttpClient) {
  }
    postContrat(Contrat : Contrat2): Observable<Contrat2> {


      return this.httpClient.post<Contrat2>(UrlApi + "/contrats/add",Contrat).pipe();
    }
    getContrats():Observable<Contrat2[]>
    {

      return this.httpClient.get<Contrat2[]>(UrlApi+"/contrats/getAll").pipe();
    }

    getContrat(id :number):Observable<Contrat2>
    {
      return this.httpClient.get<Contrat2>(UrlApi+'/contrats/getOne/'+id).pipe();
    }

    deleteContrat(id : number)
    {
      return this.httpClient.get(UrlApi+'/contrats/delete/'+id).pipe();
    }

    updateContrat(Contrat) : Observable <Contrat2>
    {
      return this.httpClient.post<Contrat2>(UrlApi+'/contrats/update', Contrat).pipe();
    }

    getContratByClient(idClient :number):Observable<Contrat2[]>
    {
      return this.httpClient.get<Contrat2[]>(UrlApi+'/contrats/findByClient/'+idClient);
    }

    getOperationByContrat(idContrat: number):Observable<Operation[]>
    {
        return this.httpClient.get<Operation[]>(UrlApi+'/operations/getOperationByContrat/'+idContrat);
    }




   }


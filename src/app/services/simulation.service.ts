import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { UrlApi } from 'src/environments/environment';
import { Simulation } from '../models/simulation';
import { ListeValeur } from '../models/liste-valeur';

@Injectable({
  providedIn: 'root'
})
export class SimulationService {

  constructor(private httpClient: HttpClient) {
  }
  postSimulation(Simulation: Simulation): Observable<Simulation> {


    return this.httpClient.post<Simulation>(UrlApi + "/simulations/add", Simulation).pipe();
  }
  getSimulations(): Observable<Simulation[]> {

    return this.httpClient.get<Simulation[]>(UrlApi + "/simulations/getAll").pipe();
  }

  getSimulation(id: number): Observable<Simulation> {
    return this.httpClient.get<Simulation>(UrlApi + '/simulations/getOne/' + id).pipe();
  }

  deleteSimulation(id: number) {
    return this.httpClient.get(UrlApi + '/simulations/delete/' + id).pipe();
  }

  updateSimulation(Simulation): Observable<Simulation> {
    return this.httpClient.post<Simulation>(UrlApi + '/simulations/update', Simulation).pipe();
  }


}

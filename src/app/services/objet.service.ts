import { Automobile } from 'src/app/models/automobile';
import { Injectable } from "@angular/core";
import { Observable } from "rxjs";
import { HttpHeaders, HttpClient } from "@angular/common/http";
import { UrlApi, initialeCG, initialeCT } from "src/environments/environment";
import { ObjetAssures2 } from '../models/objet-assures';
import { AttributAussure } from '../models/attribut-assure';

@Injectable({
  providedIn: "root",
})
export class ObjetService {
  constructor(private httpClient: HttpClient) { }

  upload(formData, objet): Observable<Boolean> {
    var headers = new HttpHeaders();

    headers.append("Content-Type", "multipart/form-data");
    switch (objet) {
      case initialeCG:
        return this.httpClient
          .post<Boolean>(
            UrlApi + "/file/uploadCarteGrise",
            formData,
            { headers: headers }
          )
          .pipe();
      case initialeCT:
        return this.httpClient
          .post<Boolean>(
            UrlApi + "/file/uploadControleTechnique",
            formData,
            { headers: headers }
          )
          .pipe();
    }
  }

  /**
   * * Cette api nous permet d'enregistré
   * * un objet assuré
   */
  addObjetAssures(ObjetAssures: ObjetAssures2): Observable<ObjetAssures2> {
    return this.httpClient.post<ObjetAssures2>(UrlApi + "/objetsassures/add", ObjetAssures).pipe();
  }

  /**
   * * Cette api nous retourne les automobiles enregistrés
   * * d'un client
   */
  getObjetsAutoByClient(idClient): Observable<Automobile[]> {
    return this.httpClient.get<Automobile[]>(UrlApi + "/automobiles/getOaByProprietaire/" + idClient);
  }

 /**
   * * Cette api nous permet de mettre à jour
   * * un objet assuré
   */
  updateObjetAssure(ObjetAssures: ObjetAssures2): Observable<ObjetAssures2>
    {
      return this.httpClient.post<ObjetAssures2>(UrlApi+'/objetsassures/update', ObjetAssures);
    }

  /**
   * * Cette api nous retourne les objets assurés
   * * d'un client
   */
  getObjetsAssuresByClient(idClient): Observable<ObjetAssures2[]> {
    return this.httpClient.get<ObjetAssures2[]>(UrlApi + "/objetsassures/getOaByProprietaire/" + idClient);
  }

  /**
   *
   * @param intitule
   * @param numeroTitulaire
   * * Cette api permet de verifier si un objet assure
   * * existe en lui donnant en paramètre
   * * son intitule et son numero
   */
  checkIfObjetAssureExist(intitule, numeroTitulaire): Observable<ObjetAssures2[]> {
    return this.httpClient.get<ObjetAssures2[]>(UrlApi + "/objetsassures/exist/" + intitule + "/" + numeroTitulaire);
  }

  /**
   * * Cette api nous retourne les attributs
   * * d'un objet assuré
   */
   getAttributsByObjetAssure(idObjet): Observable<AttributAussure[]> {
    return this.httpClient.get<AttributAussure[]>(UrlApi + "/attributassure/findByobjet/" + idObjet);
  }
}

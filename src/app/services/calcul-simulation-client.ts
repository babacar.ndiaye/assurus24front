import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { Observable } from "rxjs";
import { UrlApi } from "src/environments/environment";
import { SimulationClient, SimulationClient2 } from "../models/simulation-client";

@Injectable({
    providedIn: 'root'
})
export class CalculSimulationClient {

    constructor(private httpClient: HttpClient) {
    }
    getSimulationClient(simulation: SimulationClient2): Observable<SimulationClient2> {
        return this.httpClient.post<SimulationClient2>(UrlApi + "/simulations/calculate",simulation)
    }
}
import { Injectable } from '@angular/core';
import { ReponseForum } from '../models/reponse-forum';
import { Observable } from 'rxjs';
import { UrlApi } from 'src/environments/environment';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class ReponseForumService {

  constructor( private httpClient : HttpClient) {
  }
    postReponseForum(ReponseForum : ReponseForum): Observable<ReponseForum> {

   
      return this.httpClient.post<ReponseForum>(UrlApi + "/reponseForums/add",ReponseForum).pipe();
    }
    getReponseForums():Observable<ReponseForum[]>
    {
     
      return this.httpClient.get<ReponseForum[]>(UrlApi+"/reponseForums/getAll").pipe();
    }
   
    getReponseForum(id :number):Observable<ReponseForum>
    {
      return this.httpClient.get<ReponseForum>(UrlApi+'/reponseForums/getOne/'+id).pipe();
    }
   
    deleteReponseForum(id : number)
    {
      return this.httpClient.get(UrlApi+'/reponseForums/delete/'+id).pipe();
    }
   
    updateReponseForum(ReponseForum) : Observable <ReponseForum>
    {
      return this.httpClient.post<ReponseForum>(UrlApi+'/reponseForums/update', ReponseForum).pipe();
    }
}

import { Contrat2 } from './../models/contrat';
import { AssureContrat } from './../models/assure-contrat';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { UrlApi } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class AssureContratService {

  constructor(private httpClient : HttpClient) {

   }

   getContratByObjetAssure(idObjet :number):Observable<AssureContrat[]>
    {
      return this.httpClient.get<AssureContrat[]>(UrlApi+'/assurecontrat/findByobjet/'+idObjet);
    }

    getContratByObjetAssure2(objetAssure):Observable<Contrat2[]>
    {
      return this.httpClient.post<Contrat2[]>(UrlApi+'/contrats/findByObjetAssure',objetAssure);
    }

    getAssureContratByContrat(idContrat :number):Observable<AssureContrat[]>
    {
      return this.httpClient.get<AssureContrat[]>(UrlApi+'/assurecontrat/findBycontrat/'+idContrat);
    }
}

import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { UrlApi } from 'src/environments/environment';
import { HttpClient } from '@angular/common/http';
import { Reseau } from '../models/reseau';

@Injectable({
  providedIn: 'root'
})
export class reseauxervice {

  constructor( private httpClient : HttpClient) {
  }
    postReseau(Reseau : Reseau): Observable<Reseau> {


      return this.httpClient.post<Reseau>(UrlApi + "/reseaux/add",Reseau).pipe();
    }
    getreseaux():Observable<Reseau[]>
    {

      return this.httpClient.get<Reseau[]>(UrlApi+"/reseaux/getAll").pipe();
    }

    getReseau(id :number):Observable<Reseau>
    {
      return this.httpClient.get<Reseau>(UrlApi+'/reseaux/getOne/'+id).pipe();
    }

    deleteReseau(id : number)
    {
      return this.httpClient.get(UrlApi+'/reseaux/delete/'+id).pipe();
    }

    updateReseau(Reseau) : Observable <Reseau>
    {
      return this.httpClient.post<Reseau>(UrlApi+'/reseaux/update', Reseau).pipe();
    }
}

import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { TypePrestation } from '../models/type-prestation';
import { Observable } from 'rxjs';
import { UrlApi } from 'src/environments/environment';
import { HTTP } from '@ionic-native/http/ngx';

@Injectable({
  providedIn: 'root'
})
export class typePrestationservice {

  
  constructor(private httpClient : HttpClient) {
   
   }

  postTypePrestation(TypePrestation : TypePrestation): Observable<TypePrestation> {

    
    return this.httpClient.post<TypePrestation>(UrlApi + "/typePrestations/add",TypePrestation).pipe();
  }
  getTypePrestations():Observable<TypePrestation[]>
  {
   
    return this.httpClient.get<TypePrestation[]>(UrlApi+"/typePrestations/getAll").pipe();
  }

  getTypePrestation(id :number):Observable<TypePrestation>
  {
    return this.httpClient.get<TypePrestation>(UrlApi+'/typePrestations/getOne/'+id).pipe();
  }

  deleteTypePrestation(id : number)
  {
    return this.httpClient.get(UrlApi+'/typePrestations/delete/'+id).pipe();
  }

  updateTypePrestation(TypePrestation) : Observable <TypePrestation>
  {
    return this.httpClient.post<TypePrestation>(UrlApi+'/typePrestations/update', TypePrestation).pipe();
  }

}


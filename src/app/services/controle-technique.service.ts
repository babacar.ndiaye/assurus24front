import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { ControleTechnique } from '../models/controle-technique';
import { Observable } from 'rxjs';
import { UrlApi } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class ControleTechniqueService {

  constructor( private httpClient : HttpClient) {
  }
    postControleTechnique(ControleTechnique : ControleTechnique): Observable<ControleTechnique> {

   
      return this.httpClient.post<ControleTechnique>(UrlApi + "/controleTechniques/add",ControleTechnique).pipe();
    }
    getControleTechniques():Observable<ControleTechnique[]>
    {
     
      return this.httpClient.get<ControleTechnique[]>(UrlApi+"/controleTechniques/getAll").pipe();
    }
   
    getControleTechnique(id :number):Observable<ControleTechnique>
    {
      return this.httpClient.get<ControleTechnique>(UrlApi+'/controleTechniques/getOne/'+id).pipe();
    }
   
    deleteControleTechnique(id : number)
    {
      return this.httpClient.get(UrlApi+'/controleTechniques/delete/'+id).pipe();
    }
   
    updateControleTechnique(ControleTechnique) : Observable <ControleTechnique>
    {
      return this.httpClient.post<ControleTechnique>(UrlApi+'/controleTechniques/update', ControleTechnique).pipe();
    }
}

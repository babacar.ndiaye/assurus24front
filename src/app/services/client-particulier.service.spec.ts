import { TestBed } from '@angular/core/testing';

import { ClientParticulierService } from './client-particulier.service';

describe('ClientParticulierService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ClientParticulierService = TestBed.get(ClientParticulierService);
    expect(service).toBeTruthy();
  });
});

import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Paiement } from '../models/paiement';
import { UrlApi } from 'src/environments/environment';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class PaiementService {

  constructor( private httpClient : HttpClient) {
  }
    postPaiement(Paiement : Paiement): Observable<Paiement> {

   
      return this.httpClient.post<Paiement>(UrlApi + "/paiements/add",Paiement).pipe();
    }
    getPaiements():Observable<Paiement[]>
    {
     
      return this.httpClient.get<Paiement[]>(UrlApi+"/paiements/getAll").pipe();
    }
   
    getPaiement(id :number):Observable<Paiement>
    {
      return this.httpClient.get<Paiement>(UrlApi+'/paiements/getOne/'+id).pipe();
    }
   
    deletePaiement(id : number)
    {
      return this.httpClient.get(UrlApi+'/paiements/delete/'+id).pipe();
    }
   
    updatePaiement(Paiement) : Observable <Paiement>
    {
      return this.httpClient.post<Paiement>(UrlApi+'/paiements/update', Paiement).pipe();
    }
}

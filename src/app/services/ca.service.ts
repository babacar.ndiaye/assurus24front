import { BrancheCa } from './../models/branche-ca';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { CA } from '../models/ca';
import { Observable } from 'rxjs';
import { UrlApi } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class CaService {

  constructor(private httpClient : HttpClient) { }
  postCA(ca : CA): Observable<CA> {


    return this.httpClient.post<CA>(UrlApi + "/cas/add",ca).pipe();
  }
  getCAs():Observable<CA[]>
  {

    return this.httpClient.get<CA[]>(UrlApi+"/cas/getAll").pipe();
  }

  getCA(id :number):Observable<CA>
  {
    return this.httpClient.get<CA>(UrlApi+'/cas/getOne/'+id).pipe();
  }

  deleteCA(id : number)
  {
    return this.httpClient.get(UrlApi+'/cas/delete/'+id).pipe();
  }

  updateCA(CA) : Observable <CA>
  {
    return this.httpClient.post<CA>(UrlApi+'/cas/update', CA).pipe();
  }

  getBrancheCA(idBA: number,idCA: number):Observable<BrancheCa>
  {
    return this.httpClient.get<BrancheCa>(UrlApi+'/branchecas/findByBaCa/'+idBA+'/'+idCA).pipe();
  }
}

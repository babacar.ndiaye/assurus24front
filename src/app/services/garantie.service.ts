
import { Injectable } from '@angular/core';
import { Garantie } from '../models/garantie';
import { Observable } from 'rxjs';
import { UrlApi } from 'src/environments/environment';
import { HttpClient } from '@angular/common/http';
import { GarantieCA } from '../models/garantie-ca';

@Injectable({
  providedIn: 'root'
})
export class GarantieService {

  constructor( private httpClient : HttpClient) {
  }
    postGarantie(Garantie : Garantie): Observable<Garantie> {


      return this.httpClient.post<Garantie>(UrlApi + "/garanties/add",Garantie).pipe();
    }
    getGaranties():Observable<Garantie[]>
    {

      return this.httpClient.get<Garantie[]>(UrlApi+"/garanties/getAll").pipe();
    }

    getGarantie(id :number):Observable<Garantie>
    {
      return this.httpClient.get<Garantie>(UrlApi+'/garanties/getOne/'+id).pipe();
    }

    deleteGarantie(id : number)
    {
      return this.httpClient.get(UrlApi+'/garanties/delete/'+id).pipe();
    }

    updateGarantie(Garantie) : Observable <Garantie>
    {
      return this.httpClient.post<Garantie>(UrlApi+'/garanties/update', Garantie).pipe();
    }

     getGarantieCA(idBrancheCa :number,idTypegarantie :number):Observable<GarantieCA>
    {
      return this.httpClient.get<GarantieCA>(UrlApi+'/GarantieCAs/findByBrancheCAandTypeGarantie/'+idBrancheCa+'/'+idTypegarantie);
    }
}

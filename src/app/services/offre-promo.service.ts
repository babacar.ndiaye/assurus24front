import { Injectable } from '@angular/core';
import { OffrePromo } from '../models/OffrePromo';
import { Observable } from 'rxjs';
import { UrlApi } from 'src/environments/environment';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class OffrePromoService {

  constructor( private httpClient : HttpClient) {
  }
    postOffrePromo(OffrePromo : OffrePromo): Observable<OffrePromo> {

   
      return this.httpClient.post<OffrePromo>(UrlApi + "/offrePromos/add",OffrePromo).pipe();
    }
    getOffrePromos():Observable<OffrePromo[]>
    {
     
      return this.httpClient.get<OffrePromo[]>(UrlApi+"/offrePromos/getAll").pipe();
    }
   
    getOffrePromo(id :number):Observable<OffrePromo>
    {
      return this.httpClient.get<OffrePromo>(UrlApi+'/offrePromos/getOne/'+id).pipe();
    }
   
    deleteOffrePromo(id : number)
    {
      return this.httpClient.get(UrlApi+'/offrePromos/delete/'+id).pipe();
    }
   
    updateOffrePromo(OffrePromo) : Observable <OffrePromo>
    {
      return this.httpClient.post<OffrePromo>(UrlApi+'/offrePromos/update', OffrePromo).pipe();
    }
}

import { TestBed } from '@angular/core/testing';

import { CommunicationCommercialeService } from './communication-commerciale.service';

describe('CommunicationCommercialeService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: CommunicationCommercialeService = TestBed.get(CommunicationCommercialeService);
    expect(service).toBeTruthy();
  });
});

import { TestBed } from '@angular/core/testing';

import { AssureContratService } from './assure-contrat.service';

describe('AssureContratService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: AssureContratService = TestBed.get(AssureContratService);
    expect(service).toBeTruthy();
  });
});

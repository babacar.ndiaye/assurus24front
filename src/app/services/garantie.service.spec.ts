import { TestBed } from '@angular/core/testing';

import { GarantieService } from './garantie.service';

describe('GarantieService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: GarantieService = TestBed.get(GarantieService);
    expect(service).toBeTruthy();
  });
});

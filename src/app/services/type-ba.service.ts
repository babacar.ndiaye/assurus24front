import { Injectable } from '@angular/core';
import { TypeBA } from '../models/type-ba';
import { UrlApi } from 'src/environments/environment';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';


@Injectable({
  providedIn: 'root'
})
export class TypeBAService {

  constructor( private httpClient : HttpClient) {
  }
    postTypeBA(TypeBA : TypeBA): Observable<TypeBA> {

   
      return this.httpClient.post<TypeBA>(UrlApi + "/typeBAs/add",TypeBA).pipe();
    }
    getTypeBAs():Observable<TypeBA[]>
    {
     
      return this.httpClient.get<TypeBA[]>(UrlApi+"/typeBAs/getAll").pipe();
    }
   
    getTypeBA(id :number):Observable<TypeBA>
    {
      return this.httpClient.get<TypeBA>(UrlApi+'/typeBAs/getOne/'+id).pipe();
    }
   
    deleteTypeBA(id : number)
    {
      return this.httpClient.get(UrlApi+'/typeBAs/delete/'+id).pipe();
    }
   
    updateTypeBA(TypeBA) : Observable <TypeBA>
    {
      return this.httpClient.post<TypeBA>(UrlApi+'/typeBAs/update', TypeBA).pipe();
    }
}

import { TestBed } from '@angular/core/testing';

import { ReponseForumService } from './reponse-forum.service';

describe('ReponseForumService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ReponseForumService = TestBed.get(ReponseForumService);
    expect(service).toBeTruthy();
  });
});

import { TestBed } from '@angular/core/testing';

import { ClientProfessionnelService } from './client-professionnel.service';

describe('ClientProfessionnelService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ClientProfessionnelService = TestBed.get(ClientProfessionnelService);
    expect(service).toBeTruthy();
  });
});

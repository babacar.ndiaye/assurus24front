import { Injectable } from '@angular/core';
import { HttpClient} from '@angular/common/http';
import { Observable } from 'rxjs';
import { UrlApi } from 'src/environments/environment';
import { Compte } from '../models/compte';
import { ClientParticulier } from '../models/client-particulier';
import { ValidationClientCA } from '../models/validation-client-ca';

@Injectable({
  providedIn: 'root'
})
export class ClientParticulierService {


  constructor(private httpClient : HttpClient) { }
  inscription(compte : Compte,clientParticulier :ClientParticulier): Observable<ClientParticulier> {
      console.log("inscription client particulier")
        clientParticulier.compte = compte
        clientParticulier.telephone=clientParticulier.compte.telephone;
        
    return this.httpClient.post<ClientParticulier>(UrlApi + "/clientparticuliers/add",clientParticulier).pipe();
  }
  updateClient(clientParticulier :ClientParticulier) : Observable <Compte>
  {
    // clientParticulier.telephone=clientParticulier.compte.telephone;
    console.log("Service", clientParticulier);
    

    return this.httpClient.post<Compte>(UrlApi+'/clientparticuliers/update', clientParticulier);
  }
}

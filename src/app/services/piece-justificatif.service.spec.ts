import { TestBed } from '@angular/core/testing';

import { PieceJustificatifService } from './piece-justificatif.service';

describe('PieceJustificatifService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: PieceJustificatifService = TestBed.get(PieceJustificatifService);
    expect(service).toBeTruthy();
  });
});

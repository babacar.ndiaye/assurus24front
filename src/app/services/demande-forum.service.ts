import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { DemandeForum } from '../models/demande-forum';
import { HttpClient } from '@angular/common/http';
import { UrlApi } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class DemandeForumService {

  
  constructor( private httpClient : HttpClient) {
  }
    postDemandeForum(DemandeForum : DemandeForum): Observable<DemandeForum> {

   
      return this.httpClient.post<DemandeForum>(UrlApi + "/demandeForums/add",DemandeForum).pipe();
    }
    getDemandeForums():Observable<DemandeForum[]>
    {
     
      return this.httpClient.get<DemandeForum[]>(UrlApi+"/demandeForums/getAll").pipe();
    }
   
    getDemandeForum(id :number):Observable<DemandeForum>
    {
      return this.httpClient.get<DemandeForum>(UrlApi+'/demandeForums/getOne/'+id).pipe();
    }
   
    deleteDemandeForum(id : number)
    {
      return this.httpClient.get(UrlApi+'/demandeForums/delete/'+id).pipe();
    }
   
    updateDemandeForum(DemandeForum) : Observable <DemandeForum>
    {
      return this.httpClient.post<DemandeForum>(UrlApi+'/demandeForums/update', DemandeForum).pipe();
    }

}

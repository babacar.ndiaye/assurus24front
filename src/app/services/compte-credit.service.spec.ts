import { TestBed } from '@angular/core/testing';

import { CompteCreditService } from './compte-credit.service';

describe('CompteCreditService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: CompteCreditService = TestBed.get(CompteCreditService);
    expect(service).toBeTruthy();
  });
});

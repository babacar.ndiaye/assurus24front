import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Prestataire } from '../models/prestataire';
import { Observable } from 'rxjs';
import { UrlApi } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class PrestataireService {

  constructor( private httpClient : HttpClient) {
  }
    postPrestataire(Prestataire : Prestataire): Observable<Prestataire> {

   
      return this.httpClient.post<Prestataire>(UrlApi + "/prestataires/add",Prestataire).pipe();
    }
    getPrestataires():Observable<Prestataire[]>
    {
     
      return this.httpClient.get<Prestataire[]>(UrlApi+"/prestataires/getAll").pipe();
    }
   
    getPrestataire(id :number):Observable<Prestataire>
    {
      return this.httpClient.get<Prestataire>(UrlApi+'/prestataires/getOne/'+id).pipe();
    }
   
    deletePrestataire(id : number)
    {
      return this.httpClient.get(UrlApi+'/prestataires/delete/'+id).pipe();
    }
   
    updatePrestataire(Prestataire) : Observable <Prestataire>
    {
      return this.httpClient.post<Prestataire>(UrlApi+'/prestataires/update', Prestataire).pipe();
    }
}

import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Pays } from '../models/pays';
import { UrlApi } from 'src/environments/environment';


@Injectable({
  providedIn: 'root'
})
export class PaysService {
  constructor( private httpClient : HttpClient) {
  }
    postPays(Pays : Pays): Observable<Pays> {

   
      return this.httpClient.post<Pays>(UrlApi + "/pays/add",Pays).pipe();
    }
    getPays():Observable<Pays[]>
    {
     
      return this.httpClient.get<Pays[]>(UrlApi+"/pays/getAll").pipe();
    }
   
    getOnePays(id :number):Observable<Pays>
    {
      return this.httpClient.get<Pays>(UrlApi+'/pays/getOne/'+id).pipe();
    }
   
    deletePays(id : number)
    {
      return this.httpClient.get(UrlApi+'/pays/delete/'+id).pipe();
    }
   
    updatePays(Pays) : Observable <Pays>
    {
      return this.httpClient.post<Pays>(UrlApi+'/pays/update', Pays).pipe();
    }

   }
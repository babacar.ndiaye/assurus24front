import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { PrestationService } from '../models/prestationService';
import { UrlApi } from 'src/environments/environment';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class PrestationServiceService {

  constructor( private httpClient : HttpClient) {
  }
    postPrestationService(PrestationService : PrestationService): Observable<PrestationService> {

   
      return this.httpClient.post<PrestationService>(UrlApi + "/prestations/add",PrestationService).pipe();
    }
    getPrestationServices():Observable<PrestationService[]>
    {
     
      return this.httpClient.get<PrestationService[]>(UrlApi+"/prestations/getAll").pipe();
    }
   
    getPrestationService(id :number):Observable<PrestationService>
    {
      return this.httpClient.get<PrestationService>(UrlApi+'/prestations/getOne/'+id).pipe();
    }
   
    deletePrestationService(id : number)
    {
      return this.httpClient.get(UrlApi+'/prestations/delete/'+id).pipe();
    }
   
    updatePrestationService(PrestationService) : Observable <PrestationService>
    {
      return this.httpClient.post<PrestationService>(UrlApi+'/prestations/update', PrestationService).pipe();
    }
}

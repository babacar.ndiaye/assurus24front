import { Injectable } from '@angular/core';
import { CompteCredit } from '../models/compte-credit';
import { Observable } from 'rxjs';
import { UrlApi } from 'src/environments/environment';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class CompteCreditService {

  constructor(private httpClient : HttpClient) { }
  postCompte(CompteCredit : CompteCredit): Observable<CompteCredit> {

   
    return this.httpClient.post<CompteCredit>(UrlApi + "/compteCredits/add",CompteCredit).pipe();
  }
  getComptes():Observable<CompteCredit[]>
  {
   
    return this.httpClient.get<CompteCredit[]>(UrlApi+"/compteCredits/getAll").pipe();
  }
 
  getCompte(id :number):Observable<CompteCredit>
  {
    return this.httpClient.get<CompteCredit>(UrlApi+'/compteCredits/getOne/'+id).pipe();
  }
 
  deleteCompte(id : number)
  {
    return this.httpClient.get(UrlApi+'/compteCredits/delete/'+id).pipe();
  }
 
  updateCompte(CompteCredit) : Observable <CompteCredit>
  {
    return this.httpClient.post<CompteCredit>(UrlApi+'/compteCredits/update', CompteCredit).pipe();
  }
}




import { Platform, NavController } from '@ionic/angular';
import { Injectable } from '@angular/core';

import { BehaviorSubject } from 'rxjs';
import { CompteService } from './compte.service';
import { Compte } from '../models/compte';
import { idSessionUser } from 'src/environments/environment';
 

 
@Injectable({
  providedIn: 'root'
})
export class AuthService {
 
  authenticationState =false;
  compte : Compte ;
  constructor(private plt: Platform ,   private serviceCompte: CompteService,private navCtrl:NavController) { 
    this.plt.ready().then(() => {
      this.checkToken();
    });
  }
 
  checkToken() {
  
      if (window.sessionStorage.getItem(idSessionUser)) {
        this.authenticationState =true;
      }
  
    }
 
  
  logout() {
    window.sessionStorage.clear() ;
  
   this.authenticationState = false;
    
    
  }
 
  isAuthenticated() {
    return this.authenticationState ;
  }
  seConnecter(){
   this.logout()
    this.navCtrl.navigateRoot('/login'); 
  }
  
 
}
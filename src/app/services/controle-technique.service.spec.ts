import { TestBed } from '@angular/core/testing';

import { ControleTechniqueService } from './controle-technique.service';

describe('ControleTechniqueService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ControleTechniqueService = TestBed.get(ControleTechniqueService);
    expect(service).toBeTruthy();
  });
});

import { Injectable } from '@angular/core';
import { ListeNoire } from '../models/liste-noire';
import { Observable } from 'rxjs';
import { UrlApi } from 'src/environments/environment';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class ListeNoireService {

  constructor( private httpClient : HttpClient) {
  }
    postListeNoire(ListeNoire : ListeNoire): Observable<ListeNoire> {

   
      return this.httpClient.post<ListeNoire>(UrlApi + "/listeNoires/add",ListeNoire).pipe();
    }
    getListeNoires():Observable<ListeNoire[]>
    {
     
      return this.httpClient.get<ListeNoire[]>(UrlApi+"/listeNoires/getAll").pipe();
    }
   
    getListeNoire(id :number):Observable<ListeNoire>
    {
      return this.httpClient.get<ListeNoire>(UrlApi+'/listeNoires/getOne/'+id).pipe();
    }
   
    deleteListeNoire(id : number)
    {
      return this.httpClient.get(UrlApi+'/listeNoires/delete/'+id).pipe();
    }
   
    updateListeNoire(ListeNoire) : Observable <ListeNoire>
    {
      return this.httpClient.post<ListeNoire>(UrlApi+'/listeNoires/update', ListeNoire).pipe();
    }
}

import { TestBed } from '@angular/core/testing';

import { TypeGarantieService } from './type-garantie.service';

describe('TypeGarantieService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: TypeGarantieService = TestBed.get(TypeGarantieService);
    expect(service).toBeTruthy();
  });
});

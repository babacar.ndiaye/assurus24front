import { TestBed } from '@angular/core/testing';

import { ListeNoireService } from './liste-noire.service';

describe('ListeNoireService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ListeNoireService = TestBed.get(ListeNoireService);
    expect(service).toBeTruthy();
  });
});

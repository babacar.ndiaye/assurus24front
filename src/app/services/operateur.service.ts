import { Injectable } from '@angular/core';
import { Operateur } from '../models/operateur';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { UrlApi } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class OperateurService {

  constructor( private httpClient : HttpClient) {
  }
    postOperateur(Operateur : Operateur): Observable<Operateur> {

   
      return this.httpClient.post<Operateur>(UrlApi + "/operateurs/add",Operateur).pipe();
    }
    getOperateurs():Observable<Operateur[]>
    {
     
      return this.httpClient.get<Operateur[]>(UrlApi+"/operateurs/getAll").pipe();
    }
   
    getOperateur(id :number):Observable<Operateur>
    {
      return this.httpClient.get<Operateur>(UrlApi+'/operateurs/getOne/'+id).pipe();
    }
   
    deleteOperateur(id : number)
    {
      return this.httpClient.get(UrlApi+'/operateurs/delete/'+id).pipe();
    }
   
    updateOperateur(Operateur) : Observable <Operateur>
    {
      return this.httpClient.post<Operateur>(UrlApi+'/operateurs/update', Operateur).pipe();
    }
}

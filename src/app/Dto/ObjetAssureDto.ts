import { AttributAussureDto } from './AttributAussureDto';
import { AttributAussure } from "../models/attribut-assure";
import { Client } from "../models/client";
import { TypeBA2 } from "../models/type-ba";

export class ObjetAssuresDto {
    id: number;
    intitule: string;
    validationCA: false;
    validationOP: false;
    typeba: TypeBA2;
    numeroTitulaire: string;
    nomTitulaire: string;
    prenomTitulaire: string;
    proprietaire: Client;
    TabAtrributs?: AttributAussure[];
    attributs: AttributAussureDto[];
    infosValide: boolean;
}

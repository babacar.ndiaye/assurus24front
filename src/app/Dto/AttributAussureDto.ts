import { AttributAussureBrancheDto } from './AttributAussureBrancheDto';
export class AttributAussureDto{
    id?: number;
    attribut: AttributAussureBrancheDto;
    valeur: string;
  }

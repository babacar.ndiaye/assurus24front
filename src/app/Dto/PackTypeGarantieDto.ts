import { TypeGarantie } from '../models/type-garantie';
import { TypeGarantieDto } from './TypeGarantieDto';
export class PackTypeGarantieDto{
    id?: number;
    libelle: string;
    code: string;
    description: string;
    slogan: string;
    checked: Boolean = false;
    ExludePacke: Boolean = false;
    typeGaranties: TypeGarantie[];
  }

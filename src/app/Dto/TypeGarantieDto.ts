export class TypeGarantieDto{
    id?: number;
    libelle: string;
    description: string;
    slogan: string;
  }

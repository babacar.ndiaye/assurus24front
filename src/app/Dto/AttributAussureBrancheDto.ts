import { ListeValeur } from "../models/liste-valeur";

export class AttributAussureBrancheDto {
    id: number;
    code: string;
    libelle: string;
    type: string;
    taille: number;
    nbdec: number;
    codeListeVal: string;
    listevaleur: ListeValeur[];
    affEnResume: Boolean;
    idrecord: Boolean;
    ordreAff: number;
    ordreAffFront: number;
    datesupp: Date;
    placeholder: string;
    typeInput: string;
    affichageEtape: number;
    radioImage: Boolean;
    radioSimple: Boolean;
    selectImage: Boolean;
    selectSimple: Boolean;
valeurMaxRange: number;
    anneeDeCirculationMin: number;
}

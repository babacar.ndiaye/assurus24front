import { ModifierObjetPage } from './pages/monobjet/modifier-objet/modifier-objet.page';
import { ResetpasswordPageModule } from './pages/resetpassword/resetpassword.module';
import { EmailComposer } from '@ionic-native/email-composer/ngx';
import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { RouteReuseStrategy } from '@angular/router';
import { HttpClientModule, HttpClient } from '@angular/common/http';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { IonicModule, IonicRouteStrategy } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';

import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing.module';

// Modal Pages
import { ImagePageModule } from './pages/modal/image/image.module';
import { SearchFilterPageModule } from './pages/modal/search-filter/search-filter.module';

// Components
import { NotificationsComponent } from './components/notifications/notifications.component';
import { TooltipsModule } from 'ionic-tooltips';
import { FingerprintAIO } from '@ionic-native/fingerprint-aio/ngx';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HTTP } from '@ionic-native/http/ngx';
import { File, FileEntry } from '@ionic-native/file/ngx';
import { FileOpener } from '@ionic-native/file-opener/ngx';
import { LazyLoadImageModule } from 'ng-lazyload-image'

import { FileTransfer, FileTransferObject } from '@ionic-native/file-transfer/ngx';
import { IonicSelectableModule } from 'ionic-selectable';

import { AndroidPermissions } from '@ionic-native/android-permissions/ngx';

import { ModalMarquePageModule } from './pages/modal-marque/modal-marque.module';
import { NgxCurrencyModule } from "ngx-currency";
import { InAppBrowser } from '@ionic-native/in-app-browser/ngx';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';
import { TranslateModule, TranslateLoader  } from '@ngx-translate/core';
import { Globalization } from '@ionic-native/globalization/ngx';
import { FaqPipe } from './pipes/faq.pipe';
import { PipesModule } from './pipes/pipes.module';
import { ModalAllPageModule } from './pages/faq/modal-all/modal-all.module';
import { ModalOnePageModule } from './pages/faq/modal-one/modal-one.module';
import {NgxPaginationModule} from 'ngx-pagination';

import {MatToolbarModule} from '@angular/material/toolbar';
import {MatButtonModule} from '@angular/material/button';
import {MatMenuModule} from '@angular/material/menu';
import {MatSidenavModule} from '@angular/material/sidenav';
import {MatSelectModule} from '@angular/material/select';
import {MatTabsModule} from '@angular/material/tabs';
import {MatChipsModule} from '@angular/material/chips';
import {MatIconModule} from '@angular/material/icon';
import {MatCardModule} from '@angular/material/card';
import {MatExpansionModule} from '@angular/material/expansion';
import {MatSnackBarModule} from '@angular/material/snack-bar';
import {MatDividerModule} from '@angular/material/divider';
import {MatRadioModule} from '@angular/material/radio';
import {MatListModule} from '@angular/material/list';
import {MatSliderModule} from '@angular/material/slider';
import {MatInputModule} from '@angular/material/input';
import {MatDialogModule} from '@angular/material/dialog';
import { MatTableModule } from '@angular/material';
import { ModifierObjetPageModule } from './pages/monobjet/modifier-objet/modifier-objet.module';
import { ModalDateSimulationPageModule } from './pages/simbranauto/modal-date-simulation/modal-date-simulation.module';
import { ScrollToModule } from '@nicky-lenaers/ngx-scroll-to';
//import { AccordionchecklistComponent } from './components/accordionchecklist/accordionchecklist.component';
//import { AccordionListComponent } from './components/accordion-list/accordion-list.component';

//Cette function est pour la translation de l'application
export function HttpLoaderFactory(http: HttpClient) {
    return new TranslateHttpLoader(http, "./assets/i18n/", ".json");
    }


@NgModule({
  declarations: [AppComponent, NotificationsComponent],

  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    ScrollToModule.forRoot(),
    TranslateModule.forRoot({
        loader: {
          provide: TranslateLoader,
          useFactory: HttpLoaderFactory,
          deps: [HttpClient]
        }
      }),
    IonicModule.forRoot(),
    AppRoutingModule,
    HttpClientModule,
    ImagePageModule,
    SearchFilterPageModule,
    TooltipsModule.forRoot(),
    FormsModule,
    NgxPaginationModule,
    NgxCurrencyModule,
    ReactiveFormsModule,
    ResetpasswordPageModule,
    ModalMarquePageModule,
    IonicSelectableModule,
    PipesModule,
    ModalAllPageModule,
    ModalOnePageModule,
    ModalDateSimulationPageModule,
    MatButtonModule,
    MatSnackBarModule,
    MatToolbarModule,
    MatListModule,
    MatSliderModule,
    MatExpansionModule,
    MatMenuModule,
    MatTableModule,
    MatRadioModule,
    MatDialogModule,
    MatChipsModule,
    MatInputModule,
    MatIconModule,
    MatSidenavModule,
    MatSelectModule,
    MatTabsModule,
    MatDividerModule,
    MatCardModule,
    ModifierObjetPageModule

  ],
  entryComponents: [NotificationsComponent],
  providers: [
    StatusBar,
    HTTP,
    File,
    FileTransfer,
    FileTransferObject,
    FileOpener,
    InAppBrowser,
    AndroidPermissions,
    LazyLoadImageModule,
    EmailComposer,
    FingerprintAIO,
    SplashScreen,
    Globalization,
    { provide: RouteReuseStrategy, useClass: IonicRouteStrategy }
  ],
  bootstrap: [AppComponent]
})

export class AppModule {}

import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AuthGuard } from './guards/auth.guard';

const routes: Routes = [
  { path: '', redirectTo: '/epocno', pathMatch: 'full' },
  { path: 'epocno', loadChildren: './pages/tabs/tabs.module#TabsPageModule'},
  { path: 'login', loadChildren: './pages/login/login.module#LoginPageModule' },
  { path: 'register', loadChildren: './pages/register/register.module#RegisterPageModule' },
  { path: 'about', loadChildren: './pages/about/about.module#AboutPageModule' },
  { path: 'settings', loadChildren: './pages/settings/settings.module#SettingsPageModule' },
  { path: 'edit-profile', loadChildren: './pages/edit-profile/edit-profile.module#EditProfilePageModule' },
  { path: 'ecranPageSimulation', loadChildren: './pages/home-results/home-results.module#HomeResultsPageModule' },
  { path: 'accueiltwo', loadChildren: './pages/accueiltwo/accueiltwo.module#AccueiltwoPageModule' },
  { path: 'identification', loadChildren: './pages/identification/identification.module#IdentificationPageModule' },
  { path: 'contact', loadChildren: './pages/contact/contact.module#ContactPageModule' },
  { path: 'uploaddoc', loadChildren: './pages/uploaddoc/uploaddoc.module#UploaddocPageModule' },
  { path: 'mycar', loadChildren: './pages/mycar/mycar.module#MycarPageModule' },
  { path: 'myprofile', loadChildren: './pages/myprofile/myprofile.module#MyprofilePageModule' },
  { path: 'choiceoption', loadChildren: './pages/choiceoption/choiceoption.module#ChoiceoptionPageModule' },
  { path: 'infocar', loadChildren: './pages/infocar/infocar.module#InfocarPageModule' },
  { path: 'comptecredit', loadChildren: './pages/comptecredit/comptecredit.module#ComptecreditPageModule' },
  { path: 'resultatssimulation', loadChildren: './pages/resultatssimulation/resultatssimulation.module#ResultatssimulationPageModule' },
  { path: 'mesobjetsassures', loadChildren: './pages/mesobjetsassures/mesobjetsassures.module#MesobjetsassuresPageModule' },
  { path: 'mesassurances', loadChildren: './pages/mesassurances/mesassurances.module#MesassurancesPageModule' },
  { path: 'myprofilecom', loadChildren: './pages/myprofilecom/myprofilecom.module#MyprofilecomPageModule' },
  { path: 'contrat', loadChildren: './pages/contrat/contrat.module#ContratPageModule' },
  { path: 'choicestepone', loadChildren: './pages/choicestepone/choicestepone.module#ChoicesteponePageModule' },
  { path: 'ccac', loadChildren: './pages/ccac/ccac.module#CcacPageModule' },
  { path: 'pageaccueilcom',
 // canActivate: [AuthGuard],
  loadChildren: './pages/pageaccueilcom/pageaccueilcom.module#PageaccueilcomPageModule' },
  { path: 'simplepageaccueil', loadChildren: './pages/simplepageaccueil/simplepageaccueil.module#SimplepageaccueilPageModule' },
  // { path: 'epocno', loadChildren: './pages/epocno/epocno.module#EpocnoPageModule' },
  { path: 'selection-profil', loadChildren: './pages/selection-profil/selection-profil.module#SelectionProfilPageModule' },
  { path: 'assurinfove', loadChildren: './pages/assurinfove/assurinfove.module#AssurinfovePageModule' },
  { path: 'inscridocassur', loadChildren: './pages/inscridocassur/inscridocassur.module#InscridocassurPageModule' },
  { path: 'mes-contrats', loadChildren: './pages/mes-contrats/mes-contrats.module#MesContratsPageModule' },
  // tslint:disable-next-line: max-line-length
  { path: 'gestion-mes-simulations', loadChildren: './pages/gestion-mes-simulations/gestion-mes-simulations.module#GestionMesSimulationsPageModule' },
  { path: 'assurinfove', loadChildren: './pages/assurinfove/assurinfove.module#AssurinfovePageModule' },
  { path: 'moncomptecredit', loadChildren: './pages/moncomptecredit/moncomptecredit.module#MoncomptecreditPageModule' },
  { path: 'sim-decla-objet', loadChildren: './pages/sim-decla-objet/sim-decla-objet.module#SimDeclaObjetPageModule' },
  { path: 'monobjet', loadChildren: './pages/monobjet/monobjet.module#MonobjetPageModule' },
  { path: 'mes-clients', loadChildren: './pages/mes-clients/mes-clients.module#MesClientsPageModule' },
  { path: 'page-client-commercial', loadChildren: './pages/page-client-commercial/page-client-commercial.module#PageClientCommercialPageModule' },
  { path: 'simbranauto', loadChildren: './pages/simbranauto/simbranauto.module#SimbranautoPageModule' },
  { path: 'ecran-recapitulatif', loadChildren: './pages/ecran-recapitulatif/ecran-recapitulatif.module#EcranRecapitulatifPageModule' },
  { path: 'accueil-client-logge',
  //canActivate: [AuthGuard],
  loadChildren: './pages/accueil-client-non-logge/accueil-client-non-logge.module#AccueilClientNonLoggePageModule' },
  { path: 'accueil-commercial', loadChildren: './pages/accueil-commercial/accueil-commercial.module#AccueilCommercialPageModule' },
  { path: 'type-prestation',

   children :[
    { path : '',loadChildren: './pages/type-prestation/type-prestation.module#TypePrestationPageModule' },
    { path: 'ajouter', loadChildren: './pages/type-prestation/ajouter/ajouter.module#AjouterPageModule' },
    { path: 'modifier/:id', loadChildren: './pages/type-prestation/modifier/modifier.module#ModifierPageModule' }

   ]
  },
  { path: 'resetpassword', loadChildren: './pages/resetpassword/resetpassword.module#ResetpasswordPageModule' },

  { path: 'modifier-objet', loadChildren: './pages/monobjet/modifier-objet/modifier-objet.module#ModifierObjetPageModule' },
  { path: 'modal-marque', loadChildren: './pages/modal-marque/modal-marque.module#ModalMarquePageModule' },

  { path: 'assurinfove-vehicule-flotte', loadChildren: './pages/assurinfove-vehicule-flotte/assurinfove-vehicule-flotte.module#AssurinfoveVehiculeFlottePageModule' },
  { path: 'liste-operations-contrats', loadChildren: './modals/liste-operations-contrats/liste-operations-contrats.module#ListeOperationsContratsPageModule' },
  { path: 'mes-services', loadChildren: './pages/mes-services/mes-services.module#MesServicesPageModule' },
  { path: 'monservice', loadChildren: './pages/monservice/monservice.module#MonservicePageModule' },
  { path: 'liste-contact-professionel', loadChildren: './pages/liste-contact-professionel/liste-contact-professionel.module#ListeContactProfessionelPageModule' },
  { path: 'form-contacts-de-professionel', loadChildren: './pages/form-contacts-de-professionel/form-contacts-de-professionel.module#FormContactsDeProfessionelPageModule' },
  { path: 'accueil', loadChildren: './pages/accueil/accueil.module#AccueilPageModule' },
  { path: 'contactez-nous', loadChildren: './pages/contactez-nous/contactez-nous.module#ContactezNousPageModule' },
  { path: 'faq', loadChildren: './pages/faq/faq.module#FaqPageModule' },
  { path: 'modal-all', loadChildren: './pages/faq/modal-all/modal-all.module#ModalAllPageModule' },
  { path: 'modal-one', loadChildren: './pages/faq/modal-one/modal-one.module#ModalOnePageModule' },
  { path: 'notifications', loadChildren: './pages/notifications/notifications.module#NotificationsPageModule' },
  { path: 'mes-activites', loadChildren: './pages/mes-activites/mes-activites.module#MesActivitesPageModule' }






















];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})

export class AppRoutingModule {}

import { TarifGarantie } from "./tarif-garantie";
import { TypeGarantie } from "./type-garantie";
import { BrancheCa } from "./branche-ca";

export class GarantieCA {
	id: number ;
	code: string ;
	codeExterne: string  = "";
	privalibellete :string ;
	description: string ;
	taux :number  = 100.0;
	minimum :number  = 0.0;
	franchise :number  = 0.0;
	privaageminte :number  = 0;
	agemax :number  = 0;
	attributmontant :string ;
	codeFormule :TarifGarantie ;
	typegarantie :TypeGarantie ;
	brancheca :BrancheCa;
	pack :boolean  = false;
}

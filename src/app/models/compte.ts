import { ListeValeur } from "./liste-valeur";
import { Reseau } from "./reseau";
import { Operateur } from "./operateur";
import { Prestataire } from "./prestataire";
import { CA } from "./ca";
import { Profil } from "./profil";
import { Client } from "./client";
import { Contact } from "./contact";

export class Compte {
    id: number ;
    infos : string;
    identifiant : string;
    motDePasse : string ;
    telephone : string ;
    essais : number;
    actif : ListeValeur;
    myProfil : Profil ;
    dateCreation : Date;
    dateModif : Date;
    dateSuppression : Date;
    ca : CA;
    operateur : Operateur;
    prestataire : Prestataire;
    reseau : Reseau;
    client : Client ;
    contact: Contact ;


}

import { Contrat2 } from 'src/app/models/contrat';
import { DetailOperation } from './detail-operation';
import { Paiement } from "./paiement";
import { Garantie } from "./garantie";
import { Sinistre } from "./sinistre";
import { ObjetAssures } from "./objet-assures";
import { CouponPromo } from "./CouponPromo";

// export class Operation {
//     id :number;
// 	intitule : string;
// 	codePin :  number;

// 	 //format date MM-dd-YYYY
// 	 dateEffet : Date;
// 	 dateCloture: Date;
// 	 contrat :Contrat;

// 	paiement :Paiement

// 	 garanties : Array<Garantie>

// 	sinistres : Array<Sinistre>
// 	statut : string ;

// }

export class Operation {
    id :number;
	intitule : string;
	codePin :  number;
	 //format date MM-dd-YYYY
	dateEffet : Date;
	dateCloture: Date;
	contrat :Contrat2;
	paiement :Paiement
	//garanties : Array<Garantie>
	sinistre : Sinistre
    montantbrut: any;
    montantnet: any;
    tauxremise: any;
    forfaitremise: any;
    coupon: CouponPromo;
    details: DetailOperation[];

}

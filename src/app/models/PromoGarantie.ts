import { OperateurPromo } from './OperateurPromo';
import { TypeGarantie } from './type-garantie';
export class PromoGarantie {
    id: number;
    garantie: TypeGarantie;
    promo: OperateurPromo;
}

import { PieceJustificatif } from "./piece-justificatif";
import { ObjetAssures } from "./objet-assures";

export class ControleTechnique  {
    id:number ;
    dateValidite : Date ;
    objet : ObjetAssures ;
}

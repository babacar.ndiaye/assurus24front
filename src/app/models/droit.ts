import { Profil } from "./profil";
import { Organisation } from "./organisation";
import { SousMenu } from "./sous-menu";

export class Droit {
    id : number;
    ajouter : boolean;
    modifier :boolean;
    supprimer : boolean;
    consulter : boolean;
    mySousMenu : SousMenu;
    myProfile : Profil;
    myOrganisation : Organisation;

}

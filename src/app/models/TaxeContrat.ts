import { TaxeAssurance } from './TaxeAssurance';
import { AssureContrat } from "./assure-contrat";
import { Garantie } from "./garantie";

export class TaxeContrat {
    id: number;
	libelle: string;
    montant: any = 0.0;
    taxe: TaxeAssurance;
    garantie: Garantie;
    assure: AssureContrat;
}

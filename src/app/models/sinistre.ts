import { AssureContrat } from './assure-contrat';
import { Contrat2 } from './contrat';

import { Operation } from "./operation";
import { TypeSinistre } from './TypeSinistre';

export class Sinistre {
    id : number;
    numSinistre: string;
	date: Date;
    lieu: string;
    typeSinistre: TypeSinistre;
    contrat: Contrat2;
    assure: AssureContrat;

}

import { Pays } from "./pays";
import { Region } from "./region";
import { Compte } from "./compte";
import { Reseau } from "./reseau";
import { CompteCredit } from "./compte-credit";
import { ListeValeur } from "./liste-valeur";
import { ContactClient } from "./contact-client";
import { PaysOperateur } from "./pays-operateur";

export class Operateur {
	id : number
	nom : string;
	typeSociete : ListeValeur;
	ninea : string;
	registreCommerce : string;
	adresse : string;
	telephone : string;
	email : string;
	siteweb : string;
	tauxfraisTransac : number=0;
	forfaitTransac :number=0;
	parrainage :number=0;
	logo : string;
	adresse2 : string;
	telephone2 : string;
	fax : string;
	codepostal : string;
	pays: Pays ;
	ville: Region ;
	actif: ListeValeur ;
	contacts: Array<ContactClient>;
	paysOperateurs :Array<PaysOperateur> ;
	compteutilisateurs : Array<Compte>;
	reseaux :Array<Reseau>;
	//---------------------------

	compte :Compte;
	compteCredits :Array<CompteCredit>;

}
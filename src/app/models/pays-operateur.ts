import { Operateur } from "./operateur";
import { Pays } from "./pays";

export class PaysOperateur {
    id: number;

	myPays: Pays ;

	myOperateur: Operateur ;
}

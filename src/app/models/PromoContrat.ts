import { AssureContrat } from "./assure-contrat";
import { Garantie } from "./garantie";
import { OffrePromo } from "./OffrePromo";

export class PromoContrat {
    id: number;
    montant: any = 0.0;
	libelle: string;
    offre: OffrePromo;
    garantie: Garantie;
    assure: AssureContrat;
}

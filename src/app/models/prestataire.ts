import { Pays } from "./pays";
import { Region } from "./region";
import { Compte } from "./compte";
import { TypePrestation } from "./type-prestation";
import { PrestationService } from "./prestationService";
import { CompteCredit } from "./compte-credit";
import { ListeValeur } from "./liste-valeur";
import { ContactClient } from "./contact-client";
import { PaysOperateur } from "./pays-operateur";

export class Prestataire {

    id : number;

	nom : string;
	typeSociete: ListeValeur ;

	ninea : string ;
	registreCommerce : string ;
	adresse : string ;
	email : string ;
	siteweb : string ;
	logo : string ;
	adresse2 : string ;
	telephone2 : string ;
	fax : string ;
	codepostal : string ;
	pays: Pays ;
	ville: Region ;
	actif: ListeValeur ;
	contacts: Array<ContactClient>;
	paysOperateurs :Array<PaysOperateur>;
	compteutilisateurs: Array<Compte>;
  //---------------------

	 prenom :string;
	
     siteWeb :string;

    telephone : string;
   
	
	compte :Compte ;
	
	 typePrestations : Array<TypePrestation>;
	
	 prestations : Array<PrestationService>;
	
	 compteCredits : Array<CompteCredit>;

}

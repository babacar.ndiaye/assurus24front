import { Client } from "./client";
import { CA } from "./ca";

export class ValidationClientCA {
    id : number;
    client : Client;
    ca : CA;
    idExterne : string ='';
    malus : number = 0;
    validation : boolean=false;
    datevalidation : Date;
    datemodification : Date;
    
}

import { TableCoeffGarantie } from "./TableCoeffGarantie";

export class ParamColonneTableCoeff {
    id: number;
    tablecoeffgarantie: TableCoeffGarantie;
    libelle: string;
    pivot: string;
    attributpivot: string;
    typedonnee: string;
    taillezone: string;
    listevaleur: string;
}

import { TarifGarantie } from "./tarif-garantie";

export class ParamColonneTarif {
	id: number ;
	tarifgarantie: TarifGarantie ;
	libelle: string ;
	pivot: string ; // Oui-Non
	attributpivot: string ;
	typedonnee: string ;
	taillezone: string  = "";
	listevaleur: string  = "";
}

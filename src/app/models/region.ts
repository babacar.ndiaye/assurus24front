import { Pays } from "./pays";

export class Region {
    id : number;
    nom :string;
    code : String;
    pays :Pays ;

}

import { Garantie } from 'src/app/models/garantie';
import { AttributGarantieBA } from './AttributGarantieBA';
export class AttributGarantie {
	id: number;
    attribut: AttributGarantieBA;
    garantie: Garantie;
    valeur: string;
}

import { ObjetAssures } from "./objet-assures";
import { ContactClient } from "./contact-client";

export class Proprietaire {
    id : number ;
    contact : ContactClient;
    objetAssure : ObjetAssures

}

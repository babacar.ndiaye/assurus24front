import { Garantie } from "./garantie";
import { CA } from "./ca";
import { ObjetAssures } from "./objet-assures";
import { TypeBA } from "./type-ba";

export class BA {
	 id : number;
	 nom :string;
	 typeBA :TypeBA
	 ca  : CA ;
	 garanties :Array<Garantie> 
	//
	objetAssures : Array<ObjetAssures>
}

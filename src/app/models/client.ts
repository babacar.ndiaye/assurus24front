import { Contrat2 } from 'src/app/models/contrat';
import { Compte } from "./compte";
import { Simulation } from "./simulation";
import { PieceJustificatif } from "./piece-justificatif";
import { CompteCredit } from "./compte-credit";
import { Reseau } from "./reseau";
import { ListeNoire } from "./liste-noire";
import { DemandeForum } from "./demande-forum";
import { ReponseForum } from "./reponse-forum";
import { Pays } from "./pays";
import { Region } from "./region";
import { ListeValeur } from "./liste-valeur";
import { ObjetAssures } from "./objet-assures";
import { ValidationClientCA } from "./validation-client-ca";
import { Civilite } from "./civilite";


export class Client {
	id : number ;
	nom :  string ;
	prenom : string;
    adresse :  string ;
    adresse2 :  string ;
    telephone : string ;
    telephone2 : string ;
	email : string ;
    fax : string ;
    codePostal :string;
    civilite: ListeValeur;
    pays : Pays;
	ville : Region ;
	actif : ListeValeur;
	reseau :Reseau ;
	validationReseau : boolean = false;
	datevalidationReseau : Date;
	validationOP : boolean = false;
	userOP : string ='';
	datevalidationOp : Date;
	datecreation : Date;
	validationCA : Array<ValidationClientCA>;
	compte : Compte;
	objetassures : Array<ObjetAssures>;
	contrats :Array<Contrat2> ;
	demandeForums  : Array<DemandeForum>;

}

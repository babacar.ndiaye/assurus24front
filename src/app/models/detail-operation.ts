import { AssureContrat } from "./assure-contrat";
import { Garantie } from "./garantie";
import { Operation } from "./operation";

export class DetailOperation {
    id: number ;
	operation: Operation;
	assure: AssureContrat;
	garantie: Garantie;
	montant: number  = 0;
}

import { SousMenu } from "./sous-menu";

export class Menu {
    id : number;
    code : String;
    libelle : String;
    image : String;
    rang : Number;
    myParent : Menu;
    sousMenus : Array<Menu>;
    sousMenuItems : Array <SousMenu>
}

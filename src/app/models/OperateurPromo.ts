import { PromoBranche } from './PromoBranche';
import { CouponPromo } from './CouponPromo';
import { PromoCA } from './PromoCA';
import { PromoGarantie } from './PromoGarantie';
export class OperateurPromo {
    compagnies: PromoCA[];
    garanties: PromoGarantie[];
    branches: PromoBranche[];
    coupons: CouponPromo[];
}

import { AssureSimulation } from "./assure-simulation";
import { CA } from "./ca";
import { Client } from "./client";
import { Contrat2 } from "./contrat";
import { Reseau } from "./reseau";

export class SimulationClient {
    id: number = 0;
    codepromo: string = "";
    client: Client;
    reseau: Reseau;
    assures: AssureSimulation[];
    contrats: Contrat2[];
    dateSimulation?: Date;   // à supprimer
    numeroDansSession? :  number;    // à supprimer
    origineRoutine? : String ;   // à supprimer
    ca? : CA ;   // à supprimer
	eligiblePeriode?:boolean;    // à supprimer
}
export class SimulationClient2 {
    id: number = 0;
    codepromo: string = "";
    client: Client;
    reseau: Reseau;
    assures: AssureSimulation[];
    contrats: Contrat2[];
}
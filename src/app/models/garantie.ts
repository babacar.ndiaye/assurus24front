import { OffrePromo } from "./OffrePromo";
import { BA } from "./ba";
import { Operation } from "./operation";
import { TypeGarantie } from "./type-garantie";
import { ConditionGenerale } from "./condition-generale";
import { TarifGarantie } from "./tarif-garantie";
import { AssureContrat } from "./assure-contrat";
import { GarantieCA } from "./garantie-ca";
import { TableCoeffGarantie } from "./TableCoeffGarantie";
import { AttributGarantie } from "./AttributGarantie";

export class Garantie {
	id: number ;
	code ?:string ;
	codeExterne :string  = "";
	libelle :string ;
	description :string ;
	taux :number  = 100;
	minimum :number  = 0;
	franchise :number  = 0;
	agemin :number  = 0;
	agemax :number  = 0;
	attributmontant :string ;
	codeFormule: TarifGarantie;
    tablecoeff: TableCoeffGarantie;
    pack: boolean  = false;
	typegarantie: GarantieCA ;
    primepure: any; // montant prime pure
    primecommerciale: any = 0.0; // montant primce commerciale
    montantchargement: any = 0.0; // montant chargement
    primeht: any = 0.0; // montant hors taxe
    montanttaxe: any = 0.0; // montant montant taxe
    primettc: any = 0.0; // montant TTC
    montantTarif: any  = 0; // montant à payer sur la garantie
	assure: AssureContrat ;
    attributs: AttributGarantie[];

}

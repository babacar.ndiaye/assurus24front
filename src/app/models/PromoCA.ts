import { OperateurPromo } from './OperateurPromo';
import { CA } from './ca';
export class PromoCA {
    id: number;
    compagnie: CA;
    promo: OperateurPromo;
}

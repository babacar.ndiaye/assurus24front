import { TypeGarantie } from "./type-garantie";

export class AttributGarantieBA {
	id: number;
    code: string;
    libelle: string;
    type: string;
    taille: number;
    nbdec: number;
    codeListeVal: string;
    ordreAff: number = 0;
    typeGarantie: TypeGarantie;
    datesupp: Date;
}

import { TypeGarantie } from "./type-garantie";

export class PackTypeGarantie {
	id :number;
	libelle :string;
    garantieprincipale: TypeGarantie;
    garantiesecondaire: TypeGarantie;

}

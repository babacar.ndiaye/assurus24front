import { Client } from 'src/app/models/client';
import { PieceJustificatif } from "./piece-justificatif";
import { ControleTechnique } from "./controle-technique";
import { BA } from "./ba";
import { TypeBA, TypeBA2 } from "./type-ba";
import { Proprietaire } from "./proprietaire";
import { AttributAussure } from './attribut-assure';

export class ObjetAssures {
     id: number;
     libelle: string;
     statut: number;
     //contrats: Array<Contrat>;
     typeBA: TypeBA;
     controleTechnique: ControleTechnique;

     //nullable
     proprietaire: Client
     infosValide: Boolean = false;
}
export class ObjetAssures2 {
     id: number;
     intitule: string;
     validationCA: false;
     validationOP: false;
     typeba: TypeBA2;
     numeroTitulaire: string;
     nomTitulaire: string;
     prenomTitulaire: string;
     proprietaire: Client;
     TabAtrributs?: AttributAussure[];
     attributs: AttributAussure[];
     infosValide: boolean;
}

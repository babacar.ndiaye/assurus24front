import { ParamColonneTableCoeff } from "./ParamColonneTableCoeff";

export class TableCoeffGarantie {
    id: number;
    code: string;
    libelle: string;
    description: string;
    donnees: string;
    intervalle: Boolean = false;
    valexact: Boolean = true;
    colonnes: ParamColonneTableCoeff[];
}

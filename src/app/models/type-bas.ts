export class TypeBas {
    id : number ;
    libelle : string;
    description :string;
    slogan: string;
    logo: string;
    parent: TypeBas;
    niveau: number;
}
import { AttributAussureBranche } from './attribut-assure-branche';

export interface IAttributAussure {
    id?: number;
    attribut: AttributAussureBranche;
    valeur: string;
  }
export class AttributAussure implements IAttributAussure {

    constructor(
        public attribut: AttributAussureBranche,
        public valeur: string,
        public id?: number,
      ) {
          //this.attribut = attributSend;
          //this.valeur = valeurSend;
      }
}

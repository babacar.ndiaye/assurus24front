import { AttributAussure } from './attribut-assure';
import { Garantie } from './garantie';
import { TypeGarantie } from "./type-garantie";
import { BA } from "./ba";
import { AttributAussureBranche } from './attribut-assure-branche';

export class TypeBA {
    id : number ;
    nom : string ;
    typeGaranties : Array<TypeGarantie> ;
    bas : Array<BA> ;
    ConditionsDisposition : String ;
    description :String ;


}
export class TypeBA2 {
    id : number;
    libelle : string;
    description: string;
    slogan: string;
    logo: string;
    parent: TypeBA2;
    garanties: Garantie[];
    attributs: AttributAussureBranche[];
    niveau: number;
    datesupp: Date;
    codeFront: string;
    publier: Boolean;

}

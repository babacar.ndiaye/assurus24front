import { Compte } from "./compte";
import { Pays } from "./pays";
import { ViewportScroller } from "@angular/common";
import { Region } from "./region";
import { ListeValeur } from "./liste-valeur";
import { CA } from "./ca";
import { Reseau } from "./reseau";
import { Operateur } from "./operateur";
import { Prestataire } from "./prestataire";
import { Contact } from "./contact";
import { ClientProfessionnel } from "./client-professionnel";

export class ContactClient extends Contact{
	 prenom :  string ;
	 intituleposte :  String ;
	 civilite :  ListeValeur ;
	 dateDeNaissance : Date ;
	 operateur : Operateur;
     ca : CA;
	 reseau : Reseau;
	 prestataire : Prestataire;
	 clientProfessionnel : Array <ClientProfessionnel>;
 	
}

import { Client } from "./client";
import { ContactClient } from "./contact-client";
import { ClientListeNoire } from "./client-liste-noire";

export class ClientProfessionnel extends Client {

	ninea : string;
	registreCommerce :  string;
	siteweb : string;
	logo : string;
	contacts : Array <ContactClient>;
	listenoires : Array <ClientListeNoire>;

}

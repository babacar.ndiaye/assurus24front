import { GarantieCA } from "./garantie-ca";

export class PackGarantie {
	id :number;
	libelle :string;
	garantieprincipale :GarantieCA;
	garantiesecondaire :GarantieCA;

}

import { ParamColonneTableValeur } from "./ParamColonneTableValeur";

export class TableCalculValeur {
    id : number;
    code: string;
    libelle: string;
    description: string;
    donnees: string;
    intervalle: Boolean;
    valexact: Boolean;
    colonnes: ParamColonneTableValeur;
}

import { ListeValeur } from "./liste-valeur";
import { Organisation } from "./organisation";

export class CompteSociete {
    id: number ;
	typecompte: ListeValeur ;
	idcompte: String ;
	observation: String ;
	myOrganisation: Organisation ;
}

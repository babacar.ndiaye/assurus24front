import { Devise } from "./devise";
import { Organisation } from "./organisation";

export class DeviseOrganisation {
    id : number;
    myDevise : Devise;
    myOrganisation : Organisation;
}

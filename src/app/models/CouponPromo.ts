import { OperateurPromo } from "./OperateurPromo";

export class CouponPromo {
    id :number;
	code: string;
	promo: OperateurPromo;
	used: Boolean;
}

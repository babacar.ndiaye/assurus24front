import { TarifGarantie } from "./tarif-garantie";

export class DetailTarif {
    id: number ;
	tarifgarantie: TarifGarantie ;
	numLigne :number ;
	numColonne :number ;
	valeur: string ;
}

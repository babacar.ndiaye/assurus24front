import { Organisation } from "./organisation";
import { Operateur } from "./operateur";
import { CA } from "./ca";
import { Reseau } from "./reseau";
import { Prestataire } from "./prestataire";
import { Profil } from "./profil";
import { Compte } from "./compte";

export class PermissionEntite {
    id : number;
    inclusExclus : String;
    typeObjet : String;
    idObjet : String;
    myOrganisation : Organisation;
    operateur : Operateur;
    ca : CA;
    reseau : Reseau;
    prestataire : Prestataire;
    profil : Profil;
    compte : Compte ;
    

}

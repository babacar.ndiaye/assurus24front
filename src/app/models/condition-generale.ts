import { Garantie } from "./garantie";
import { CA } from "./ca";

export class ConditionGenerale {
    id :  number ;
    
    conditionsSpeciales :Array<String> ;
    garantie : Garantie ;
    
}

export class Document {

	id: number ;

	reference: string ;

	libelle: string ;

	entite: string ;

	entiteId: number ;

	lien: string ;

	observation: string ;
	dateproduction: Date ;
}

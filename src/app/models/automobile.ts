import { ObjetAssures } from "./objet-assures";

export class Automobile extends ObjetAssures {
	public immatriculation: string;
	public numeroCartegrise: string;
	public valTypeVéhicule: string;
	public marqueVehicule: string;
	public appellationComm: string;
	valDate1ereCirculation: number;
	valCarosserie: string;
	valTypeEnergie: string;
	valPuissance: number;
	valCylindree: number;
	valNombrePlace: number;
	valPoidsPV: number;
	mntValeurNeuve: number;
	mntValeurDeclaree: number;
	valRemorque: string;
	valUtilisation: string;
	valUtilisationVehiculeMoto: string;
	valCategorieVehicule: string;
	valTypeCarosserieAuto: string;
	dateVisiteTech: Date;
	constructor() {
		super();
	}
}

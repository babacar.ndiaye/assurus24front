import { ObjetAssures2 } from "./objet-assures";
import { TypeBA2 } from "./type-ba";
import { TypeGarantie } from "./type-garantie";

export class AssureSimulation {

    id: number = 0;
    assure: ObjetAssures2;
    dateEffet: Date;
    dateCloture: Date;
    branche: TypeBA2;
    garanties: TypeGarantie[];
    numeroDansFlotte? : number;  // à supprimer
    montantUniqueObjet? : number ;   // à supprimer
	montantTTCCompagnie? : number;   // à supprimer
    constructor(){
		this.numeroDansFlotte = null ;
	}
}
export class AssureSimulation2 {

    id: number = 0;
    assure: ObjetAssures2;
    dateEffet: Date;
    dateCloture: Date;
    branche: TypeBA2;
    garanties: TypeGarantie[];
}
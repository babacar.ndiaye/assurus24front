import { ListeValeur } from "./liste-valeur";
import { Droit } from "./droit";
export class Profil {
    id : number;
    codeGroupe : String;
    libelleGroupe : String;
    dateCreation : Date;
    dateModif : Date;
    dateSuppression : Date;
    actif : ListeValeur;
    permission : String;
    myDroit : Array<Droit> ;
}

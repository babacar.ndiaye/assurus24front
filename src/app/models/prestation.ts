import { Prestataire } from "./prestataire";
import { TypePrestation } from "./type-prestation";

export class Prestation {
    id :number ;
	nom :string ;
	prix :number ;
	prestataire :Prestataire ;
	typePrestation: TypePrestation ;
}

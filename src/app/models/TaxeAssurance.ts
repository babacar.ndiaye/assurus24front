import { BrancheCa } from './branche-ca';
import { GarantieCA } from "./garantie-ca";
import { ListeValeur } from "./liste-valeur";
import { TableCalculValeur } from "./TableCalculValeur";

export class TaxeAssurance {
    id: number;
    nature: string;
    libelle: string;
    description: string;
    dateDebut: Date;
    dateFin:Date;
    montant:any = 0.0;
    taux:any = 0.0;
    tableCoeff: TableCalculValeur;
    tableMontant: TableCalculValeur;
    baseCalcul: ListeValeur; // PUR, PCOM, PHT, PTTC
    garantie: GarantieCA;
    brancheCA: BrancheCa;
}

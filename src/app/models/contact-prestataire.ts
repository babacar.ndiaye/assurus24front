import { Contact } from "./contact";

export class ContactPrestataire extends Contact{
    logo: string ;
	registreCommerce: string ;
	siteWeb: string ;
}

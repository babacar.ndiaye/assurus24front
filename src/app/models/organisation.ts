import { PermissionEntite } from "./permission-entite";
import { Droit } from "./droit";
import { DeviseOrganisation } from "./devise-organisation";
import { CompteOrganisation } from "./compte-organisation";
import { LangueOrganisation } from "./langue-organisation";

export class Organisation {
    id : number;
    code : String;
    libelle : String;
    tauxCommission : number;
    montantCommission : number;
    myParent : Organisation;
    myChilds : Array <Organisation>;
    myPermissions : Array<PermissionEntite>;
    myDroits : Array<Droit>;
    myLangues : Array<LangueOrganisation>;
    myDevises : Array<DeviseOrganisation>;
    myComptes : Array<CompteOrganisation>

}
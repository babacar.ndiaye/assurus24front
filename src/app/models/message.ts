import { Compte } from "./compte";

export class Message {
    id: number;
	objet : string;
    contenu: string;
    compte : Compte;
    
}

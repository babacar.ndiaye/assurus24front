import { TypeBA, TypeBA2 } from "./type-ba";
import { CA } from "./ca";
export class BrancheCa {
id : number;
code:string;
codeExterne : string;
libelle:string;
description : string;
slogan:string;
logo : string;
typeba : TypeBA2;
ca : CA;
parent : BrancheCa



}

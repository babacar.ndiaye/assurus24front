import { BrancheCa } from './branche-ca';
import { Sinistre } from './sinistre';
import { Client } from "./client";
import { Reseau } from "./reseau";
import { Avenant } from "./avenant";
import { Operation } from "./operation";
import { CA } from "./ca";
import { AssureContrat } from './assure-contrat';

// export class Contrat {
//     id : number;
// 	police :String;
// 	 //format date MM-dd-YYYY   par contre pouur les inputs type date //format date YYYY-MM--dd cf fonction displayDateFormatInput
// 	dateEffet : Date;
// 	dateCloture: Date;
// 	statut : string ;
// 	franchise : number ;
// 	capitauxGarantis : number ;
// 	objetAssures: Array<ObjetAssures>
//     client :Client;
// 	reseau :Reseau;
// 	// avenants :Avenant ;
//     operations :Array<Operation> ;
//     ca :CA ;

// 	}

    export class Contrat2 {
        id: number;
        police: string;
        dateEffet: Date;
        dateCloture: Date;
        etat: number;
        myCA: CA;
        client :Client;
        reseau: Reseau;
        //branche: BrancheCa;
        objetAssures: AssureContrat[];
        avenants: Avenant[];
        sinistres: Sinistre[];
        operations: Operation[];
        validationCA: boolean;
        validationOP: boolean;
        montantBrut: any;
        montant: any;
        montantPromo: any;
        montantFrais: any;
        montantPromoFrais: any;
    }



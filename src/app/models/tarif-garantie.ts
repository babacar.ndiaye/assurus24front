import { ParamColonneTarif } from "./param-colonne-tarif";

export class TarifGarantie {
	code: string ;
	libelle:string ;
	description: string ;
	donnees: string ;
	colonnes :Array<ParamColonneTarif>;
}

import { PromoContrat } from './PromoContrat';

import { BrancheCa } from "./branche-ca";
import { Contrat2 } from "./contrat";
import { Garantie } from "./garantie";
import { ObjetAssures, ObjetAssures2 } from "./objet-assures";
import { TaxeContrat } from "./TaxeContrat";

export class AssureContrat {
    id: number;
	contrat: Contrat2;
	assure: ObjetAssures2;
	dateEffet: Date ;
	dateCloture: Date ;
    brancheCA: BrancheCa;
    primepure:any = 0.; // montant prime pure
    primecommerciale:any = 0.; // montant primce commerciale
    montantchargement:any = 0.; // montant chargement
    primeht:any = 0.; // montant hors taxe
    montanttaxe:any = 0.; // montant montant taxe
    primettc:any = 0.; // montant TTC
    montantTarif:any = 0.; // montant  payer sur la garantie
	garanties: Array<Garantie>;
    taxes: TaxeContrat[];
    promos: PromoContrat[];
}

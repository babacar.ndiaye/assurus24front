import { CA } from "./ca";
import { Client } from "./client";
import { Pays } from "./pays";
import { Prestataire } from "./prestataire";
import { Operateur } from "./operateur";
import { Reseau } from "./reseau";

export class CompteCredit {

    id : number ;
	reference : string ;
	
	pays : Array<Pays> ;
	
	
	ca :CA ;
	
	
	client :Client ;
	
	
	prestataire : Prestataire;
	
	
	operateur :Operateur;
	
	
	reseau :Reseau ;
	
}

import { Menu } from "./menu";


export class SousMenu {
    id : number;
    code : String;
    libelle : String;
    lien : String;
    image : String;
    rang : Number;
    myMenu : Menu;
}

import { Pays } from "./pays";
import { Region } from "./region";
import { Compte } from "./compte";

export class Contact {
    id : number;
    nom :  string ;
    adresse :  string ;
    adresse2 :  string ;
    telephone : string ;
    telephone2 : string ;
    fax : string ;
    email : string ;
    codePostal :string;
    pays : Pays;
    ville : Region ;
    compte : Compte;
}

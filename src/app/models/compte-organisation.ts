import { Compte } from "./compte";
import { ListeValeur } from "./liste-valeur";
import { Organisation } from "./organisation";

export class CompteOrganisation {
    id : number;
    type : ListeValeur;
    myCompte : Compte;
    myOrganisation : Organisation;
}

export class AttributAussureBranche {
    id: number;
    code: string;
    libelle: string;
    type: string;
    taille: number;
    nbdec: number;
    codeListeVal: string;
    affEnResume: Boolean;
    idrecord: Boolean;
    ordreAff: number;
    ordreAffFront: number;
    datesupp: Date;
    placeholder: string;
    typeInput: string;
    affichageEtape: number;
    radioImage: Boolean;
    radioSimple: Boolean;
    selectImage: Boolean;
    selectSimple: Boolean;
    valeurMaxRange: number;
    anneeDeCirculationMin: number;
}

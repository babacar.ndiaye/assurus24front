import { Contrat2 } from "./contrat";

export class Avenant {
    id : number;
    description : string;
    options : string;
    dateAvenant : Date;
    contrat : Contrat2;


}

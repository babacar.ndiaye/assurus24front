import { TableCalculValeur } from './TableCalculValeur';
export class ParamColonneTableValeur {
    id : number;
    tablecalculvaleur: TableCalculValeur;
    libelle: string;
    pivot: string;
    attributpivot: string;
    typedonnee: string;
    taillezone: string;
    listevaleur: string;
}

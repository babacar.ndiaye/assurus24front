import { Client } from "./client";
import { ObjetAssures2 } from "./objet-assures";
import { Garantie } from "./garantie";
import { BA } from "./ba";
import { CA } from "./ca";
import { TypeGarantie } from "./type-garantie";
import { FlotteSimulations } from "./flotte-simulations";
import { Reseau } from "./reseau";

export class Simulation {
     id : number;
	
	//reference : string;
	
	client : Client  ;
	dateSimulation : Date;
	//relations à ajoutéé
	objet: ObjetAssures2;
	
	//type de garanties pour uniformiser dans le choice option
	typeGaranties : Array<TypeGarantie>
	garanties : Array<Garantie> ;
	dateDebut :Date ;
	dateFin :Date ;
	origineRoutine : String ;
	numeroDansSession :  number ;
	ca : CA ;
	montantTTCCompagnie : number;
	numeroDansFlotte : number;
	eligiblePeriode:boolean;
	flotteSimulations :FlotteSimulations ;
	
	constructor(){
		this.numeroDansFlotte= null ;

	}
	


}

export class Simulation2 {
	id: number;
	reference: string;
	dateSimulation: Date;
	client: Client;
	reseau: Reseau;
	xmlSimulation: string;
}
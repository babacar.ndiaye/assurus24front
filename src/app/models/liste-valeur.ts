export class ListeValeur {
    id : number;
    code : string;
    libelle : string;
    type : string;
    dateSuppression : Date;
    description:string;
}

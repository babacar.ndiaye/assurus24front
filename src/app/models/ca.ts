import { Compte } from "./compte";
import { Reseau } from "./reseau";
import { ListeNoire } from "./liste-noire";
import { BA } from "./ba";
import { CompteCredit } from "./compte-credit";
import { CommunicationCommerciale } from "./communication-commerciale";
import { ListeValeur } from "./liste-valeur";
import { Pays } from "./pays";
import { Region } from "./region";
import { ContactClient } from "./contact-client";
import { BrancheCa } from "./branche-ca";
import { TableCalculValeur } from "./TableCalculValeur";


export class CA {
     id : number ;
	 nom : string;
	 typecompagnie : ListeValeur;
	 ninea : string;
	 registreCommerce : string;
	 adresse : string;
	 telephone : string;
	 email : string;
	 siteweb : string;
	 tauxfraisTransac : number =0;
	 forfaitTransac : number=0;
	 logo : string;
	 adresse2 : string;
	 telephone2 : string;
	 fax : string;
	 codepostal : string;
	 pays : Pays;
	 ville :Region;
     coeffPenalitefract: number = 0.0;
     tableCoeff: TableCalculValeur;
     tableMontant: TableCalculValeur;
	 actif : ListeValeur;
	 contacts : Array<ContactClient>;
	 compteutilisateurs: Array<Compte>;
	 communicationCommerciales :Array<CommunicationCommerciale>;
	 branches : Array <BrancheCa>
}

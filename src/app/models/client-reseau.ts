import { Client } from "./client";
import { Reseau } from "./reseau";

export class ClientReseau {
    id: number ;
	client: Client ;
	reseau: Reseau ;
}

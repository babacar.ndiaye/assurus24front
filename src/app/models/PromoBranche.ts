import { TypeBA2 } from './type-ba';
import { OperateurPromo } from './OperateurPromo';
export class PromoBranche {
    id: number;
    branche: TypeBA2;
    promo: OperateurPromo;
}

import { Organisation } from "./organisation";
import { Langue } from "./langue";

export class LangueOrganisation {
    id : number;
    myLangue : Langue;
    myOrganisation : Organisation;
}


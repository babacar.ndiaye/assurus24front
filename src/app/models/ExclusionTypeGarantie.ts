import { TypeGarantie } from "./type-garantie";

export class ExclusionTypeGarantie {
	id :number;
	libelle :string;
	garantie1: TypeGarantie;
    garantie2: TypeGarantie;

}

import { TypeBA } from "./type-ba";
import { AttributGarantieBA } from "./AttributGarantieBA";
import { ExclusionTypeGarantie } from "./ExclusionTypeGarantie";

export class TypeGarantie {
    id : number;
    code: string;
    libelle :string;
    description:string;
    typeBA : TypeBA;
    datesupp: Date;
    slogan: string;
    obligatoire: Boolean = false;
    pack: Boolean = false;
    attributs: AttributGarantieBA[];
    exclusions: ExclusionTypeGarantie[];
    inPack: Boolean = false;//variable tempo pour traitement pack à enlever lors de l'envoie en base
}

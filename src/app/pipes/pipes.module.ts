import { NgModule } from "@angular/core";
import { FaqPipe } from "./faq.pipe";


@NgModule({
  declarations: [FaqPipe],
  imports: [],
  exports: [FaqPipe],
})
export class PipesModule {}

import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'faq'
})
export class FaqPipe implements PipeTransform {

    transform(tabFaq: any, mot: any, selected: any): any {
        if (mot) {
            tabFaq = tabFaq.filter(
            (a) => {
                return a.questions.findIndex(questionObj =>
                    questionObj.libelle.toLowerCase().indexOf(mot.toLowerCase()) != -1 ) > -1
            }
              //a.libelle.toLowerCase().indexOf(mot.toLowerCase()) != -1
            //   a.client.prenom.toLowerCase().indexOf(mot.toLowerCase()) != -1 ||
            //   a.client.tel.toLowerCase().indexOf(mot.toLowerCase()) != -1 ||
            //   a.etat.toLowerCase().indexOf(mot.toLowerCase()) != -1 ||
            //   a.tissu.toLowerCase().indexOf(mot.toLowerCase()) != -1 ||
            //   a.couleur.toLowerCase().indexOf(mot.toLowerCase()) != -1 ||
            //   a.modele.toLowerCase().indexOf(mot.toLowerCase()) != -1
          );
          // console.log(tabcom);
        }

        // if (selected) {
        //   tabFaq = tabFaq.filter((a) => a.etat == selected);
        // }
        return tabFaq;
      }

}

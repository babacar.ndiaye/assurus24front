import { ClientParticulier } from './models/client-particulier';
import { Client } from './models/client';
import { Component, OnInit } from '@angular/core';

import { Platform, NavController, MenuController } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';

import { Pages } from './interfaces/pages';
import { TranslateService } from '@ngx-translate/core';
import { Globalization } from '@ionic-native/globalization/ngx';
import {SessionCompte } from 'src/environments/environment';
import { ClientProfessionnel } from './models/client-professionnel';
import { ClientReseau } from './models/client-reseau';
import { UtilsService } from './services/utils.service';
@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {

  public appPages: Array<Pages>;

  //Pour la gestion de la langue en affichant les drapeaux
  public flags = [
    { name: 'Français', image: '../assets/flags/fr.svg' },
    { name: 'English', image: '../assets/flags/gb.svg' },
  ]
  public flag: any;
  public title: string;
  public title_2: string;
  public description: string;
  public name: string;
  public language: string;

  //Variable pour récuperer l'utilisateur connecter
  userPaConnected: ClientParticulier;
  userProConnected: ClientProfessionnel;
  ClientReseau: ClientReseau;
  //variable pour verifier
  isConnected = false;
  //variable pour verifier le type de client
  typeClient: string = '';
  constructor(
    private platform: Platform,
    private splashScreen: SplashScreen,
    private statusBar: StatusBar,
    public navCtrl: NavController,
    private globalization: Globalization,
    private _translate: TranslateService,
    private menu: MenuController,
    private utilsService: UtilsService,

  ) {
    this.appPages = [
      {
        title: 'Accueil',
        url: '/epocno',
        direct: 'root',
        icon: 'home'
      },
      {
        title: 'Mon compte',
        url: '/myprofile',
        direct: 'root',
        icon: 'person'
      },
      {
        title: 'Mes assurances',
        url: '/mesassurances',
        direct: 'root',
        icon: 'folder'
      },
      {
        title: 'Mes simulations',
        url: '/gestion-mes-simulations',
        direct: 'root',
        icon: 'flask'
      },
      {
        title: 'Promotions',
        url: '/home-results',
        direct: 'root',
        icon: 'star'
      },
      {
        title: 'FAQ',
        url: '/faq',
        direct: 'root',
        icon: 'help'
      },
      {
        title: 'A Propos',
        url: '/about',
        direct: 'forward',
        icon: 'information-circle-outline'
      },
      {
        title: 'Accueil commercial',
        url: '/accueil-commercial',
        direct: 'forward',
        icon: 'home'
      },
      {
        title: 'Mes Clients',
        url: '/mes-clients',
        direct: 'forward',
        icon: 'contacts'
      },
      {
        title: 'Mon compte credit',
        url: '/moncomptecredit',
        direct: 'forward',
        icon: 'wallet'
      },
      {
        title: 'App Settings',
        url: '/settings',
        direct: 'forward',
        icon: 'cog'
      }
    ];

    this.initializeApp();
    this.getDeviceLanguage();
    this.getUserConnectedInfos();
  }
    ngOnInit(): void {
        this.flag = this.flags[0];
    }

  initializeApp() {
    this.platform.ready().then(() => {
      this.statusBar.styleDefault();
      this.splashScreen.hide();
    }).catch(() => { });
  }

  public changeLang(flag) {
    this.flag = flag;
    console.log(this.flag.name)
    this.changeLanguage(this.flag.name);
  }
  /**GESTION LOGIQUE TRANSLATION DES LANGUES */
  ionViewDidEnter(): void {
    this.getDeviceLanguage();
    // this.getDefaultLanguageBabs();    //this.language =
  }
  //   ionViewDidLoad(){
  //     alert('Babs')
  //   }

  _initialiseTranslation(): void {
    this._translate.get('TITLE').subscribe((res: string) => {
      this.title = res;
    });
    this._translate.get('description').subscribe((res: string) => {
      this.description = res;
    });
    this._translate.get('TITLE_2', { value: 'Babs' }).subscribe((res: string) => {
      this.title_2 = res;
    });
    this._translate.get('data.name', { name_value: 'Babs Idev' }).subscribe((res: string) => {
      this.name = res;
    });

  }

  public changeLanguage(langueChoosen: string): void {
      console.log("langueChoosen",langueChoosen)
      if(langueChoosen == 'Français'){
        this.language = 'fr-FR';
      }else{
        this.language = 'en-US';
      }
    this._translateLanguage();
  }

  _translateLanguage(): void {
    this._translate.use(this.language);
    this._initialiseTranslation();
  }

  _initTranslate(language) {
    // Set the default language for translation strings, and the current language.
    this._translate.setDefaultLang('fr-FR');
    if (language) {
      this.language = language;
    }
    else {
      // Set your language here
      this.language = 'fr-FR';
    }
    this._translateLanguage();
  }

  getDefaultLanguageBabs() {
    let language = this._translate.getBrowserLang();
    this._translate.setDefaultLang(language);
    //alert('In')
    console.log('Naviagateur langage babs', language)
    return language;
  }


  getDeviceLanguage() {
    // if (window.Intl && typeof window.Intl === 'object') {
    //     console.log('Naviagateur langage', navigator.language)
    //   this._initTranslate(navigator.language);
    // }
    // else {
    //   this.globalization.getPreferredLanguage()
    //     .then(res => {
    //       this._initTranslate(res.value)
    //     })
    //     .catch(e => {console.log(e);});
    // }
    if (!this.platform.is('android') || !this.platform.is('ios')) {
      //alert('Navigateur')
      console.log('Navigateur langage', navigator.language)
      this._initTranslate(navigator.language);
    } else {
      this.globalization.getPreferredLanguage()
        .then(res => {
          this._initTranslate(res.value)
        })
        .catch(e => { console.log(e); });
    }
  }
  /**END TRANSLATION */

  goToEditProgile() {
    this.navCtrl.navigateForward('edit-profile');
  }

  /**
   *  * Cette fonction permet de récupérer les infos
   *  * de l'utilisateur à partir de la session
   */
  getUserConnectedInfos(){
    //alert('On Founction')
      if(sessionStorage.getItem(SessionCompte)){
          this.isConnected = true;
        //alert('inside if')
          // Récupérer des données depuis sessionStorage
          let data =JSON.parse(sessionStorage.getItem(SessionCompte));
          //console.log('Data User', data);
          console.log("Type client", data.myProfil.codeGroupe);
          this.typeClient = data.myProfil.codeGroupe;
          if(this.typeClient == 'userClientParticulier'){
            this.userPaConnected= data.client;
            console.log('Connected User Part', this.userPaConnected);
          }else if(this.typeClient == 'userReseau'){
            this.ClientReseau = data;
            console.log('Connected User ClientReseau', this.ClientReseau);
          }
          else{
            this.userProConnected = data.client;
            console.log('Connected User Pro', this.userProConnected);
          }
            // if(this.userConnected){
            //     console.log('Connected User', this.userConnected);
            //     console.log('Nom', this.userConnected.nom);
            //     console.log('Prenom', this.userConnected.prenom);
            //     console.log('Adresse', this.userConnected.adresse);
            //     //console.log('Pays', this.userConnected.pays.nom);
            // }
      }else{
        //alert('Not connected');
        this.isConnected = false;
        //this.menu.enable(false);
      }
  }
  /**
   *  * Cette fonction permet de détecter
   *  * que le menu est ouvert pour charger
   *  * les informations du client connecté
   */
  onMenuOpen() {
    this.menu.enable(true, 'assurus24Menu');
    this.menu.open('assurus24Menu');
    //alert('Menu ouvert')
    this.getUserConnectedInfos()
  }

  logout() {
    this.utilsService.initialiserSessionRoutine().then(()=> {
            sessionStorage.removeItem(SessionCompte)
            this.navCtrl.navigateRoot('/login');
        }
    );
  }

}

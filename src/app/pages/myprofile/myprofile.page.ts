import { Component, OnInit } from '@angular/core';
import { MenuController, ToastController, AlertController, LoadingController, NavController, ModalController, Platform } from '@ionic/angular';
import { ContactClient } from 'src/app/models/contact-client';
import { CNI, CONTACTSESSION, typeRoutineProfil, routineUpdateProfil, routineUpdateProfilPro, LOGIN, IDINSCRIPTION, LIENLOGIN, CLIENTPARTICULIERSESSION, SessionCompte, SessionCompteUpdate, CLIENTPROFESSIONNELSESSION } from 'src/environments/environment';



import { Compte } from 'src/app/models/compte';
import { CompteService } from 'src/app/services/compte.service';
import { UtilsService } from 'src/app/services/utils.service';
import { ClientParticulier } from 'src/app/models/client-particulier';
import { Globalization } from '@ionic-native/globalization/ngx';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-myprofile',
  templateUrl: './myprofile.page.html',
  styleUrls: ['./myprofile.page.scss'],
})
export class MyprofilePage implements OnInit {
  language: string;
  compte : Compte ;
  //profil   à tirer de la table des contrats profil reseau ou client
  profil = "profilVendeurs  " ;
  contactParam="contactParam"
  identificationParam="identificationParam"
  uploadParam="uploadParam"
  constructor(  public menuCtrl: MenuController,
    public toastCtrl: ToastController,
    public alertCtrl: AlertController,
    public serviceCompte : CompteService,
    public loadingCtrl: LoadingController,
    public navCtrl :NavController,
    private platform: Platform,
    private globalization: Globalization,
    private _translate: TranslateService,
    private modalController : ModalController,
    private utilsService :UtilsService ) {
      this.compte = new Compte() ;
    }

  ionViewWillEnter(){
    this.menuCtrl.enable(true);
    this.recupererCompte() ;
  }
  ngOnInit() {
    this.menuCtrl.enable(true);
    this.recupererCompte() ;
  }
  //apres on va recuperer contact au lieu de compte
  recupererCompte(){
    this.compte = JSON.parse(window.sessionStorage.getItem(SessionCompte)) ;
    if(this.compte == null)
    {
      let message ="Oups la session a  expiré veuillez vous reconnecter" ;
    this.utilsService.alertNotification(message) ;
    this.navCtrl.navigateRoot(LIENLOGIN) ;
    }

    }


     formulaireContact(){
            this.formulaireModification(this.contactParam);
     }
     formulaireIdentification(){
      this.formulaireModification(this.identificationParam);
     }
     formulaireUploadDoc(){
     this.formulaireModification(this.uploadParam)
     }
    async formulaireModification(page) {

        let compte = JSON.parse(sessionStorage.getItem(SessionCompte))
        console.log(compte)
        //console.log('Baba')
        let profil=compte.myProfil.id
        if (profil == 3){
          sessionStorage.setItem(CLIENTPARTICULIERSESSION,JSON.stringify(this.compte.client))
          sessionStorage.setItem(LOGIN,JSON.stringify(compte.telephone))
          sessionStorage.setItem(typeRoutineProfil,JSON.stringify(routineUpdateProfil))
        }
        if (profil == 4){
          sessionStorage.setItem(CLIENTPROFESSIONNELSESSION,JSON.stringify(this.compte.client))
          sessionStorage.setItem(LOGIN,JSON.stringify(compte.telephone))
          sessionStorage.setItem(typeRoutineProfil,JSON.stringify(routineUpdateProfilPro))
        }
         console.log(compte)
         switch(page)
         {
           case this.contactParam :
              this.navCtrl.navigateForward("/contact")
           break ;
           case this.identificationParam:
              this.navCtrl.navigateForward("/identification")
           break;
           case this.uploadParam :
              window.sessionStorage.setItem(IDINSCRIPTION,String(compte.client.id)) ;
              this.navCtrl.navigateForward("/uploaddoc")
           break ;

         }

        }



    /**GESTION LOGIQUE TRANSLATION DES LANGUES */
    ionViewDidEnter(): void {
      this._translateLanguage()
    }
    ionViewDidLoad() {
      this._translateLanguage();
    }

    public changeLanguage(): void {
      this._translateLanguage();
    }

    _translateLanguage(): void {
      this._translate.use(this.language);
    }
    _initTranslate(language) {
      // Set the default language for translation strings, and the current language.
      this._translate.setDefaultLang('fr-FR');
      if (language) {
        this.language = language;
      }
      else {
        // Set your language here
        this.language = 'fr-FR';
      }
      this._translateLanguage();
    }

    getDeviceLanguage() {
      if (!this.platform.is('android') || !this.platform.is('ios')) {
        //alert('Navigateur')
        this._initTranslate(navigator.language);
      } else {
        this.globalization.getPreferredLanguage()
          .then(res => {
            this._initTranslate(res.value)
          })
          .catch(e => { console.log(e); });
      }
    }
    /**END TRANSLATION */







}

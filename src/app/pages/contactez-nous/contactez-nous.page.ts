import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Globalization } from '@ionic-native/globalization/ngx';
import { Platform } from '@ionic/angular';
import { TranslateService } from '@ngx-translate/core';
import { UtilsService } from 'src/app/services/utils.service';

@Component({
  selector: 'app-contactez-nous',
  templateUrl: './contactez-nous.page.html',
  styleUrls: ['./contactez-nous.page.scss'],
})
export class ContactezNousPage implements OnInit {

  contacter: FormGroup;
  language: string;
  formInvalid: boolean = false;
  constructor(
    private formBuilder: FormBuilder,
    private platform: Platform,
    private globalization: Globalization,
    private _translate: TranslateService
    ) { }

  ngOnInit() {
    this.onInitForm()
  }
  onInitForm() {
    this.contacter = this.formBuilder.group({
      nom: ['', [Validators.required]],
      prenom: ['', [Validators.required]],
      email: ['', [Validators.required, Validators.email]],
      telephone: ['', [Validators.required, Validators.pattern('([0-9]){9}')]],
      message: ['', [Validators.required]]
    });
  }

  onSubmitForm() {
    if (!this.contacter.valid) {
      // this.utilsService.alertNotification(
      //   'tous les champs ne sont pas remplis correctement'
      // );
      this.formInvalid = true;
    } else {
      console.log(this.contacter.value);
    }

  }


  /**GESTION LOGIQUE TRANSLATION DES LANGUES */
  ionViewDidEnter(): void {
    this._translateLanguage()
  }
  ionViewDidLoad() {
    this._translateLanguage();
  }

  changeLanguage(): void {
    this._translateLanguage();
  }

  _translateLanguage(): void {
    this._translate.use(this.language);
  }
  _initTranslate(language) {
    // Set the default language for translation strings, and the current language.
    this._translate.setDefaultLang('fr-FR');
    if (language) {
      this.language = language;
    }
    else {
      // Set your language here
      this.language = 'fr-FR';
    }
    this._translateLanguage();
  }

  getDeviceLanguage() {
    if (!this.platform.is('android') || !this.platform.is('ios')) {
      //alert('Navigateur')
      this._initTranslate(navigator.language);
    } else {
      this.globalization.getPreferredLanguage()
        .then(res => {
          this._initTranslate(res.value)
        })
        .catch(e => { console.log(e); });
    }
  }
  /**END TRANSLATION */
}

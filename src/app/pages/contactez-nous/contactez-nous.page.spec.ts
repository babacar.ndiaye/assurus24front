import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ContactezNousPage } from './contactez-nous.page';

describe('ContactezNousPage', () => {
  let component: ContactezNousPage;
  let fixture: ComponentFixture<ContactezNousPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ContactezNousPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ContactezNousPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MesServicesPage } from './mes-services.page';

describe('MesServicesPage', () => {
  let component: MesServicesPage;
  let fixture: ComponentFixture<MesServicesPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MesServicesPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MesServicesPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

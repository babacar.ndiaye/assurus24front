import { Component, OnInit, ViewChild } from '@angular/core';
import { ServiceConstat, statutDemandeEnCoursDeTraitement, OBJETSESSION, statutDemandeAffectee, statutDemandePriseEnCharge, statutDemandeCloturee, PrestationServiceSession, LIENLOGIN, LIENMONSERVICE } from 'src/environments/environment';
import { PrestationService } from 'src/app/models/prestationService';
import { NavController, LoadingController, IonSlides } from '@ionic/angular';

import { UtilsService } from 'src/app/services/utils.service';

@Component({
  selector: 'app-mes-services',
  templateUrl: './mes-services.page.html',
  styleUrls: ['./mes-services.page.scss'],
})
export class MesServicesPage implements OnInit {
 //teste le slider

 services ;
 

 critereFiltre ;
 valCherche ;

 filtreByType="filtreByType"
 filtreByStatut="filtraByStatut"
 servicesWhithoutFilter: any;

date :Date ;


statutDemandeEnCoursDeTraitementLoc =statutDemandeEnCoursDeTraitement
    
statutDemandeAffecteeLoc = statutDemandeAffectee
statutDemandePriseEnChargeLoc = statutDemandePriseEnCharge
statutDemandeClotureeLoc = statutDemandeCloturee                                   

 constructor(private loadingCtrl: LoadingController,
   private navCtrl : NavController,
   public utilsService :UtilsService) {
     this.utilsService.getDate().subscribe(
       dateFormatMobile => {this.date= new Date(dateFormatMobile.date)},
       error =>{
         let message ="Oups le serveur est introuvable verfiez votre connexion et recommencez svp" ;
         this.utilsService.alertNotification(message) ;
         this.navCtrl.navigateRoot(LIENLOGIN) ;
       }) ;
  // this.getServices() ;
 
  }
  async ionViewWillEnter(){
  
  await this.utilsService.getDate().subscribe(
     dateFormatMobile => {this.date= new Date(dateFormatMobile.date)},
     error =>{
       let message ="Oups le serveur est introuvable verfiez votre connexion et recommencez svp" ;
       this.utilsService.alertNotification(message) ;
       this.navCtrl.navigateRoot(LIENLOGIN) ;
     }) ;
  
     const loader = await this.loadingCtrl.create({
       message: '',
     });
   loader.present();
   this.getServices().then(
     ()=>{
      this.services.sort((a,b)=>{
        let res =  this.compareService(a,b)
        return res   
      }
      );
       this.servicesWhithoutFilter = this.services;
    
     }
   )
    ;
   loader.dismiss()
  }

 ngOnInit() {
   
 }
 async getServices(){
  
   let servicesEssais  =[{
    id:1 ,
    nom :"depannage",
    prix : 250000,
    prestataire :null,
    
    typeService : ServiceConstat ,
    statut : statutDemandeEnCoursDeTraitement,
    dateOuvertureDemande : new Date("08-01-2000")
    
    
    
    } ,
    {
      id:2 ,
      nom :"depannage",
      prix : 250000,
      prestataire :null,
      
      typeService : ServiceConstat ,
      statut : statutDemandePriseEnCharge,
      dateOuvertureDemande : new Date("07-01-2000")
      
    } ,
    {
      id:3 ,
      nom :"depannage",
      prix : 250000,
      prestataire :null,
      
      typeService : ServiceConstat ,
      statut : statutDemandeCloturee,
      dateOuvertureDemande : new Date("09-01-2000")
      
   }] ;
   this.services = servicesEssais ;
  
    
     console.log(this.date)
   console.log(this.services) ;
}

voirService(service:PrestationService)
{
  window.sessionStorage.setItem(PrestationServiceSession,JSON.stringify(service)) ;
   this.navCtrl.navigateForward(LIENMONSERVICE) ;
}

  


 onSearchService() {
   if(this.critereFiltre==null)
   {
    this.utilsService.alertNotification("veuillez choisir un critère de recherche")
   }
   else
   {
    this.services=this.servicesWhithoutFilter ;
    const val = this.valCherche;
    console.log("filtre")
    console.log(val)
    console.log(this.critereFiltre)
    if (val && val.trim() !== '') {
      console.log("filtre")
      this.services = this.services.filter(service => {
      
        switch(this.critereFiltre){
          case this.filtreByType :
        
            return service.typeService.toLowerCase().indexOf(val.trim().toLowerCase())>-1 
            
            

          break;
       
          case this.filtreByStatut:
            return service.statut.toLowerCase().indexOf(val.trim().toLowerCase())>-1 
            break;
          default : return false ;
        }
        
      });
    }
    

   }

}
compareService(a,b)
{
  
  let poidsStatutA,poidsStatutB
  let res ;
  
  switch(a.statut)
  {
    case statutDemandeEnCoursDeTraitement: poidsStatutA =1
    
    break ;
    case statutDemandeAffectee:  poidsStatutA =1
    break ;
    case  statutDemandePriseEnCharge: poidsStatutA =1
    break ;
    case statutDemandeCloturee:  poidsStatutA =2
    break ;
  
    default :  poidsStatutA =2 ;
    
  }
  switch(b.statut)
  {
    case statutDemandeEnCoursDeTraitement: poidsStatutB =1
    
    break ;
    case statutDemandeAffectee:  poidsStatutB =1
    break ;
    case  statutDemandePriseEnCharge: poidsStatutB =1
    break ;
    case statutDemandeCloturee:  poidsStatutB =2
    break ;
  
    default :  poidsStatutB =2 ;
    
  }
  res = poidsStatutA-poidsStatutB
  if(res==0) 
  {
    if (a.dateOuvertureDemande>b.dateOuvertureDemande)
    return -1 ;
    else
    if (a.dateOuvertureDemande<b.dateOuvertureDemande)
    return 1
    else
    return 0 ;
    
  }
  console.log(res)
  return res
  
}

 

}


import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { NavController } from '@ionic/angular';

@Component({
  selector: 'app-infocar',
  templateUrl: './infocar.page.html',
  styleUrls: ['./infocar.page.scss'],
})
export class InfocarPage implements OnInit {
  public onInfocarForm: FormGroup;
  public puissancefiscale = 1;


  constructor(private formBuilder: FormBuilder, public navCtrl: NavController) { }


    ngOnInit() {
      this.onInfocarForm = this.formBuilder.group({
        'numimmat': [null, Validators.compose([
          Validators.required
        ])],
        'numcartegrise': [null, Validators.compose([
          Validators.required
        ])],
        'marque': [null, Validators.compose([
          Validators.required
        ])],
        'model': [null, Validators.compose([
          Validators.required
        ])],
        'carburant': [null, Validators.compose([
          Validators.required
        ])],
        'age': [null, Validators.compose([
          Validators.required
        ])],
        'puissancefiscale': [null, Validators.compose([
          Validators.required
        ])],
        'nombredeplace': [null, Validators.compose([
          Validators.required
        ])],
        'valeurdeclarative': [null, Validators.compose([
          Validators.required
        ])],
        'valeurinitiale': [null, Validators.compose([
          Validators.required
        ])],
        'datedevalidite': [null, Validators.compose([
          Validators.required
        ])],
      });
    }
    goToHome() {
      this.navCtrl.navigateRoot('/home-results');
    }
  }



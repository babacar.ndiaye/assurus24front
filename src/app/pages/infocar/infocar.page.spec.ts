import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { InfocarPage } from './infocar.page';

describe('InfocarPage', () => {
  let component: InfocarPage;
  let fixture: ComponentFixture<InfocarPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ InfocarPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InfocarPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

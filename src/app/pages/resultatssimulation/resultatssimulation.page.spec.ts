import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ResultatssimulationPage } from './resultatssimulation.page';

describe('ResultatssimulationPage', () => {
  let component: ResultatssimulationPage;
  let fixture: ComponentFixture<ResultatssimulationPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ResultatssimulationPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ResultatssimulationPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';
import { ComponentsModule } from '../../components/components.module';

import { IonicModule } from '@ionic/angular';

import { AssurinfovePage } from './assurinfove.page';
import {  IonicSelectableModule } from 'ionic-selectable';
import { NgxCurrencyModule } from 'ngx-currency';
import { TranslateLoader, TranslateModule } from '@ngx-translate/core';
import { HttpLoaderFactory } from 'src/app/app.module';
import { HttpClient } from '@angular/common/http';


const routes: Routes = [
  {
    path: '',
    component: AssurinfovePage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    IonicModule,
    TranslateModule.forChild({
      loader: {
        provide: TranslateLoader,
        useFactory: HttpLoaderFactory,
        deps: [HttpClient]
      }
    }),
    NgxCurrencyModule,
    RouterModule.forChild(routes),
    ComponentsModule,
   IonicSelectableModule
  ],
  declarations: [AssurinfovePage]
})
export class AssurinfovePageModule {}

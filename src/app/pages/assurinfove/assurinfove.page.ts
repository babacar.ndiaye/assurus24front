import { Contrat2 } from 'src/app/models/contrat';
import { TypeBA2 } from './../../models/type-ba';
import { ObjetAssuresDto } from "./../../Dto/ObjetAssureDto";
import { element } from "protractor";
import { ObjetAssures2 } from "./../../models/objet-assures";
import { Client } from "./../../models/client";
import {
    LIENLOGIN,
    Modele,
    SessionCompte,
    typeSousBranche,
} from "./../../../environments/environment";
import { Component, OnInit, ViewChild } from "@angular/core";
import {
    IonSlides,
    NavController,
    LoadingController,
    AlertController,
    ModalController,
    Platform,
} from "@ionic/angular";
import { Simulation } from "src/app/models/simulation";
import {
    simulationActifenSession,
    initialeCT,
    initialeCG,
    Marque,
    MARQUES,
    typeRoutineSimulation,
    typeRoutine,
    typeRoutineNewContrat,
    contratActifenSession,
    objetActifenSession,
    typeRoutineNewObject,
    flotte,
    listeSimulationDansFlotte,
    listeSimulationDansFlotteValide,
    statutValidationKo,
    statutValideEnCours,
    constMinNombrePlace,
    constMaxNombrePlace,
    constMinAnnee,
    constMaxAnnee,
    MessageVerificationReseau,
    typeRoutineEditObject,
    MessageErreur,
    OBJETSESSION,
    typeRoutineRenewContrat,
    CONTROLETECHNIQUEPATHNAME,
    downloadControleTechnique,
    CARTEGRISEPATHNAME,
    downloadCarteGrise,
    LIENMESCONTRATS,
    LIENASSURINFOVEVEHICULEFLOTTE,
    LIENECRANRECAPITULATIF,
    editObjet,
    operationActifenSession,
    LIENCHOICEOPTION,
    LIENASSURINFOVE,
} from "src/environments/environment";
import { UtilsService } from "src/app/services/utils.service";
import { Automobile } from "src/app/models/automobile";
import {
    FormGroup,
    FormBuilder,
    Validators,
    FormControl,
} from "@angular/forms";
import { CompteService } from "src/app/services/compte.service";
import { ObjetService } from "src/app/services/objet.service";
import { IonicSelectableComponent } from "ionic-selectable";
import { ControleTechnique } from "src/app/models/controle-technique";
import { Alert } from "selenium-webdriver";
import { ObjetAssures } from "src/app/models/objet-assures";
import { Operation } from "src/app/models/operation";
import { Proprietaire } from "src/app/models/proprietaire";
import { ContactClient } from "src/app/models/contact-client";
import { ModalMarquePageModule } from "../modal-marque/modal-marque.module";
import { ModalMarquePage } from "../modal-marque/modal-marque.page";
import { AuthService } from "src/app/services/auth.service";
import { Compte } from "src/app/models/compte";
import { RoutineContrat } from "src/app/pojos/routine-contrat";
import { Globalization } from "@ionic-native/globalization/ngx";
import { TranslateService } from "@ngx-translate/core";
import { ListeValeurService } from "src/app/services/liste-valeur.service";
import { ListeValeur } from "src/app/models/liste-valeur";
import { TypeBas } from "src/app/models/type-bas";
import { AttributAussure } from "src/app/models/attribut-assure";
import { AttributAussureBranche } from "src/app/models/attribut-assure-branche";
import { UrlTree } from "@angular/router";
import { AssureContratService } from "src/app/services/assure-contrat.service";
import { AttributAussureBrancheDto } from "src/app/Dto/AttributAussureBrancheDto";
import { OperationService } from "src/app/services/operation.service";
import { AssureContrat } from 'src/app/models/assure-contrat';

@Component({
    selector: "app-assurinfove",
    templateUrl: "./assurinfove.page.html",
    styleUrls: ["./assurinfove.page.scss"],
})
/**
 * ! Gerer aussi les champs obligatoires
 * ! masqués en elevant la propriété
 * ! requiered dans le formulaire pour validation
 */
export class AssurinfovePage implements OnInit {
    @ViewChild("civilite") inputCivilite;
    @ViewChild("nom") inputNom;
    @ViewChild("prenom") inputPrenom;
    @ViewChild("portModele") inputModele;
    @ViewChild("essence") inputEssence;
    @ViewChild("myFileInputCg") inputFileCg: any;
    @ViewChild("myFileInputCt") inputFileCt: any;
    @ViewChild(IonSlides) slides: IonSlides;
    valuePuissance: number;
    valueCylindre: number;
    simulationActif: Simulation;
    isFlotte = false;
    contratActif: Contrat2;
    objetActif: ObjetAssures2;
    ob;
    operationActif: Operation;
    infosAutomobile;
    fileCarteGrise: any;
    fileControleTechnique: any;
    onDeclarationForm: FormGroup;
    formInvalide: boolean = false;
    marques: Marque[];
    modeles: Modele[];
    marque: Marque = new Marque();
    modele: Modele = new Modele();
    typeRoutineNewContratLoc = typeRoutineNewContrat;
    typeRoutineSimulationLoc = typeRoutineSimulation;
    typeRoutineRenewContratLoc = typeRoutineRenewContrat;
    typeRoutineNewObject = typeRoutineNewObject;
    typeRoutineEditObject = typeRoutineEditObject;
    listeSimFlotte: Array<Simulation> = [];
    objetsEssais = [];
    objetsAssures: ObjetAssures2[] = [];
    objetSelection;
    objetsClientEnregistre = [];
    oldFile = "oldFile";
    isEditObjet = false;
    gasoil: string;
    essence: string;
    valRemorque: string;
    typeOrigineRoutine: any = JSON.parse(
        window.sessionStorage.getItem(typeRoutine)
    );
    contact: ContactClient;
    //babs
    compte: Compte;
    connectedClient: Client;
    civiliteClient;
    prenomClient;
    nomClient;
    toggleInfos: boolean;
    minNombrePlace: number = constMinNombrePlace;
    maxNombrePlace: number = constMaxNombrePlace;
    minAnnee: number = constMinAnnee;
    maxAnnee: number = constMaxAnnee;
    language: string;
    effacer: string;
    retour: string;
    objetAssure: ObjetAssures2;
    sousBranche;
    TabAttributs = [];
    sousBrancheParentAttributs = [];
    //Parent Attributs
    showImmatriculation: boolean = false;
    showCarteGrise: boolean = false;
    showNIV: boolean = false;
    showTypeVehicule: boolean = false;
    showMarqueVehicule: boolean = false;
    showAppelationCom: boolean = false;
    showMarqueMoto: boolean = false;
    showAppelationComMoto: boolean = false;
    showDateFirstCir: boolean = false;
    showValPuissance: boolean = false;
    showNombrePlace: boolean = false;
    showValeurNeuve: boolean = false;
    showValeurDeclare: boolean = false;
    showValRemorque: boolean = false;
    showValUtilisation: boolean = false;
    showDateVisiteTech: boolean = false;
    showCategorieVehicule: boolean = false;
    //showTypeVehiculeMoto: boolean = false;
    //End Parent Attributs
    //More Attributs
    showCylyndre: boolean = false;
    showTypeEnergie: boolean = false;
    showTypeCarosserie: boolean = false;
    carosseries: ListeValeur[];
    monCarosseries: ListeValeur[]; // variable de teste pour la traduction
    valCarburant;
    ListCheckObjetAssureExist: ObjetAssures2[] = [];
    listeVehiculeMoto: ListeValeur[] = [];
    // controlUtilisation = new FormControl(null, Validators.compose([
    //     Validators.required
    // ]))
    // controlTypeVehiculeMoto = new FormControl(null, Validators.compose([
    //     Validators.required
    // ]))
    // controlPuissanceFiscale = new FormControl(null, Validators.compose([
    //     Validators.required
    // ]))
    // controlTypeCarosserie = new FormControl(null, Validators.compose([
    //     Validators.required
    // ]))
    // controlCylindree = new FormControl(null, Validators.required)
    infosValide: boolean;
    valCarosserie;
    valTypeVehicule;
    TabAttributsParentAndFilsObjets: AttributAussureBrancheDto[] = [];
    //Tableaux pour les select
    energies: ListeValeur[] = [];
    remorque: ListeValeur[] = [];
    categorieMoto: ListeValeur[] = [];
    allListeValeur: ListeValeur[] = [];
    monCategorieMoto: ListeValeur[] = [];
    monListeVehiculeMoto: ListeValeur[] = [];
    TabUtilisationVehiculeMoto: ListeValeur[] = [];
    valModelePassed: string;
    //End
    constructor(
        private navCtrl: NavController,

        private formBuilder: FormBuilder,
        private loadingCtrl: LoadingController,
        private modalController: ModalController,
        private alertCtrl: AlertController,
        public utilsService: UtilsService,
        private platform: Platform,
        private globalization: Globalization,
        private _translate: TranslateService,
        public authService: AuthService,
        public serviceCompte: CompteService,
        private listeValeurService: ListeValeurService,
        private serviceObjet: ObjetService,
        private assurecontrat: AssureContratService,
        private operation: OperationService,
    ) {
        this.infosAutomobile = new Object();
        this.infosAutomobile.controleTechnique = new ControleTechnique();
        this.contact = new ContactClient();
        this.compte = new Compte();
        this.marques = MARQUES;
        //cette fonction permet de charger les infos de la simuluation en session
        //this.chargerInfoSimSession();
    }
    ionViewWillLeave() {
        this.loadingCtrl.dismiss();
        sessionStorage.setItem(editObjet, JSON.stringify(false));
    }

    ngOnInit() {
        this.effacer = this._translate.instant("effacer");
        this.retour = this._translate.instant("retour");
        this.initialisationFormulaire();
    }

    loadAllMarques() {
        this.marques = [];
        this.marques = MARQUES;
    }

    ionViewWillEnter() {
        this.isFlotte = false;
        this.utilsService.recupererCompte(LIENASSURINFOVE).then((compte) => {
            //this.contact = contact//eRREUR Type 'Compte' is not assignable to type 'ContactClient'
            this.compte = compte;
            console.log("compte promise", this.compte);
            if (compte.client.id != 0) {
                console.log("compte", this.compte);
                this.LoadClientSavedObject();
                this.chargerInfoSimSession();
                if (this.typeOrigineRoutine == typeRoutineSimulation) {
                    this.recupererListSimFlotteDeLaSimActive().then(() => {
                        console.log(this.listeSimFlotte);
                    });
                    sessionStorage.setItem("pagePrecedent", "/assurinfove");
                }
            }
        });
    }
    /**
     * * Cette fonction permet
     * * d'initialiser le formulaire
     */
    initialisationFormulaire() {
        this.getNewObjectSousBrancheOnSession();
        console.log("Before form",this.onDeclarationForm);
        let control = new Object();
        this.TabAttributsParentAndFilsObjets.forEach(att => {
            control[att.code] = [null, Validators.required]
            if (att.code == "valDate1ereCirculation")
              control[att.code] = [null, Validators.compose([Validators.required, Validators.min(this.minAnnee), Validators.max(this.maxAnnee)])]
            if (att.code == "valNombrePlace")
              control[att.code] = [null, Validators.compose([Validators.required, Validators.min(this.minNombrePlace), Validators.max(this.maxNombrePlace)])]
            if (att.code == "valCategorieVehicule")
              control[att.code] = [null]
        });
        control["numTitulaire"]= [null, Validators.required];
        control["prenom"]= [null, Validators.compose([Validators.required])];
        control["nom"]= [null, Validators.compose([Validators.required])];
        control["civilite"]= [null, Validators.compose([Validators.required])];
        control["fileCarteGrise"]= [null, Validators.compose([Validators.required])];
        this.onDeclarationForm = this.formBuilder.group(control);
        console.log(this.onDeclarationForm.value);
        console.log("After form",this.onDeclarationForm);
        //Ici on remplit les champs quand le formulaire sera crée


    }
    // initialisationFormulaire() {
    //     this.onDeclarationForm = this.formBuilder.group({

    //         numTitulaire: [null, Validators.compose([Validators.required])],
    //         prenom: [null, Validators.compose([Validators.required])],
    //         nom: [null, Validators.compose([Validators.required])],
    //         civilite: [null, Validators.compose([Validators.required])],
    //         fileCarteGrise: [null, Validators.compose([Validators.required])],

    //         immatriculation: [null, Validators.compose([Validators.required])],
    //         numeroCartegrise: [null, Validators.compose([Validators.required])],
    //         NIV: [null, Validators.compose([Validators.required])],
    //         marqueVehiculeAuto: [
    //             null,
    //             Validators.compose([Validators.required]),
    //         ],
    //         appellationCom: [null, Validators.compose([Validators.required])],
    //         valNombrePlace: [
    //             null,
    //             Validators.compose([
    //                 Validators.required,
    //                 Validators.min(this.minNombrePlace),
    //                 Validators.max(this.maxNombrePlace),
    //             ]),
    //         ],
    //         valTypeEnergie: [null, Validators.compose([Validators.required])],
    //         valRemorque: [null, Validators.compose([Validators.required])],
    //         valPuissance: [null, Validators.compose([Validators.required])],
    //         valCylindree: [null, Validators.compose([Validators.required])],
    //         marqueVehiculeMoto: [
    //             null,
    //             Validators.compose([Validators.required]),
    //         ],
    //         appellationComMoto: [
    //             null,
    //             Validators.compose([Validators.required]),
    //         ],
    //         valCategorieVehicule: [
    //             null
    //         ],
    //         valTypeCarosserieAuto: [
    //             null,
    //             Validators.compose([Validators.required]),
    //         ],
    //         valUtilisationVehiculeMoto: [
    //             null,
    //             Validators.compose([Validators.required]),
    //         ],
    //         valTypeVehicule: [null, Validators.compose([Validators.required])],
    //         valDate1ereCirculation: [
    //             null,
    //             Validators.compose([
    //                 Validators.required,
    //                 Validators.min(this.minAnnee),
    //                 Validators.max(this.maxAnnee),
    //             ]),
    //         ],
    //         mntValeurDeclaree: [
    //             null,
    //             Validators.compose([Validators.required]),
    //         ],
    //         mntValeurNeuve: [null, Validators.compose([Validators.required])],
    //         dateVisiteTech: [null, Validators.compose([Validators.required])],

    //     });
    // }

    /**
     * *Chargement des listes de valeurs
     * *selon le code
     */
    onSelectListeVal(onSelectListeVal: string) {
        return new Promise<ListeValeur[]>((resolve, reject) => {
            this.listeValeurService
                .getAllListeValeur(onSelectListeVal)
                .subscribe(
                    (data) => {
                        resolve(data);
                    },
                    (error) => {
                        reject(error);
                    }
                );
        });
    }

    /**
     * * Cette fonction permet de récuperer
     * * les objets enregistrés du client avec l'api
     * * de les charger au niveau du select
     */
    LoadClientSavedObject() {
        if (this.compte) {
            //let data = this.compte;
            this.connectedClient = this.compte.client;
            console.log("Client connected", this.connectedClient);
            this.inputCivilite.value = this.connectedClient.civilite.code;
            this.civiliteClient = this.connectedClient.civilite.code;
            this.prenomClient = this.connectedClient.prenom;
            this.nomClient = this.connectedClient.nom;
            //A faire pour type routine new contrat
            let typeOrigineRoutine = JSON.parse(
                window.sessionStorage.getItem(typeRoutine)
            );
            /**
             * *Ceci permet de charger les objets assures du client
             * *dans le select pour type routine contrat
             */
            if (typeOrigineRoutine == typeRoutineNewContrat) {
                console.log(
                    "Chargement objets assures client type routine new contrat"
                );
                //console.log("Client connected", this.connectedClient);
                if (this.connectedClient) {
                    this.serviceObjet
                        .getObjetsAssuresByClient(this.connectedClient.id)
                        .subscribe(
                            (data) => {
                                console.log("data", data);
                                //Pour les objets selon la sous-branche ou usage
                                data = data.filter(item => item.typeba.id === this.sousBranche.id)
                                this.objetsEssais = data;
                                this.toggleInfos = false;
                                this.inputCivilite.value = this.connectedClient.civilite.code;
                                this.inputPrenom.value = this.connectedClient.prenom;
                                this.inputNom.value = this.connectedClient.nom;
                                let completedCount = 0;
                                data.forEach((item) => {
                                    this.getAttributsObjetAssure(item.id).then(
                                        (tabAttributs) => {
                                            //console.log("promise getAttributs objets", tabAttributs);
                                            item.attributs = tabAttributs;
                                            if (item.attributs.length > 0) {
                                                item.TabAtrributs = [];
                                                for (let att of item.attributs) {
                                                    if (
                                                        att.attribut.code ==
                                                        "immatriculation" ||
                                                        att.attribut.code ==
                                                        "marqueVehiculeAuto" ||
                                                        att.attribut.code ==
                                                        "appellationCom" ||
                                                        att.attribut.code ==
                                                        "marqueVehiculeMoto" ||
                                                        att.attribut.code ==
                                                        "appellationComMoto" ||
                                                        att.attribut.code ==
                                                        "dateVisiteTech"
                                                    ) {
                                                        item.TabAtrributs.push(
                                                            att
                                                        );
                                                    }
                                                }
                                            }
                                            completedCount += 1;
                                            console.log(
                                                "completedCount",
                                                completedCount
                                            );

                                            if (completedCount == data.length) {
                                                this.objetsAssures = data;

                                                console.log(
                                                    "List Objet Assure client",
                                                    this.objetsAssures
                                                );
                                            }
                                        }
                                    );
                                });
                            },
                            (error) => {
                                console.log(error);
                            }
                        );
                }
            }
        }
    }

    checkIfObjetHasContrat(objetId) {
        return new Promise<AssureContrat[]>((resolve, reject) => {
      this.assurecontrat
            .getContratByObjetAssure(objetId)
            .subscribe((data: AssureContrat[]) => {

                resolve(data);
            },
            (error) => {
                resolve(error);
            });
    });

    }

    /**
       * *Cette fonction permet de verifier
       * *si un contrat est toujours valide
       * *en vérifiant si la date du jour
       * *est toujours dans l'intervalle entre
       * *la date d'effet et la date de clotûre
       * @param dateEffet
       * @param dateCloture
       */
    checkIfContratObjetIsValid(dateEffet, dateCloture): Boolean {
        var currentDate = new Date().toJSON().slice(0, 10);
        var check = new Date(currentDate);
        console.log(check);
        var from = new Date(dateEffet);
        var to = new Date(dateCloture);
        var check = new Date(currentDate);
        console.log(check > from && check < to);

        return check > from && check < to;
    }

    /**
     * *Cette fonction permet de récupérer les
     * * attributs en fonction de l'objet
     * ! On l'utlise seulement pour routine contrat
     * @param id
     */
    getAttributsObjetAssure(id) {
        let TabAttributs: AttributAussure[] = [];
        let promise = new Promise<AttributAussure[]>((resolve, reject) => {
            this.serviceObjet.getAttributsByObjetAssure(id).subscribe(
                (data) => {
                    //TabAttributs = [];
                    if (data) {
                        //console.log("OperationsContrat", data);
                        TabAttributs = data;
                        //pour résoudre la promesse
                        resolve(TabAttributs);
                    }
                },
                (error) => {
                    //pour rejeter la promesse
                    reject(error);
                }
            );
        });

        return promise;
    }

    /**
     *  * Cette fonction permet de récupérer les infos
     *  * garder en session à travers la sousbranche
     *  * pour initialiser les attributs
     *  * de l'objet objet et rendrent les champs correspondants visible
     */
    getNewObjectSousBrancheOnSession() {
        //alert('On Founction')
        this.TabAttributsParentAndFilsObjets = [];
        if (sessionStorage.getItem(typeSousBranche)) {
            this.sousBranche = JSON.parse(
                sessionStorage.getItem(typeSousBranche)
            );
            console.log("sousBranche", this.sousBranche);

            this.TabAttributs = this.sousBranche.attributs;
            this.sousBrancheParentAttributs = this.sousBranche.parent.attributs;
            console.log("TabFilsAttributs", this.TabAttributs);
            console.log("TabParentsAttributs", this.sousBrancheParentAttributs);

            //Récuperation attributs fils
            this.TabAttributs.forEach((att) => {
                this.TabAttributsParentAndFilsObjets.push(att);
            });
            //Récuperation attributs parent
            this.sousBrancheParentAttributs.forEach((att) => {
                this.TabAttributsParentAndFilsObjets.push(att);
            });
            console.log(
                "TabAttributsParentAndFilsObjets",
                this.TabAttributsParentAndFilsObjets
            );
            //Ici on ordonne la liste des attributs suivant l'odre pour l'affichage au niveau du front
            this.TabAttributsParentAndFilsObjets = this.TabAttributsParentAndFilsObjets.sort(
                (a, b) =>
                    a.ordreAffFront > b.ordreAffFront
                        ? 1
                        : b.ordreAffFront > a.ordreAffFront
                            ? -1
                            : 0
            );
            //console.log("ordered",ordered);
            //Nouveau tableau Attributs pour verification

            this.TabAttributsParentAndFilsObjets.forEach((att) => {
                switch (att.code) {
                    case "immatriculation":
                        this.showImmatriculation = true;
                        break;
                    case "numeroCartegrise":
                        this.showCarteGrise = true;
                        break;
                    case "NIV":
                        this.showNIV = true;
                        break;
                    case "valTypeVehicule":
                        this.showTypeVehicule = true;
                        break;
                    case "marqueVehiculeAuto":
                        this.showMarqueVehicule = true;
                        break;
                    case "appellationCom":
                        this.showAppelationCom = true;
                        break;
                    case "marqueVehiculeMoto":
                        this.showMarqueMoto = true;
                        break;
                    case "appellationComMoto":
                        this.showAppelationComMoto = true;
                        break;
                    case "valDate1ereCirculation":
                        this.showDateFirstCir = true;
                        break;
                    case "valPuissance":
                        this.showValPuissance = true;
                        break;
                    case "valTypeEnergie":
                        this.showTypeEnergie = true;
                        break;
                    case "valCylindree":
                        this.showCylyndre = true;
                        break;
                    case "valNombrePlace":
                        this.showNombrePlace = true;
                        break;
                    case "mntValeurNeuve":
                        this.showValeurNeuve = true;
                        break;
                    case "mntValeurDeclaree":
                        this.showValeurDeclare = true;
                        break;
                    case "valRemorque":
                        this.showValRemorque = true;
                        break;
                    case "valUtilisationVehiculeMoto":
                        this.showValUtilisation = true;
                        break;
                    case "valTypeCarosserieAuto":
                        this.showTypeCarosserie = true;
                        break;
                    case "valCategorieVehicule":
                        this.showCategorieVehicule = true;
                        break;
                    case "dateVisiteTech":
                        this.showDateVisiteTech = true;
                        break;
                    default:
                    // this.onDeclarationForm.removeControl('cylindre');
                    // this.onDeclarationForm.removeControl('typeVehiculeMoto');
                }
            });
        }
    }

    /**
     * Pour gerer les range
     * puissance & cylindre
     */
    onChangeRange(e, typeVehicule: string) {
        if (typeVehicule == "valPuissance")
            this.valuePuissance = e.detail.value;
        if (typeVehicule == "valCylindree") this.valueCylindre = e.detail.value;
    }

    /**
     *  * Cette fonction permet de récupérer les infos
     *  * de l'objet garder en session pour
     * * initialiser le formulaire
     */
    getObjectOnSessionForUpdate() {
        //alert('On Founction')
        this.initialisationFormulaire();
        this.loadAllMarques();
        let ObjetAssureDto: ObjetAssuresDto;
        ObjetAssureDto = new ObjetAssuresDto();
        if (sessionStorage.getItem(OBJETSESSION)) {
            this.objetAssure = JSON.parse(sessionStorage.getItem(OBJETSESSION));
            console.log(
                "OBJETSESSION",
                JSON.parse(sessionStorage.getItem(OBJETSESSION))
            );

            //Verification parent

            //if (this.objetAssure) {

            let attributs: AttributAussure[];
            // attributs = this.objetAssure.attributs;
            // delete this.objetAssure.attributs;

            ObjetAssureDto.attributs = new Array(
                this.objetAssure.attributs.length
            );
            let count = 0;
            this.objetAssure.attributs.forEach((element: any, index) => {
                count += 1;
                console.log(count);

                ObjetAssureDto.id = this.objetAssure.id;
                ObjetAssureDto.intitule = this.objetAssure.intitule;
                ObjetAssureDto.validationCA = this.objetAssure.validationCA;
                ObjetAssureDto.validationOP = this.objetAssure.validationOP;
                ObjetAssureDto.typeba = this.objetAssure.typeba;
                ObjetAssureDto.numeroTitulaire = this.objetAssure.numeroTitulaire;
                ObjetAssureDto.nomTitulaire = this.objetAssure.nomTitulaire;
                ObjetAssureDto.prenomTitulaire = this.objetAssure.prenomTitulaire;
                ObjetAssureDto.proprietaire = this.objetAssure.proprietaire;
                ObjetAssureDto.infosValide = this.objetAssure.infosValide;
                ObjetAssureDto.TabAtrributs = this.objetAssure.TabAtrributs;
                ObjetAssureDto.attributs[index] = element;
                let listeValeurDto: ListeValeur[];
                if (element.attribut.codeListeVal) {
                    switch (element.attribut.codeListeVal) {
                        case element.attribut.codeListeVal:
                            this.onSelectListeVal(
                                element.attribut.codeListeVal
                            ).then((data) => {
                                element.attribut["listevaleur"] = data;
                                ObjetAssureDto.attributs[index] = element;
                            });
                            break;
                    }
                }
            });
            //this.TabAttributsParentAndFilsObjets = [];
            if (count == this.objetAssure.attributs.length) {
                let countPush = 0;

                ObjetAssureDto.attributs.forEach((item) => {
                    countPush += 1;
                    console.log("countPush", countPush);
                    if (item.attribut.code == "valTypeCarosserieAuto"
                        || item.attribut.code == "valCategorieVehicule"
                        || item.attribut.code == "valUtilisationVehiculeMoto"
                        || item.attribut.code == "valTypeVehicule")
                        item.valeur = item.valeur.substr(8);
                    this.TabAttributsParentAndFilsObjets.push(item.attribut);
                })
                if (countPush == ObjetAssureDto.attributs.length) {

                    this.initForm(this.objetAssure);

                    console.log("ObjetAssureDto", ObjetAssureDto);
                    console.log("TabAttributsParentAndFilsObjets", this.TabAttributsParentAndFilsObjets);
                }
            }
            //this.initialisationFormulaire();

            //console.log(this.onDeclarationForm.value);
            //}
            //console.log("sousBrancheParentAttributs", this.sousBrancheParentAttributs);
        }
    }

    /**
     * * Cette permet de cherger les informations de la
     * * simulation garder en session
     */

    chargerInfoSimSession() {
        this.initialisationFormulaire();
        let typeOrigineRoutine = JSON.parse(
            window.sessionStorage.getItem(typeRoutine)
        );
        this.isFlotte = JSON.parse(sessionStorage.getItem(flotte));
        switch (typeOrigineRoutine) {
            case typeRoutineSimulation:
                //Ceci permet d'initialiser les attributs parents
                this.getNewObjectSousBrancheOnSession();
                console.log("type simulation ");
                this.simulationActif = JSON.parse(
                    sessionStorage.getItem(simulationActifenSession)
                );
                if (
                    sessionStorage.getItem("pagePrecedent") !=
                    "/assurinfove-vehicule-flotte"
                ) {
                    if (!this.isFlotte) {
                        this.listeSimFlotte = JSON.parse(
                            sessionStorage.getItem(listeSimulationDansFlotte)
                        );
                        this.simulationActif = this.listeSimFlotte[0];
                    } else {
                        this.listeSimFlotte = JSON.parse(
                            sessionStorage.getItem(listeSimulationDansFlotte)
                        );
                        this.simulationActif = this.listeSimFlotte[
                            this.simulationActif.numeroDansFlotte - 1
                        ];
                    }
                }
                if (this.simulationActif == null) {
                    this.utilsService.alertNotification(
                        "veuillez lancer d'abord lancer  une simulation"
                    );
                    this.navCtrl.navigateRoot("/sim-decla-objet");
                }

                console.log("simulation active", this.simulationActif.objet);
                this.initForm(this.simulationActif.objet);
                if (this.infosAutomobile.carburant == "gasoil")
                    this.gasoil = this.infosAutomobile.carburant;
                if (this.infosAutomobile.carburant == "essence")
                    this.essence = this.infosAutomobile.carburant;
                if (this.infosAutomobile.controleTechnique == null)
                    this.infosAutomobile.controleTechnique = new ControleTechnique();

                this.infosAutomobile.infosValide = false;
                //ici on récupère l'objet de la simulation
                this.objetSelection = this.infosAutomobile;

                break;
            case typeRoutineRenewContrat:
            case typeRoutineNewContrat:
                console.log("type contrat");
                this.objetSelection = null;
                //Ceci permet d'initialiser les attributs parents
                this.getNewObjectSousBrancheOnSession();
                this.contratActif = JSON.parse(
                    window.sessionStorage.getItem(contratActifenSession)
                );
                this.objetActif = JSON.parse(
                    window.sessionStorage.getItem(objetActifenSession)
                );
                console.log("Contratactif", this.contratActif);
                if (this.contratActif == null) {
                    this.contratActif = new Contrat2();
                    //alert('Hello');
                    //this.objetActif = new Automobile();
                    //this.infosAutomobile = new Automobile();
                } else {
                    //alert('Nzw');
                    if (this.objetActif != null) {
                        //this.infosAutomobile = this.objetActif;
                    } else {
                        console.log("tableau nul");
                        //this.infosAutomobile = new Automobile();
                    }
                }

                break;
            case typeRoutineNewObject:
                //old one
                /*this.infosAutomobile = new Automobile();
        this.infosAutomobile.controleTechnique = new ControleTechnique();*/
                this.objetAssure = new ObjetAssures2();
                this.getNewObjectSousBrancheOnSession();
                break;
            case typeRoutineEditObject:
                // this.infosAutomobile = JSON.parse(
                //     sessionStorage.getItem(OBJETSESSION)
                // );
                // if (this.infosAutomobile == null) {
                //     this.utilsService.alertNotification(MessageErreur);
                //     this.navCtrl.navigateForward("/monobjet");
                // }
                // this.infosAutomobile.controleTechnique.dateValidite = new Date(
                //     this.infosAutomobile.controleTechnique.dateValidite
                // );
                this.getObjectOnSessionForUpdate();

                //console.log("Shva", this.showValPuissance)

                break;

            default:
                this.effacerDonnees();
        }
    }
    /**
     * *Cette fonction permet d'enlever les
     * * champs requis en fonction des attributs
     * * de l'objet
     */
    async initialiseValidatorsSpecifique() {
        console.log("initialise");
        this.showImmatriculation
            ? ""
            : this.onDeclarationForm.removeControl("immatriculation");
        this.showCarteGrise
            ? ""
            : this.onDeclarationForm.removeControl("numeroCartegrise");
        this.showNIV ? "" : this.onDeclarationForm.removeControl("NIV");
        this.showMarqueVehicule
            ? ""
            : this.onDeclarationForm.removeControl("marqueVehiculeAuto");
        this.showAppelationCom
            ? ""
            : this.onDeclarationForm.removeControl("appellationCom");
        this.showMarqueMoto
            ? ""
            : this.onDeclarationForm.removeControl("marqueVehiculeMoto");
        this.showAppelationComMoto
            ? ""
            : this.onDeclarationForm.removeControl("appellationComMoto");
        this.showNombrePlace
            ? ""
            : this.onDeclarationForm.removeControl("valNombrePlace");
        this.showTypeVehicule
            ? ""
            : this.onDeclarationForm.removeControl("valTypeVehicule");
        this.showValPuissance
            ? ""
            : this.onDeclarationForm.removeControl("valPuissance");
        this.showCylyndre
            ? ""
            : this.onDeclarationForm.removeControl("valCylindree");
        this.showTypeEnergie
            ? ""
            : this.onDeclarationForm.removeControl("valTypeEnergie");
        this.showTypeCarosserie
            ? ""
            : this.onDeclarationForm.removeControl("valTypeCarosserieAuto");
        this.showValUtilisation
            ? ""
            : this.onDeclarationForm.removeControl(
                "valUtilisationVehiculeMoto"
            );
        // this.showCategorieVehicule
        //     ? ""
        //     : this.onDeclarationForm.removeControl("valCategorieVehicule");
        //this.onDeclarationForm.removeControl("valCategorieVehicule");
    }
    initFormSimulation(objetAssure: ObjetAssures2) {
       objetAssure.attributs = objetAssure.attributs.filter(
            function (el) {
                return el != null;
            }
        );
        console.log("object init sim", objetAssure)
        objetAssure.attributs.forEach((att) => {
            switch (att.attribut.code) {
                case "immatriculation":
                    this.onDeclarationForm
                        .get("immatriculation")
                        .setValue(att.valeur);
                    break;
                case "numeroCartegrise":
                    this.onDeclarationForm
                        .get("numeroCartegrise")
                        .setValue(att.valeur);
                    break;
                case "valTypeVehicule":
                    //this.onDeclarationForm.addControl('typeVehiculeMoto', this.controlTypeVehiculeMoto)
                    this.onDeclarationForm
                        .get("valTypeVehicule")
                        .setValue(att.valeur);
                    break;
                case "valDate1ereCirculation":
                    this.onDeclarationForm
                        .get("valDate1ereCirculation")
                        .setValue(att.valeur);
                    break;
                case "valPuissance":
                    //this.onDeclarationForm.addControl('puissance', this.controlPuissanceFiscale)
                    this.onDeclarationForm
                        .get("valPuissance")
                        .setValue(att.valeur);
                    break;
                case "valTypeEnergie":
                    this.onDeclarationForm
                        .get("valTypeEnergie")
                        .setValue(att.valeur);
                    this.valCarburant = att.valeur;
                    break;
                case "valCarosserie":
                    //this.onDeclarationForm.addControl('typeCarosserie', this.controlTypeCarosserie)
                    this.onDeclarationForm
                        .get("valCarosserie")
                        .setValue(att.valeur);
                    break;
                case "valCylindree":
                    //this.onDeclarationForm.addControl('cylindre', this.controlTypeCarosserie)
                    this.onDeclarationForm
                        .get("valCylindree")
                        .setValue(att.valeur);
                    break;
                case "valNombrePlace":
                    this.onDeclarationForm
                        .get("valNombrePlace")
                        .setValue(att.valeur);
                    break;
                case "mntValeurNeuve":
                    this.onDeclarationForm
                        .get("mntValeurNeuve")
                        .setValue(att.valeur);
                    break;
                case "mntValeurDeclaree":
                    this.onDeclarationForm
                        .get("mntValeurDeclaree")
                        .setValue(att.valeur);
                    break;
                case "valUtilisation":
                    this.onDeclarationForm
                        .get("valUtilisation")
                        .setValue(att.valeur);
                    break;
            }
        });
    }
    async valider() {
        //alert("Clear cylyndre validation")
        //this.onDeclarationForm.get('cylindre').clearValidators();
        // this.onDeclarationForm.updateValueAndValidity({
        //     onlySelf: true
        //     });
        let typeOrigineRoutine = JSON.parse(
            window.sessionStorage.getItem(typeRoutine)
        );
        /**
         *
         * Pour la routine simulation on enlève les contrôle des input qui n'existe pas sur le formulaire
         */
        if (typeOrigineRoutine == typeRoutineSimulation) {
            this.initialiseValidatorsSpecifique().then(() => {
                this.initFormSimulation(this.simulationActif.objet);
                console.log("OK");
            });
        }

        if (
            typeOrigineRoutine == typeRoutineNewObject ||
            typeOrigineRoutine == typeRoutineEditObject ||
            typeOrigineRoutine == typeRoutineNewContrat
        ) {
            this.initialiseValidatorsSpecifique();
        }

        console.log(this.onDeclarationForm.value);
        console.log(this.fileCarteGrise);
        if (this.infosAutomobile.id != null) {
            if (this.onDeclarationForm.get("fileCarteGrise").value == null) {
                console.log("pas de fichier installé");
                this.onDeclarationForm
                    .get("fileCarteGrise")
                    .setValue(this.oldFile);
                this.fileCarteGrise = this.oldFile;
            }
        }
        if (!this.onDeclarationForm.valid) {
            console.log(
                "tous les champs ne sont pas remplis correctement",
                this.onDeclarationForm.value
            );
            this.utilsService.alertNotification(
                "tous les champs ne sont pas remplis correctement"
            );
            this.formInvalide = true;
        } else {
            let typeOrigineRoutine = JSON.parse(
                window.sessionStorage.getItem(typeRoutine)
            );
            if (typeOrigineRoutine == typeRoutineSimulation) {
                // if (
                //     this.objetSelection.matricule !=
                //     this.onDeclarationForm.get("immatriculation").value
                // ) {
                //     console.log("different");
                //     this.objetAssure = new ObjetAssures2();
                // }
                let marqueChoosen: Marque;
                let modeleChoosen: Modele;
                if (this.showMarqueVehicule && this.showAppelationCom) {
                    marqueChoosen = this.onDeclarationForm.get(
                        "marqueVehiculeAuto"
                    ).value;
                    modeleChoosen = this.onDeclarationForm.get("appellationCom")
                        .value;
                } else {
                    marqueChoosen = this.onDeclarationForm.get(
                        "marqueVehiculeMoto"
                    ).value;
                    modeleChoosen = this.onDeclarationForm.get(
                        "appellationComMoto"
                    ).value;
                }

                let carte = new AttributAussureBranche();
                let marque = new AttributAussureBranche();
                let modele = new AttributAussureBranche();
                let niv = new AttributAussureBranche();
                let dateVisiTech = new AttributAussureBranche();
                //this.sousBrancheParentAttributs = this.simulationActif.objet.typeba.parent.attributs;
                this.TabAttributsParentAndFilsObjets.forEach((att) => {
                    if (att.affichageEtape == 4) {
                        switch (att.code) {
                            case "numeroCartegrise":
                                carte = att;
                                break;
                            case "NIV":
                                niv = att;
                                break;
                            case "marqueVehiculeAuto":
                                marque = att;
                                marqueChoosen = this.onDeclarationForm.get(
                                    "marqueVehiculeAuto"
                                ).value;
                                break;
                            case "appellationCom":
                                modele = att;
                                modeleChoosen = this.onDeclarationForm.get(
                                    "appellationCom"
                                ).value;
                                break;
                            case "marqueVehiculeMoto":
                                marque = att;
                                marqueChoosen = this.onDeclarationForm.get(
                                    "marqueVehiculeMoto"
                                ).value;
                                break;
                            case "appellationComMoto":
                                modele = att;
                                modeleChoosen = this.onDeclarationForm.get(
                                    "appellationComMoto"
                                ).value;
                                break;
                            case "dateVisiteTech":
                                dateVisiTech = att;
                                break;
                        }
                    }
                });
                this.simulationActif.objet.attributs.push(
                    // new AttributAussure(
                    //     niv,
                    //     this.onDeclarationForm.get("NIV").value
                    // )
                     this.showNIV
                    ? new AttributAussure(
                        niv,
                        this.onDeclarationForm.get("NIV").value
                    )
                    : null,
                );
                this.simulationActif.objet.attributs.push(
                    new AttributAussure(
                        carte,
                        this.onDeclarationForm.get("numeroCartegrise").value
                    )
                );
                this.simulationActif.objet.attributs.push(
                    new AttributAussure(marque, marqueChoosen.name)
                );
                this.simulationActif.objet.attributs.push(
                    new AttributAussure(modele, modeleChoosen.name)
                );
                this.simulationActif.objet.attributs.push(
                    new AttributAussure(
                        dateVisiTech,
                        this.onDeclarationForm.get("dateVisiteTech").value
                    )
                );
                this.simulationActif.objet.numeroTitulaire = this.onDeclarationForm.get(
                    "numTitulaire"
                ).value;
                this.simulationActif.objet.nomTitulaire = this.compte.client.nom;
                this.simulationActif.objet.prenomTitulaire = this.compte.client.prenom;
                // const loader = await this.loadingCtrl.create({
                //     message: "upload carte grise en cours",
                // });
                // loader.present();
                this.exporterCg().then(async (res) => {
                    if (res == true) {
                        this.simulationActif.objet.infosValide = true;
                        //loader.dismiss();
                        if (this.isFlotte) {
                            this.serviceObjet
                                .checkIfObjetAssureExist(
                                    this.simulationActif.objet.intitule,
                                    this.simulationActif.objet.numeroTitulaire
                                )
                                .subscribe(
                                    async (data) => {
                                        console.log("Data", data);
                                        if (data.length == 1) {
                                            //Objet existe
                                            this.assurecontrat
                                                .getContratByObjetAssure(
                                                    data[0].id
                                                )
                                                .subscribe(async (data) => {
                                                    console.log("Contrat", data);
                                                    let resultat = null;
                                                    this.operation.getOperationByContrat(data[0].id)
                                                        .subscribe((op) => {
                                                            if (op.length >= 1) {
                                                                op.forEach((item) => {
                                                                    let simDateDebut = new Date(this.simulationActif.dateDebut).toLocaleDateString();
                                                                    let simDateFin = new Date(this.simulationActif.dateFin).toLocaleDateString();
                                                                    let contratDateEffet = new Date(item.dateEffet).toLocaleDateString();
                                                                    let contratDateCloture = new Date(item.dateCloture).toLocaleDateString();
                                                                    resultat = this.dateObjetAssureEtDateContrat(this.simulationActif, item);
                                                                    if (resultat.day == 0 && simDateFin == contratDateEffet) {
                                                                        this.utilsService.alertNotification(`Vous avez un contrat en cours qui début le ${contratDateEffet.split("/").reverse().join("/")}`);
                                                                    }
                                                                    if (resultat.day == 0 && simDateDebut == contratDateCloture) {
                                                                        this.utilsService.alertNotification(`Vous avez un contrat en cours qui se termine le ${contratDateCloture} à 23h:59`);
                                                                    }
                                                                    if (resultat == { sec: -1, min: -1, hour: -1, day: -1 }) {
                                                                        this.utilsService.alertNotification(`Vous avez un contrat en cours du ${contratDateEffet} à 00h:00 au ${contratDateCloture} à 23h:59`);
                                                                    }
                                                                    if (resultat == { sec: 1, min: 1, hour: 1, day: 1, }) {
                                                                        this.utilsService.alertNotification(`Vous avez un contrat en cours du ${contratDateEffet} à 00h:00 au ${contratDateCloture} à 23h:59`);
                                                                    }
                                                                    resultat == { sec: 0, min: 0, hour: 0, day: 0 }
                                                                    console.log("simulation", this.simulationActif.dateDebut, this.simulationActif.dateFin);
                                                                    console.log("opération", item.dateEffet, item.dateCloture);
                                                                });
                                                            }

                                                            if (!resultat || (resultat.day < 0 || resultat.day > 0)) {
                                                                this.listeSimFlotte = JSON.parse(window.sessionStorage.getItem(listeSimulationDansFlotteValide));
                                                                for (let i = 0; i < this.listeSimFlotte.length; i++) {
                                                                    if (this.listeSimFlotte[i].numeroDansFlotte === this.simulationActif.numeroDansFlotte)
                                                                        this.listeSimFlotte[i] = this.simulationActif;
                                                                }
                                                                sessionStorage.setItem(listeSimulationDansFlotteValide, JSON.stringify(this.listeSimFlotte));
                                                                this.listeSimFlotte.forEach((element) => {
                                                                    if (!element.objet.infosValide)
                                                                        this.infosValide = false;
                                                                });
                                                                console.log("liste simulation valide", this.listeSimFlotte);
                                                                this.navCtrl.navigateRoot(LIENASSURINFOVEVEHICULEFLOTTE);
                                                                // this.enregistrer()
                                                            }
                                                        });
                                                });
                                        } else {
                                            this.listeSimFlotte = JSON.parse(
                                                window.sessionStorage.getItem(
                                                    listeSimulationDansFlotteValide
                                                )
                                            );

                                            for (
                                                let i = 0;
                                                i < this.listeSimFlotte.length;
                                                i++
                                            ) {
                                                if (
                                                    this.listeSimFlotte[i]
                                                        .numeroDansFlotte ===
                                                    this.simulationActif
                                                        .numeroDansFlotte
                                                )
                                                    this.listeSimFlotte[
                                                        i
                                                    ] = this.simulationActif;
                                            }

                                            sessionStorage.setItem(
                                                listeSimulationDansFlotteValide,
                                                JSON.stringify(
                                                    this.listeSimFlotte
                                                )
                                            );
                                            this.listeSimFlotte.forEach(
                                                (element) => {
                                                    if (
                                                        !element.objet
                                                            .infosValide
                                                    )
                                                        this.infosValide = false;
                                                }
                                            );

                                            console.log(
                                                "liste simulation valide",
                                                this.listeSimFlotte
                                            );
                                            this.navCtrl.navigateRoot(
                                                LIENASSURINFOVEVEHICULEFLOTTE
                                            );
                                            // this.enregistrer()
                                        }
                                    },
                                    (error) => {
                                        console.log(error);
                                        //loader.dismiss();
                                    }
                                );
                        } else {
                            this.serviceObjet
                                .checkIfObjetAssureExist(
                                    this.simulationActif.objet.intitule,
                                    this.simulationActif.objet.numeroTitulaire
                                )
                                .subscribe(
                                    async (data) => {
                                        console.log("Data", data);
                                        if (data.length == 1) {
                                            //Objet existe
                                            this.assurecontrat.getContratByObjetAssure(data[0].id)
                                                .subscribe(async (data) => {
                                                    console.log("Contrat", data);
                                                    this.operation.getOperationByContrat(data[0].id)
                                                        .subscribe(async (op) => {
                                                            let resultat = null;
                                                            if (op.length >= 1) {
                                                                op.forEach((item) => {
                                                                    let simDateDebut = new Date(this.simulationActif.dateDebut).toLocaleDateString();
                                                                    let simDateFin = new Date(this.simulationActif.dateFin).toLocaleDateString();
                                                                    let contratDateEffet = new Date(item.dateEffet).toLocaleDateString();
                                                                    let contratDateCloture = new Date(item.dateCloture).toLocaleDateString();
                                                                    resultat = this.dateObjetAssureEtDateContrat(this.simulationActif, item);
                                                                    if (resultat.day == 0 && simDateFin == contratDateEffet) {
                                                                        this.utilsService.alertNotification(`Vous avez un contrat en cours qui début le ${contratDateEffet.split("/").reverse().join("/")}`);
                                                                    }
                                                                    if (resultat.day == 0 && simDateDebut == contratDateCloture) {
                                                                        this.utilsService.alertNotification(`Vous avez un contrat en cours qui se termine le ${contratDateCloture} à 23h:59`);
                                                                    }
                                                                    if (resultat == { sec: -1, min: -1, hour: -1, day: -1 }) {
                                                                        this.utilsService.alertNotification(`Vous avez un contrat en cours du ${contratDateEffet} à 00h:00 au ${contratDateCloture} à 23h:59`);
                                                                    }
                                                                    if (resultat == { sec: 1, min: 1, hour: 1, day: 1, }) {
                                                                        this.utilsService.alertNotification(`Vous avez un contrat en cours du ${contratDateEffet} à 00h:00 au ${contratDateCloture} à 23h:59`);
                                                                    }
                                                                    resultat == { sec: 0, min: 0, hour: 0, day: 0 }
                                                                    console.log("simulation", this.simulationActif.dateDebut, this.simulationActif.dateFin);
                                                                    console.log("opération", item.dateEffet, item.dateCloture);
                                                                });
                                                            }
                                                            if (!resultat || (resultat.day < 0 || resultat.day > 0)) {
                                                                const loader = await this.loadingCtrl.create({ message: "ajout en cours" });
                                                                loader.present();
                                                                sessionStorage.setItem(
                                                                    simulationActifenSession,
                                                                    JSON.stringify(
                                                                        this
                                                                            .simulationActif
                                                                    )
                                                                );
                                                                this.listeSimFlotte[0] = this.simulationActif;
                                                                sessionStorage.setItem(
                                                                    listeSimulationDansFlotteValide,
                                                                    JSON.stringify(
                                                                        this
                                                                            .listeSimFlotte
                                                                    )
                                                                );
                                                                console.log("ras");
                                                                this.navCtrl.navigateForward(
                                                                    LIENECRANRECAPITULATIF
                                                                );
                                                            }
                                                        });
                                                });
                                        } else {
                                            const loader = await this.loadingCtrl.create(
                                                {
                                                    message: "ajout en cours",
                                                }
                                            );
                                            loader.present();
                                            sessionStorage.setItem(
                                                simulationActifenSession,
                                                JSON.stringify(
                                                    this.simulationActif
                                                )
                                            );
                                            this.listeSimFlotte[0] = this.simulationActif;
                                            sessionStorage.setItem(
                                                listeSimulationDansFlotteValide,
                                                JSON.stringify(
                                                    this.listeSimFlotte
                                                )
                                            );
                                            console.log("ras");
                                            this.navCtrl.navigateForward(
                                                LIENECRANRECAPITULATIF
                                            );
                                        }
                                    },
                                    (error) => {
                                        console.log(error);
                                    }
                                );
                        }
                    } else {
                        //loader.dismiss();
                        console.log("Enregistrement échoué !!!");
                        this.utilsService.alertNotification(
                            "Carte grise non enregistrée !"
                        );
                    }
                });
            } else {
                if (typeOrigineRoutine == typeRoutineNewContrat || typeOrigineRoutine == typeRoutineRenewContrat) {
                            this.enregistrer();
                            //   if (!this.isFlotte) {
                            //     alert("non flotte");

                            //   } else {
                            //     alert("flotte");
                            //     console.log(
                            //       this.infosAutomobile.matricule +
                            //       "est eligible"
                            //     );
                            //     this.infosAutomobile.infosValide = true;
                            //     this.enregistrer();
                            //   }
                        } else if (typeOrigineRoutine == typeRoutineNewObject) {
                            //Cette fonction permet de créer (construire) un objet assure
                            //avant son enregistrement en base
                            this.remplirInformationsObjet().then(() => {
                            console.log("Objet before ajout", this.objetAssure);
                            this.enregistrer();
                            });

                        } else if (typeOrigineRoutine == typeRoutineEditObject) {
                            console.log("Modifiaction objet", this.objetAssure);
                            this.enregistrer();
                        }

            }
        }
    }

    dateObjetAssureEtDateContrat(objet, contrat: Operation) {
        let diff = { sec: 0, min: 0, hour: 0, day: 0 };
        if (
            new Date(objet.dateFin) >= new Date(contrat.dateEffet) &&
            new Date(objet.dateDebut) < new Date(contrat.dateEffet)
        ) {
            return { sec: 0, min: 0, hour: 0, day: 0 };
        }
        if (
            new Date(contrat.dateCloture) >= new Date(objet.dateDebut) &&
            new Date(objet.dateFin) > new Date(contrat.dateCloture)
        ) {
            return { sec: 0, min: 0, hour: 0, day: 0 };
        }
        if (
            new Date(objet.dateDebut) < new Date(contrat.dateEffet) &&
            new Date(objet.dateFin) < new Date(contrat.dateCloture)
        ) {
            return { sec: -1, min: -1, hour: -1, day: -1 };
        }
        if (
            new Date(objet.dateDebut) == new Date(contrat.dateEffet) &&
            new Date(objet.dateFin) == new Date(contrat.dateCloture)
        ) {
            return { sec: 1, min: 1, hour: 1, day: 1 };
        }
        if (new Date(objet.dateDebut) > new Date(contrat.dateCloture)) {
            return this.dateDiff(
                new Date(objet.dateDebut),
                new Date(contrat.dateCloture)
            );
        }
        if (new Date(objet.dateFin) < new Date(contrat.dateEffet)) {
            return this.dateDiff(
                new Date(objet.dateFin),
                new Date(contrat.dateEffet)
            );
        }
        return diff;
    }

    dateDiff(date1, date2) {
        var diff = { sec: 0, min: 0, hour: 0, day: 0 }; // Initialisation du retour
        var tmp = date2 - date1;
        tmp = Math.floor(tmp / 1000); // Nombre de secondes entre les 2 dates
        diff.sec = tmp % 60; // Extraction du nombre de secondes
        tmp = Math.floor((tmp - diff.sec) / 60); // Nombre de minutes (partie entière)
        diff.min = tmp % 60; // Extraction du nombre de minutes
        tmp = Math.floor((tmp - diff.min) / 60); // Nombre d'heures (entières)
        diff.hour = tmp % 24; // Extraction du nombre d'heures
        tmp = Math.floor((tmp - diff.hour) / 24); // Nombre de jours restants
        diff.day = tmp;
        return diff;
    }

    async alertConfirmation(message) {
        let changeLocation;

        changeLocation = await this.alertCtrl.create({
            header: "Notification",
            message: message,

            buttons: [
                {
                    text: "non",
                    handler: (data) => { },
                },

                {
                    text: "oui",
                    handler: (data) => {
                        // this.testifObjetCanHaveContratRoutineSimulation(true);
                    },
                },
            ],
        });

        changeLocation.present();
    }

    //il se peut  que pour les routines dfferents d eroutine objet on attende  jusquau niveau recapitulatif
    //comme il est spécifi dans les   specs
    //A faire apres la validation
    enregistrer(update = false) {
        /** pour les routines de simulations et de nouveau contrat on attend la fin pour enregistrer les objets
         * **/
        //this.loadingCtrl.dismiss();

        //console.log(this.infosAutomobile);
        //dispatching
        let typeOrigineRoutine = JSON.parse(
            window.sessionStorage.getItem(typeRoutine)
        );
        switch (typeOrigineRoutine) {
            case typeRoutineSimulation:
                this.simulationActif.objet = this.infosAutomobile;
                window.sessionStorage.setItem(
                    simulationActifenSession,
                    JSON.stringify(this.simulationActif)
                );
                let listeSimFlotte = JSON.parse(
                    window.sessionStorage.getItem(
                        listeSimulationDansFlotteValide
                    )
                );
                if (listeSimFlotte != null && listeSimFlotte.length > 1) {
                    this.utilsService
                        .remplacerSimulationFlotteBeforeRecap(
                            this.simulationActif
                        )
                        .then(() => {
                            this.navCtrl.navigateForward(
                                LIENASSURINFOVEVEHICULEFLOTTE
                            );
                        });
                } else {
                    if (listeSimFlotte != null && listeSimFlotte.length == 1)
                        this.utilsService
                            .remplacerSimulationFlotteBeforeRecap(
                                this.simulationActif
                            )
                            .then(() => {
                                this.navCtrl.navigateForward(
                                    LIENECRANRECAPITULATIF
                                );
                            });
                    else
                        this.utilsService.alertNotification(
                            "erreur veuillez rééssayer"
                        );
                }

                break;
            case typeRoutineNewContrat:
                sessionStorage.setItem("choosenUsageId", this.sousBranche.id.toString());
                if (this.objetSelection != null) {
                    //alert("Objet selectionner");
                    console.log('Routine nouveau contrat objet existant');
                    window.sessionStorage.setItem(
                        objetActifenSession,
                        JSON.stringify(this.objetAssure)
                    );
                    window.sessionStorage.setItem(
                        contratActifenSession,
                        JSON.stringify(this.contratActif)
                    );
                    //Pour voir si c'est un nouveau ou objet deja existant
                    window.sessionStorage.setItem(
                        "TypeObjet",
                        "Existant"
                    );
                    this.navCtrl.navigateForward(
                        LIENCHOICEOPTION
                    );
                } else {
                    //alert("Objet non selectionner");
                    console.log('Routine nouveau contrat nouveau objet');

                    this.remplirInformationsObjet().then(() =>{
                        console.log(this.objetAssure);
                         this.serviceObjet
                        .checkIfObjetAssureExist(
                            this.objetAssure.intitule,
                            this.objetAssure.numeroTitulaire
                        )
                        .subscribe(
                            (data) => {
                                console.log("Data", data);
                                if (data.length == 1) {
                                    //Objet existe
                                    this.utilsService.alertNotification(
                                        "Vous avez déjà enregistré cette objet"
                                    );
                                } else {
                                    //Nouveau objet
                                    window.sessionStorage.setItem(
                                            objetActifenSession,
                                            JSON.stringify(this.objetAssure)
                                        );
                                        window.sessionStorage.setItem(
                                            contratActifenSession,
                                            JSON.stringify(this.contratActif)
                                        );
                                        //Pour voir si c'est un nouveau ou objet deja existant
                                        window.sessionStorage.setItem(
                                            "TypeObjet",
                                            "Nouveau"
                                        );
                                        this.navCtrl.navigateForward(
                                            LIENCHOICEOPTION
                                        );
                                }
                            },
                            (error) => {
                                console.log(error);
                            }
                        );
                    })

                }
                break;
            case typeRoutineRenewContrat:
                this.objetActif = this.infosAutomobile;
                this.isEditObjet = JSON.parse(
                    sessionStorage.getItem(editObjet)
                );
                console.log(this.isEditObjet);
                if (!this.isEditObjet) {
                    window.sessionStorage.setItem(
                        objetActifenSession,
                        JSON.stringify(this.objetActif)
                    );
                    window.sessionStorage.setItem(
                        contratActifenSession,
                        JSON.stringify(this.contratActif)
                    );
                    this.navCtrl.navigateForward(LIENCHOICEOPTION);
                } else {
                    let operation = JSON.parse(
                        sessionStorage.getItem(operationActifenSession)
                    );
                    let contrat = JSON.parse(
                        sessionStorage.getItem(contratActifenSession)
                    );
                    let objet = JSON.parse(
                        sessionStorage.getItem(objetActifenSession)
                    );

                    ////console.log(this.operationActif) ;
                    ////console.log(this.contratActif) ;
                    ////console.log(JSON.parse(window.sessionStorage.getItem(objetActifenSession))) ;

                    let routineContrat = new RoutineContrat();
                    routineContrat.contrat = contrat;
                    routineContrat.objet = this.objetActif;
                    routineContrat.operation = operation;

                    window.sessionStorage.setItem(
                        objetActifenSession,
                        JSON.stringify(this.objetActif)
                    );

                    this.utilsService
                        .remplacerRoutineContrat(routineContrat)
                        .then(() => {
                            this.navCtrl.navigateRoot(
                                LIENASSURINFOVEVEHICULEFLOTTE
                            );
                        });
                }

                break;
            case typeRoutineNewObject:
                //*  Meme routine que ajouter contact   **/
                //appel service modele ajouter(this.objet)
                //si ajout a reussi
                //on recuperer lid généré lors de l'ajout
                this.serviceObjet
                    .checkIfObjetAssureExist(
                        this.objetAssure.intitule,
                        this.objetAssure.numeroTitulaire
                    )
                    .subscribe(
                        (data) => {
                            console.log("Data", data);
                            if (data.length == 1) {
                                //Objet existe
                                this.utilsService.alertNotification(
                                    "Vous avez déjà enregistré cette objet"
                                );
                            } else {
                                //Nouveau objet
                                this.addNewObjetAssure();
                            }
                        },
                        (error) => {
                            console.log(error);
                        }
                    );

                // sessionStorage.setItem(OBJETSESSION, JSON.stringify(this.infosAutomobile));
                // this.navCtrl.navigateRoot("/monobjet");
                break;
            case typeRoutineEditObject:

                break;

            default:
                this.effacerDonnees();
        }

        //fin dispatching

        //si ajout a echoué
        //loader.dismiss()
    }

    /**
     * *Cette fonction permet d'ajouter un nouveau
     * *objet assure en uploadant ces fichiers pdf
     */
    async addNewObjetAssure() {
        this.objetAssure.attributs = this.objetAssure.attributs.filter(
            function (el) {
                return el != null;
            }
        );
        this.objetAssure.attributs.forEach((element: any) => {
            if (element.attribut != null) {
                delete element.attribut.listevaleur;
            }
            //element.typeba.parent
        });

        this.objetAssure.typeba.attributs.forEach((element: any) => {
            if (element != null) {
                delete element.listevaleur;
            }
            //element.typeba.parent
        });
        console.log("object", this.objetAssure);
        this.serviceObjet.addObjetAssures(this.objetAssure).subscribe(
            (data) => {
                if (data) {
                    this.loadingCtrl.dismiss();
                    this.exporterCg();
                    //this.exporterCt();
                    this.utilsService.alertNotification(
                        "Enregistré avec succes"
                    );
                    //api post objet
                    console.log(
                        "enregistrement de l'objet au niveu de la base"
                    );
                    //ici on enlève tous les attributs null du tableau
                    data.attributs = data.attributs.filter(function (el) {
                        return el != null;
                    });
                    console.log("Objet saved", data);
                    window.sessionStorage.setItem(
                        OBJETSESSION,
                        JSON.stringify(data)
                    );
                    this.navCtrl.navigateRoot("/monobjet");
                }
            },
            (error) => {
                console.log(error);
            }
        );
    }

    getCarburant(carburant) {
        this.infosAutomobile.carburant = carburant;
        //console.log(carburant);
        this.valCarburant = carburant;
        return carburant;
    }
    initForm(objetAssure: ObjetAssures2) {
        let typeOrigineRoutine = JSON.parse(
            window.sessionStorage.getItem(typeRoutine)
        );
        let valMarque;
        let valModel;
        this.onDeclarationForm
            .get("numTitulaire")
            .setValue(objetAssure.numeroTitulaire);
        this.onDeclarationForm
            .get("prenom")
            .setValue(objetAssure.prenomTitulaire);
        this.onDeclarationForm.get("nom").setValue(objetAssure.nomTitulaire);

        objetAssure.attributs.forEach((att) => {
            switch (att.attribut.code) {
                case att.attribut.code:
                    console.log(att.valeur);

                    this.onDeclarationForm
                        .get(att.attribut.code)
                        .setValue(att.valeur);
                    if (att.attribut.code == "valPuissance")
                        this.valuePuissance = Number(att.valeur);
                    if (att.attribut.code == "marqueVehiculeAuto")
                        valMarque = att.valeur;
                    if (att.attribut.code == "marqueVehiculeMoto")
                        valMarque = att.valeur;
                    if (att.attribut.code == "appellationCom")
                        this.valModelePassed = att.valeur;
                    if (att.attribut.code == "appellationComMoto")
                        this.valModelePassed = att.valeur;
                    if (att.attribut.code == "valCylindree")
                        this.valueCylindre = Number(att.valeur);
                    if (att.attribut.code == "valRemorque")
                        this.valRemorque = att.valeur;
                    if (att.attribut.code == "valTypeEnergie")
                        this.valCarburant = att.valeur;
                    if (att.attribut.code == "valTypeVehicule")
                        this.valTypeVehicule = att.valeur;
                    if (att.attribut.code == "valTypeCarosserieAuto")
                        this.valCarosserie = att.valeur;
                    break;
            }
        });
        console.log("All Marques", this.marques);
        console.log("Marque init", valMarque);
        //this.onDeclarationForm.get('marque').setValue(valMarque)
        //this.getMarque(valMarque, valModel);
        if(typeOrigineRoutine != typeRoutineSimulation){
            this.getMarqueActive(valMarque);

        }

    }

    /**
     * * Fonction pour initialiser
     * * les marques et modèles
     * * routine new contrat
     */
    getMarqueActive(marqueObjet) {
        this.marques.forEach((objetMarque) => {
            if (objetMarque.name == marqueObjet) {
                this.marque = this.marques[objetMarque.id];
                console.log(this.marque.listeModeles[4]);
            }
        });

        this.getModeleActif();
    }
    getModeleActif() {
        this.marque.listeModeles.forEach((objetModele) => {
            this.modeles = this.marque.listeModeles;
            console.log(objetModele.name + " " + this.valModelePassed);
            if (objetModele.name == this.valModelePassed) {
                this.modele = this.marque.listeModeles[objetModele.id];
            }
        });
    }
    //End
    /**
     * exporter la carte grise
     */
    async exporterCg() {
        const loader = await this.loadingCtrl.create({
                    message: "upload carte grise en cours",
                });
                loader.present();
        let promesse;
        if (
            this.fileCarteGrise != null &&
            this.fileCarteGrise != this.oldFile
        ) {
            console.log("yes");
            if (this.testExtension(initialeCG)) {
                //id recuperé apres l'enregistrement de l'objet
                let id = this.recupererId(initialeCG);
                let body = new FormData();

                body.append("file", this.fileCarteGrise);
                body.append("id", id);

                promesse = new Promise((resolve, reject) => {
                    this.serviceObjet.upload(body, initialeCG).subscribe(
                        (res) => {
                            if (res) {
                                resolve(true);
                                loader.dismiss();
                            } else {
                                let message = "Erreur insertion carte grise";
                                this.utilsService.alertNotification(message);
                                reject(false);
                                loader.dismiss();
                            }
                        },
                        (erreur) => {
                            let message = "Erreur connexion avec le serveur Veuilez réessayer";
                            this.utilsService.alertNotification(message);
                            loader.dismiss();
                            return false;
                        }
                    );
                });
            } else {
                this.utilsService.alertNotification("carte grise non soumis");
                loader.dismiss();
            }
        } else {
            this.utilsService.alertNotification(
                "Veuillez choisir un fichier pdf"
            );
            loader.dismiss();
        }
        return promesse;
    }

    exporterCt() {
        if (
            this.fileControleTechnique != null &&
            this.fileControleTechnique != this.oldFile
        ) {
            if (this.testExtension(initialeCT)) {
                let id = this.recupererId(initialeCT);
                let body = new FormData();

                body.append("file", this.fileControleTechnique);
                body.append("id", id);

                this.serviceObjet.upload(body, initialeCT).subscribe(
                    (res) => {
                        if (res) {
                            // let message ="Enregistré avec succès" ;
                            // this.utilsService.alertNotification(message) ;
                        } else {
                            let message = "Erreur inertion visite technique";
                            this.utilsService.alertNotification(message);
                        }
                    },
                    (erreur) => {
                        // let message ="Erreur connexion Veuilez réessayer" ;
                        // this.utilsService.alertNotification(message) ;
                    }
                );
            } else {
                this.utilsService.alertNotification(
                    "controle technique non soumis"
                );
            }
        } else {
            this.utilsService.alertNotification(
                "Veuillez choisir un fichier pdf"
            );
        }
    }
    // k

    onInputTimeCg(event) {
        this.fileCarteGrise = event.target.files[0];
    }
    onInputTimeCt(event) {
        this.fileControleTechnique = event.target.files[0];
    }
    testExtension(obj) {
        let nom: String;
        let extension;
        switch (obj) {
            case initialeCG:
                nom = this.inputFileCg.value;
                extension = nom.substring(nom.lastIndexOf("."));

                if (extension == ".pdf") return true;
                else return false;

            case initialeCT:
                nom = this.inputFileCt.value;
                extension = nom.substring(nom.lastIndexOf("."));
                if (extension == ".pdf") return true;
                else return false;
        }
    }

    //on simule la base de donnee
    recupererId(obj) {
        return "199";
    }
    //fin gestion des fichiers

    portChange(event: { component: IonicSelectableComponent; value: any }) {
        this.inputModele.value = null;

        if (this.marque == null) {
            this.marque = new Marque();
            this.marque.listeModeles = [];
        }
    }
    /**
     * fonction de traitement lorsque l'utilisatoir choisit un objet parmi la liste des objets deja enregistrés
     */
    portChangeObjet() {
        //alert("Objet selectioner");
        let hasOneContratValid: Boolean = false;
        console.log("SELECTED OBJET", this.objetSelection);
        this.objetAssure = this.objetSelection;
        this.initForm(this.objetAssure);
        // this.checkIfObjetHasContrat(this.objetSelection.id).then((Assurecontrats) => {
        //     console.log('Assurecontrats', Assurecontrats);
        //     if(Assurecontrats.length >= 1){
        //         let count = 0;
        //         Assurecontrats.forEach((assurecontrat) => {
        //             this.operation.getOperationByContrat(assurecontrat.contrat.id)
        //                 .subscribe((operations) => {
        //                     let resultat = null;
        //                     console.log("operations",operations)
        //                     if (operations.length >= 1) {
        //                         operations.forEach((op) => {

        //                             let operationDateDebut = new Date(op.dateEffet).toLocaleDateString();
        //                             let operationDateFin = new Date(op.dateCloture).toLocaleDateString();
        //                             let contratDateEffet = new Date(assurecontrat.contrat.dateEffet).toLocaleDateString();
        //                             let contratDateCloture = new Date(assurecontrat.contrat.dateCloture).toLocaleDateString();
        //                             resultat = this.dateObjetAssureEtDateContrat(assurecontrat.contrat, op);
        //                             // if (resultat.day == 0 && contratDateCloture == operationDateDebut) {
        //                             //     this.utilsService.alertNotification(`Vous avez un contrat en cours qui début le ${operationDateDebut.split("/").reverse().join("/")}`);
        //                             // }
        //                             // if (resultat.day == 0 && contratDateEffet == operationDateFin) {
        //                             //     this.utilsService.alertNotification(`Vous avez un contrat en cours qui se termine le ${operationDateFin} à 23h:59`);
        //                             // }
        //                             // if (resultat == { sec: -1, min: -1, hour: -1, day: -1 }) {
        //                             //     this.utilsService.alertNotification(`Vous avez un contrat en cours du ${operationDateDebut} à 00h:00 au ${operationDateFin} à 23h:59`);
        //                             // }
        //                             // if (resultat == { sec: 1, min: 1, hour: 1, day: 1, }) {
        //                             //     this.utilsService.alertNotification(`Vous avez un contrat en cours du ${operationDateDebut} à 00h:00 au ${operationDateFin} à 23h:59`);
        //                             // }
        //                             //resultat == { sec: 0, min: 0, hour: 0, day: 0 }
        //                             if (resultat.day == 0) {
        //                                  this.utilsService.alertNotification(`Vous avez un contrat en cours qui se termine le ${operationDateFin} à 23h:59`);
        //                             }
        //                             console.log("resultat", resultat);
        //                             console.log("contrat", assurecontrat.contrat.dateEffet, assurecontrat.contrat.dateCloture);
        //                             console.log("opération", op.dateEffet, op.dateCloture);
        //                         });
        //                     }

        //                     if (!resultat || (resultat.day < 0 || resultat.day > 0)) {

        //                         console.log("Vous pouvez enregistrer un nouveau contrat sur cette objet")
        //                         // this.enregistrer()
        //                     }
        //                 });
        //             });

        //             // if(count === contrats.length){
        //             //     if(hasOneContratValid){
        //             //         this.utilsService.alertNotification("Cet objet ")
        //             //     }
        //             // }

        //     }else{
        //         //Si l'objet à aucun contrat
        //        // this.initForm(this.objetAssure);
        //        console.log("Cette objet n'a aucun contrat");
        //     }
        // })

        // if (
        //     this.infosAutomobile.controleTechnique &&
        //     this.infosAutomobile.controleTechnique.dateValidite
        // )
        //     this.onDeclarationForm
        //         .get("controleTechnique")
        //         .setValue(this.infosAutomobile.controleTechnique.dateValidite);
        let typeOrigineRoutine = JSON.parse(
            window.sessionStorage.getItem(typeRoutine)
        );
        switch (typeOrigineRoutine) {
            case typeRoutineSimulation:
                this.simulationActif.objet = this.objetSelection;
                break;
            case typeRoutineNewContrat:
                this.objetActif = this.objetSelection;
                break;
            case typeRoutineRenewContrat:
                this.objetActif = this.objetSelection;
                //console.log(this.infosAutomobile.id);
                break;
        }

        // this.getMarque(
        //     this.infosAutomobile.marque,
        //     this.infosAutomobile.modele
        // );
    }
    onClear(event: { component: IonicSelectableComponent; items: any[] }) {
        this.marque = new Marque();
    }
    onOpen(event: { component: IonicSelectableComponent }) {
        if (this.marque.name == null) console.log(true);
        else console.log(false);
    }
    arreterSimulation() {
        this.alertStop();
    }
    async alertStop() {
        let changeLocation;

        changeLocation = await this.alertCtrl.create({
            header: "",
            message: "Voulez vous enregistrer la simulation",

            buttons: [
                {
                    text: "OUI",
                    handler: (data) => {
                        this.utilsService.enregistrerSimulation(
                            this.listeSimFlotte
                        );
                        this.utilsService.arreterSimulation(
                            this.simulationActif.origineRoutine
                        );
                    },
                },
                {
                    text: "NON",
                    handler: (data) => {
                        this.utilsService.arreterSimulation(
                            this.simulationActif.origineRoutine
                        );
                    },
                },
                {
                    text: "ANNULER",
                    handler: (data) => { },
                },
            ],
        });

        changeLocation.present();
    }

    effacerDonnees() {
        this.navCtrl.navigateRoot("/accueil-client-non-logge");
        //  window.sessionStorage.setItem(simulationActifenSession,JSON.stringify(this.simulationActif))
    }
    /**
     * cette fonction nous permet de verifier si un objet est deja enregistré dans la base
     */
    verifierMatricule() {
        ///verifier matricule comme telephone

        return true;
    }

    async choisirVehicule() {
        const modal = await this.modalController.create({
            component: ModalMarquePage,
            componentProps: {
                //   'id': this.id,
                //  'telephone' : login
            },
            cssClass: "modal-css",
        });

        return await modal.present();
    }

    async recupererListSimFlotteDeLaSimActive() {
        let liste = JSON.parse(
            window.sessionStorage.getItem(listeSimulationDansFlotte)
        );
        console.log("Liste simulation flotte", liste);
        this.listeSimFlotte = [];
        liste.forEach((simulation) => {
            if (
                simulation.numeroDansSession ==
                this.simulationActif.numeroDansSession
            ) {
                this.listeSimFlotte.push(simulation);
            }
        });

        this.objetAssure = new ObjetAssures2();

        return true;
    }

    recupererListeObjet() {
        this.utilsService.getObjets().then((objets) => {
            this.objetsEssais = objets;
        });
    }
    getMarque(nameMarque, nameModele) {
        this.marques.forEach((element) => {
            if (element.name == nameMarque) {
                this.marque = element;
                console.log("marque", this.marque);
                this.marque.listeModeles.forEach((modele) => {
                    if (modele.name == nameModele) this.modele = modele;
                    console.log("modele", this.modele);
                    console.log(this.onDeclarationForm.value);
                });
            }
        });
    }

    testCarburant(carburant) {
        if (this.infosAutomobile.carburant == carburant) return true;
        else return false;
    }

    toggleInfosChange() {
        console.log(this.toggleInfos);
        if (!this.toggleInfos) {
            this.inputCivilite.value = this.connectedClient.civilite.code;
            this.civiliteClient = this.connectedClient.civilite.code;
            this.inputPrenom.value = this.connectedClient.prenom;
            this.inputNom.value = this.connectedClient.nom;
            console.log(this.civiliteClient);
        }
    }
    updateObjet(objetAssure: ObjetAssures2) {
        this.serviceObjet.updateObjetAssure(objetAssure).subscribe(
            (data) => {
                if (data) {
                    this.loadingCtrl.dismiss();
                    //this.exporterCg();
                    //this.exporterCt();
                    this.utilsService.alertNotification("Modifié avec succès");
                    window.sessionStorage.setItem(
                        OBJETSESSION,
                        JSON.stringify(data)
                    );
                    this.navCtrl.navigateRoot("/monobjet");
                    //api post objet
                    console.log("modification de l'objet au niveu de la base");
                }
            },
            (error) => {
                console.log(error);
            }
        );
    }

    modifierObjetAssureForUpdate(objetAssureUpdate: ObjetAssures2) {
        let promise = new Promise<ObjetAssures2>((resolve, reject) => {
            let marqueChoosen: Marque;
            let modeleChoosen: Modele;
            console.log("Objet Assure Attributs", objetAssureUpdate.attributs);
            objetAssureUpdate.intitule = this.onDeclarationForm.get(
                "matricule"
            ).value;
            objetAssureUpdate.numeroTitulaire = this.onDeclarationForm.get(
                "numTitulaire"
            ).value;
            objetAssureUpdate.proprietaire = this.connectedClient;
            this.objetAssure.prenomTitulaire = this.onDeclarationForm.get(
                "prenom"
            ).value;
            objetAssureUpdate.nomTitulaire = this.onDeclarationForm.get(
                "nom"
            ).value;

            marqueChoosen = this.onDeclarationForm.get("marque").value;
            modeleChoosen = this.onDeclarationForm.get("modele").value;

            //Modification valeur attributs
            objetAssureUpdate.attributs.forEach((att: AttributAussure) => {
                switch (att.attribut.code) {
                    case "immatriculation":
                        att.valeur = this.onDeclarationForm.get(
                            "matricule"
                        ).value;
                        break;
                    case "numeroCartegrise":
                        att.valeur = this.onDeclarationForm.get(
                            "numCarteGrise"
                        ).value;
                        break;
                    case "valTypeVehicule":
                        att.valeur = this.onDeclarationForm.get("usage").value;
                        break;
                    case "marqueVehiculeAuto":
                        att.valeur = marqueChoosen.name;
                        break;
                    case "appellationCom":
                        att.valeur = modeleChoosen.name;
                        break;
                    case "valDate1ereCirculation":
                        att.valeur = this.onDeclarationForm.get("annee").value;
                        break;
                    case "valPuissance":
                        att.valeur = this.onDeclarationForm.get(
                            "puissance"
                        ).value;
                        break;
                    case "valTypeEnergie":
                        att.valeur = this.onDeclarationForm.get(
                            "carburant"
                        ).value;
                        break;
                    case "valCarosserie":
                        att.valeur = this.onDeclarationForm.get(
                            "typeCarosserie"
                        ).value;
                        break;
                    case "valCylindree":
                        att.valeur = this.onDeclarationForm.get(
                            "valCylindree"
                        ).value;
                        break;
                    case "valNombrePlace":
                        att.valeur = this.onDeclarationForm.get(
                            "nbPlaces"
                        ).value;
                        break;
                    case "mntValeurNeuve":
                        att.valeur = this.onDeclarationForm.get(
                            "valeurInitiale"
                        ).value;
                        break;
                    case "mntValeurDeclaree":
                        att.valeur = this.onDeclarationForm.get(
                            "valeurDeclarative"
                        ).value;
                        break;
                    // case "valRemorque":
                    //     att.valeur = this.onDeclarationForm.get("matricule").value;
                    //     break;
                    case "dateVisiteTech":
                        att.valeur = this.onDeclarationForm.get(
                            "controleTechnique"
                        ).value;
                        break;
                }
                resolve(objetAssureUpdate);
            });
        });

        return promise;
    }

    defaultHref() {
        this.typeOrigineRoutine = JSON.parse(
            window.sessionStorage.getItem(typeRoutine)
        );

        switch (this.typeOrigineRoutine) {
            case typeRoutineSimulation:
                return "assurinfove-vehicule-flotte";
                break;
            case typeRoutineNewContrat:
            case typeRoutineRenewContrat:
                return "simbranauto";
                break;
        }
    }
    async chargerControleTechnique() {
        const loader = await this.loadingCtrl.create({
            message: "",
        });

        loader.present();
        // To download the PDF file
        this.serviceCompte.download(
            CONTROLETECHNIQUEPATHNAME,
            this.infosAutomobile.id,
            downloadControleTechnique
        );
    }
    async chargerCarteGrise() {
        const loader = await this.loadingCtrl.create({
            message: "",
        });

        loader.present();
        // To download the PDF file
        this.serviceCompte.download(
            CARTEGRISEPATHNAME,
            this.infosAutomobile.id,
            downloadCarteGrise
        );
    }
    /**
     * cette fonction est appelée à chaque fois que le matricule change et
     * permet de charger les informations de l'objet correspondat au matricule correspondant
     */
    getObjetByMatricule() {
        this.simulationAppelApiObjetByMatricule(
            this.onDeclarationForm.get("matricule").value
        ).then(() => {
            console.log(this.objetSelection);
            if (this.objetSelection == null) {
                this.infosAutomobile = new Automobile();
                // this.remplirInformationsAutomobile().then(() => {
                //     this.objetSelection = this.infosAutomobile;
                //     this.portChangeObjet();
                // });
            } else {
                this.utilsService.alertNotification(
                    "chargement des infos du vehicule <strong>" +
                    this.objetSelection.matricule +
                    "</strong>"
                );
                this.portChangeObjet();
            }
        });
    }
    /**
     * Cette foncion permet de tester si l'objet dont le matricule est spéci fait partie des objets deja enregistrés
     * return  un element ou nul sinon ObjetAssure si l'objetest dans la liste
     */
    async simulationAppelApiObjetByMatricule(matricule) {
        // //simuler cs ou ca retourne null
        this.objetSelection = null;
        this.objetsAssures.forEach((objet) => {
            if (objet.intitule == matricule) this.objetSelection = objet;
        });
        //simuler cas ou ca retourne un objet
        //  this.utilsService.getObjets().then(
        //     objets=>{ console.log(objets[0])
        //       this.objetSelection =objets[0]}
        //     )
    }

    /**
     * Cette fonction nous permet de remplir les informations de l'objet à partir des informations
     * saisies par l'utilisateur apres verification et validation
     */
    async remplirInformationsObjet() {
        let typeOrigineRoutine = JSON.parse(
            window.sessionStorage.getItem(typeRoutine)
        );
        if (
            typeOrigineRoutine == typeRoutineNewObject ||
            typeOrigineRoutine == typeRoutineNewContrat ||
            typeOrigineRoutine == typeRoutineSimulation
        ) {
            this.objetAssure = new ObjetAssures2();
            let imat = new AttributAussureBranche();
            let carte = new AttributAussureBranche();
            let niv = new AttributAussureBranche();
            let marque = new AttributAussureBranche();
            let modele = new AttributAussureBranche();
            let puissance = new AttributAussureBranche();
            let cylindre = new AttributAussureBranche();
            let carburant = new AttributAussureBranche();
            let annee = new AttributAussureBranche();
            let nbrplace = new AttributAussureBranche();
            let valeurDecla = new AttributAussureBranche();
            let valeurInit = new AttributAussureBranche();
            let dateVisiTech = new AttributAussureBranche();
            let utilisation = new AttributAussureBranche();
            let typeVehicule = new AttributAussureBranche();
            let carosserie = new AttributAussureBranche();
            let categorieVehicule = new AttributAussureBranche();
            let remorque = new AttributAussureBranche();
            let marqueChoosen: Marque;
            let modeleChoosen: Modele;

            this.TabAttributsParentAndFilsObjets.forEach((att) => {
                switch (att.code) {
                    case "immatriculation":
                        imat = att;
                        break;
                    case "numeroCartegrise":
                        carte = att;
                        break;
                    case "NIV":
                        niv = att;
                        break;
                    case "valTypeVehicule":
                        typeVehicule = att;
                        break;
                    case "marqueVehiculeAuto":
                        marque = att;
                        marqueChoosen = this.onDeclarationForm.get(
                            "marqueVehiculeAuto"
                        ).value;
                        break;
                    case "appellationCom":
                        modele = att;
                        modeleChoosen = this.onDeclarationForm.get(
                            "appellationCom"
                        ).value;
                        break;
                    case "marqueVehiculeMoto":
                        marque = att;
                        marqueChoosen = this.onDeclarationForm.get(
                            "marqueVehiculeMoto"
                        ).value;
                        break;
                    case "appellationComMoto":
                        modele = att;
                        modeleChoosen = this.onDeclarationForm.get(
                            "appellationComMoto"
                        ).value;
                        break;
                    case "valDate1ereCirculation":
                        annee = att;
                        break;
                    case "valPuissance":
                        puissance = att;
                        break;
                    case "valTypeEnergie":
                        carburant = att;
                        break;
                    case "valCylindree":
                        cylindre = att;
                        break;
                    case "valNombrePlace":
                        nbrplace = att;
                        break;
                    case "mntValeurNeuve":
                        valeurInit = att;
                        break;
                    case "mntValeurDeclaree":
                        valeurDecla = att;
                        break;
                    case "valRemorque":
                        remorque = att;
                        break;
                    case "valUtilisationVehiculeMoto":
                        utilisation = att;
                        break;
                    case "valTypeCarosserieAuto":
                        carosserie = att;
                        break;
                    case "valCategorieVehicule":
                        categorieVehicule = att;
                        break;
                    case "dateVisiteTech":
                        dateVisiTech = att;
                        break;
                }
            });

            // on renseigne le champs CatégorieVéhicule par défaut
            this.sousBranche.parent.attributs.forEach((element: AttributAussureBrancheDto) => {
                if (element.affichageEtape == - 1) {
                    console.log("element", element.libelle);
                    console.log("element.listevaleur", element.listevaleur);
                    element.listevaleur.forEach(listevaleur => {
                        if (listevaleur.code == this.sousBranche.codeFront){
                            console.log("listevaleur.libelle", listevaleur.libelle);
                            this.onDeclarationForm.get("valCategorieVehicule").setValue(listevaleur.libelle)
                        }

                    });
                }
            });

            this.objetAssure.intitule = this.onDeclarationForm.get(
                "immatriculation"
            ).value;

            this.objetAssure.attributs = [
                this.showImmatriculation
                    ? new AttributAussure(
                        imat,
                        this.onDeclarationForm.get("immatriculation").value
                    )
                    : null,
                this.showCarteGrise
                    ? new AttributAussure(
                        carte,
                        this.onDeclarationForm.get("numeroCartegrise").value
                    )
                    : null,
                this.showNIV
                    ? new AttributAussure(
                        niv,
                        this.onDeclarationForm.get("NIV").value
                    )
                    : null,
                this.showTypeVehicule
                    ? new AttributAussure(
                        typeVehicule,
                        this.onDeclarationForm.get("valTypeVehicule").value
                    )
                    : null,
                this.showValUtilisation
                    ? new AttributAussure(
                        utilisation,
                        this.onDeclarationForm.get(
                            "valUtilisationVehiculeMoto"
                        ).value
                    )
                    : null,
                this.showMarqueVehicule
                    ? new AttributAussure(marque, marqueChoosen.name)
                    : null,
                this.showAppelationCom
                    ? new AttributAussure(modele, modeleChoosen.name)
                    : null,
                this.showMarqueMoto
                    ? new AttributAussure(marque, marqueChoosen.name)
                    : null,
                this.showAppelationComMoto
                    ? new AttributAussure(modele, modeleChoosen.name)
                    : null,
                this.showValPuissance
                    ? new AttributAussure(
                        puissance,
                        this.onDeclarationForm.get("valPuissance").value
                    )
                    : null,
                this.showCylyndre
                    ? new AttributAussure(
                        cylindre,
                        this.onDeclarationForm.get("valCylindree").value
                    )
                    : null,
                this.showTypeEnergie
                    ? new AttributAussure(
                        carburant,
                        this.onDeclarationForm.get("valTypeEnergie").value
                    )
                    : null,
                this.showTypeCarosserie
                    ? new AttributAussure(
                        carosserie,
                        this.onDeclarationForm.get(
                            "valTypeCarosserieAuto"
                        ).value
                    )
                    : null,
                this.showNombrePlace
                    ? new AttributAussure(
                        nbrplace,
                        this.onDeclarationForm.get("valNombrePlace").value
                    )
                    : null,
                this.showDateFirstCir
                    ? new AttributAussure(
                        annee,
                        this.onDeclarationForm.get(
                            "valDate1ereCirculation"
                        ).value
                    )
                    : null,
                this.showValeurDeclare
                    ? new AttributAussure(
                        valeurDecla,
                        this.onDeclarationForm.get("mntValeurDeclaree").value
                    )
                    : null,
                this.showValeurNeuve
                    ? new AttributAussure(
                        valeurInit,
                        this.onDeclarationForm.get("mntValeurNeuve").value
                    )
                    : null,
                this.showDateVisiteTech
                    ? new AttributAussure(
                        dateVisiTech,
                        this.onDeclarationForm.get("dateVisiteTech").value
                    )
                    : null,
                this.showValRemorque
                    ? new AttributAussure(
                        remorque,
                        this.onDeclarationForm.get("valRemorque").value
                    )
                    : null,
                this.showCategorieVehicule
                    ? new AttributAussure(
                        categorieVehicule,
                        this.onDeclarationForm.get(
                            "valCategorieVehicule"
                        ).value
                    )
                    : null,
            ];
            this.objetAssure.numeroTitulaire = this.onDeclarationForm.get(
                "numTitulaire"
            ).value;
            this.objetAssure.proprietaire = this.connectedClient;
            this.objetAssure.prenomTitulaire = this.onDeclarationForm.get(
                "prenom"
            ).value;
            this.objetAssure.nomTitulaire = this.onDeclarationForm.get(
                "nom"
            ).value;
            this.objetAssure.typeba = this.sousBranche;
        }
    }

    /**GESTION LOGIQUE TRANSLATION DES LANGUES */
    ionViewDidEnter(): void {
        this.getDeviceLanguage();
    }

    public changeLanguage(): void {
        this._translateLanguage();
    }

    _translateLanguage(): void {
        this._translate.use(this.language);
    }

    _initTranslate(language) {
        // Set the default language for translation strings, and the current language.
        this._translate.setDefaultLang("fr-FR");
        if (language) {
            this.language = language;
        } else {
            // Set your language here
            this.language = "fr-FR";
        }
        this._translateLanguage();
    }

    getDeviceLanguage() {
        if (!this.platform.is("android") || !this.platform.is("ios")) {
            //alert('Navigateur')
            console.log("Navigateur langage", navigator.language);
            this._initTranslate(navigator.language);
        } else {
            this.globalization
                .getPreferredLanguage()
                .then((res) => {
                    this._initTranslate(res.value);
                })
                .catch((e) => {
                    console.log(e);
                });
        }
    }
    /**END TRANSLATION */
}

import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AssurinfovePage } from './assurinfove.page';

describe('AssurinfovePage', () => {
  let component: AssurinfovePage;
  let fixture: ComponentFixture<AssurinfovePage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AssurinfovePage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AssurinfovePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import { CompteService } from './../../services/compte.service';
import { Compte } from './../../models/compte';
import { AuthService } from './../../services/auth.service';
import { Component, OnInit, ViewChild, Input } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { NavController, MenuController, ToastController, AlertController, LoadingController, NavParams, ModalController, Platform } from '@ionic/angular';
import { ActivatedRoute } from '@angular/router';
import { MailLien } from 'src/app/models/mail-lien';
import { TranslateService } from '@ngx-translate/core';
import { Globalization } from '@ionic-native/globalization/ngx';



@Component({
  selector: 'app-resetpassword',
  templateUrl: './resetpassword.page.html',
  styleUrls: ['./resetpassword.page.scss'],
})
export class ResetpasswordPage implements OnInit {
  public onResetForm: FormGroup;
  checkConnexion:boolean=false ;
  passwordVisible=false ;
  confirmPasswordVisible =false
  tokenValide= false ;
  @ViewChild('password') inputPassword ;
  @ViewChild('confirm') inputConfirm;
  @Input()  id ;
  language: string;

  @Input() telephone : any ;
 compteId: number;
  compte: Compte;

  constructor(
    public navCtrl: NavController,
    public menuCtrl: MenuController,
    public toastCtrl: ToastController,
    public alertCtrl: AlertController,
    public loadingCtrl: LoadingController,
    private formBuilder: FormBuilder,
    private route :ActivatedRoute ,
    private  auth : AuthService,
    private serviceCompte : CompteService,
    private modalController : ModalController,
    private platform: Platform,
    private globalization: Globalization,
    private _translate: TranslateService
  ) {
    
  
   }


  ionViewWillEnter() {
    this.menuCtrl.enable(false);
    this.loadingCtrl.dismiss() ;
  
  }

  ngOnInit() {
  

    this.onResetForm = this.formBuilder.group({
    
      'code': [null, Validators.compose([
        Validators.required,Validators.maxLength(4)
      ])],
      
      'confirmCode': [null, Validators.compose([
        Validators.required,Validators.maxLength(4)
      ])]
    });
  }



 

 

 
  voirPassword(){
    this.inputPassword.type = 'text' ;
    this.passwordVisible = true ;

  }
  voirConfirm(){
    this.inputConfirm.type = 'text' ;
    this.confirmPasswordVisible = true ;

  }
  cacherPassword(){
    this.inputPassword.type = 'password' ;
    this.passwordVisible = false ;
  }
  cacherConfirm(){
    this.inputConfirm.type = 'password' ;
    this.confirmPasswordVisible= false ;
  }
  changePassword(){
    if(this.onResetForm.get('code').value  != this.onResetForm.get('confirmCode').value )
    {
      let messageInfo = 'Les codes ne sont pas conformes' ;
          
      this.alertNotification(messageInfo) ;

    }
    else{
            let compte = new Compte ;
            compte.id = this.id ;
            compte.motDePasse = this.onResetForm.get('code').value ;
            compte.telephone=this.telephone ;
            console.log("compte"+compte) ;
            this.serviceCompte.updateCompte(compte).subscribe(typePrestation=> {
              
              let messageInfo = 'Modification reussie cliquez sur ok pour vous connecter' ;
          
              this.alertNotification(messageInfo) ;
              this.dismiss() ;
            
            },
            async erreur =>{
              let messageInfo = 'erreur veuillez reessayer' ;
          
              this.alertNotification(messageInfo) ;
              this.dismiss() ;

            }
            
            )
          }
  
  }
 

async alertNotification(message) {
 
  let changeLocation ;
  

    changeLocation= await this.alertCtrl.create({
      header: 'Notification',
      message: message,
      
      
      buttons: [
        
        {
          text: 'ok',
          handler: data => {
            
          }
          
        }
      ]
    });
  
  changeLocation.present();
}
delay(ms: number) {
  return new Promise( resolve => setTimeout(resolve, ms) );
}
dismiss(){
  this.modalController.dismiss() ;
}
    /**GESTION LOGIQUE TRANSLATION DES LANGUES */
    ionViewDidEnter(): void {
      this._translateLanguage()
    }
    ionViewDidLoad() {
      this._translateLanguage();
    }
  
    public changeLanguage(): void {
      this._translateLanguage();
    }
  
    _translateLanguage(): void {
      this._translate.use(this.language);
    }
    _initTranslate(language) {
      // Set the default language for translation strings, and the current language.
      this._translate.setDefaultLang('fr-FR');
      if (language) {
        this.language = language;
      }
      else {
        // Set your language here
        this.language = 'fr-FR';
      }
      this._translateLanguage();
    }
  
    getDeviceLanguage() {
      if (!this.platform.is('android') || !this.platform.is('ios')) {
        //alert('Navigateur')
        console.log('Navigateur langage', navigator.language)
        this._initTranslate(navigator.language);
      } else {
        this.globalization.getPreferredLanguage()
          .then(res => {
            this._initTranslate(res.value)
          })
          .catch(e => { console.log(e); });
      }
    }
    /**END TRANSLATION */
}

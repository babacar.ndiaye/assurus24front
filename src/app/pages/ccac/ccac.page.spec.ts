import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CcacPage } from './ccac.page';

describe('CcacPage', () => {
  let component: CcacPage;
  let fixture: ComponentFixture<CcacPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CcacPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CcacPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

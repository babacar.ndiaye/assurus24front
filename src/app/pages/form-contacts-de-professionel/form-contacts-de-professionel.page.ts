import { CompteService } from './../../services/compte.service';
import { ContactClient } from './../../models/contact-client';
import { Component, OnInit, ViewChild } from '@angular/core';
import { NavController, AlertController, ToastController, LoadingController, Platform } from '@ionic/angular';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MailLien } from 'src/app/models/mail-lien';
import { Compte } from 'src/app/models/compte';
import { typeRoutineProfil, routineInscription, routineInscriptionPro, routineUpdateProfil, routineInscriptionClientReseau, IDINSCRIPTION, LIENIDENTIFICATION, LIENUPLOADDOC, LIENACCUEILCLIENTLOGGE, LIENMYPROFILE, LIENSELECTIONPROFIL, CodeHomme, codeFemme, SessionCompte, SessionCompteUpdate, routineResetPassword, CLIENTPROFESSIONNELSESSION, CONTACTDECLIENTPROFESSIONNELSESSION, LISTECONTACTCLIENTPROFESSIONNELSESSION, LIENLISTECONTACTProfessionnel, INDEXCONTACTDECLIENTPROFESSIONNELSESSION } from 'src/environments/environment';
import { UtilsService } from 'src/app/services/utils.service';
import { Client } from 'src/app/models/client';

import { Region } from 'src/app/models/region';
import { Pays } from 'src/app/models/pays';
import { ListeValeur } from 'src/app/models/liste-valeur';
import { PaysService } from 'src/app/services/pays.service';
import { ListeValeurService } from 'src/app/services/liste-valeur.service';

import { ClientProfessionnel } from 'src/app/models/client-professionnel';
import { Globalization } from '@ionic-native/globalization/ngx';
import { TranslateService } from '@ngx-translate/core';

@Component({
    selector: 'app-form-contacts-de-professionel',
    templateUrl: './form-contacts-de-professionel.page.html',
    styleUrls: ['./form-contacts-de-professionel.page.scss'],
})
export class FormContactsDeProfessionelPage implements OnInit {

    public onContactForm: FormGroup;
    @ViewChild('indicatif') selectIndicatif;
    @ViewChild('telephone') inputTelephone;
    @ViewChild('selectRegion') selectRegion;
    @ViewChild('pays') selectPays;
    language: string;
    contactClient: ContactClient;

    lienListe = LIENLISTECONTACTProfessionnel;
    nombreEssais: number;
    formInvalide: boolean = false;
    typeRoutineProfilLoc = JSON.parse(sessionStorage.getItem(typeRoutineProfil))
    routineInscriptionLoc = routineInscription
    routineInscriptionProLoc = routineInscriptionPro
    routineUpdateProfilLoc = routineUpdateProfil
    routineResetPasswordLoc = routineResetPassword
    routineInscriptionClientReseauLoc = routineInscriptionClientReseau
    contactClientUpdate: ContactClient;
    compteUpdate: Compte;
    allPays: Pays[] = [];
    allCivilites: ListeValeur[] = [];
    allRegions: Region[] = []
    // objets civilites masculin  et féminin

    civiliteHomme: ListeValeur = { id: 17, type: "LISTE_CIVILITE", description: "Liste des civilités", code: "Mr", libelle: "Monsieur", dateSuppression: null };

    civiliteFemme: ListeValeur = { id: 18, type: "LISTE_CIVILITE", description: "Liste des civilités", code: "Mme", libelle: "Madame", dateSuppression: null };
    ch = "Mr"
    cf = "Mme"
    constructor(
        public navCtrl: NavController,
        private formBuilder: FormBuilder,
        public alertCtrl: AlertController,
        public toastCtrl: ToastController,
        public loadingCtrl: LoadingController,
        public serviceCompte: CompteService,
        public utilsService: UtilsService,

        private platform: Platform,
        private globalization: Globalization,
        private _translate: TranslateService,
        public serviceListeValeur: ListeValeurService,
        public servicePays: PaysService) {
        //  console.log(JSON.parse(window.sessionStorage.getItem(CONTACTSESSION)));
        this.contactClient = new ContactClient();
        this.contactClient.ville = new Region()
        this.contactClient.pays = new Pays();
        this.contactClient.civilite = new ListeValeur();
        this.contactClientUpdate = new ContactClient();

        this.compteUpdate = new Compte();





    }
    getPays(): void {
        this.servicePays.getPays().subscribe(Pays => {
            this.allPays = Pays;
            console.log(this.allPays);



        },
            error => {
                let message = "Oups le serveur est introuvable verfiez votre connexion et recommencez svp";
                this.utilsService.alertNotification(message);
                this.navCtrl.navigateRoot(LIENLISTECONTACTProfessionnel);

            });
    }
    getCivilites(): void {
        this.serviceListeValeur.getListeCivilites().subscribe(civilites => {
            this.allCivilites = civilites;
            console.log(this.allCivilites);
            //Recuperer les civilités
            this.allCivilites.forEach(civilite => {
                if (civilite.code == CodeHomme)
                    this.civiliteHomme = civilite
                if (civilite.code == codeFemme)
                    this.civiliteFemme = civilite


            });


        },
            error => {
                let message = "Oups le serveur est introuvable verfiez votre connexion et recommencez svp";
                this.utilsService.alertNotification(message);
                this.navCtrl.navigateRoot(LIENLISTECONTACTProfessionnel);

            });

    }
    ionViewWillEnter() {
        this.getPays();
        this.typeRoutineProfilLoc = JSON.parse(sessionStorage.getItem(typeRoutineProfil))

        this.getCivilites();
        console.log(this.typeRoutineProfilLoc);
        this.contactClient = JSON.parse(window.sessionStorage.getItem(CONTACTDECLIENTPROFESSIONNELSESSION));

        if (this.contactClient == null) {
            this.contactClient = new ContactClient();
            this.contactClient.nom = "";
            this.contactClient.prenom = "";
            this.contactClient.civilite = new ListeValeur();
            this.contactClient.adresse = "";
            this.contactClient.intituleposte = "";
            this.contactClient.dateDeNaissance = new Date();
            this.contactClient.email = "";
            this.contactClient.ville = new Region();
            this.contactClient.ville.nom = "";
            this.contactClient.pays = new Pays();
            this.contactClient.pays.nom = "";
        }
        console.log(this.contactClient)
        this.initForm()





    }

    ngOnInit() {

        // this.getPays() ;

        this.getCivilites();
        this.contactClient = new ContactClient();
        this.contactClient.civilite = new ListeValeur();
        this.contactClient.pays = new Pays();
        this.contactClient.ville = new Region();

        this.onContactForm = this.formBuilder.group({
            'nom': [null, Validators.compose([
                Validators.required
            ])],
            'prenom': [null, Validators.compose([
                Validators.required
            ])],
            'civility': [null, Validators.compose([
                Validators.required
            ])],
            'telephone': [null, Validators.compose([
                Validators.required
            ])],
            'datenais': [null, Validators.compose([
                Validators.required
            ])],
            'adresse': [null, Validators.compose([
                Validators.required
            ])],
            'intituleposte': [null, Validators.compose([
                Validators.required
            ])],
            'ville': [null, Validators.compose([
                Validators.required
            ])],
            'pays': [null, Validators.compose([
                Validators.required
            ])],
            'email': [null, Validators.compose([
                Validators.required, Validators.email
            ])]

        });
        //this.initForm()
    }
    async continuer() {

        if (!this.onContactForm.valid) {

            this.utilsService.alertNotification("tous les champs ne sont pas remplis correctement")
            this.formInvalide = true;

        }
        else {

            const loader = await this.loadingCtrl.create({
                message: '',
            });


            this.remplirInformationsContact().then(
                () => {

                    this.validerInsertion();

                }
            )

        }
    }

    async alertNotification(message) {

        let changeLocation;


        changeLocation = await this.alertCtrl.create({
            header: 'Notification',
            message: message,


            buttons: [

                {
                    text: 'ok',
                    handler: data => {

                    }

                }
            ]
        });

        changeLocation.present();
    }
    getCiviliteClicked(civilite) {
        if (this.contactClient.civilite == null)
            this.contactClient.civilite = new ListeValeur();
        this.contactClient.civilite = civilite;
        console.log(this.onContactForm.get('civility').value)
        //this.utilsService.delay(2000)


    }

    erreurConnexion() {
        this.loadingCtrl.dismiss();
        let messageInfo = 'Desolé erreur de connexion,veuillez reessayer';
        this.alertNotification(messageInfo);

    }


    async remplirInformationsContact() {
        let selectcivilite = this.contactClient.civilite;

        this.contactClient = JSON.parse(window.sessionStorage.getItem(CONTACTDECLIENTPROFESSIONNELSESSION));
        if (this.contactClient == null) {

            this.contactClient = new ContactClient();
            this.contactClient.ville = new Region();
            this.contactClient.pays = new Pays();
            this.contactClient.civilite = new ListeValeur();

        }
        this.contactClient.telephone = this.getTelephone();
        this.contactClient.nom = this.onContactForm.get('nom').value;
        this.contactClient.prenom = this.onContactForm.get('prenom').value;
        this.contactClient.civilite = new ListeValeur();
        this.contactClient.civilite = selectcivilite
        this.contactClient.adresse = this.onContactForm.get('adresse').value;
        this.contactClient.intituleposte = this.onContactForm.get('intituleposte').value;
        this.contactClient.dateDeNaissance = this.onContactForm.get('datenais').value;
        this.contactClient.email = this.onContactForm.get('email').value;
        this.contactClient.ville = new Region();
        this.contactClient.pays = new Pays();
        this.contactClient.ville = this.onContactForm.get('ville').value;
        this.contactClient.pays = this.onContactForm.get('pays').value;

    }


    chargerRegionsduPays() {
        this.selectRegion.value = null;
        this.allRegions = this.onContactForm.get("pays").value.regions
        console.log(this.allRegions)

    }
    initForm() {

        if (this.contactClient.civilite != null) {
            this.onContactForm.get('civility').setValue(this.contactClient.civilite.code);
        }
        //intinitialer les inputs de telephone : le select indicatif et le champ numero
        let telephone = this.contactClient.telephone;
        console.log(telephone)
        if (telephone == null) {
            //this.selectIndicatif.value = INDICATIFDEFAUT;

        }
        else {
            if (telephone.indexOf('"') >= 0)
                telephone = telephone.substring(1, telephone.length - 1)
            this.inputTelephone.value = telephone.substring(4);
            this.selectIndicatif.value = telephone.substring(0, 4);

        }
        // if(this.contactClient!=null)
        // this.selectPays.value = this.contactClient.pays



    }
    compareWith = (o1: any, o2: any) => {

        return o1 && o2 ? o1.id === o2.id : o1 === o2;
    };
    cw = this.compareWith;


    validerInsertion() {

        let listeContactDeClientPro = JSON.parse(sessionStorage.getItem(LISTECONTACTCLIENTPROFESSIONNELSESSION));
        if (listeContactDeClientPro == null)
            listeContactDeClientPro = [];
        let indexUpdate = JSON.parse(sessionStorage.getItem(INDEXCONTACTDECLIENTPROFESSIONNELSESSION));
        //cas où l'index  est nul,on est dans un scénario de modification
        if (indexUpdate == null) {
            console.log("liste contact client pro avant ajout", listeContactDeClientPro);
            listeContactDeClientPro.push(this.contactClient);
            console.log("liste contact client pro apres ajout", listeContactDeClientPro);
            sessionStorage.setItem(LISTECONTACTCLIENTPROFESSIONNELSESSION, JSON.stringify(listeContactDeClientPro));
            sessionStorage.removeItem(CONTACTDECLIENTPROFESSIONNELSESSION);
            this.loadingCtrl.dismiss();
            this.navCtrl.navigateRoot(LIENLISTECONTACTProfessionnel);
        }
        //cas où l'index  n'est pas nul,on est dans un scénario d'insertion
        else {
            listeContactDeClientPro[indexUpdate] = this.contactClient
            sessionStorage.setItem(LISTECONTACTCLIENTPROFESSIONNELSESSION, JSON.stringify(listeContactDeClientPro));
            sessionStorage.removeItem(CONTACTDECLIENTPROFESSIONNELSESSION);
            sessionStorage.removeItem(INDEXCONTACTDECLIENTPROFESSIONNELSESSION)
            this.loadingCtrl.dismiss();
            this.navCtrl.navigateRoot(LIENLISTECONTACTProfessionnel);

        }


    }

 /**GESTION LOGIQUE TRANSLATION DES LANGUES */
 ionViewDidEnter(): void {
    this._translateLanguage()
  }
  ionViewDidLoad() {
    this._translateLanguage();
  }

  public changeLanguage(): void {
    this._translateLanguage();
  }

  _translateLanguage(): void {
    this._translate.use(this.language);
  }
  _initTranslate(language) {
    // Set the default language for translation strings, and the current language.
    this._translate.setDefaultLang('fr-FR');
    if (language) {
      this.language = language;
    }
    else {
      // Set your language here
      this.language = 'fr-FR';
    }
    this._translateLanguage();
  }

  getDeviceLanguage() {
    if (!this.platform.is('android') || !this.platform.is('ios')) {
      //alert('Navigateur')
      this._initTranslate(navigator.language);
    } else {
      this.globalization.getPreferredLanguage()
        .then(res => {
          this._initTranslate(res.value)
        })
        .catch(e => { console.log(e); });
    }
  }
  /**END TRANSLATION */



    getTelephone(): string {
        return this.selectIndicatif.value.concat(this.onContactForm.get('telephone').value);
    }

}

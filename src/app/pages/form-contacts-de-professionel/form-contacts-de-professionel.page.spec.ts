import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FormContactsDeProfessionelPage } from './form-contacts-de-professionel.page';

describe('FormContactsDeProfessionelPage', () => {
  let component: FormContactsDeProfessionelPage;
  let fixture: ComponentFixture<FormContactsDeProfessionelPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FormContactsDeProfessionelPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FormContactsDeProfessionelPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

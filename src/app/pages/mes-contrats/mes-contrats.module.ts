import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';
import { ComponentsModule } from '../../components/components.module';
import { IonicModule } from '@ionic/angular';
import {NgxPaginationModule} from 'ngx-pagination';
import { MesContratsPage } from './mes-contrats.page';
import { TranslateLoader, TranslateModule } from '@ngx-translate/core';
import { HttpLoaderFactory } from 'src/app/app.module';
import { HttpClient } from '@angular/common/http';


const routes: Routes = [
  {
    path: '',
    component: MesContratsPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    TranslateModule.forChild({
        loader: {
          provide: TranslateLoader,
          useFactory: HttpLoaderFactory,
          deps: [HttpClient]
        }
      }),
    NgxPaginationModule,
    RouterModule.forChild(routes),
    ComponentsModule,

  ],
  declarations: [MesContratsPage]
})
export class MesContratsPageModule {}

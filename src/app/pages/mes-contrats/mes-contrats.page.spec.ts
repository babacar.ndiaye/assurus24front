import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MesContratsPage } from './mes-contrats.page';

describe('MesContratsPage', () => {
  let component: MesContratsPage;
  let fixture: ComponentFixture<MesContratsPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MesContratsPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MesContratsPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

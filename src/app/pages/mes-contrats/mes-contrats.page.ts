import { ObjetAssures2 } from "./../../models/objet-assures";
import { ContratService } from "./../../services/contrat.service";
import { Contrat2 } from "./../../models/contrat";
import { Component, OnInit, ViewChild, ElementRef } from "@angular/core";
import {
    IonList,
    AlertController,
    NavController,
    LoadingController,
    Platform,
} from "@ionic/angular";
import { ObjetAssures } from "src/app/models/objet-assures";
import { AlertOptions } from "@ionic/core";
import { Router } from "@angular/router";
import {
    CONTRATSESSION,
    OBJETSESSION,
    DescAutos,
    contratActifenSession,
    typeRoutine,
    typeRoutineNewContrat,
    UrlApiLogo,
    statutContratEnCoursDeValidation,
    statutContratActif,
    statutContratValideNonActif,
    statutContratSuspendu,
    statutContratResilie,
    statutContratExpire,
    UrlApiLogoReseau,
    LIENLOGIN,
    SessionCompte,
    OriginePageMesObjets,
    OrigineMesContrats,
} from "src/environments/environment";
import { Operation } from "src/app/models/operation";
import { Paiement } from "src/app/models/paiement";
import { Garantie } from "src/app/models/garantie";
import { OffrePromo } from "src/app/models/OffrePromo";
import { CA } from "src/app/models/ca";
import { BA } from "src/app/models/ba";
import { Client } from "src/app/models/client";
import { ClientParticulier } from "src/app/models/client-particulier";
import { Sinistre } from "src/app/models/sinistre";
import { CommunicationCommerciale } from "src/app/models/communication-commerciale";
import { Automobile } from "src/app/models/automobile";
import { ControleTechnique } from "src/app/models/controle-technique";
import { PieceJustificatif } from "src/app/models/piece-justificatif";
import { UtilsService } from "src/app/services/utils.service";
import { TypeBA, TypeBA2 } from "src/app/models/type-ba";
import { TypeGarantie } from "src/app/models/type-garantie";
import { SwitchView } from "@angular/common/src/directives/ng_switch";
import { Reseau } from "src/app/models/reseau";
import { Region } from "src/app/models/region";
import { Pays } from "src/app/models/pays";
import { ListeValeur } from "src/app/models/liste-valeur";
import { TarifGarantie } from "src/app/models/tarif-garantie";
import { AssureContrat } from "src/app/models/assure-contrat";
import { GarantieCA } from "src/app/models/garantie-ca";
import { Compte } from "src/app/models/compte";
import { Contact } from "src/app/models/contact";
import { ContactClient } from "src/app/models/contact-client";
import { CompteService } from "src/app/services/compte.service";
import { BrancheCa } from "src/app/models/branche-ca";
import { Globalization } from "@ionic-native/globalization/ngx";
import { TranslateService } from "@ngx-translate/core";
import { TypeBas } from "src/app/models/type-bas";
import { ListeValeurService } from "src/app/services/liste-valeur.service";
import { AssureContratService } from "src/app/services/assure-contrat.service";

@Component({
    selector: "app-mes-contrats",
    templateUrl: "./mes-contrats.page.html",
    styleUrls: ["./mes-contrats.page.scss"],
})
export class MesContratsPage implements OnInit {
    urlApiLogo = UrlApiLogo;
    urlApiLogoReseau = UrlApiLogoReseau;
    op1: Operation;
    o1;
    o2;
    g1: Garantie;
    g2: Garantie;
    g3: Garantie;
    typeG1: TypeGarantie;
    typeG2: TypeGarantie;
    typeG3: TypeGarantie;
    p1: Paiement;
    p2: Paiement;
    controleTechnique1: ControleTechnique;
    sinistre1: Sinistre;
    sinistre2: Sinistre;
    com1: CommunicationCommerciale;
    com2: CommunicationCommerciale;
    branche1: BA;
    typeBranche1: TypeBA;
    offre1: OffrePromo;
    client: ClientParticulier;
    compagnie1: CA;
    contratsEssais: Array<Contrat2>;
    contrats;
    tableauStatutsContrats;
    largeur = screen.width;
    longueur = screen.height;
    critereFiltre;
    valCherche;
    filtreBydebut = "filtreByDebut";
    filtreByfin = "filtreByFin";
    filtreByIm = "filtraByIm";
    filtreByNum = "filtraByNum";
    date: Date;
    op2: Operation;
    contratsWhithoutFilter: any;
    statutContratEnCoursDeValidationLoc = statutContratEnCoursDeValidation;
    statutContratActifLoc = statutContratActif;
    statutContratValideNonActifLoc = statutContratValideNonActif;
    statutContratSuspenduLoc = statutContratSuspendu;
    statutContratResilieLoc = statutContratResilie;
    statutContratExpireLoc = statutContratExpire;
    colonneReseau: boolean = false;
    reseauTest = new Reseau();
    op3: Operation;
    myContrats: Contrat2[] = [];
    compte: Compte;
    //TabObjetsAssureContrat: AssureContrat[] = [];
    p: number = 1;
    constructor(
        public alertCtrl: AlertController,
        public navCtrl: NavController,
        public utilsService: UtilsService,
        private loadingCtrl: LoadingController,
        private contratService: ContratService,
        private listeValeurService: ListeValeurService,
        private assureContratService: AssureContratService,
        private platform: Platform,
        private globalization: Globalization,
        private _translate: TranslateService
    ) {
        this.compte = new Compte();
        this.reseauTest.id = 1;
    }
    async ionViewWillEnter() {
        this.getUserConnectedInfos();
    }

    ngOnInit() {
        this.getUserConnectedInfos();
    }

    /**
     * *Cette fonction permet de récupérer les
     * *contrats de l'utilsateur connecté
     * *et charger en même temps l'ensemble
     * *des objets assurés et opérations de chaque contrat
     * @param idClient
     */
    async getClientContrats(idClient) {
        const loader = await this.loadingCtrl.create({
            message: "Chargement en cours",
        });

        loader.present();

        this.contratService.getContratByClient(idClient).subscribe((data) => {
            this.myContrats = data;
            if (this.myContrats.length >= 1) {
                this.myContrats.forEach((contrat) => {
                    contrat.objetAssures = [];
                    //Récupération objets assurés de chaque contrat
                    this.getObjetsAussureByContrat(contrat.id).then((data) => {
                        //this.TabObjetsAssureContrat = [];
                        console.log("promise getObjetsAussureByContrat", data);
                        contrat.objetAssures = data;

                        loader.dismiss();
                    });


                    //Récupération opérations de chaque contrat
                    this.getOperationsByContrat(contrat.id).then((operations) => {
                        contrat.operations = [];
                        console.log("promise getOperationsByContrat", operations);
                        contrat.operations = operations;
                        loader.dismiss();
                    });
                });
            }else{
                loader.dismiss();
            }
            console.log("myContrats", this.myContrats);
            this.contratsWhithoutFilter = this.myContrats;
        },
        (error)=>{
            console.log("error", error);
            loader.dismiss();
        });
    }

    /**
     *  * Cette fonction permet de récupérer les infos
     *  * de l'utilisateur à partir de la session
     *  * puis de charger l'ensemble de ses contrats
     */
    getUserConnectedInfos() {
        //alert('On Founction')
        if (sessionStorage.getItem(SessionCompte)) {
            let data = JSON.parse(sessionStorage.getItem(SessionCompte));
            this.compte = data;
            console.log("compte actuel", this.compte);
            if (this.compte.client.id) {
                console.log("IdClient", this.compte.client.id);
                this.getClientContrats(this.compte.client.id);
            }
        } else {
            let message = "Veuillez vous connecter pour continuer";
            this.utilsService.alertNotification(message);
            this.navCtrl.navigateRoot(LIENLOGIN);
        }
    }
    // async getContrats() {
    //     let assurecontrat = new AssureContrat();
    //     let compte = new Compte();
    //     let garantieca = new GarantieCA();
    //     let pays = new Pays();
    //     let region = new Region();
    //     let listevaleur = new ListeValeur();
    //     let tarifgarantie = new TarifGarantie();
    //     let objetassures: Array<ObjetAssures>;
    //     let contacts: Array<ContactClient>;
    //     let comptes: Array<Compte>;
    //     let brancheca: Array<BrancheCa>;
    //     //constantes à remplacer par dao
    //     this.controleTechnique1 = {
    //         id: 1,

    //         objet: null,
    //         dateValidite: new Date("05-05-2019"),
    //     };
    //     this.com1 = {
    //         id: 1,

    //         description:
    //             "Pour tout parrainage d'un nouveau client ,beneficier d'un bonus fkkfkfkfkfffffffffffkfkfkfkffffffffffffff",

    //         titreCommunication: "parrainage",

    //         ca: null,
    //         titre: "",
    //     };
    //     this.com2 = {
    //         id: 1,

    //         description: "parrainage",

    //         titreCommunication:
    //             "Pour tout parrainage d'un nouveau client ,beneficier d'un bonus fkkfkfkfkfffffffffffkfkfkfkffffffffffffff",

    //         ca: null,
    //         titre: "",
    //     };
    //     this.typeBranche1 = {
    //         id: 1,

    //         nom: "Automobile",
    //         bas: [],
    //         description: DescAutos,
    //         typeGaranties: [],
    //         ConditionsDisposition:
    //             "<p>Quam ob rem circumspecta cautela observatum est deinceps </p> <p>et cum edita montium petere coeperint grassatores, loci iniquitati milites cedunt. ubi autem in planitie potuerint reperiri, quod contingit adsidue, nec exsertare lacertos nec crispare permissi tela, quae vehunt bina vel terna, pecudum ritu inertium trucidantur.Huic Arabia est conserta, ex alio latere Nabataeis contigua; opima varistatute conmerciorum castrisque oppleta validis et castellis, quae ad repellendos gentium vicinarum excursus sollicitudo pervigil veterum per oportunos saltus erexit et cautos. haec quoque civitates habet inter oppida quaedam ingentes Bostram et Gerasam atque Philadelphiam murorum firmitate cautissimas. hanc provinciae inposito nomine rectoreque adtributo obtemperare legibus nostris Traianus conpulit imperator incolarum tumore saepe contunso cum glorioso marte Mediam urgeret et Partho.Utque proeliorum periti rectores primo catervas densas opponunt et fortes, deinde leves armaturas, post iaculatores ultimasque subsidiales acies, si fors adegerit, iuvaturas, ita praepositis urbanae familiae suspensae digerentibus sollicite,</p> <p> quos insignes faciunt virgae dexteris aptatae velut tessera data castrensi iuxta vehiculi frontem omne textrinum incedit: huic atratum coquinae iungitur ministerium, dein totum promiscue servitium cum otiosis plebeiis de vicinitate coniunctis: postrema multitudo spadonum a senibus in pueros desinens, obluridi distortaque lineamentorum conpage deformes, ut quaqua incesserit quisquam cernens mutilorum hominum agmina detestetur memoriam Samiramidis reginae illius veteris, quae teneros mares castravit omnium prima velut vim iniectans naturae, eandemque ab instituto cursu retorquens, quae inter ipsa oriundi crepundia per primigenios seminis fontes tacita quodam modo lege vias propagandae posteritatis ostendit.Qui cum venisset ob haec festinatis itineribus Antiochiam, praestrictis palatii ianuis, contempto Caesare, quem videri decuerat, ad praetorium cum pompa sollemni perrexit morbosque diu causatus nec regiam introiit nec processit in publicum, sed abditus multa in eius moliebatur exitium addens quaedam relationibus supervacua, quas subinde dimittebat ad principem.Cuius acerbitati uxor grave accesserat incentivum, germanitate Augusti turgida supra modum, quam Hannibaliano regi fratris filio antehac Constantinus iunxerat pater, Megaera quaedam mortalis, inflammatrix saevientis adsidua, humani cruoris avida nihil mitius quam maritus; qui paulatim eruditiores facti processu temporis ad nocendum per clandestinos versutosque rumigerulos conpertis leviter addere quaedam male suetos falsa et placentia sibi discentes, adfectati regni vel artium nefandarum calumnias insontibus adfligebant.</p>",
    //     };
    //     this.branche1 = {
    //         id: 1,

    //         nom: "Axa Automobile",

    //         ca: null,
    //         objetAssures: [],
    //         typeBA: this.typeBranche1,

    //         garanties: [],
    //     };
    //     this.o1 = {
    //         id: 1,
    //         libelle: "Toyota",
    //         contrats: [],
    //         matricule: "DK-2454-AB",
    //         numCarteGrise: "44445",
    //         statut: "actif",
    //         marque: "Toyota",
    //         modele: "hilux",
    //         carburant: "gasoil",
    //         annee: 2015,
    //         puissanceFiscale: "18 cv",
    //         nombrePlaces: 6,
    //         valeurDeclarative: 2500000,
    //         valeurInitiale: 1750000,
    //         ba: this.branche1,
    //         controleTechnique: this.controleTechnique1,
    //     };
    //     this.o2 = {
    //         id: 2,
    //         libelle: "Tcamry",
    //         contrats: [
    //             {
    //                 id: 2,
    //                 police: "488-4445-552",
    //                 statut: statutContratValideNonActif,
    //                 franchise: 25000,
    //                 capitauxGarantis: 250000,
    //                 objetAssures: [this.o1],
    //                 client: this.client,
    //                 reseau: this.reseauTest,
    //                 ca: this.compagnie1,
    //                 operations: [this.op1, this.op2, this.op3],
    //                 dateEffet: new Date("05-02-2019"),
    //                 dateCloture: new Date("05-25-2024"),
    //             },
    //             {
    //                 id: 3,
    //                 police: "488-4445-552",
    //                 statut: statutContratValideNonActif,
    //                 franchise: 25000,
    //                 capitauxGarantis: 250000,
    //                 objetAssures: [this.o1],
    //                 client: this.client,
    //                 reseau: this.reseauTest,
    //                 ca: this.compagnie1,
    //                 operations: [this.op1, this.op2, this.op3],
    //                 dateEffet: new Date("05-02-2019"),
    //                 dateCloture: new Date("05-25-2023"),
    //             },
    //         ],
    //         matricule: "DK-2454-AB",
    //         statut: "inactif",
    //         numCarteGrise: "44445",
    //         marque: "Toyota",
    //         modele: "hilux",
    //         carburant: "gasoil",
    //         ba: this.branche1,
    //         annee: 2015,
    //         puissanceFiscale: "18cv",
    //         nombrePlaces: 6,
    //         valeurDeclarative: 2500000,
    //         valeurInitiale: 1750000,
    //         controleTechnique: this.controleTechnique1,
    //     };
    //     this.p1 = {
    //         id: 1,

    //         referencePaiement: 9252545,

    //         montant: 7500000,
    //     };
    //     this.p2 = {
    //         id: 2,

    //         referencePaiement: 9252545,

    //         montant: 2500000,
    //     };

    //     this.typeG1 = {
    //         id: 6,
    //         nom: "bris de glace",
    //         resume: "Soyez couvert en cas de vitres abimées",
    //         typeBA: this.typeBranche1,
    //         garanties: [],

    //         obligatoire: false,
    //         general: false,
    //         description:
    //             "Tout propriétaire d’un véhicule à moteur doit obligatoirement l’assurer avec, au minimum, une garantie responsabilité civile (également appelée « assurance au tiers ». Cette garantie couvre exclusivement les dommages que pourrait causer la voiture assuré à un tiers (passager, autre conducteur, piéton…) ou à un autre véhicule.Tout propriétaire d’un véhicule à moteur doit obligatoirement l’assurer avec, au minimum, une garantie responsabilité civile (également appelée « assurance au tiers ». Cette garantie couvre exclusivement les dommages que pourrait causer la voiture assuré à un tiers (passager, autre conducteur, piéton…) ou à un autre véhicule.Tout propriétaire d’un véhicule à moteur doit obligatoirement l’assurer avec, au minimum, une garantie responsabilité civile (également appelée « assurance au tiers ». Cette garantie couvre exclusivement les dommages que pourrait causer la voiture assuré à un tiers (passager, autre conducteur, piéton…) ou à un autre véhicule.Tout propriétaire d’un véhicule à moteur doit obligatoirement l’assurer avec, au minimum, une garantie responsabilité civile (également appelée « assurance au tiers ». Cette garantie couvre exclusivement les dommages que pourrait causer la voiture assuré à un tiers (passager, autre conducteur, piéton…) ou à un autre véhicule.",
    //     };
    //     this.typeG2 = {
    //         id: 1,
    //         nom: "Responsabilite civile",
    //         description:
    //             "Tout propriétaire d’un véhicule à moteur doit obligatoirement l’assurer avec, au minimum, une garantie responsabilité civile (également appelée « assurance au tiers ». Cette garantie couvre exclusivement les dommages que pourrait causer la voiture assuré à un tiers (passager, autre conducteur, piéton…) ou à un autre véhicule.Tout propriétaire d’un véhicule à moteur doit obligatoirement l’assurer avec, au minimum, une garantie responsabilité civile (également appelée « assurance au tiers ». Cette garantie couvre exclusivement les dommages que pourrait causer la voiture assuré à un tiers (passager, autre conducteur, piéton…) ou à un autre véhicule.Tout propriétaire d’un véhicule à moteur doit obligatoirement l’assurer avec, au minimum, une garantie responsabilité civile (également appelée « assurance au tiers ». Cette garantie couvre exclusivement les dommages que pourrait causer la voiture assuré à un tiers (passager, autre conducteur, piéton…) ou à un autre véhicule.Tout propriétaire d’un véhicule à moteur doit obligatoirement l’assurer avec, au minimum, une garantie responsabilité civile (également appelée « assurance au tiers ». Cette garantie couvre exclusivement les dommages que pourrait causer la voiture assuré à un tiers (passager, autre conducteur, piéton…) ou à un autre véhicule.",

    //         resume: "La garantie qui protège les autres en cas d'accident",

    //         typeBA: this.typeBranche1,
    //         garanties: [],

    //         obligatoire: false,
    //         general: false,
    //     };
    //     this.typeG3 = {
    //         id: 2,
    //         nom: "Défense/Recours",
    //         description:
    //             "Tout propriétaire d’un véhicule à moteur doit obligatoirement l’assurer avec, au minimum, une garantie responsabilité civile (également appelée « assurance au tiers ». Cette garantie couvre exclusivement les dommages que pourrait causer la voiture assuré à un tiers (passager, autre conducteur, piéton…) ou à un autre véhicule.Tout propriétaire d’un véhicule à moteur doit obligatoirement l’assurer avec, au minimum, une garantie responsabilité civile (également appelée « assurance au tiers ». Cette garantie couvre exclusivement les dommages que pourrait causer la voiture assuré à un tiers (passager, autre conducteur, piéton…) ou à un autre véhicule.Tout propriétaire d’un véhicule à moteur doit obligatoirement l’assurer avec, au minimum, une garantie responsabilité civile (également appelée « assurance au tiers ». Cette garantie couvre exclusivement les dommages que pourrait causer la voiture assuré à un tiers (passager, autre conducteur, piéton…) ou à un autre véhicule.Tout propriétaire d’un véhicule à moteur doit obligatoirement l’assurer avec, au minimum, une garantie responsabilité civile (également appelée « assurance au tiers ». Cette garantie couvre exclusivement les dommages que pourrait causer la voiture assuré à un tiers (passager, autre conducteur, piéton…) ou à un autre véhicule.",

    //         resume: "L'assurance agit comme votre avocat",

    //         typeBA: this.typeBranche1,
    //         garanties: [],

    //         obligatoire: false,
    //         general: false,
    //     };
    //     this.g1 = {
    //         id: 1,
    //         nom: "Axa bris de glace",

    //         offrePromos: [this.offre1],

    //         typeGarantie: this.typeG1,
    //         ba: this.branche1,

    //         operations: [],
    //         code: "",
    //         codeExterne: "",
    //         libelle: "",
    //         description: "",
    //         taux: 0,
    //         minimum: 0,
    //         franchise: 0,
    //         agemin: 10,
    //         agemax: 0,
    //         attributmontant: "",
    //         codeFormule: tarifgarantie,
    //         pack: false,
    //         typegarantie: garantieca,
    //         montantTarif: 0,
    //         assure: assurecontrat,
    //     };
    //     this.g2 = {
    //         id: 2,
    //         nom: "Axa RC",

    //         offrePromos: [this.offre1],

    //         typeGarantie: this.typeG2,
    //         ba: this.branche1,

    //         operations: [],
    //         code: "",
    //         codeExterne: "",
    //         libelle: "",
    //         description: "",
    //         taux: 0,
    //         minimum: 0,
    //         franchise: 0,
    //         agemin: 10,
    //         agemax: 0,
    //         attributmontant: "",
    //         codeFormule: tarifgarantie,
    //         pack: false,
    //         typegarantie: garantieca,
    //         montantTarif: 0,
    //         assure: assurecontrat,
    //     };
    //     this.g3 = {
    //         id: 3,
    //         nom: "Axa D R",

    //         offrePromos: [this.offre1],

    //         typeGarantie: this.typeG3,
    //         ba: this.branche1,

    //         operations: [],
    //         code: "",
    //         codeExterne: "",
    //         libelle: "",
    //         description: "",
    //         taux: 0,
    //         minimum: 0,
    //         franchise: 0,
    //         agemin: 10,
    //         agemax: 0,
    //         attributmontant: "",
    //         codeFormule: tarifgarantie,
    //         pack: false,
    //         typegarantie: garantieca,
    //         montantTarif: 0,
    //         assure: assurecontrat,
    //     };
    //     this.sinistre1 = {
    //         id: 1,

    //         date: new Date("09-15-2018"),

    //         lieu: "castors",

    //         libelle: "accident",
    //         statut: "en cours",

    //         operation: null,
    //     };
    //     this.sinistre2 = {
    //         id: 2,

    //         date: new Date("09-15-2018"),

    //         lieu: "castors",

    //         libelle: "accident",
    //         statut: "en cours",

    //         operation: null,
    //     };
    //     this.op1 = {
    //         id: 1,

    //         intitule: "renouvellement",

    //         codePin: 5412544,

    //         dateEffet: new Date("07-02-2019"),
    //         dateCloture: new Date("05-05-2020"),
    //         contrat: null,

    //         paiement: this.p1,
    //         statut: statutContratResilie,

    //         garanties: [this.g1, this.g2, this.g3],

    //         sinistres: [this.sinistre1, this.sinistre2],
    //     };
    //     this.op3 = {
    //         id: 3,

    //         intitule: "renouvellement",

    //         codePin: 5412544,

    //         dateEffet: new Date("07-02-2019"),
    //         dateCloture: new Date("05-05-2020"),
    //         contrat: null,

    //         paiement: this.p1,
    //         statut: statutContratEnCoursDeValidation,

    //         garanties: [this.g1, this.g2, this.g3],

    //         sinistres: [this.sinistre1, this.sinistre2],
    //     };
    //     this.client = {
    //         id: 1,
    //         email: "ngompapa2@gmail.com",
    //         ///contact:null ,
    //         //: "",
    //         nom: "Ngom",
    //         prenom: "Pape",
    //         //cni :"55444445215475555554" ,
    //         adresse: "Castors",

    //         //tel : "+221771570386"  ,
    //         dateDeNaissance: new Date("09-08-1995"),
    //         validationCA: [],
    //         validationOP: true,
    //         contrats: [],
    //         //simulations	:[] ,

    //         //pieceJustificatifs :[] ,

    //         //compteCredits : [] ,

    //         //listeNoires  : [] ,

    //         reseau: null,

    //         demandeForums: [],

    //         //reponseForums : []
    //         civilite: listevaleur,
    //         adresse2: "",
    //         telephone: "",
    //         telephone2: "",
    //         fax: "",
    //         codePostal: "",
    //         pays: pays,
    //         ville: region,
    //         actif: listevaleur,
    //         validationReseau: false,
    //         datevalidationReseau: new Date(),
    //         userOP: "",
    //         datevalidationOp: new Date(),
    //         datecreation: new Date(),
    //         compte: compte,
    //         objetassures: objetassures,
    //     };
    //     this.op2 = {
    //         id: 2,

    //         intitule: "renouvellement",

    //         codePin: 5412544,

    //         dateEffet: new Date("05-02-2018"),
    //         dateCloture: new Date("05-05-2020"),
    //         contrat: null,

    //         paiement: this.p1,
    //         statut: statutContratActif,

    //         garanties: [this.g1, this.g2, this.g3],

    //         sinistres: [this.sinistre1, this.sinistre2],
    //     };
    //     this.compagnie1 = {
    //         id: 1,

    //         nom: "Axa",
    //         typecompagnie: listevaleur,
    //         ninea: "",
    //         adresse: "",
    //         telephone: "",
    //         email: "",
    //         siteweb: "",
    //         tauxfraisTransac: 10,
    //         forfaitTransac: 0,
    //         registreCommerce: "hfhfh",
    //         logo: "",
    //         adresse2: "",
    //         telephone2: "",
    //         fax: "",
    //         codepostal: "",
    //         pays: pays,
    //         region: region,
    //         actif: listevaleur,

    //         compte: null,

    //         communicationCommerciales: [
    //             this.com1,
    //             this.com2,
    //             this.com1,
    //             this.com1,
    //             this.com1,
    //         ],

    //         bas: [this.branche1],

    //         listeNoire: null,

    //         reseaux: [],

    //         compteCredits: [],
    //         contacts: contacts,
    //         compteutilisateurs: comptes,
    //         branches: brancheca,
    //     };

    //     this.contratsEssais = [
    //         {
    //             id: 1000001,
    //             police: "488-4445-551",
    //             statut: statutContratActif,
    //             franchise: 25000,
    //             capitauxGarantis: 250000,
    //             objetAssures: [this.o1, this.o2],
    //             client: this.client,
    //             reseau: this.reseauTest,
    //             ca: this.compagnie1,
    //             operations: [this.op1, this.op2, this.op3],
    //             dateEffet: new Date("05-02-2019"),
    //             dateCloture: new Date("01-05-2000"),
    //         },
    //         {
    //             id: 1000002,
    //             police: "488-4445-552",
    //             statut: statutContratEnCoursDeValidation,
    //             franchise: 25000,
    //             capitauxGarantis: 250000,
    //             objetAssures: [this.o1],
    //             client: this.client,
    //             reseau: this.reseauTest,
    //             ca: this.compagnie1,
    //             operations: [this.op1, this.op2, this.op3],
    //             dateEffet: new Date("05-02-2019"),
    //             dateCloture: new Date("05-25-2020"),
    //         },
    //         {
    //             id: 1000003,
    //             police: "488-4445-553",
    //             statut: statutContratValideNonActif,
    //             franchise: 25000,
    //             capitauxGarantis: 250000,
    //             objetAssures: [this.o2],
    //             client: this.client,
    //             reseau: this.reseauTest,
    //             ca: this.compagnie1,
    //             operations: [this.op1, this.op2, this.op3],
    //             dateEffet: new Date("05-02-2019 18:05:55"),
    //             dateCloture: new Date("01-02-2020"),
    //         },

    //         {
    //             id: 1000004,
    //             police: "488-4445-554",
    //             statut: statutContratActif,
    //             franchise: 25000,
    //             capitauxGarantis: 250000,
    //             objetAssures: [this.o2],
    //             client: this.client,
    //             reseau: this.reseauTest,
    //             ca: this.compagnie1,
    //             operations: [this.op1, this.op2, this.op3],
    //             dateEffet: new Date("05-02-2019"),
    //             dateCloture: new Date("05-02-2021"),
    //         },

    //         {
    //             id: 1000005,
    //             police: "488-4445-555",
    //             statut: statutContratEnCoursDeValidation,
    //             franchise: 25000,
    //             capitauxGarantis: 250000,
    //             objetAssures: [this.o2],
    //             client: this.client,
    //             reseau: this.reseauTest,
    //             ca: this.compagnie1,
    //             operations: [this.op1, this.op2, this.op3],
    //             dateEffet: new Date("05-02-2019"),
    //             dateCloture: new Date("06-26-2021"),
    //         },

    //         {
    //             id: 1000006,
    //             police: "488-4445-556",
    //             statut: statutContratResilie,
    //             franchise: 25000,
    //             capitauxGarantis: 250000,
    //             objetAssures: [this.o2],
    //             client: this.client,
    //             reseau: this.reseauTest,
    //             ca: this.compagnie1,
    //             operations: [this.op1, this.op2, this.op3],
    //             dateEffet: new Date("05-02-2019"),
    //             dateCloture: new Date("06-02-2020"),
    //         },
    //     ];

    //     this.offre1 = {
    //         id: 1,
    //         titre: "2 mois offerts",
    //         description:
    //             " 2 Mois d'Assurance offert pour la souscription d'un Contrat",
    //         date_debut: new Date("05-04-2019"),
    //         date_fin: new Date("02-20-2020"),
    //         garantie: this.g1,
    //         dateDebut: new Date("05-04-2019"),
    //         dateFin: new Date("02-20-2020"),
    //         reduction: 10,
    //     };

    //     //fin initialition
    //     this.contrats = this.contratsEssais;

    //     //fonction appelant l'api
    //     this.utilsService.getDate().subscribe(
    //         (dateFormatMobile) => {
    //             this.date = new Date(dateFormatMobile.date);
    //             //console.log(this.date);
    //         },
    //         (error) => {
    //             let message =
    //                 "Oups le serveur est introuvable verfiez votre connexion et recommencez svp";
    //             this.utilsService.alertNotification(message);
    //             this.navCtrl.navigateRoot(LIENLOGIN);
    //         }
    //     );
    //     console.log(this.contrats);

    //     this.creerArrayComparaisonStatut().then(() => {
    //         this.contrats.sort((a, b) => {
    //             let res = this.compare(a, b);
    //             console.log(res);

    //             return res;
    //         });
    //         console.log(this.contrats);
    //     });
    // }

    voirContratObjet(contrat: Contrat2) {
        if (contrat.objetAssures.length == 1) {
            window.sessionStorage.setItem(
                CONTRATSESSION,
                JSON.stringify(contrat)
            );
            window.sessionStorage.setItem(
                OBJETSESSION,
                JSON.stringify(contrat.objetAssures[0])
            );
            this.navCtrl.navigateRoot("/contrat");
        } else this.choixObjets(contrat);
    }

    /**
     * *Cette fonction permet de récupérer les
     * * objets assurés dans un contrat
     * @param idContrat
     */
    getObjetsAussureByContrat(idContrat) {
        let TabAssureContrat: AssureContrat[] = [];
        let promise = new Promise<AssureContrat[]>((resolve, reject) => {
            this.assureContratService
                .getAssureContratByContrat(idContrat)
                .subscribe(
                    (data) => {
                        if (data) {
                            //console.log("AssureContrat", data);
                            TabAssureContrat = data;
                            //pour résoudre la promesse
                            resolve(TabAssureContrat);
                        }
                    },
                    (error) => {
                        //pour rejeter la promesse
                        reject(error);
                    }
                );
        });

        return promise;
    }

    /**
     * *Cette fonction permet de récupérer les
     * * opérations dans un contrat
     * @param idContrat
     */
     getOperationsByContrat(idContrat) {
        let TabOperations: Operation[] = [];
        let promise = new Promise<Operation[]>((resolve, reject) => {
            this.contratService
                .getOperationByContrat(idContrat)
                .subscribe(
                    (data) => {
                        if (data) {
                            //console.log("OperationsContrat", data);
                            TabOperations = data;
                            //pour résoudre la promesse
                            resolve(TabOperations);
                        }
                    },
                    (error) => {
                        //pour rejeter la promesse
                        reject(error);
                    }
                );
        });

        return promise;
    }

    async choixObjets(contrat: Contrat2) {
        var options: AlertOptions = {
            header: "Objet",
            message: "Veuillez choisir un objet?",
            buttons: [
                {
                    text: "Cancel",
                    role: "cancel",
                    handler: () => {
                        console.log("Cancel clicked");
                    },
                },
                {
                    text: "Ok",
                    handler: (data) => {
                        console.log(data);
                        if (data) {
                            window.sessionStorage.setItem(
                                CONTRATSESSION,
                                JSON.stringify(contrat)
                            );
                            window.sessionStorage.setItem(
                                OBJETSESSION,
                                JSON.stringify(data)
                            );
                            this.navCtrl.navigateRoot("/contrat");
                        } else
                            this.utilsService.alertNotification(
                                "vous n'avez pas choisi d'objet"
                            );
                    },
                },
            ],
        };

        options.inputs = [];

        // Now we add the radio buttons
        for (let i = 0; i < contrat.objetAssures.length; i++) {
            options.inputs.push({
                name: "options" + i,
                value: contrat.objetAssures[i].assure,
                label: contrat.objetAssures[i].assure.intitule,
                type: "radio",
            });
        }

        // Create the alert with the options
        let alert = this.alertCtrl.create(options);
        (await alert).present();
    }

    nouveauContrat() {
        this.utilsService.initialiserSessionRoutine();
        this.recupererTypeBranches();
    }
    /**
     *
     * @param TabBranche
     */
    async selectionBranche(TabBranche) {
        this.utilsService.initialiserSessionRoutine();
        var options: AlertOptions = {
            header: "Branche",
            message: "Veuillez choisir une branche?",
            buttons: [
                {
                    text: "Cancel",
                    role: "cancel",
                    handler: () => {
                        console.log("Cancel clicked");
                    },
                },
                {
                    text: "Ok",
                    handler: (data) => {
                        console.log(data);
                        if (data) {
                            switch (data.libelle) {
                                case "AUTOMOBILE":
                                    let brancheId;
                                    if (data.id) {
                                        brancheId = data.id;
                                        console.log(
                                            "this.brancheAutoId",
                                            data.id
                                        );
                                        //sessionStorage.setItem("typeBrancheId", data.id.toString());
                                        //on récupère les sous branches de auto
                                        this.listeValeurService
                                            .getUsages(data.id)
                                            .subscribe((data: TypeBA2[]) => {
                                                if(data){
                                                    this.utilsService.getTypeBasAndSousBrancheAttributs(brancheId)
                                                    .subscribe((attributsParents) => {
                                                        this.utilsService.selectionSousBrancheObjet(OrigineMesContrats, data, "AUTOMOBILE",attributsParents,brancheId);
                                                    })
                                                }

                                            });
                                    }

                                    break;
                                default:
                                    this.utilsService.alertNotification(
                                        "Cette branche n'est pas encore disponible"
                                    );
                            }
                        } else
                            this.utilsService.alertNotification(
                                "vous n'avez pas choisi de branche"
                            );
                    },
                },
            ],
        };

        options.inputs = [];

        // Now we add the radio buttons
        let branches: TypeBA2[];
        branches = TabBranche;
        for (let i = 0; i < branches.length; i++) {
            options.inputs.push({
                name: "branche" + i,
                value: branches[i],
                label: this._translate.instant(branches[i].libelle),
                type: "radio",
            });
        }

        // Create the alert with the options
        let alert = this.alertCtrl.create(options);
        (await alert).present();
    }
    /**
     * *Cette fonction permet de récupérer les branches
     * *pour
     * @returns la liste des sous branches de la branche
     */
    recupererTypeBranches() {
        return this.utilsService.getTypeBas().subscribe((data) => {
            this.selectionBranche(data);
        });
    }

    /**
     * *Cette fonction permet de filter
     * *le tableau des contrats selon
     * *la sélection choisit
     *
     * !Attention il faut impérative faire
     * !un choix sur un critère pour filtrer
     */
    onSearchContrat() {
        if (this.critereFiltre == null) {
            this.utilsService.alertNotification(
                "veuillez choisir un critère de recherche"
            );
        } else {
            console.log("myContrats", this.myContrats);
            console.log("contratsWhithoutFilter", this.contratsWhithoutFilter);
            this.myContrats = this.contratsWhithoutFilter;
            const val = this.valCherche;
            console.log(val);
            console.log(this.critereFiltre);
            if (val && val.trim() !== "") {
                console.log("filtre");
                this.myContrats = this.myContrats.filter((contrat) => {
                    switch (this.critereFiltre) {
                        case this.filtreByNum:
                            return (
                                contrat.police
                                    .toLowerCase()
                                    .indexOf(val.trim().toLowerCase()) > -1
                            );
                        case this.filtreByfin:
                            console.log("Date",this.utilsService
                            .displayDate(contrat.dateCloture));
                            console.log("Val", val.trim().toLowerCase());

                            return (
                                this.utilsService
                                    .displayDate(contrat.dateCloture)
                                    .toLowerCase()
                                    .indexOf(val.trim().toLowerCase()) > -1
                            );

                        default:
                            return this.contratsWhithoutFilter
                    }
                });
            }
        }
    }
    /**
     * *Cette fonction permet de formater
     * *une date au format yyyy-mm-dd
     * @param date
     * @returns
     */
    formatDate(date: string|number|Date) {
        var d = new Date(date),
            month = '' + (d.getMonth() + 1),
            day = '' + d.getDate(),
            year = d.getFullYear();

        if (month.length < 2)
            month = '0' + month;
        if (day.length < 2)
            day = '0' + day;

        return [year, month, day].join('-');
    }
    compare(a: Contrat2, b: Contrat2) {
        console.log(this.tableauStatutsContrats);
        let res;

        res =
            this.tableauStatutsContrats[this.contrats.indexOf(a)] -
            this.tableauStatutsContrats[this.contrats.indexOf(b)];
        console.log(res);
        if (res == 0) {
            if (a.dateCloture > b.dateCloture) return -1;
            else if (a.dateCloture < b.dateCloture) return 1;
            else return 0;
        }
        return res;
    }
    async creerArrayComparaisonStatut() {
        if (this.contrats != null) {
            this.tableauStatutsContrats = new Array();
            for (let i = 0; i < this.contrats.length; i++) {
                switch (this.contrats[i].statut) {
                    case statutContratEnCoursDeValidation:
                        this.tableauStatutsContrats.push(1);

                        break;
                    case statutContratActif:
                        this.tableauStatutsContrats.push(2);
                        break;
                    case statutContratValideNonActif:
                        this.tableauStatutsContrats.push(2);
                        break;
                    case statutContratSuspendu:
                        this.tableauStatutsContrats.push(3);
                        break;
                    case statutContratResilie:
                        this.tableauStatutsContrats.push(3);
                        break;
                    case statutContratExpire:
                        this.tableauStatutsContrats.push(3);
                        break;
                    default:
                        this.tableauStatutsContrats.push(3);
                }
            }
        }
    }

    verifierIfColonneReseau() {
        this.contrats.forEach((contrat) => {
            if (contrat.reseau != null) {
                this.colonneReseau = true;
            }
        });
    }
}

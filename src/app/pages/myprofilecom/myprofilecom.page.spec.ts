import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MyprofilecomPage } from './myprofilecom.page';

describe('MyprofilecomPage', () => {
  let component: MyprofilecomPage;
  let fixture: ComponentFixture<MyprofilecomPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MyprofilecomPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MyprofilecomPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

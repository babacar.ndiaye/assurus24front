import { Component, OnInit } from '@angular/core';
import { Globalization } from '@ionic-native/globalization/ngx';
import { NavController, Platform } from '@ionic/angular';
import { TranslateService } from '@ngx-translate/core';
import { ClientParticulier } from 'src/app/models/client-particulier';
import { ClientProfessionnel } from 'src/app/models/client-professionnel';
import { SessionCompte } from 'src/environments/environment';

@Component({
  selector: 'app-settings',
  templateUrl: './settings.page.html',
  styleUrls: ['./settings.page.scss'],
})
export class SettingsPage implements OnInit {
  enableNotifications: any;
  currency: any;
  enablePromo: any;
  enableHistory: any;
  currencies: any = ['FCFA', 'EURO', 'DOLLAR'];
  public language: string;
//Variable pour récuperer l'utilisateur connecter
userPaConnected: ClientParticulier;
userProConnected: ClientProfessionnel;
//variable pour verifier
isConnected = false;
//variable pour verifier le type de client
typeClient: string = '';
  constructor(
    private platform: Platform,
    private globalization: Globalization,
    private _translate: TranslateService,
      public navCtrl: NavController) { }

  ngOnInit() {
      this.getDeviceLanguage();
      this.getUserConnectedInfos();
  }

  /**
   *  * Cette fonction permet de récupérer les infos
   *  * de l'utilisateur à partir de la session
   */
  getUserConnectedInfos(){
    //alert('On Founction')
      if(sessionStorage.getItem(SessionCompte)){
          this.isConnected = true;
        //alert('inside if')
          // Récupérer des données depuis sessionStorage
          let data =JSON.parse(sessionStorage.getItem(SessionCompte));
          //console.log('Data User', data);
          console.log("Type client", data.myProfil.codeGroupe);
          this.typeClient = data.myProfil.codeGroupe;
          if(this.typeClient == 'userClientParticulier'){
            this.userPaConnected= data.client;
            console.log('Connected User Part', this.userPaConnected);
          }else{
            this.userProConnected = data.client;
            console.log('Connected User Pro', this.userProConnected);
          }
            // if(this.userConnected){
            //     console.log('Connected User', this.userConnected);
            //     console.log('Nom', this.userConnected.nom);
            //     console.log('Prenom', this.userConnected.prenom);
            //     console.log('Adresse', this.userConnected.adresse);
            //     //console.log('Pays', this.userConnected.pays.nom);
            // }
      }else{
        //alert('Not connected');
        this.isConnected = false;
        //this.menu.enable(false);
      }
  }

  editProfile() {
    this.navCtrl.navigateForward('edit-profile');
  }

  logout() {
    this.navCtrl.navigateRoot('/');
  }

  /**GESTION LOGIQUE TRANSLATION DES LANGUES */
  ionViewDidEnter(): void {
    this.getDeviceLanguage()
  }

  public changeLanguage(): void {
    this._translateLanguage();
  }

  _translateLanguage(): void {
    this._translate.use(this.language);
  }

  _initTranslate(language) {
    // Set the default language for translation strings, and the current language.
    this._translate.setDefaultLang('fr-FR');
    if (language) {
      this.language = language;
    }
    else {
      // Set your language here
      this.language = 'fr-FR';
    }
    this._translateLanguage();
  }

  getDeviceLanguage() {
    if (!this.platform.is('android') || !this.platform.is('ios')) {
      //alert('Navigateur')
      console.log('Navigateur langage', navigator.language)
      this._initTranslate(navigator.language);
    } else {
      this.globalization.getPreferredLanguage()
        .then(res => {
          this._initTranslate(res.value)
        })
        .catch(e => { console.log(e); });
    }
  }
  /**END TRANSLATION */

}

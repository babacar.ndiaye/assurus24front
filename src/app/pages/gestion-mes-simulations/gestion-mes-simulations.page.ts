import { Component, OnInit } from '@angular/core';
import { Simulation } from 'src/app/models/simulation';
import { DescAutos, ListeSimulationEnSession, CONTRATSESSION, simulationActifenSession, debutRoutine, OriginePageGesSim, OriginePageMonObjet, typeRoutine, typeRoutineSimulation, listeSimulationDansFlotte, resultatFlotteOrigineGesSimulation, listeFlotteOrigineGestionSimulation, LIENSIMBRANAUTO } from 'src/environments/environment';
import { Garantie } from 'src/app/models/garantie';
import { ObjetAssures } from 'src/app/models/objet-assures';
import { Client } from 'src/app/models/client';
import { ControleTechnique } from 'src/app/models/controle-technique';
import { BA } from 'src/app/models/ba';
import { NavController, AlertController, LoadingController } from '@ionic/angular';
import { AlertOptions } from '@ionic/core';
import { UtilsService } from 'src/app/services/utils.service';
import { TypeBA, TypeBA2 } from 'src/app/models/type-ba';
import { TypeGarantie } from 'src/app/models/type-garantie';
import { FlotteSimulations } from 'src/app/models/flotte-simulations';
import { type } from 'os';

@Component({
    selector: 'app-gestion-mes-simulations',
    templateUrl: './gestion-mes-simulations.page.html',
    styleUrls: ['./gestion-mes-simulations.page.scss'],
})
export class GestionMesSimulationsPage implements OnInit {
    simulationEssais: Array<Simulation>;
    type: TypeBA2[];

    simulations;
    flotteSimulations;
    flotteSimulationIssuBase = [];
    date: Date;
    g1: Garantie;
    typeG1: TypeGarantie;
    client;
    o1;
    branche1: BA;
    typeBranche1: TypeBA;
    controleTechnique1: ControleTechnique;
    o2;
    constructor(private navCtrl: NavController, private alertCtrl: AlertController, public utilsService: UtilsService, private loadingCtrl: LoadingController) {

    }

    ngOnInit() {
    }
    async ionViewWillEnter() {
        const loader = await this.loadingCtrl.create({
            message: '',
        });

        loader.present();
        this.recuperation();
        loader.dismiss();

    }
    recuperation() {
        this.flotteSimulations = [];
        // this.recupererSimulationBase();
        //a recuperer si lappel a lapi a reussi avant d'enlever le loader
        this.recuperationSimulationSession();
        console.log(this.flotteSimulations)
        this.traitementDate();
    }
    recupererSimulationBase() {
        //recuperation au niveau de la base {}
        this.flotteSimulationIssuBase = [];
        this.recuperationFaussesDonnéesBase().then(
            () => {
                console.group("simulations issu base")
                console.log(this.flotteSimulationIssuBase)
                this.flotteSimulationIssuBase.forEach(flotte => {
                    this.flotteSimulations.push(flotte)
                })
            }

        )

    }
    recuperationSimulationSession() {

        //utilisation des fakes données
        this.UniformiserSimulationenSession();

    }
    /**
     * au niveau de cette  on simule la récupération des simulations au niveau de la base
     * on alimente le tableau de flotte flotteSimulationIssuBase
     */
    async  recuperationFaussesDonnéesBase() {

        this.controleTechnique1 = {
            id: 1,

            objet: null,
            dateValidite: new Date("05-05-2019")
        }

        this.typeBranche1 = {
            id: 1,

            nom: "Automobile",
            bas: [],
            description: DescAutos,
            typeGaranties: [],
            ConditionsDisposition: "<p>Quam ob rem circumspecta cautela observatum est deinceps </p> <p>et cum edita montium petere coeperint grassatores, loci iniquitati milites cedunt. ubi autem in planitie potuerint reperiri, quod contingit adsidue, nec exsertare lacertos nec crispare permissi tela, quae vehunt bina vel terna, pecudum ritu inertium trucidantur.Huic Arabia est conserta, ex alio latere Nabataeis contigua; opima varietate conmerciorum castrisque oppleta validis et castellis, quae ad repellendos gentium vicinarum excursus sollicitudo pervigil veterum per oportunos saltus erexit et cautos. haec quoque civitates habet inter oppida quaedam ingentes Bostram et Gerasam atque Philadelphiam murorum firmitate cautissimas. hanc provinciae inposito nomine rectoreque adtributo obtemperare legibus nostris Traianus conpulit imperator incolarum tumore saepe contunso cum glorioso marte Mediam urgeret et Partho.Utque proeliorum periti rectores primo catervas densas opponunt et fortes, deinde leves armaturas, post iaculatores ultimasque subsidiales acies, si fors adegerit, iuvaturas, ita praepositis urbanae familiae suspensae digerentibus sollicite,</p> <p> quos insignes faciunt virgae dexteris aptatae velut tessera data castrensi iuxta vehiculi frontem omne textrinum incedit: huic atratum coquinae iungitur ministerium, dein totum promiscue servitium cum otiosis plebeiis de vicinitate coniunctis: postrema multitudo spadonum a senibus in pueros desinens, obluridi distortaque lineamentorum conpage deformes, ut quaqua incesserit quisquam cernens mutilorum hominum agmina detestetur memoriam Samiramidis reginae illius veteris, quae teneros mares castravit omnium prima velut vim iniectans naturae, eandemque ab instituto cursu retorquens, quae inter ipsa oriundi crepundia per primigenios seminis fontes tacita quodam modo lege vias propagandae posteritatis ostendit.Qui cum venisset ob haec festinatis itineribus Antiochiam, praestrictis palatii ianuis, contempto Caesare, quem videri decuerat, ad praetorium cum pompa sollemni perrexit morbosque diu causatus nec regiam introiit nec processit in publicum, sed abditus multa in eius moliebatur exitium addens quaedam relationibus supervacua, quas subinde dimittebat ad principem.Cuius acerbitati uxor grave accesserat incentivum, germanitate Augusti turgida supra modum, quam Hannibaliano regi fratris filio antehac Constantinus iunxerat pater, Megaera quaedam mortalis, inflammatrix saevientis adsidua, humani cruoris avida nihil mitius quam maritus; qui paulatim eruditiores facti processu temporis ad nocendum per clandestinos versutosque rumigerulos conpertis leviter addere quaedam male suetos falsa et placentia sibi discentes, adfectati regni vel artium nefandarum calumnias insontibus adfligebant.</p>",


        }
        this.branche1 = {
            id: 1,

            nom: "Axa Automobile",

            ca: null,
            objetAssures: [],
            typeBA: this.typeBranche1,


            garanties: []
        }
        this.o1 = {
            id: 1,
            libelle: "Toyota",
            contrats: [],
            matricule: "DK-2454-AB",
            numCarteGrise: "44445",
            statut: "actif",
            marque: "Toyota",
            modele: "hilux",
            carburant: "gasoil",
            annee: 2015,
            puissanceFiscale: "18 cv",
            nombrePlaces: 6,
            valeurDeclarative: 2500000,
            valeurInitiale: 1750000,
            ba: this.branche1,
            controleTechnique: this.controleTechnique1
        }
        this.o2 = {
            id: 2,
            libelle: "Tcamry",
            contrats: [],
            matricule: "DK-2454-AB",
            statut: "inactif",
            numCarteGrise: "44445",
            marque: "Toyota",
            modele: "camry",
            carburant: "gasoil",
            ba: this.branche1,
            annee: 2015,
            puissanceFiscale: "18cv",
            nombrePlaces: 6,
            valeurDeclarative: 2500000,
            valeurInitiale: 1750000,
            controleTechnique: this.controleTechnique1
        }



        this.typeG1 = {
            id: 1,
            libelle: "bris de glace",
            typeBA: this.typeBranche1,
            code: "05D55",
            datesupp: new Date(),
            slogan: "",
            obligatoire: false,
            pack: false,
            attributs: [],
            exclusions: [],
            inPack: false,
            description: "Tout propriétaire d’un véhicule à moteur doit obligatoirement l’assurer avec, au minimum, une garantie responsabilité civile (également appelée « assurance au tiers ». Cette garantie couvre exclusivement les dommages que pourrait causer la voiture assuré à un tiers (passager, autre conducteur, piéton…) ou à un autre véhicule.Tout propriétaire d’un véhicule à moteur doit obligatoirement l’assurer avec, au minimum, une garantie responsabilité civile (également appelée « assurance au tiers ». Cette garantie couvre exclusivement les dommages que pourrait causer la voiture assuré à un tiers (passager, autre conducteur, piéton…) ou à un autre véhicule.Tout propriétaire d’un véhicule à moteur doit obligatoirement l’assurer avec, au minimum, une garantie responsabilité civile (également appelée « assurance au tiers ». Cette garantie couvre exclusivement les dommages que pourrait causer la voiture assuré à un tiers (passager, autre conducteur, piéton…) ou à un autre véhicule.Tout propriétaire d’un véhicule à moteur doit obligatoirement l’assurer avec, au minimum, une garantie responsabilité civile (également appelée « assurance au tiers ». Cette garantie couvre exclusivement les dommages que pourrait causer la voiture assuré à un tiers (passager, autre conducteur, piéton…) ou à un autre véhicule.",

        },
        // this.g1 = {
        //     id: 1,
        //     nom: "Axa bris de glace",


        //     offrePromos: [],

        //     typeGarantie: this.typeG1,
        //     ba: this.branche1,


        //     operations: [],
        // }

        this.client = {
            id: 1,
            malus: 1,
            email: "ngompapa2@gmail.com",
            compte: null,
            nom: "Ngom",
            prenom: "Pape",
            cni: "55444445215475555554",
            adresse: "Castors",

            tel: "+221771570386",
            dateDeNaissance: new Date("09-08-1995"),
            validationCA: 4,
            validationOP: 4,
            contrats: [],
            simulations: [],


            pieceJustificatifs: [],

            compteCredits: [],

            listeNoires: [],

            reseau: null,

            demandeForums: [],

            reponseForums: []
        }


        this.simulationEssais = [
            /*{
                id: 1,

                objet: this.o1,
                client: this.client, numeroDansSession: null, numeroDansFlotte: null,
                origineRoutine: OriginePageMonObjet,

                dateSimulation: new Date("05-02-2019"),

                ca: null, montantTTCCompagnie: 25000, montantUniqueObjet: 25000,
                typeGaranties: [], garanties: [], eligiblePeriode: true,

                dateDebut: new Date("06-02-2019"),
                dateFin: new Date("06-02-2019"),
                flotteSimulations: null,
            },
            {
                id: 2,
                objet: this.o1,
                client: this.client, numeroDansSession: null, numeroDansFlotte: null,
                origineRoutine: OriginePageGesSim,

                dateSimulation: new Date("05-02-2020"),
                ca: null, montantTTCCompagnie: 25000, montantUniqueObjet: 25000,


                typeGaranties: [], garanties: [], eligiblePeriode: true,

                dateDebut: new Date("06-02-2019"),
                dateFin: new Date("06-02-2019"),
                flotteSimulations: null


            }
            */
        ]
        let flotteIssuBase = new FlotteSimulations();
        flotteIssuBase.simulations = this.simulationEssais
        this.flotteSimulationIssuBase.push(flotteIssuBase)
        return this.simulationEssais;
    }
    traitementDate() {


        if (this.simulations != null && this.simulations.length != 0) {
            this.simulations.forEach(element => {
                element.dateSimulation = new Date(element.dateSimulation);


            });
            this.simulations.sort((a, b) => b.dateSimulation - a.dateSimulation);
            console.log(this.simulations);

        }


    }
    //un simulation enregistrée a un id  de base de donnée non null
    voirSimulation(flotteSimulation: FlotteSimulations) {
        console.log(flotteSimulation.simulations)
        window.sessionStorage.setItem(typeRoutine, JSON.stringify(typeRoutineSimulation))
        if (flotteSimulation.simulations[0].id != null) {
            sessionStorage.setItem(resultatFlotteOrigineGesSimulation, JSON.stringify(true))
            window.sessionStorage.setItem(listeFlotteOrigineGestionSimulation, JSON.stringify(flotteSimulation.simulations));
            this.navCtrl.navigateForward(LIENSIMBRANAUTO);

        }
        else {
            sessionStorage.setItem(simulationActifenSession, JSON.stringify(flotteSimulation.simulations[0]))
            this.navCtrl.navigateForward(LIENSIMBRANAUTO);

        }






    }

    displayDate(date: Date): String {
        let jour, mois;
        let dd = date.getDate();

        let mm = date.getMonth() + 1;
        let yyyy = date.getFullYear();
        if (dd < 10) {
            jour = '0' + dd;
        }
        else
            jour = dd;

        if (mm < 10) {
            mois = '0' + mm;
        }
        else
            mois = mm;
        return jour + "/" + mois + "/" + yyyy;
    }

    selectionBranche() {
        this.utilsService.getTypeBas().subscribe((data: TypeBA2[]) => {
            this.utilsService.selectionBrancheSimulation2(OriginePageGesSim, data);
        })

    }
    // lancerAuto() {
    //     this.utilsService.lancerAuto();
    // }

    UniformiserSimulationenSession() {

        let liste = JSON.parse(window.sessionStorage.getItem(listeSimulationDansFlotte));
        console.group("simulations en session")

        console.log(liste)
        if (liste != null && liste.length > 0) {
            this.flotteSimulations = [];
            let listeSimulation = [];
            liste.forEach(simulation => {
                if (simulation.numeroDansFlotte == 1) {
                    listeSimulation = [];
                    liste.forEach(simulationBis => {
                        if (simulationBis.numeroDansSession == simulation.numeroDansSession) {
                            listeSimulation.push(simulationBis)
                        }

                    });
                    let flotteSim = new FlotteSimulations();
                    console.log(listeSimulation)
                    flotteSim.simulations = listeSimulation;
                    this.flotteSimulations.push(flotteSim)
                }

            });

        }


    }


}

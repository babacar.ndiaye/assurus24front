import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GestionMesSimulationsPage } from './gestion-mes-simulations.page';

describe('GestionMesSimulationsPage', () => {
  let component: GestionMesSimulationsPage;
  let fixture: ComponentFixture<GestionMesSimulationsPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GestionMesSimulationsPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GestionMesSimulationsPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

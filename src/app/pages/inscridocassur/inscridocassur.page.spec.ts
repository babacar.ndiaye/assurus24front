import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { InscridocassurPage } from './inscridocassur.page';

describe('InscridocassurPage', () => {
  let component: InscridocassurPage;
  let fixture: ComponentFixture<InscridocassurPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ InscridocassurPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InscridocassurPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

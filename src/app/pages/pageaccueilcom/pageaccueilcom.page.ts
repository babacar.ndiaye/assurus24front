import { NavController } from '@ionic/angular';
import { AuthService } from './../../services/auth.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-pageaccueilcom',
  templateUrl: './pageaccueilcom.page.html',
  styleUrls: ['./pageaccueilcom.page.scss'],
})
export class PageaccueilcomPage implements OnInit {

  constructor(private authService : AuthService,private navCtrl : NavController) { }

  ngOnInit() {
  }
  deconnecter(){
        this.authService.logout()
        this.navCtrl.navigateRoot('/epocno'); ;
  }

}

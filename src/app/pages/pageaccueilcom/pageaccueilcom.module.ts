import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { PageaccueilcomPage } from './pageaccueilcom.page';
import { PopmenuComponent } from './../../components/popmenu/popmenu.component';

const routes: Routes = [
  {
    path: '',
    component: PageaccueilcomPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [PageaccueilcomPage, PopmenuComponent]
})
export class PageaccueilcomPageModule {}

import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ModifierObjetPage } from './modifier-objet.page';

describe('ModifierObjetPage', () => {
  let component: ModifierObjetPage;
  let fixture: ComponentFixture<ModifierObjetPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ModifierObjetPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ModifierObjetPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

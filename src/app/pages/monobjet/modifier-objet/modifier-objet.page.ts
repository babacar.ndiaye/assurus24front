import { ObjetAssures2 } from "./../../../models/objet-assures";
import { Component, OnInit, ViewChild } from "@angular/core";
import { UtilsService } from "src/app/services/utils.service";
import { ObjetAssures } from "src/app/models/objet-assures";
import { ModalController, NavParams, LoadingController } from "@ionic/angular";
import {
    FormGroup,
    Validators,
    FormBuilder,
    FormControl,
} from "@angular/forms";
import {
    Marque,
    Modele,
    initialeCG,
    initialeCT,
    MARQUES,
    CONTROLETECHNIQUEPATHNAME,
    downloadControleTechnique,
    CARTEGRISEPATHNAME,
    downloadCarteGrise,
    constMinNombrePlace,
    constMaxNombrePlace,
    constMinAnnee,
    constMaxAnnee,
} from "src/environments/environment";
import { Automobile } from "src/app/models/automobile";
import { IonicSelectableComponent } from "ionic-selectable";
import { ObjetService } from "src/app/services/objet.service";
import { CompteService } from "src/app/services/compte.service";
import { Client } from "src/app/models/client";
import { Compte } from "src/app/models/compte";
import { AttributAussureBrancheDto } from "src/app/Dto/AttributAussureBrancheDto";
import { AttributAussure } from "src/app/models/attribut-assure";

@Component({
    selector: "app-modifier-objet",
    templateUrl: "./modifier-objet.page.html",
    styleUrls: ["./modifier-objet.page.scss"],
})
export class ModifierObjetPage implements OnInit {
    ancienInfosAutomobile: ObjetAssures;
    ObjetAssure: ObjetAssures2;
    @ViewChild("portModele") inputModele;
    @ViewChild("myFileInputCg") inputFileCg: any;
    @ViewChild("myFileInputCt") inputFileCt: any;

    valuePuissance: number;
    valueCylindre: number;
    valCarburant: string;
    valRemorque: string;
    valCarosserie;
    valTypeVehicule;
    fileCarteGrise: any;
    fileControleTechnique: any;
    public onDeclarationForm: FormGroup;
    formInvalide: boolean = false;
    marques: Marque[];
    modeles: Modele[];
    marque: Marque = new Marque();
    modele: Modele = new Modele();
    compte: Compte;
    connectedClient: Client;
    civiliteClient;
    prenomClient;
    nomClient;
    toggleInfos: boolean;
    minNombrePlace: number = constMinNombrePlace;
    maxNombrePlace: number = constMaxNombrePlace;
    minAnnee: number = constMinAnnee;
    maxAnnee: number = constMaxAnnee;
    oldFile = "oldFile";
    TabAttributsParentAndFilsObjets: AttributAussureBrancheDto[] = [];
    valModelePassed: string;
    constructor(
        private modalController: ModalController,
        private navParams: NavParams,
        private formBuilder: FormBuilder,
        private loadingCtrl: LoadingController,
        private serviceObjet: ObjetService,
        public utilsService: UtilsService,
        private serviceCompte: CompteService
    ) {
        //this.initialisationFormulaire();
        this.ObjetAssure = new ObjetAssures2();
        this.marques = MARQUES;
    }
    ionViewWillEnter() {
        //this.chargerObjet();
    }

    ngOnInit() {
        this.chargerObjet();
    }

    /**
     * * Cette fonction permet
     * * d'initialiser le formulaire
     */
    initialisationFormulaire() {
        console.log("Before form",this.onDeclarationForm);
        let control = new Object();
        this.ObjetAssure.attributs.forEach(att => {
            control[att.attribut.code] = [null, Validators.required]
            if (att.attribut.code == "valDate1ereCirculation")
              control[att.attribut.code] = [null, Validators.compose([Validators.required, Validators.min(this.minAnnee), Validators.max(this.maxAnnee)])]
            if (att.attribut.code == "valNombrePlace")
              control[att.attribut.code] = [null, Validators.compose([Validators.required, Validators.min(this.minNombrePlace), Validators.max(this.maxNombrePlace)])]
        });
        control["numTitulaire"]= [null, Validators.required];
        control["prenom"]= [null, Validators.compose([Validators.required])];
        control["nom"]= [null, Validators.compose([Validators.required])];
        control["civilite"]= [null, Validators.compose([Validators.required])];
        control["fileCarteGrise"]= [null, Validators.compose([Validators.required])];
        this.onDeclarationForm = this.formBuilder.group(control);
        console.log(this.onDeclarationForm.value);
        console.log("After form",this.onDeclarationForm);
        //Ici on remplit les champs quand le formulaire sera crée
        if(this.onDeclarationForm != undefined){
            this.initForm(this.ObjetAssure);
        }

    }

    initForm(objetAssure: ObjetAssures2) {
        let valMarque;
        let valModel;
        this.onDeclarationForm
            .get("civilite")
            .setValue(this.civiliteClient);
        this.onDeclarationForm
            .get("numTitulaire")
            .setValue(objetAssure.numeroTitulaire);
        this.onDeclarationForm
            .get("prenom")
            .setValue(objetAssure.prenomTitulaire);
        this.onDeclarationForm.get("nom").setValue(objetAssure.nomTitulaire);

        objetAssure.attributs.forEach((att) => {
            switch (att.attribut.code) {
                case att.attribut.code:
                    console.log(att.valeur);

                    this.onDeclarationForm
                        .get(att.attribut.code)
                        .setValue(att.valeur);
                    if (att.attribut.code == "valPuissance")
                        this.valuePuissance = Number(att.valeur);
                    if (att.attribut.code == "marqueVehiculeAuto")
                        valMarque = att.valeur;
                    if (att.attribut.code == "marqueVehiculeMoto")
                        valMarque = att.valeur;
                    if (att.attribut.code == "appellationCom")
                        this.valModelePassed = att.valeur;
                    if (att.attribut.code == "appellationComMoto")
                        this.valModelePassed = att.valeur;
                    if (att.attribut.code == "valCylindree")
                        this.valueCylindre = Number(att.valeur);
                    if (att.attribut.code == "valRemorque")
                        this.valRemorque = att.valeur;
                    if (att.attribut.code == "valTypeEnergie")
                        this.valCarburant = att.valeur;
                    if (att.attribut.code == "valTypeVehicule")
                        this.valTypeVehicule = att.valeur;
                    if (att.attribut.code == "valTypeCarosserieAuto")
                        this.valCarosserie = att.valeur;
                    break;
            }
        });
        console.log("All Marques", this.marques);
        console.log("Marque init", valMarque);
        this.getMarqueActive(valMarque);
        //this.onDeclarationForm.get('marque').setValue(valMarque)
        //this.getMarque(valMarque, valModel);
    }
    fermer() {
        this.modalController.dismiss();
    }
    chargerObjet() {

        this.connectedClient = this.navParams.get("ClientConnectedPassed");
        this.civiliteClient = this.connectedClient.civilite.code;
        this.prenomClient = this.connectedClient.prenom;
        this.nomClient = this.connectedClient.nom;
        this.ObjetAssure = this.navParams.get("ObjetAssurePassed");
        this.TabAttributsParentAndFilsObjets = this.navParams.get(
            "TabAttributsParentAndFilsObjetsPassed"
        );

        console.log("ClientConnectedPassed", this.connectedClient);
        console.log("ObjetAssurePassed", this.ObjetAssure);
        console.log(
            "TabAttributsParentAndFilsObjetsPassed",
            this.TabAttributsParentAndFilsObjets
        );

        //if (this.ObjetAssure) this.initForm(this.ObjetAssure);
        if(this.ObjetAssure)this.initialisationFormulaire();
    }

    closeModal() {
        this.modalController.dismiss();
    }
    async validerModification() {
        if (!this.onDeclarationForm.valid) {
            this.formInvalide = true;
            this.utilsService.alertNotification(
                "tous les champs ne sont pas remplis correctement"
            );
        } else {
            const loader = await this.loadingCtrl.create({
                message: "Modification en cours",
            });

            loader.present();
            console.log("ancien");
            console.log(this.ancienInfosAutomobile);
            //   if(this.infosAutomobile != this.ancienInfosAutomobile)
            // {
            this.enregistrer();
            this.fermer();
        }
    }

    /**Logique Modication OBjet */
    modifierObjet() {
        if (!this.onDeclarationForm.valid) {
            this.formInvalide = true;
            this.utilsService.alertNotification(
                "tous les champs ne sont pas remplis correctement"
            );
            console.log(this.onDeclarationForm.value);

        } else {
            console.log("object before", this.ObjetAssure);

            delete this.ObjetAssure.TabAtrributs;

            this.modifierObjetAssureForUpdate(this.ObjetAssure).then(
                (data: ObjetAssures2) => {
                    console.log("object update", data);
                    let objetAssureUpdate: ObjetAssures2;
                    objetAssureUpdate = data;
                    if(objetAssureUpdate){
                        this.updateObjet(objetAssureUpdate);
                    }
                }
            );
        }
    }
    annulerModification() {
        this.fermer();
    }
    modifierObjetAssureForUpdate(objetAssureUpdate: ObjetAssures2) {
        let promise = new Promise<ObjetAssures2>((resolve, reject) => {
            let marqueChoosen: Marque = new Marque();
            let modeleChoosen: Modele = new Modele();
            console.log("Objet Assure Attributs", objetAssureUpdate.attributs);
            objetAssureUpdate.intitule = this.onDeclarationForm.get(
                "immatriculation"
            ).value;
            objetAssureUpdate.numeroTitulaire = this.onDeclarationForm.get(
                "numTitulaire"
            ).value;
            objetAssureUpdate.proprietaire = this.connectedClient;
            this.ObjetAssure.prenomTitulaire = this.onDeclarationForm.get(
                "prenom"
            ).value;
            objetAssureUpdate.nomTitulaire = this.onDeclarationForm.get(
                "nom"
            ).value;
            console.log("objetAssureUpdate", objetAssureUpdate);
            // marqueChoosen = this.onDeclarationForm.get("marque").value;
            // modeleChoosen = this.onDeclarationForm.get("modele").value;

            //Modification valeur attributs
            objetAssureUpdate.attributs.forEach((att: AttributAussure) => {
                switch (att.attribut.code) {
                    case att.attribut.code:
                        //console.log(att.valeur);
                        att.valeur = att.attribut.code == "marqueVehiculeMoto"
                        || att.attribut.code == "marqueVehiculeAuto"
                        || att.attribut.code == "appellationCom"
                        || att.attribut.code == "appellationComMoto"
                        ?''
                        :this.onDeclarationForm.get(
                            att.attribut.code
                        ).value;
                        if (att.attribut.code == "marqueVehiculeAuto"){
                            marqueChoosen = this.onDeclarationForm.get(
                                att.attribut.code
                            ).value;
                            console.log("marqueChoosen",marqueChoosen.name);
                            //console.log("marqueChoosen name",marqueChoosen.name);
                            att.valeur = marqueChoosen.name;
                        }

                        if (att.attribut.code == "marqueVehiculeMoto"){
                            marqueChoosen = this.onDeclarationForm.get(
                                att.attribut.code
                            ).value;
                        att.valeur = marqueChoosen.name;
                        }
                        if (att.attribut.code == "appellationCom"){
                            modeleChoosen = this.onDeclarationForm.get(
                                att.attribut.code
                            ).value;
                        att.valeur = modeleChoosen.name;
                        }
                        if (att.attribut.code == "appellationComMoto"){
                            modeleChoosen = this.onDeclarationForm.get(
                                att.attribut.code
                            ).value;
                        att.valeur = modeleChoosen.name;
                        }
                        break;
                }
                resolve(objetAssureUpdate);
            });
        });

        return promise;
    }
    updateObjet(objetAssure: ObjetAssures2) {
        objetAssure.attributs.forEach((element: any) => {
            if (element.attribut != null) {
              delete element.attribut.listevaleur
            }
          })
        this.serviceObjet.updateObjetAssure(objetAssure).subscribe(
            (data) => {
                if (data) {
                    this.loadingCtrl.dismiss();
                    console.log("Objet modifier", data)
                    console.log("modification de l'objet au niveu de la base");
                }
            },
            (error) => {
                console.log(error);
            }
        );
    }
    /** End Logique */
    onOpen(event: { component: IonicSelectableComponent }) {
        if (this.marque.name == null) console.log(true);
        else console.log(false);
    }
    portChange(event: { component: IonicSelectableComponent; value: any }) {
        this.inputModele.value = null;

        if (this.marque == null) {
            this.marque = new Marque();
            this.marque.listeModeles = [];
        }
    }

    enregistrer() {
        this.loadingCtrl.dismiss();
        // this.importerCg() ;

        this.utilsService.alertNotification("modifié avec succs");
        this.fermer();
    }

    /**
     * exporter la carte grise
     */
    async exporterCg() {
        let promesse;
        if (
            this.fileCarteGrise != null &&
            this.fileCarteGrise != this.oldFile
        ) {
            console.log("yes");
            if (this.testExtension(initialeCG)) {
                //id recuperé apres l'enregistrement de l'objet
                let id = this.recupererId(initialeCG);
                let body = new FormData();

                body.append("file", this.fileCarteGrise);
                body.append("id", id);

                promesse = new Promise((resolve, reject) => {
                    this.serviceObjet.upload(body, initialeCG).subscribe(
                        (res) => {
                            if (res) {
                                resolve(true);
                            } else {
                                let message = "Erreur insertion carte grise";
                                this.utilsService.alertNotification(message);
                                reject(false);
                            }
                        },
                        (erreur) => {
                            let message = "Erreur connexion Veuilez réessayer";
                            this.utilsService.alertNotification(message);
                            return false;
                        }
                    );
                });
            } else {
                this.utilsService.alertNotification("carte grise non soumis");
            }
        } else {
            this.utilsService.alertNotification(
                "Veuillez choisir un fichier pdf"
            );
        }
        return promesse;
    }
    testExtension(obj) {
        let nom: String;
        let extension;
        switch (obj) {
            case initialeCG:
                nom = this.inputFileCg.value;
                extension = nom.substring(nom.lastIndexOf("."));

                if (extension == ".pdf") return true;
                else return false;

            case initialeCT:
                nom = this.inputFileCt.value;
                extension = nom.substring(nom.lastIndexOf("."));
                if (extension == ".pdf") return true;
                else return false;
        }
    }

    recupererId(obj) {
        return "199";
    }
    onInputTimeCg(event) {
        this.fileCarteGrise = event.target.files[0];
    }

    getCarburant(carburant) {
        this.valCarburant = carburant;
        this.onDeclarationForm.get("valTypeEnergie").setValue(carburant);
    }
    getMarqueActive(marqueObjet) {
        this.marques.forEach((objetMarque) => {
            if (objetMarque.name == marqueObjet) {
                this.marque = this.marques[objetMarque.id];
                console.log(this.marque.listeModeles[4]);
            }
        });

        this.getModeleActif();
    }
    getModeleActif() {
        this.marque.listeModeles.forEach((objetModele) => {
            this.modeles = this.marque.listeModeles;
            console.log(objetModele.name + " " + this.valModelePassed);
            if (objetModele.name == this.valModelePassed) {
                this.modele = this.marque.listeModeles[objetModele.id];
            }
        });
    }

    toggleInfosChange() {
        console.log(this.toggleInfos);
        if (!this.toggleInfos) {
            this.civiliteClient = this.connectedClient.civilite.code;
            this.prenomClient = this.connectedClient.prenom;
            this.nomClient = this.connectedClient.nom;
            console.log(this.civiliteClient);
        }
    }

    displayDate(date: Date): String {
        date = new Date(date);
        let jour, mois;
        let dd = date.getDate();

        let mm = date.getMonth() + 1;
        let yyyy = date.getFullYear();
        if (dd < 10) {
            jour = "0" + dd;
        } else jour = dd;

        if (mm < 10) {
            mois = "0" + mm;
        } else mois = mm;
        return jour + "/" + mois + "/" + yyyy;
    }
    async chargerControleTechnique() {
        const loader = await this.loadingCtrl.create({
            message: "",
        });

        loader.present();
        // To download the PDF file
        this.serviceCompte.download(
            CONTROLETECHNIQUEPATHNAME,
            this.ObjetAssure.id,
            downloadControleTechnique
        );
    }
    async chargerCarteGrise() {
        const loader = await this.loadingCtrl.create({
            message: "",
        });

        loader.present();
        // To download the PDF file
        this.serviceCompte.download(
            CARTEGRISEPATHNAME,
            this.ObjetAssure.id,
            downloadCarteGrise
        );
    }
}

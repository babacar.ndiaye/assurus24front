import { ObjetAssures2 } from "./../../models/objet-assures";
import { Automobile } from "./../../models/automobile";
import { Component, OnInit, ViewChild } from "@angular/core";
import {
    OBJETSESSION,
    CONTRATSESSION,
    debutRoutine,
    OriginePageGesSim,
    simulationActifenSession,
    OriginePageMonObjet,
    Marque,
    MARQUES,
    Modele,
    typeRoutine,
    typeRoutineSimulation,
    UrlApiLogo,
    typeRoutineEditObject,
    objetActifenSession,
    CARTEGRISEPATHNAME,
    downloadCarteGrise,
    downloadControleTechnique,
    CONTROLETECHNIQUEPATHNAME,
    statutContratEnCoursDeValidation,
    statutContratActif,
    statutContratValideNonActif,
    statutContratSuspendu,
    statutContratResilie,
    statutContratExpire,
    UrlApiLogoReseau,
    OperationActifDeplie,
    LIENLOGIN,
    SessionCompte,
} from "src/environments/environment";
import {
    NavController,
    AlertController,
    LoadingController,
    ModalController,
} from "@ionic/angular";
import { UtilsService } from "src/app/services/utils.service";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { Simulation } from "src/app/models/simulation";

import { Proprietaire } from "src/app/models/proprietaire";
import { ContactClient } from "src/app/models/contact-client";
import { ObjetAssures } from "src/app/models/objet-assures";
import { CompteService } from "src/app/services/compte.service";
import { Operation } from "src/app/models/operation";
import { Client } from "src/app/models/client";
import { AttributAussure } from "src/app/models/attribut-assure";
import { AssureContratService } from "src/app/services/assure-contrat.service";
import { AssureContrat } from "src/app/models/assure-contrat";
import { Contrat2 } from "src/app/models/contrat";
import { ModifierObjetPage } from "./modifier-objet/modifier-objet.page";
import { ObjetAssuresDto } from "src/app/Dto/ObjetAssureDto";
import { ListeValeur } from "src/app/models/liste-valeur";
import { ListeValeurService } from "src/app/services/liste-valeur.service";
import { AttributAussureBrancheDto } from "src/app/Dto/AttributAussureBrancheDto";

@Component({
    selector: "app-monobjet",
    templateUrl: "./monobjet.page.html",
    styleUrls: ["./monobjet.page.scss"],
})
export class MonobjetPage implements OnInit {
    objet: ObjetAssures2;
    //babs
    ObjetAuto;
    urlApiLogo = UrlApiLogo;
    urlApiLogoReseau = UrlApiLogoReseau;
    contactProprietaire = new ContactClient();

    date: Date = new Date(null);
    titreStatutVisible: boolean;
    infoVehVisible: boolean;
    docVehVisible: boolean;
    infoPropVisible: boolean;
    contratsVisible: boolean;
    historiqueVisible: boolean;
    operationVisible: boolean;
    sinistreVisible: boolean;
    titreStatutParam = "titreStatu";
    infoVehParam = "infoVehParam";
    docVehParam = "docVehParam";
    infoPropParam = "infoPtopParam";
    contratsParam = "contratsParam";
    historiqueParam = "historiqueParam";
    operationParam = "operationParams";
    sinistreParam = "sinistreParams";
    listeOperationsWithStatut;
    tableauOperations: Array<Operation>;

    statutContratEnCoursDeValidationLoc = statutContratEnCoursDeValidation;
    statutContratActifLoc = statutContratActif;
    statutContratValideNonActifLoc = statutContratValideNonActif;
    statutContratSuspenduLoc = statutContratSuspendu;
    statutContratResilieLoc = statutContratResilie;
    statutContratExpireLoc = statutContratExpire;
    colonneReseau: boolean = false;

    TabAttributs = [];
    TabParentAttributsAssureDisplay: AttributAussure[] = [];
    //Parent Attributs
    showImmatriculation: boolean = false;
    showCarteGrise: boolean = false;
    showNIV: boolean = false;
    showTypeVehicule: boolean = false;
    showMarqueVehicule: boolean = false;
    showAppelationCom: boolean = false;
    showMarqueMoto: boolean = false;
    showAppelationComMoto: boolean = false;
    showDateFirstCir: boolean = false;
    showValPuissance: boolean = false;
    showNombrePlace: boolean = false;
    showValeurNeuve: boolean = false;
    showValeurDeclare: boolean = false;
    showValRemorque: boolean = false;
    showValUtilisation: boolean = false;
    showDateVisiteTech: boolean = false;
    showCategorieVehicule: boolean = false;
    //End Parent Attributs
    //More Attributs
    showCylyndre: boolean = false;
    showTypeEnergie: boolean = false;
    showTypeCarosserie: boolean = false;
    isContratValid: boolean = false;
    connectedClient: Client;
    TabContrats: Contrat2[] = [];
    constructor(
        private navCtrl: NavController,

        public utilsService: UtilsService,
        private formBuilder: FormBuilder,
        private loadingCtrl: LoadingController,
        private serviceCompte: CompteService,
        private listeValeurService: ListeValeurService,
        private assureContratService: AssureContratService,
        public modalController: ModalController
    ) {
        this.objet = new ObjetAssures2();
        //this.ObjetAuto = new Automobile();
        this.objet.proprietaire = new Client();
        //this.objet.proprietaire.contact =  new ContactClient() ;
    }

    ngOnInit() {
        this.chargerInfosObjetInSession();
        this.LoadClientConnectedFromSession();
    }

    ionViewWillEnter() {
        this.initialiserSectiontoInvisible();
        this.chargerInfosObjetInSession();
        this.LoadClientConnectedFromSession();
    }
    async chargerInfosObjetInSession() {
        const loader = await this.loadingCtrl.create({
            message: "Chargement en cours",
        });

        loader.present();
        this.objet = JSON.parse(window.sessionStorage.getItem(OBJETSESSION));
        this.objet.attributs = this.objet.attributs.sort((a, b) =>
            a.attribut.ordreAffFront > b.attribut.ordreAffFront
                ? 1
                : b.attribut.ordreAffFront > a.attribut.ordreAffFront
                ? -1
                : 0
        );
        if (this.objet == null) {
            this.utilsService.alertNotification(
                "La session a expiré veuillez vous reconnecter"
            );
            this.navCtrl.navigateRoot(LIENLOGIN);
        } else {
            console.log(this.objet);
            let objet: ObjetAssures2 = this.objet;
            delete objet.TabAtrributs;
            this.assureContratService
                .getContratByObjetAssure2(objet)
                .subscribe((data: Contrat2[]) => {
                    if (data) {
                        console.log("objet contrat", data);
                        this.TabContrats = [];
                        data.forEach((item) => {
                            this.TabContrats.push(item);
                            console.log(
                                this.checkIfContratObjetIsValid(
                                    item.dateEffet,
                                    item.dateCloture
                                )
                            );
                            if (
                                this.checkIfContratObjetIsValid(
                                    item.dateEffet,
                                    item.dateCloture
                                )
                            ) {
                                console.log("Objet contrat valid");
                                this.isContratValid = true;
                            } else console.log("Objet contrat not valid");
                        });
                        loader.dismiss();
                    }
                });

            //this.TabAttributs = this.objet.TabAtrributs;
            console.log("TabAttributs", this.TabAttributs);
            this.objet.attributs.forEach((att) => {
                switch (att.attribut.code) {
                    case "immatriculation":
                        this.showImmatriculation = true;
                        break;
                    case "numeroCartegrise":
                        this.showCarteGrise = true;
                        break;
                    case "NIV":
                        this.showNIV = true;
                        break;
                    case "valTypeVéhicule":
                        this.showTypeVehicule = true;
                        break;
                    case "marqueVehiculeAuro":
                        this.showMarqueVehicule = true;
                        break;
                    case "appellationCom":
                        this.showAppelationCom = true;
                        break;
                    case "marqueVehiculeMoto":
                        this.showMarqueMoto = true;
                        break;
                    case "appellationComMoto":
                        this.showAppelationComMoto = true;
                        break;
                    case "valDate1ereCirculation":
                        this.showDateFirstCir = true;
                        break;
                    case "valPuissance":
                        this.showValPuissance = true;
                        break;
                    case "valTypeEnergie":
                        this.showTypeEnergie = true;
                        break;
                    case "valCylindree":
                        this.showCylyndre = true;
                        break;
                    case "valNombrePlace":
                        this.showNombrePlace = true;
                        break;
                    case "mntValeurNeuve":
                        this.showValeurNeuve = true;
                        break;
                    case "mntValeurDeclaree":
                        this.showValeurDeclare = true;
                        break;
                    case "valRemorque":
                        this.showValRemorque = true;
                        break;
                    case "valUtilisationVehiculeMoto":
                        this.showValUtilisation = true;
                        break;
                    case "valTypeCarosserieAuto":
                        this.showTypeCarosserie = true;
                        break;
                    case "valCategorieVehicule":
                        this.showCategorieVehicule = true;
                        break;
                    case "dateVisiteTech":
                        this.showDateVisiteTech = true;
                        break;
                }
            });
            //console.log("sousBrancheParentAttributs", this.sousBrancheParentAttributs);
        }
        if (this.objet.proprietaire != null)
            //this.contactProprietaire =this.objet.proprietaire.contact ;
            this.utilsService.getDate().subscribe(
                (dateFormatMobile) => {
                    this.date = new Date(dateFormatMobile.date);
                },
                (error) => {
                    let message =
                        "Oups le serveur est introuvable verfiez votre connexion et recommencez svp";
                    this.utilsService.alertNotification(message);
                    this.navCtrl.navigateRoot(LIENLOGIN);
                }
            );
        //this.traitementDate() ;
        //this.verifierIfColonneReseau();

        this.regrouperListeOperationEtStatut().then(() => {
            console.log(this.listeOperationsWithStatut);
            this.listeOperationsWithStatut.sort((a, b) => {
                let res = this.compare(a, b);
                console.log(res);

                return res;
            });
        });

        console.log(this.listeOperationsWithStatut);
    }

    /**
     * * Cette fonction permet de récuperer
     * * le client connecté en session
     */
    LoadClientConnectedFromSession() {
        if (window.sessionStorage.getItem(SessionCompte)) {
            let data = JSON.parse(window.sessionStorage.getItem(SessionCompte));
            this.connectedClient = data.client;
            console.log("Client connected", this.connectedClient);
        }
    }

    /**
     * *Cette fonction permet de verifier
     * *si un contrat est toujours valide
     * *en vérifiant si la date du jour
     * *est toujours dans l'intervalle entre
     * *la date d'effet et la date de clotûre
     * @param dateEffet
     * @param dateCloture
     */
    checkIfContratObjetIsValid(dateEffet, dateCloture): Boolean {
        var currentDate = new Date().toJSON().slice(0, 10);
        var check = new Date(currentDate);
        console.log(check);
        var from = new Date(dateEffet);
        var to = new Date(dateCloture);
        var check = new Date(currentDate);
        console.log(check > from && check < to);

        return check > from && check < to;
    }

    /**
     * * A revoir quand le contrat sera
     * * disponible dans l'objet
     */
    traitementDate() {
        // if(this.objet.contrats !=null && this.objet.contrats.length !=0)
        // {
        //   this.objet.contrats.forEach(element => {
        //     element.dateEffet = new Date(element.dateEffet) ;
        //     element.dateCloture = new Date(element.dateCloture) ;
        //   });
        // }
        //this.objet.controleTechnique.dateValidite = new Date (this.objet.controleTechnique.dateValidite) ;
    }

    voirContratObjet(contrat: Contrat2) {
        contrat.objetAssures = [];
        //contrat.objetAssures.push(this.objet);
        console.log(contrat);

        window.sessionStorage.setItem(CONTRATSESSION, JSON.stringify(contrat));
        window.sessionStorage.setItem(
            OBJETSESSION,
            JSON.stringify(contrat.objetAssures[0])
        );
        window.sessionStorage.setItem(OriginePageMonObjet, "true");
        this.navCtrl.navigateRoot("/contrat");
    }
    voirContratObjetFromOperation(operation) {
        this.utilsService.alertNotification(
            "la redirection ne marche pas avec lesconstantes puisque ya des donnée obligatoires manquantes"
        );
        console.log(operation.contrat);
        window.sessionStorage.setItem(
            CONTRATSESSION,
            JSON.stringify(operation.contrat)
        );
        window.sessionStorage.setItem(
            OperationActifDeplie,
            JSON.stringify(operation)
        );
        this.navCtrl.navigateForward("/contrat");
    }
    displayDate(date: Date): String {
        date = new Date(date);
        //console.log("Date visite", date)
        let jour, mois;
        let dd = date.getDate();

        let mm = date.getMonth() + 1;
        let yyyy = date.getFullYear();
        if (dd < 10) {
            jour = "0" + dd;
        } else jour = dd;

        if (mm < 10) {
            mois = "0" + mm;
        } else mois = mm;
        return jour + "/" + mois + "/" + yyyy;
    }
    diffdate(d1, d2, u) {
        const d1Format = new Date(d1);
        let div = 1;
        switch (u) {
            case "seconde":
                div = 1000;
                break;
            case "minute":
                div = 1000 * 60;
                break;
            case "heure":
                div = 1000 * 60 * 60;
                break;
            case "jour":
                div = 1000 * 60 * 60 * 24;
                break;
        }

        var Diff = d1Format.getTime() - d2.getTime() + 1;
        return Math.ceil(Diff / div);
    }
    pageModifier() {
        this.utilsService.initialiserSessionRoutine();
        sessionStorage.setItem(
            typeRoutine,
            JSON.stringify(typeRoutineEditObject)
        );
        sessionStorage.setItem(OBJETSESSION, JSON.stringify(this.objet));
        this.navCtrl.navigateForward("/assurinfove");
        //this.formulaireModification() ;
    }

    enregistrer() {
        /**  Meme routine que modifier contact   **/
        //appel service modele modifier(this.objet)
        //si modification a reussi
        this.loadingCtrl.dismiss();
        this.utilsService.alertNotification("modifié avec succs");
        //si modification a echoué
        //loader.dismiss()
        //resetInfos() ; garder les anciennes informations notifier le probleme
        //this.autoriserModifier =false ;
    }
    /**
     * * Cette fonction permet d'assuer
     * * un objet
     */
    assurer() {
        this.utilsService.initialiserSessionRoutine();
        console.log(this.objet);
        if (this.objet.typeba.parent == null) {
            switch (this.objet.typeba.libelle) {
                case "AUTOMOBILE":
                    let simulationActif = new Simulation();
                    simulationActif.origineRoutine = OriginePageMonObjet;
                    //simulationActif.objet =this.objet ;
                    sessionStorage.setItem(
                        simulationActifenSession,
                        JSON.stringify(simulationActif)
                    );
                    window.sessionStorage.setItem(
                        typeRoutine,
                        JSON.stringify(typeRoutineSimulation)
                    );
                    this.navCtrl.navigateForward("/choiceoption");
                    break;
            }
        } else {
            switch (this.objet.typeba.parent.libelle) {
                case "AUTOMOBILE":
                    let simulationActif = new Simulation();
                    simulationActif.origineRoutine = OriginePageMonObjet;
                    //simulationActif.objet =this.objet ;
                    sessionStorage.setItem(
                        simulationActifenSession,
                        JSON.stringify(simulationActif)
                    );
                    window.sessionStorage.setItem(
                        typeRoutine,
                        JSON.stringify(typeRoutineSimulation)
                    );
                    this.navCtrl.navigateForward("/choiceoption");
                    break;
            }
        }
    }

    async formulaireModification() {
        let ObjetAssureDto: ObjetAssuresDto;
        let TabAttributsParentAndFilsObjets: AttributAussureBrancheDto[] = [];
        ObjetAssureDto = new ObjetAssuresDto();

        ObjetAssureDto.attributs = new Array(this.objet.attributs.length);
        let count = 0;
        this.objet.attributs.forEach((element: any, index) => {
            count += 1;
            console.log(count);

            ObjetAssureDto.id = this.objet.id;
            ObjetAssureDto.intitule = this.objet.intitule;
            ObjetAssureDto.validationCA = this.objet.validationCA;
            ObjetAssureDto.validationOP = this.objet.validationOP;
            ObjetAssureDto.typeba = this.objet.typeba;
            ObjetAssureDto.numeroTitulaire = this.objet.numeroTitulaire;
            ObjetAssureDto.nomTitulaire = this.objet.nomTitulaire;
            ObjetAssureDto.prenomTitulaire = this.objet.prenomTitulaire;
            ObjetAssureDto.proprietaire = this.objet.proprietaire;
            ObjetAssureDto.infosValide = this.objet.infosValide;
            ObjetAssureDto.TabAtrributs = this.objet.TabAtrributs;
            ObjetAssureDto.attributs[index] = element;
            let listeValeurDto: ListeValeur[];
            if (element.attribut.codeListeVal) {
                switch (element.attribut.codeListeVal) {
                    case element.attribut.codeListeVal:
                        this.onSelectListeVal(
                            element.attribut.codeListeVal
                        ).then((data) => {
                            element.attribut["listevaleur"] = data;
                            ObjetAssureDto.attributs[index] = element;
                        });
                        break;
                }
            }
        });
        //this.TabAttributsParentAndFilsObjets = [];
        if (count == this.objet.attributs.length) {
            let countPush = 0;

            ObjetAssureDto.attributs.forEach((item) => {
                countPush += 1;
                console.log("countPush", countPush);
                if (
                    item.attribut.code == "valTypeCarosserieAuto" ||
                    item.attribut.code == "valCategorieVehicule" ||
                    item.attribut.code == "valUtilisationVehiculeMoto" ||
                    item.attribut.code == "valTypeVehicule"
                )
                    //item.valeur = item.valeur.substr(8);
                    TabAttributsParentAndFilsObjets.push(item.attribut);
            });
            if (countPush == ObjetAssureDto.attributs.length) {
                console.log("ObjetAssureDto", ObjetAssureDto);
                console.log(
                    "TabAttributsParentAndFilsObjets",
                    TabAttributsParentAndFilsObjets
                );
                const modal = await this.modalController.create({
                    component: ModifierObjetPage,
                    componentProps: {
                        ObjetAssurePassed: this.objet,
                        TabAttributsParentAndFilsObjetsPassed: TabAttributsParentAndFilsObjets,
                        ClientConnectedPassed: this.connectedClient
                    },
                });
                return await modal.present();
            }
        }



    }

    /**
   * *Chargement des listes de valeurs
   * *selon le code
   */
  onSelectListeVal(onSelectListeVal: string) {
    return new Promise<ListeValeur[]>((resolve, reject) => {
      this.listeValeurService
        .getAllListeValeur(onSelectListeVal)
        .subscribe(
          (data) => {
            resolve(data);
          },
          (error) => {
            reject(error);
          }
        );
    });
  }

    initialiserSectiontoInvisible() {
        this.titreStatutVisible = true;
        this.infoVehVisible = true;
        this.docVehVisible = false;
        this.infoPropVisible = false;
        this.contratsVisible = false;
        this.historiqueVisible = false;
        this.operationVisible = false;
        this.sinistreVisible = false;
    }
    affichageDetails(section, visible: boolean) {
        switch (section) {
            case this.titreStatutParam:
                this.titreStatutVisible = visible;
                break;
            case this.infoVehParam:
                this.infoVehVisible = visible;
                break;
            case this.contratsParam:
                this.contratsVisible = visible;
                break;
            case this.docVehParam:
                this.docVehVisible = visible;
                break;
            case this.infoPropParam:
                this.infoPropVisible = visible;
                break;
            case this.historiqueParam:
                this.historiqueVisible = visible;
                break;
            case this.operationParam:
                this.operationVisible = visible;
                break;
            case this.sinistreParam:
                this.sinistreVisible = visible;
                break;
        }
    }
    async chargerControleTechnique() {
        const loader = await this.loadingCtrl.create({
            message: "",
        });

        loader.present();
        // To download the PDF file
        this.serviceCompte.download(
            CONTROLETECHNIQUEPATHNAME,
            this.objet.id,
            downloadControleTechnique
        );
    }
    async chargerCarteGrise() {
        const loader = await this.loadingCtrl.create({
            message: "",
        });

        loader.present();
        // To download the PDF file
        this.serviceCompte.download(
            CARTEGRISEPATHNAME,
            this.objet.id,
            downloadCarteGrise
        );
    }
    /**
     * ici on compare les operations d'abord par le statut puis par la date de cloture
     * chaq
     * @param a
     * @param b
     */
    compare(a, b) {
        let poidsStatutA, poidsStatutB;
        let res;

        switch (a.statut) {
            case statutContratEnCoursDeValidation:
                poidsStatutA = 1;

                break;
            case statutContratActif:
                poidsStatutA = 2;
                break;
            case statutContratValideNonActif:
                poidsStatutA = 2;
                break;
            case statutContratSuspendu:
                poidsStatutA = 3;
                break;
            case statutContratResilie:
                poidsStatutA = 3;
                break;
            case statutContratExpire:
                poidsStatutA = 3;
                break;
            default:
                poidsStatutA = 3;
        }
        switch (b.statut) {
            case statutContratEnCoursDeValidation:
                poidsStatutB = 1;

                break;
            case statutContratActif:
                poidsStatutB = 2;
                break;
            case statutContratValideNonActif:
                poidsStatutB = 2;
                break;
            case statutContratSuspendu:
                poidsStatutB = 3;
                break;
            case statutContratResilie:
                poidsStatutB = 3;
                break;
            case statutContratExpire:
                poidsStatutB = 3;
                break;
            default:
                poidsStatutB = 3;
        }
        res = poidsStatutA - poidsStatutB;
        if (res == 0) {
            if (a.dateCloture > b.dateCloture) return -1;
            else if (a.dateCloture < b.dateCloture) return 1;
            else return 0;
        }
        console.log(res);
        return res;
    }
    /**
     * ! A revoir quand le contrat sera
     * ! disponible dans l'objet
     */
    async regrouperListeOperationEtStatut() {
        this.listeOperationsWithStatut = [];
        // this.objet.contrats.forEach(contrat => {
        //   contrat.operations.forEach(operation => {
        //     switch(operation.statut)
        //     {
        //       case statutContratEnCoursDeValidation: this.listeOperationsWithStatut.push(operation)

        //       break ;
        //       case statutContratActif:  this.listeOperationsWithStatut.push(operation)
        //       break ;
        //       case  statutContratValideNonActif: this.listeOperationsWithStatut.push(operation)
        //       break ;
        //       case statutContratSuspendu:  this.listeOperationsWithStatut.push(operation)
        //       break ;
        //       case statutContratResilie:  this.listeOperationsWithStatut.push(operation)
        //       break ;
        //       case statutContratExpire:  this.listeOperationsWithStatut.push(operation)
        //       break ;
        //       default :  this.listeOperationsWithStatut.push(operation) ;
        //     }

        //   });

        // });
    }
    /**
     * ! A revoir quand le contrat sera
     * ! disponible dans l'objet
     */
    verifierIfColonneReseau() {
        console.log("colonneReseau non null?");
        // this.objet.contrats.forEach(contrat => {
        //   console.log(contrat.reseau)
        //   if(contrat.reseau!=null)
        //   {
        //     console.log("colonneReseau non null")
        //     this.colonneReseau = true
        //   }

        // });
    }
}

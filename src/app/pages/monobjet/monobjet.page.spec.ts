import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MonobjetPage } from './monobjet.page';

describe('MonobjetPage', () => {
  let component: MonobjetPage;
  let fixture: ComponentFixture<MonobjetPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MonobjetPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MonobjetPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

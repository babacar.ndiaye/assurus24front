import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';
import { ComponentsModule } from '../../components/components.module';

import { IonicModule } from '@ionic/angular';

import { MonobjetPage } from './monobjet.page';
import { IonicSelectableComponent, IonicSelectableModule } from 'ionic-selectable';

const routes: Routes = [
  {
    path: '',
    component: MonobjetPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    IonicModule,
    RouterModule.forChild(routes),
    ComponentsModule,
    IonicSelectableModule
  ],
  declarations: [MonobjetPage]
})
export class MonobjetPageModule {}

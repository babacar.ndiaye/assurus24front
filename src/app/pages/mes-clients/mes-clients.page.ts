import { Component, OnInit } from '@angular/core';
import { LIENACCUEILCOMMERCIAL, LIENMESCLIENTS, typeRoutineProfil, routineInscriptionClientReseau, LIENSELECTIONPROFIL, CLIENTENSESSION, LIENPAGECLIENTCOMMERCIAL } from 'src/environments/environment';
import { ContactClient } from 'src/app/models/contact-client';
import { ComponentsModule } from 'src/app/components/components.module';
import { Client } from 'src/app/models/client';
import { UtilsService } from 'src/app/services/utils.service';
import { NavController } from '@ionic/angular';

@Component({
  selector: 'app-mes-clients',
  templateUrl: './mes-clients.page.html',
  styleUrls: ['./mes-clients.page.scss'],
})
export class MesClientsPage implements OnInit {
  lienAccueilCommercial = LIENACCUEILCOMMERCIAL ;
  lienMesClients = LIENMESCLIENTS ;
  clients  :Array<Client> = [] ;
  critereFiltre ;
  valCherche ;
 contact : ContactClient ;
  filtreByNom="filtreByNom"
  clientsWhithoutFilter: any;
  constructor(
    public utilsService: UtilsService ,
    public navCtrl : NavController ,
  ) {
    this.contact = new ContactClient() ;
   }

  ngOnInit() {
  }
  ionViewWillEnter(){
    // this.utilsService.recupererCompte(LIENMESCLIENTS).then(
    //   contact=>{this.contact =contact ;
    //             this.recupererClientsReseaux().then(
    //               ()=>{
    //                 console.log(this.clients)
    //                 //trier les clients par ordre alphabetique du nom
    //                 this.clients.sort(
    //                   (a,b)=>{
    //                     let res = a.compte.contact.nom>b.compte.contact.nom
    //                     if(res)
    //                        return 1
    //                     else
    //                       return -1
    //                   }
    //                 )
    //                   this.clientsWhithoutFilter = this.clients
    //               }
    //             ) ;

    //   }

    // ) ;

    console.log("contact actuel"+this.contact) ;

  }

  async recupererClientsReseaux(){
    this.utilsService.recupererClientsReseaux().then(
     clients=>{
               this.clients = clients ;
      }
    )
   }
   onSearchClient() {
    if(this.critereFiltre==null)
    {
     this.utilsService.alertNotification("veuillez choisir un critère de recherche")
    }
    else
    {
     this.clients=this.clientsWhithoutFilter ;
     const val = this.valCherche;
     console.log("filtre")
     console.log(val)
     console.log(this.critereFiltre)
     if (val && val.trim() !== '') {
       console.log("filtre")
       this.clients = this.clients.filter(client => {

         switch(this.critereFiltre){
           case this.filtreByNom :
             if(client.compte.contact )
             return client.compte.contact.nom.toLowerCase().indexOf(val.trim().toLowerCase())>-1
            else return false

           break;

         }

       });
     }


    }

 }
 nouveauClient(){
  sessionStorage.setItem(typeRoutineProfil,JSON.stringify(routineInscriptionClientReseau))
  this.navCtrl.navigateForward(LIENSELECTIONPROFIL);

 }
 voirClient(client){
  sessionStorage.setItem(CLIENTENSESSION,JSON.stringify(client)) ;
  this.navCtrl.navigateForward(LIENPAGECLIENTCOMMERCIAL) ;
 }

}

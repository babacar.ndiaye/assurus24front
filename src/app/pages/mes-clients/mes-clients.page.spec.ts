import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MesClientsPage } from './mes-clients.page';

describe('MesClientsPage', () => {
  let component: MesClientsPage;
  let fixture: ComponentFixture<MesClientsPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MesClientsPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MesClientsPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PageClientCommercialPage } from './page-client-commercial.page';

describe('PageClientCommercialPage', () => {
  let component: PageClientCommercialPage;
  let fixture: ComponentFixture<PageClientCommercialPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PageClientCommercialPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PageClientCommercialPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import { Component, OnInit } from '@angular/core';
import { UtilsService } from 'src/app/services/utils.service';
import { CLIENTENSESSION } from 'src/environments/environment';
import { LoadingController } from '@ionic/angular';
import { Client } from 'src/app/models/client';

@Component({
  selector: 'app-page-client-commercial',
  templateUrl: './page-client-commercial.page.html',
  styleUrls: ['./page-client-commercial.page.scss'],
})
export class PageClientCommercialPage implements OnInit {
  nom = "Abdou" ;
  montantCompteCredit = "45000FCFA" ;
  largeur =screen.width ;
  clients :Array<Client> ;
  clientSelection
  LIENMESCLIENTS
  LIENPAGECLIENTCOMMERCIAL
  constructor(
    public utilsService : UtilsService ,
    public loadingCtrl : LoadingController
  ) { }

  ngOnInit() {
  }
  ionViewWillEnter(){
    this.recupererClientsReseaux().then(
      ()=>{
        console.log(this.clients)
        this.clientSelection =JSON.parse(sessionStorage.getItem(CLIENTENSESSION))
        console.log(this.clientSelection)

      }
    
    )

   
  }
  async recupererClientsReseaux(){
    this.utilsService.recupererClientsReseaux().then(
     clients=>{
               this.clients = clients ;
      }
    )
   }
   async portChangeObjet() {
     console.log(this.clientSelection)
   if(this.clientSelection != null)
   {
   
     const loader = await this.loadingCtrl.create({
      message: '',
    });
    sessionStorage.setItem(CLIENTENSESSION,JSON.stringify(this.clientSelection)) ;
     window.location.reload() ;
   }
    
    
  }
  getNomClient(){
    return "pape" ;

  }

}

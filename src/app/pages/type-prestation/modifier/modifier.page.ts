import { Component, OnInit } from '@angular/core';
import { FormControl, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';

import { NavController } from '@ionic/angular';
import { TypePrestation } from '../../../models/type-prestation';
import { typePrestationservice } from '../../../services/type-prestation.service';

@Component({
  selector: 'app-modifier',
  templateUrl: './modifier.page.html',
  styleUrls: ['./modifier.page.scss'],
})
export class ModifierPage implements OnInit {

  nameControl: FormControl;
  logoControl: FormControl;
  descriptionControl: FormControl;
  formGroup: FormGroup;
  typePrestationId: number;
  typePrestation: TypePrestation;

  constructor(private builder: FormBuilder, private route: ActivatedRoute,
    private nav : NavController, private service:  typePrestationservice, private router : Router) {
    this.typePrestationId = Number(this.route.snapshot.paramMap.get('id'));
    this.service.getTypePrestation(this.typePrestationId).subscribe(typePrestation => {
      this.typePrestation = typePrestation;
      this.nameControl = new FormControl(this.typePrestation.name, [Validators.required, Validators.minLength(2)]);
      this.logoControl = new FormControl(this.typePrestation.logo, Validators.required);
      
      this.formGroup = this.builder.group({
        name: this.nameControl,
        logo: this.logoControl,
      
      })
    })

  }

  modifier(): void {
    this.typePrestation.name=this.formGroup.get('name').value ;
    this.typePrestation.logo=this.formGroup.get('logo').value ;
    this.typePrestation.id=this.typePrestationId;
    this.service.updateTypePrestation( this.typePrestation).subscribe(typePrestation=> {
      this.nav.back();
    })
  }

  ngOnInit() {
  }

}

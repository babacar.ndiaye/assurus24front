import { Component, OnInit } from '@angular/core';
import { TypePrestation } from '../../../models/type-prestation';
import { typePrestationservice } from '../../../services/type-prestation.service';
import { Router } from '@angular/router';
import { ToastController } from '@ionic/angular';

@Component({
  selector: 'app-ajouter',
  templateUrl: './ajouter.page.html',
  styleUrls: ['./ajouter.page.scss'],
})
export class AjouterPage implements OnInit {

  typePrestation: TypePrestation;
  constructor(private service: typePrestationservice, public toastController: ToastController,
    private route: Router) {
    this.typePrestation = new TypePrestation();
  }

  ngOnInit() {
  }
  async presentToast(message: string) {
    const toast = await this.toastController.create({
      message: message,
      duration: 2000,
      color: 'success',
      position: 'top'
    });
    toast.present();
  }
  ajouterTypePrestation(): void {
    this.service.postTypePrestation(this.typePrestation).subscribe(typePrestation => {
      this.presentToast("TypePrestation ajouté avec succès.");
      this.route.navigate(['/type-prestation']);
    }, error => {

    })
  }

}

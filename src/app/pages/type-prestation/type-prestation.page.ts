import { Component, OnInit } from '@angular/core';
import { TypePrestation } from '../../models/type-prestation';
import { typePrestationservice } from '../../services/type-prestation.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-type-prestation',
  templateUrl: './type-prestation.page.html',
  styleUrls: ['./type-prestation.page.scss'],
})
export class TypePrestationPage implements OnInit {
  ngOnInit(): void {
   
  }

  ionViewDidEnter(){
    this.getTypePrestations();

  }

  typePrestations : TypePrestation [];
  constructor(private route : Router, private api : typePrestationservice) {

    this.getTypePrestations();
  }

  modifierTypePrestation (id:any):void
  {
    this.route.navigate(["type-prestation/modifier",id]);
  }

  getTypePrestations() : void
  {
    this.api.getTypePrestations().subscribe(Response=>{
      this.typePrestations = Response;
    });
  }
  delete(typePrestation:TypePrestation):void
  {
    this.api.deleteTypePrestation(typePrestation.id).subscribe(typePrestation =>{
      this.getTypePrestations();
    });
  }
}

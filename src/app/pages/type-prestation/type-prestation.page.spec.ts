import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TypePrestationPage } from './type-prestation.page';

describe('TypePrestationPage', () => {
  let component: TypePrestationPage;
  let fixture: ComponentFixture<TypePrestationPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TypePrestationPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TypePrestationPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

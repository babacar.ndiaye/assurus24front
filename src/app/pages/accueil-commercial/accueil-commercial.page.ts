import { Component, OnInit } from '@angular/core';
import { ContactClient } from 'src/app/models/contact-client';
import { SessionCompte, LIENMESCLIENTS, DescAutos, DescSante, DescMoto, DescMaison, DescBateau, DescVoyage, DescSinistre, DescDepannage, DescConstat, DescExpertise, LIENEPOCNO, LIENACCUEILCOMMERCIAL, LIENLOGIN, LIENMONCOMPTECREDIT } from 'src/environments/environment';
import { UtilsService } from 'src/app/services/utils.service';
import { NavController, AlertController, MenuController } from '@ionic/angular';
import { AuthService } from 'src/app/services/auth.service';
import { Compte } from 'src/app/models/compte';
import { ClientReseau } from 'src/app/models/client-reseau';

@Component({
    selector: 'app-accueil-commercial',
    templateUrl: './accueil-commercial.page.html',
    styleUrls: ['./accueil-commercial.page.scss'],
})
export class AccueilCommercialPage implements OnInit {
    compte: Compte;
    ClientReseau: ClientReseau;
    lienMesClients = LIENMESCLIENTS;
    lienMonCompteCredit= LIENMONCOMPTECREDIT;
    constructor(private utilsService: UtilsService, private authService: AuthService, private navCtrl: NavController, private alertCtrl: AlertController, private menuCtrl: MenuController) {
        this.compte= new Compte();
    }

    ngOnInit() {
    }
    ionViewWillEnter() {
        this.menuCtrl.enable(true);
        this.getUserConnectedInfos();
        // this.utilsService.recupererCompte(LIENACCUEILCOMMERCIAL).then(
        //     (compte)=> {
        //         this.compte= compte;
        //     }
        // );
        // console.log("compte actuel", this.compte);
    }

    /**
   *  * Cette fonction permet de récupérer les infos
   *  * de l'utilisateur à partir de la session
   */
  getUserConnectedInfos(){
    //alert('On Founction')
      if(sessionStorage.getItem(SessionCompte)){
          let data =JSON.parse(sessionStorage.getItem(SessionCompte));
          this.compte =data;
          console.log("compte actuel", this.compte);
      }else{
        let message = "Veuillez vous connecter pour continuer";
        this.utilsService.alertNotification(message);
        this.navCtrl.navigateRoot(LIENLOGIN);
      }
  }


    recupererInformationAuto(branche) {
        switch (branche) {
            case "auto": this.recupererInformationBranche("Branche Automobile", DescAutos); break;
            case "sante": this.recupererInformationBranche("Branche Sante", DescSante); break;
            case "moto": this.recupererInformationBranche("Branche Moto", DescMoto); break;
            case "maison": this.recupererInformationBranche("Branche Maison", DescMaison); break;
            case "bateau": this.recupererInformationBranche("Branche Bateau", DescBateau); break;
            case "voyage": this.recupererInformationBranche("Branche Voyage", DescVoyage); break;
            case "sinistre": this.recupererInformationBranche("Branche Sinistre", DescSinistre); break;
            case "depannage": this.recupererInformationBranche("Service Depannage", DescDepannage); break;
            case "constat": this.recupererInformationBranche("Service Constat", DescConstat); break;
            case "expertise": this.recupererInformationBranche("Service Demande d'expertise", DescExpertise); break;

        }

    }

    recupererInformationBranche(header, message) {

        this.alertNotification(header, message);

    }
    async alertNotification(header, message) {

        let changeLocation;


        changeLocation = await this.alertCtrl.create({
            header: header,
            message: message,


            buttons: [

                {
                    text: 'ok',
                    handler: data => {

                    }

                }
            ]
        });

        changeLocation.present();
    }
    deconnecter() {
        this.authService.logout()
        this.navCtrl.navigateRoot(LIENEPOCNO);;
    }
}

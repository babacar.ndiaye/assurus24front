import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AccueilCommercialPage } from './accueil-commercial.page';

describe('AccueilCommercialPage', () => {
  let component: AccueilCommercialPage;
  let fixture: ComponentFixture<AccueilCommercialPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AccueilCommercialPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AccueilCommercialPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

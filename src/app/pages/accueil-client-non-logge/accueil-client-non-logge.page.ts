import { Component, OnInit, ViewChild } from '@angular/core';

import { NavController, MenuController, ToastController, AlertController, LoadingController, IonSlides, Platform } from '@ionic/angular';
import { Compte } from 'src/app/models/compte';
import { idSessionUser, DescAutos, DescMoto, DescSante, DescMaison, DescBateau, DescVoyage, DescSinistre, DescDepannage, DescConstat, DescExpertise, UrlApi, banniereCom, LIENLOGIN, LIENEPOCNO, SessionCompte } from 'src/environments/environment';

import { OffrePromo } from 'src/app/models/OffrePromo';
import { Garantie } from 'src/app/models/garantie';
import { AuthService } from 'src/app/services/auth.service';
import { CompteService } from 'src/app/services/compte.service';
import { ObjetAssures } from 'src/app/models/objet-assures';
import { TypeBA } from 'src/app/models/type-ba';
import { UtilsService } from 'src/app/services/utils.service';
import { Client } from 'src/app/models/client';
import { ClientParticulier } from 'src/app/models/client-particulier';
import { TarifGarantie } from 'src/app/models/tarif-garantie';
import { GarantieCA } from 'src/app/models/garantie-ca';
import { AssureContrat } from 'src/app/models/assure-contrat';
import { Globalization } from '@ionic-native/globalization/ngx';
import { TranslateService } from '@ngx-translate/core';
import { ELocalNotificationTriggerUnit, LocalNotifications } from '@ionic-native/local-notifications/ngx';
@Component({
    selector: 'app-accueil-client-non-logge',
    templateUrl: './accueil-client-non-logge.page.html',
    styleUrls: ['./accueil-client-non-logge.page.scss'],
})
export class AccueilClientNonLoggePage implements OnInit {
    client: ClientParticulier;
    offrePromos;
    date: Date;
    contratsExpireBientot: any;
    urlApi = UrlApi;
    banniereComs: Array<banniereCom>;
    @ViewChild('slideWithNav') slideWithNav: IonSlides;


    @ViewChild('slideWithNavPromo') slideWithNavPromo: IonSlides;
    sliderOne: any;

    language: string;

    //Configuration for each Slider
    slideOptsOne = {
        initialSlide: 0,
        slidesPerView: 1,
        autoplay: true,
        speed: 0


    };
    isConnected: boolean;
    slideChanged(slideView) {
        switch (slideView) {
            case "promos": this.slideWithNavPromo.startAutoplay(); break;
            case "infos": this.slideWithNav.startAutoplay(); break;
        }



    }

    constructor(
        private authService: AuthService,
        private navCtrl: NavController,
        private serviceCompte: CompteService,
        public menuCtrl: MenuController,
        public toastCtrl: ToastController,
        public alertCtrl: AlertController,
        public loadingCtrl: LoadingController,
        public utilsService: UtilsService,
        private platform: Platform,
        private globalization: Globalization,
        private _translate: TranslateService,
        private localNotifications: LocalNotifications
    ) {
    }

    ionViewWillEnter() {
        this.menuCtrl.enable(true);
        let idSession = Number(window.sessionStorage.getItem(idSessionUser));
        this.recupererOffrePromos();
        this.recupererContratdontFinProche();
        // this.recupererCompte();
        console.log("client actuel", this.client);
        this.recupererBanieresCom();

    }
    ngOnInit() {
        this.getUserConnectedInfos()
        this.menuCtrl.enable(true);
        this.recupererOffrePromos();
        // this.recupererCompte();
        console.log("client actuel" + this.client);
    }
    getUserConnectedInfos() {
        if (!sessionStorage.getItem(SessionCompte)) {
            this.navCtrl.navigateRoot(LIENEPOCNO);
        }
        this.localNotifications.schedule({
            title: 'Connexion à Assurus24',
            text: "Vous êtes connectés ! aujourd'hui",
            smallIcon:"res://"
        })
        this.localNotifications.on("click").subscribe(data=>{
            this.navCtrl.navigateRoot('/notifications')

        })
    }
    deconnecter() {
        this.authService.logout()
        this.navCtrl.navigateRoot(LIENEPOCNO);
    }
    recupererInformationAuto(branche) {
        switch (branche) {
            case "auto": this.recupererInformationBranche("Branche Automobile", DescAutos); break;
            case "sante": this.recupererInformationBranche("Branche Sante", DescSante); break;
            case "moto": this.recupererInformationBranche("Branche Moto", DescMoto); break;
            case "maison": this.recupererInformationBranche("Branche Maison", DescMaison); break;
            case "bateau": this.recupererInformationBranche("Branche Bateau", DescBateau); break;
            case "voyage": this.recupererInformationBranche("Branche Voyage", DescVoyage); break;
            case "sinistre": this.recupererInformationBranche("Branche Sinistre", DescSinistre); break;
            case "depannage": this.recupererInformationBranche("Service Depannage", DescDepannage); break;
            case "constat": this.recupererInformationBranche("Service Constat", DescConstat); break;
            case "expertise": this.recupererInformationBranche("Service Demande d'expertise", DescExpertise); break;

        }

    }

    recupererInformationBranche(header, message) {
        this.slideWithNav.stopAutoplay();
        this.alertNotification(header, message);

    }
    //apres on va recuperer contact au lieu de compte
    // recupererCompte() {

    //     let compte  = JSON.parse(window.sessionStorage.getItem(SessionCompte));
    //     console.log(compte)
    //     this.client = compte.client
    //     if (this.client == null) {
    //         let message = "Oups la session a  expiré veuillez vous reconnecter";
    //         this.alertNotification("Notification", message);
    //         this.navCtrl.navigateRoot(LIENLOGIN);
    //     }

    // }
    async alertNotification(header, message) {

        let changeLocation;


        changeLocation = await this.alertCtrl.create({
            header: header,
            message: message,


            buttons: [

                {
                    text: 'ok',
                    handler: data => {
                        this.slideWithNav.startAutoplay();
                    }

                }
            ]
        });

        changeLocation.present();
    }
    recupererOffrePromos() {
        //On var recuperer les OffrePromos à partir duback getOffrePromos
        let typeBranche1: TypeBA;
        let TarifGarantie: TarifGarantie;
        let offrePromos: OffrePromo[];
        let typegarantie: GarantieCA;
        let AssureContrat: AssureContrat;
        let typeG1 = {
            id: 1,
            nom: "bris de glace",
            resume: "Soyez couvert en cas de vitres abimées",
            typeBA: typeBranche1,
            garanties: [],

            obligatoire: false,
            general: false,
            description: "Tout propriétaire d’un véhicule à moteur doit obligatoirement l’assurer avec, au minimum, une garantie responsabilité civile (également appelée « assurance au tiers ». Cette garantie couvre exclusivement les dommages que pourrait causer la voiture assuré à un tiers (passager, autre conducteur, piéton…) ou à un autre véhicule.Tout propriétaire d’un véhicule à moteur doit obligatoirement l’assurer avec, au minimum, une garantie responsabilité civile (également appelée « assurance au tiers ». Cette garantie couvre exclusivement les dommages que pourrait causer la voiture assuré à un tiers (passager, autre conducteur, piéton…) ou à un autre véhicule.Tout propriétaire d’un véhicule à moteur doit obligatoirement l’assurer avec, au minimum, une garantie responsabilité civile (également appelée « assurance au tiers ». Cette garantie couvre exclusivement les dommages que pourrait causer la voiture assuré à un tiers (passager, autre conducteur, piéton…) ou à un autre véhicule.Tout propriétaire d’un véhicule à moteur doit obligatoirement l’assurer avec, au minimum, une garantie responsabilité civile (également appelée « assurance au tiers ». Cette garantie couvre exclusivement les dommages que pourrait causer la voiture assuré à un tiers (passager, autre conducteur, piéton…) ou à un autre véhicule.",

        }

        typeBranche1 = {
            id: 1,

            nom: "Automobile",
            bas: [],
            description: DescAutos,
            typeGaranties: [],
            ConditionsDisposition: "<p>Quam ob rem circumspecta cautela observatum est deinceps </p> <p>et cum edita montium petere coeperint grassatores, loci iniquitati milites cedunt. ubi autem in planitie potuerint reperiri, quod contingit adsidue, nec exsertare lacertos nec crispare permissi tela, quae vehunt bina vel terna, pecudum ritu inertium trucidantur.Huic Arabia est conserta, ex alio latere Nabataeis contigua; opima varistatute conmerciorum castrisque oppleta validis et castellis, quae ad repellendos gentium vicinarum excursus sollicitudo pervigil veterum per oportunos saltus erexit et cautos. haec quoque civitates habet inter oppida quaedam ingentes Bostram et Gerasam atque Philadelphiam murorum firmitate cautissimas. hanc provinciae inposito nomine rectoreque adtributo obtemperare legibus nostris Traianus conpulit imperator incolarum tumore saepe contunso cum glorioso marte Mediam urgeret et Partho.Utque proeliorum periti rectores primo catervas densas opponunt et fortes, deinde leves armaturas, post iaculatores ultimasque subsidiales acies, si fors adegerit, iuvaturas, ita praepositis urbanae familiae suspensae digerentibus sollicite,</p> <p> quos insignes faciunt virgae dexteris aptatae velut tessera data castrensi iuxta vehiculi frontem omne textrinum incedit: huic atratum coquinae iungitur ministerium, dein totum promiscue servitium cum otiosis plebeiis de vicinitate coniunctis: postrema multitudo spadonum a senibus in pueros desinens, obluridi distortaque lineamentorum conpage deformes, ut quaqua incesserit quisquam cernens mutilorum hominum agmina detestetur memoriam Samiramidis reginae illius veteris, quae teneros mares castravit omnium prima velut vim iniectans naturae, eandemque ab instituto cursu retorquens, quae inter ipsa oriundi crepundia per primigenios seminis fontes tacita quodam modo lege vias propagandae posteritatis ostendit.Qui cum venisset ob haec festinatis itineribus Antiochiam, praestrictis palatii ianuis, contempto Caesare, quem videri decuerat, ad praetorium cum pompa sollemni perrexit morbosque diu causatus nec regiam introiit nec processit in publicum, sed abditus multa in eius moliebatur exitium addens quaedam relationibus supervacua, quas subinde dimittebat ad principem.Cuius acerbitati uxor grave accesserat incentivum, germanitate Augusti turgida supra modum, quam Hannibaliano regi fratris filio antehac Constantinus iunxerat pater, Megaera quaedam mortalis, inflammatrix saevientis adsidua, humani cruoris avida nihil mitius quam maritus; qui paulatim eruditiores facti processu temporis ad nocendum per clandestinos versutosque rumigerulos conpertis leviter addere quaedam male suetos falsa et placentia sibi discentes, adfectati regni vel artium nefandarum calumnias insontibus adfligebant.</p>",


        }
        // let g1: Garantie = {
        //     id: 1,
        //     libelle: "Axa bris de glace",
        //     typegarantie: typeG1,
        //     code: '',
        //     codeExterne: '',
        //     libelle: '',
        //     description: '',
        //     taux: 0,
        //     minimum: 0,
        //     franchise: 0,
        //     agemin: 0,
        //     agemax: 0,
        //     attributmontant: '',
        //     codeFormule: TarifGarantie,
        //     pack: false,
        //     typegarantie: typegarantie,
        //     montantTarif: 100,
        //     assure: AssureContrat



        // }
        // offrePromos = [
        //     {
        //         id: 1,
        //         titre: "2 mois offerts",
        //         description: " 2 Mois d'Assurance offert pour la souscription d'un Contrat",
        //         date_debut: new Date("05-04-2019"),
        //         date_fin: new Date("05-04-2019"),
        //         garantie: g1,
        //         dateDebut: new Date("05-04-2019"),
        //         dateFin: new Date("05-04-2019"),
        //         reduction: 10


        //     },
        //     {
        //         id: 2,
        //         titre: "3 mois offerts",
        //         description: " 3 Mois d'Assurance offert pour la souscription d'un Contrat",
        //         date_debut: new Date("05-04-2019"),
        //         date_fin: new Date("09-24-2020"),
        //         garantie: null,
        //         dateDebut: new Date("05-04-2019"),
        //         dateFin: new Date("05-04-2019"),
        //         reduction: 10

        //     }
        // ];
        let date = this.utilsService.getDate().subscribe(
            dateFormatMobile => { this.date = new Date(dateFormatMobile.date) },
            error => {
                let message = "Oups le serveur est introuvable verfiez votre connexion et recommencez svp";
                this.alertNotification("Notification", message);
                this.navCtrl.navigateRoot(LIENLOGIN);
            }
        );

        console.log(this.date);
        // this.offrePromos = new Array;
        // for (let i = 0; i < offrePromos.length; i++) {
        //     if (offrePromos[i].date_fin > this.date)
        //         this.offrePromos.push(offrePromos[i]);
        // }
        // console.log(this.offrePromos);

        // this.offrePromos.sort((a, b) => a.date_fin - b.date_fin);

    }
    recupererContratdontFinProche() {
        // let o2 = {
        //     id: 2,
        //     libelle: "Tcamry",
        //     contrats: [],
        //     matricule: "DK-2454-AB",
        //     statut: 1, infosValide: false,
        //     numCarteGrise: "44445",
        //     marque: "Toyota",
        //     modele: "hilux",
        //     carburant: "gasoil",
        //     typeBA: null,
        //     annee: 2015,
        //     puissanceFiscale: "18cv",
        //     nombrePlaces: 6,
        //     valeurDeclarative: 2500000,
        //     valeurInitiale: 1750000,
        //     controleTechnique: null, proprietaire: null
        // }
        // let o1 = {
        //     id: 2,
        //     libelle: "mustang",
        //     contrats: [],
        //     matricule: "DK-2454-AB",
        //     statut: 1, infosValide: false,
        //     numCarteGrise: "44445",
        //     marque: "Toyota",
        //     modele: "hilux",
        //     carburant: "gasoil",

        //     typeBA: null,
        //     annee: 2015,
        //     puissanceFiscale: "18cv",
        //     nombrePlaces: 6,
        //     valeurDeclarative: 2500000,
        //     valeurInitiale: 1750000,
        //     controleTechnique: null, proprietaire: null
        // }
        // let o3 = {
        //     id: 2,
        //     libelle: "mford aveo",
        //     contrats: [],
        //     matricule: "DK-2454-AB",
        //     statut: 1, infosValide: false,
        //     numCarteGrise: "44445",
        //     marque: "Toyota",
        //     modele: "hilux",
        //     carburant: "gasoil",

        //     typeBA: null,
        //     annee: 2015,
        //     puissanceFiscale: "18cv",
        //     nombrePlaces: 6,
        //     valeurDeclarative: 2500000,
        //     valeurInitiale: 1750000,
        //     controleTechnique: null, proprietaire: null
        // }
        // let o4 = {
        //     id: 2,
        //     libelle: "dodge charger",
        //     contrats: [],
        //     matricule: "DK-2454-AB",
        //     statut: 1, infosValide: false,
        //     numCarteGrise: "44445",
        //     marque: "Toyota",
        //     modele: "hilux",
        //     carburant: "gasoil",
        //     typeBA: null,
        //     annee: 2015,
        //     puissanceFiscale: "18cv",
        //     nombrePlaces: 6,
        //     valeurDeclarative: 2500000,
        //     valeurInitiale: 1750000,
        //     controleTechnique: null, proprietaire: null
        // }
        // let contratsEssais: Array<Contrat> = [
        //     {
        //         id: 1,
        //         police: "488-4445-552",
        //         statut: null, franchise: 25000, capitauxGarantis: 250000,
        //         objetAssures: [o1, o2],
        //         client: null,
        //         reseau: null,
        //         ca: null,
        //         operations: [],
        //         dateEffet: new Date("05-02-2019"),
        //         dateCloture: new Date("05-05-2021"),

        //     },
        //     {
        //         id: 2,
        //         police: "488-4445-552",
        //         statut: null, franchise: 25000, capitauxGarantis: 250000,
        //         objetAssures: [o2, o1],
        //         client: null,
        //         reseau: null,
        //         ca: null,
        //         operations: [],
        //         dateEffet: new Date("05-02-2019"),
        //         dateCloture: new Date("05-05-2020"),

        //     },
        //     {
        //         id: 3,
        //         police: "488-4445-552",
        //         statut: null, franchise: 25000, capitauxGarantis: 250000,
        //         objetAssures: [o3, o4],
        //         client: null,
        //         reseau: null,
        //         ca: null,
        //         operations: [],
        //         dateEffet: new Date("05-02-2019"),
        //         dateCloture: new Date("05-05-2020"),

        //     },




        // ]

        // this.contratsExpireBientot = new Array<Contrat>();
        // this.utilsService.getDate().subscribe(
        //     dateFormatMobile => {
        //         this.date = new Date(dateFormatMobile.date)
        //         contratsEssais.forEach(contrat => {
        //             if (contrat.dateCloture != null) {
        //                 let diffdate = this.diffdate(contrat.dateCloture, this.date, 'jour');
        //                 if (diffdate <= 15)
        //                     this.contratsExpireBientot.push(contrat);
        //             }


        //         });
        //     },
        //     error => {
        //         let message = "Oups le serveur est introuvable verfiez votre connexion et recommencez svp";
        //         this.utilsService.alertNotification(message);
        //         this.navCtrl.navigateRoot(LIENLOGIN);
        //     });

        //apres preliminaire back ou front
    }
    diffdate(d1, d2, u) {
        let div = 1
        switch (u) {
            case 'seconde': div = 1000;
                break;
            case 'minute': div = 1000 * 60
                break;
            case 'heure': div = 1000 * 60 * 60
                break;
            case 'jour': div = 1000 * 60 * 60 * 24
                break;
        }
        if (d1 != null && d2 != null)
            var Diff = d1.getTime() - d2.getTime() + 1;
        return Math.ceil((Diff / div))
    }
    recupererBanieresCom() {
        this.utilsService.recupererBanieresCom().then(
            liste => {
                this.banniereComs = liste;
            }

        )

    }

    /**GESTION LOGIQUE TRANSLATION DES LANGUES */
    ionViewDidEnter(): void {
        this._translateLanguage()
    }
    ionViewDidLoad() {
        this._translateLanguage();
    }

    public changeLanguage(): void {
        this._translateLanguage();
    }

    _translateLanguage(): void {
        this._translate.use(this.language);
        this._initialiseTranslation()
    }

    _initialiseTranslation(): void {
    }
    _initTranslate(language) {
        // Set the default language for translation strings, and the current language.
        this._translate.setDefaultLang('fr-FR');
        if (language) {
            this.language = language;
        }
        else {
            // Set your language here
            this.language = 'fr-FR';
        }
        this._translateLanguage();
    }

    getDeviceLanguage() {
        if (!this.platform.is('android') || !this.platform.is('ios')) {
            //alert('Navigateur')
            this._initTranslate(navigator.language);
        } else {
            this.globalization.getPreferredLanguage()
                .then(res => {
                    this._initTranslate(res.value)
                })
                .catch(e => { console.log(e); });
        }
    }
    /**END TRANSLATION */

}

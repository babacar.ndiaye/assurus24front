import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AccueilClientNonLoggePage } from './accueil-client-non-logge.page';

describe('AccueilClientNonLoggePage', () => {
  let component: AccueilClientNonLoggePage;
  let fixture: ComponentFixture<AccueilClientNonLoggePage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AccueilClientNonLoggePage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AccueilClientNonLoggePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import { Component, OnInit } from '@angular/core';
import { NavController } from '@ionic/angular';

@Component({
  selector: 'app-comptecredit',
  templateUrl: './comptecredit.page.html',
  styleUrls: ['./comptecredit.page.scss'],
})
export class ComptecreditPage implements OnInit {

  constructor(public navCtrl: NavController) { }

  ngOnInit() {
  }
  goToProfil() {
    this.navCtrl.navigateRoot('/home-results');
  }


}

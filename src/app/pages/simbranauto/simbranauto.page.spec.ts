import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SimbranautoPage } from './simbranauto.page';

describe('SimbranautoPage', () => {
  let component: SimbranautoPage;
  let fixture: ComponentFixture<SimbranautoPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SimbranautoPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SimbranautoPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

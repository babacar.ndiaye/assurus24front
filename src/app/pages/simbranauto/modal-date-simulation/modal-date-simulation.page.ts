import { Component, Input, OnInit, ViewChild } from '@angular/core';
import { NgForm } from '@angular/forms';
import { LoadingController, ModalController } from '@ionic/angular';
import { from } from 'rxjs';
import { UtilsService } from 'src/app/services/utils.service';

@Component({
  selector: 'app-modal-date-simulation',
  templateUrl: './modal-date-simulation.page.html',
  styleUrls: ['./modal-date-simulation.page.scss'],
})
export class ModalDateSimulationPage implements OnInit {

  @Input() dateDebut: Date;
  @Input() duree: any;
  @Input() dateFin: Date;
  @ViewChild('fin') inputFin;
  @ViewChild('debut') inputDebut;
  @ViewChild('jours') inputJours;
  @ViewChild('mois') inputMois;
  @ViewChild('annees') inputAnnees;
  @ViewChild('duree') inputDuree;

  dureeJour: number;
  dureeMois: number;
  dureeAnnee: number;
  dateDebutError: boolean = false;
  dateFinError: boolean = false;
  firstChangeDuree = true;
  monDuree: number;

  constructor(public modalCtrl: ModalController, public loadingCtrl: LoadingController, private utilsService: UtilsService) { }

  ngOnInit() {
    console.log(this.dateDebut, this.dateFin, this.duree);
    this.monDuree = this.duree;
  }
  onAjusteDate(e) {
    console.log(e.target.value);

  }
  ajustInputSecondeSession(input) {
    if (!this.firstChangeDuree) {
      ////console.log(this.inputFin.value)
      this.dateFinError = false;
      this.finOuDureeFirstOption(input);
    }
    else
      this.firstChangeDuree = false

  }
  ajustInput(nom) {
    switch (nom) {
      case "debut":
        this.dateDebutError = false;
        break;
      case "fin":
        ////console.log(this.inputFin.value)
        this.dateFinError = false; break;
    }
  }
  async finOuDureeFirstOption(input) {
    if (this.checkDureeFirstOption(input)) {
      this.dateFin = new Date(this.inputDebut.value);;
      ////console.log(this.dateFin)
      ////console.log(this.inputDuree.value)
      if (this.inputDuree.value == 15)
        this.dateFin.setDate(this.dateFin.getDate() + 15);
      else {
        ////console.log(this.dateFin)
        ////console.log(this.inputDuree.value)
        this.dateFin.setMonth(this.dateFin.getMonth() + Number(this.inputDuree.value));
        ////console.log(this.dateFin)

      }
      ////console.log(this.dateFin)

      // await this.utilsService.delay(500)
      this.inputFin.value = this.utilsService.displayDateFormatInput(this.dateFin);
    }
  }
  checkDureeFirstOption(input = ""): Boolean {
    if (this.inputDuree != null)
      this.duree = Number(this.inputDuree.value);
    console.log(input)
    if (this.duree == null && input == "duree") {
      this.loadingCtrl.dismiss();
      this.utilsService.alertNotification("veuillez indiquer une durée")
      return false;
    }

    if (isNaN(this.duree) || this.duree < 0) {
      console.log("isNanDuree")
      this.loadingCtrl.dismiss();
      this.utilsService.alertNotification("veuillez indiquer une durée correcte")
      return false;

    }
    if (this.duree == 0) {
      this.loadingCtrl.dismiss();
      this.utilsService.alertNotification("veuillez indiquer une durée d'au mininum 1 jour")
      return false;
    }
    return true;
  }
  dismiss(value) {
    this.modalCtrl.dismiss(value);
  }
}

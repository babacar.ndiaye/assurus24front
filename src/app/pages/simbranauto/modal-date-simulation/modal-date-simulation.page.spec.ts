import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ModalDateSimulationPage } from './modal-date-simulation.page';

describe('ModalDateSimulationPage', () => {
  let component: ModalDateSimulationPage;
  let fixture: ComponentFixture<ModalDateSimulationPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ModalDateSimulationPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ModalDateSimulationPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

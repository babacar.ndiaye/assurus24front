import { AssureContrat } from './../../models/assure-contrat';
import { CA } from 'src/app/models/ca';
import { Contrat2 } from './../../models/contrat';
import { OriginePageMesObjets, assureContratActifenSession } from './../../../environments/environment';
import { Component, OnInit, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { LoadingController, NavController, AlertController, IonSlides, IonInput, Platform, ModalController } from '@ionic/angular';
import { ConditionGenerale } from 'src/app/models/condition-generale';
import { CONDITIONSPATHNAME, downloadConditionsGenerales, simulationActifenSession, UrlApi, DescAutos, ListeSimulationEnSession, debutRoutine, OriginePageMonObjet, OriginePageGesSim, OrigineClientNonLogue, typeRoutineSimulation, typeRoutine, typeRoutineNewContrat, contratActifenSession, objetActifenSession, operationActifenSession, typeRoutineRenewContrat, typeRenouvellement, typeRenouvellementMemeCa, typeRenouvellementChangementDeCA, flotte, listeSimulationDansFlotte, retourFlotte, numeroPivot, listeSimulationDansFlotteValide, devise, loginToPage, UrlApiLogo, resultatFlotteOrigineGesSimulation, listeFlotteOrigineGestionSimulation, listeRoutineContratSession, indexRoutineContratEditingSession, LIENLOGIN, LIENSIMDECLAOBJET, LIENMESCONTRATS, LIENECRANRECAPITULATIF, LIENASSURINFOVEVEHICULEFLOTTE, LIENASSURINFOVE } from 'src/environments/environment';
import { CompteService } from 'src/app/services/compte.service';
import { Simulation } from 'src/app/models/simulation';
import { CompagniePrix } from 'src/app/pojos/compagnie-prix';
import { UtilsService } from 'src/app/services/utils.service';
import { BA } from 'src/app/models/ba';
import { AlertOptions } from '@ionic/core';
import { AuthService } from 'src/app/services/auth.service';
import { TypeBA, TypeBA2 } from 'src/app/models/type-ba';
import { Operation } from 'src/app/models/operation';
import { Automobile } from 'src/app/models/automobile';
import { Paiement } from 'src/app/models/paiement';

import { CompagnieObjetPrix } from 'src/app/pojos/compagnie-objet-prix';
import { ObjetAssures, ObjetAssures2 } from 'src/app/models/objet-assures';
import { Validators, FormBuilder } from '@angular/forms';
import { LoadedRouterConfig } from '@angular/router/src/config';
import { RoutineContrat } from 'src/app/pojos/routine-contrat';
import { forkJoin } from 'rxjs';
import { Globalization } from '@ionic-native/globalization/ngx';
import { TranslateService } from '@ngx-translate/core';
import { ListeValeurService } from 'src/app/services/liste-valeur.service';
import { TypeBas } from 'src/app/models/type-bas';
import { ModalDateSimulationPage } from './modal-date-simulation/modal-date-simulation.page';
import { CaService } from 'src/app/services/ca.service';
import { GarantieService } from 'src/app/services/garantie.service';
import { TypeGarantie } from 'src/app/models/type-garantie';
import { GarantieCA } from 'src/app/models/garantie-ca';
import { SimulationClient, SimulationClient2 } from 'src/app/models/simulation-client';
import { AssureSimulation } from 'src/app/models/assure-simulation';
import { CalculSimulationClient } from 'src/app/services/calcul-simulation-client';
import { Client } from 'src/app/models/client';
import { Reseau } from 'src/app/models/reseau';

@Component({
  selector: 'app-simbranauto',
  templateUrl: './simbranauto.page.html',
  styleUrls: ['./simbranauto.page.scss'],
})
export class SimbranautoPage implements OnInit {
  @ViewChild('fin') inputFin;
  @ViewChild('debut') inputDebut: IonInput;
  @ViewChild('jours') inputJours;
  @ViewChild('mois') inputMois;
  @ViewChild('annees') inputAnnees;
  @ViewChild('duree') inputDuree;
  classisActive: boolean = false;
  public simulationActif: SimulationClient = new SimulationClient();
  public contratActif: Contrat2 = new Contrat2
  public operationActif: Operation = new Operation
  public objetActif: ObjetAssures2 = new ObjetAssures2();
  public assureContratActif: AssureContrat = new AssureContrat();
  endChargement = false;
  typeOrigineRoutine;
  public items: any = [];
  listeCompagniePrix: CompagniePrix[];
  compagniePrixActif: CompagniePrix;
  caActif;
  deviseMonnaie = devise;
  duree: number
  valueDuree;
  listeSimulations: any;
  listeRoutineContrat: Array<RoutineContrat> = []
  listeSimFlotte: Array<AssureSimulation> = [];
  listeAllSimFlotte: Array<AssureSimulation> = [];
  dateDebutError: boolean = false;
  dateFinError: boolean = false;
  dateDebut: Date;
  dateFin: Date;
  typeGaranties = [];
  typeRoutineSimulation = typeRoutineSimulation;
  typeRoutineNewContrat = typeRoutineNewContrat;
  typeRoutineRenewContrat = typeRoutineRenewContrat;
  isFlotte = JSON.parse(window.sessionStorage.getItem(flotte));
  cachebouton = true;
  originePageMonObjet = OriginePageMonObjet;
  origineGesSimultation = OriginePageGesSim;
  liste: AssureSimulation[] = [];
  onDeclarationForm: any;
  dateFinReference: Date = null;
  ifAddNewVehicule: boolean = false;
  dureeRef: number;
  dateDuLendemain: Date;
  dateDebutMinimum = new Date(null);
  dateFinMaximumBeforeOther = new Date(null);
  indexActif: number = -1;
  lienSimbranauto = "/simbranauto"
  origineGesSim = JSON.parse(sessionStorage.getItem(resultatFlotteOrigineGesSimulation))
  resultatGeneralTestALLPeriode: boolean;
  observablesTest;
  totalSimuler: number;
  totalPrixRoutineContrat: number;
  language: string;
  brancheId: any;
  modifierDate: boolean = true;
  validerDate: boolean = false;
  garantiesCa: GarantieCA[];
  active: number = 0;
  contrats: Contrat2[];
  constructor(private router: Router,
    private loadingCtrl: LoadingController,
    private serviceCompte: CompteService,
    public utilsService: UtilsService,
    private navCtrl: NavController,
    private alertCtrl: AlertController,
    public authService: AuthService,
    private listeValeurService: ListeValeurService,
    private platform: Platform,
    private globalization: Globalization,
    private _translate: TranslateService,
    private formBuilder: FormBuilder,
    private CaService: CaService,
    private garantieService: GarantieService,
    public modalController: ModalController,
    private calculSimulation: CalculSimulationClient
  ) {
    this.listeCompagniePrix = new Array<CompagniePrix>();
    this.items = []

    //this.recupererListSimFlotteDeLaSimActive() ;
  }
  @ViewChild('slideWithNav') slideWithNav: IonSlides;

  sliderOne: any;


  @ViewChild('mySlider') mySlider: IonSlides;

  ionViewDidLoad() {
    this.mySlider.lockSwipes(true);
    sessionStorage.removeItem(indexRoutineContratEditingSession);
  }

  //Configuration for each Slider
  slideOptsOne = {
    initialSlide: 0,
    slidesPerView: 3,

    //spaceBetween: -9,
    autoplay: true,
    speed: 1200


  };
  slideChanged(slideView) {
    switch (slideView) {

      case "infos":
        this.slideWithNav.startAutoplay(); break;
    }
  }

  swipeNext() {
    this.slideWithNav.slideNext();

  }
  swipeBack() {
    this.slideWithNav.slidePrev();
  }

  initialiseItems() {
    this.items = []
    //console.log(this.listeCompagniePrix)
    if (this.listeCompagniePrix.length == 0) {
      window.sessionStorage.removeItem(simulationActifenSession);
      this.utilsService.alertNotification("Vos critères de sélection ne correspondent à aucune Offre d’assurance. Merci de modifier vos critères.")
      this.loadingCtrl.dismiss();
      this.navCtrl.navigateForward(LIENSIMDECLAOBJET);


    }
    if (this.typeOrigineRoutine == typeRoutineRenewContrat) {
      let item = {
        expanded: false,
        name: 'Risques Couverts',
        zipped: true,
        prix: this.compagniePrixActif.prix,
        compagnie: this.compagniePrixActif.ca,
        id: this.compagniePrixActif.ca.id,
        caActuel: true,
        logo: UrlApiLogo + this.compagniePrixActif.ca.id,

      }
      this.items.push(item);

    }

    this.listeCompagniePrix.sort((a, b) => a.prix - b.prix);
    this.listeCompagniePrix.forEach(compagniePrix => {
      let item = {
        expanded: false,
        name: 'Risques Couverts',
        zipped: true,
        prix: compagniePrix.prix,
        compagnie: compagniePrix.ca,
        id: compagniePrix.ca.id,
        caActuel: false,
        logo: UrlApiLogo + compagniePrix.ca.id,
        objetsPrix: compagniePrix.caObjetPrix,

      }
      this.items.push(item);

    });
    //console.log("items")
    //console.log(this.items)
    //console.log(this.listeSimFlotte)
    this.endChargement = true;

    // this.loadingCtrl.dismiss();
  }
  displayDate(date: Date): String {

    date = new Date(date);
    let jour, mois;
    let dd = date.getDate();

    let mm = date.getMonth() + 1;
    let yyyy = date.getFullYear();
    if (dd < 10) {
      jour = '0' + dd;
    }
    else
      jour = dd;

    if (mm < 10) {
      mois = '0' + mm;
    }
    else
      mois = mm;
    return jour + "/" + mois + "/" + yyyy;
  }
  ngOnInit() {
    this.onDeclarationForm = this.formBuilder.group({
      'debut': [null, Validators.compose([
        Validators.required
      ])],
      'fin': [null, Validators.compose([
        Validators.required
      ])],
    })
    if (sessionStorage.getItem("active")) {
      this.active = parseInt(sessionStorage.getItem("active"))
    } else {
      this.active = 0
    }
    console.log("active",this.active);
    if (sessionStorage.getItem(simulationActifenSession)) {
      this.contrats = JSON.parse(sessionStorage.getItem(simulationActifenSession)).contrats;
      if (this.contrats.length > 1) {
        this.contrats.sort((a,b) => a.montant - b.montant)
      }
    }
    // this.calculerSimulation()

  }

  async ionViewWillEnter() {
    // const loader = await this.loadingCtrl.create({
    //   message: 'calcul en cours',
    // });

    // loader.present();
    // console.log(this.choice.dateDebut)
    this.chargerInfoSimSession();
    this.cachebouton = false;
    //////console.log(this.simulationActif) ;

  }
  ionViewDidLeave() {
    this.cachebouton = true;
    sessionStorage.setItem(resultatFlotteOrigineGesSimulation, JSON.stringify(false));
  }
  goToLoginPage() {
    this.router.navigate(['/login'])
  }
  captureEventName(event: any) {
    //////console.log("ok");
  }

  activerLien() {
    var btnContainer = document.getElementById("simulation-ref");
    var btns = btnContainer.getElementsByClassName("btn");

    for (var i = 0; i < btns.length; i++) {
      btns[i].addEventListener("click", function () {
        var current = document.getElementsByClassName("activer");
        current[0].className = current[0].className.replace(" activer", "");
        this.className += " activer";
      });
    }
  }

  expandItem(item): void {

    item.zipped = !item.zipped;
    this.slideWithNav.stopAutoplay();
    /*for (let i = 1; i < 9; i++) {
      if (i.toString() === item.id) { continue; }
      document.getElementById(i.toString()).setAttribute(name, 'remove');
    }*/
    if (item.expanded) {
      item.expanded = false;

    } else {
      this.items.map(listItem => {
        if (item === listItem) {
          listItem.expanded = !listItem.expanded;
        }
        return listItem;
      });
    }
  }
  recupererConditionsGarantie(garantie, ca): ConditionGenerale {
    //on va recuperer les conditions de la garantie  proposé par la compagnie d'assurnace
    let conditionsGenerales: ConditionGenerale = {

      id: 1,

      conditionsSpeciales: ["Les garanties sont valables pour la traversée de la gambie", "les garanties sont valables pour la casamance", "les garanties sont valables pour la casamance", "les garanties sont valables pour la casamance", "les garanties sont valables pour la casamance", "les garanties sont valables pour la casamance", "les garanties sont valables pour la casamance", "les garanties sont valables pour la casamance"],
      garantie: null,

    }

    return conditionsGenerales;
  }
  async chargerConditionsGenerales(id) {
    const loader = await this.loadingCtrl.create({
      message: '',
    });

    loader.present();
    // To download the PDF file
    this.serviceCompte.download(CONDITIONSPATHNAME, id, downloadConditionsGenerales);
  }
  async chargerInfoSimSession() {
    //debut
    this.typeOrigineRoutine = JSON.parse(window.sessionStorage.getItem(typeRoutine));
    console.log(this.typeOrigineRoutine)
    switch (this.typeOrigineRoutine) {
      case this.typeRoutineSimulation:
        console.log("routineSimulation")
        if (this.origineGesSim == true) {
          console.log("origineFlotte")
          this.listeSimFlotte = JSON.parse(sessionStorage.getItem(listeFlotteOrigineGestionSimulation))
          this.simulationActif.assures[this.active] = this.listeSimFlotte[0];
          this.onDeclarationForm.get('debut').value = this.simulationActif.assures[this.active].dateCloture;
          this.onDeclarationForm.get('fin').value = this.simulationActif.assures[this.active].dateCloture;
          let dureeJour = this.utilsService.diffdate(this.simulationActif.assures[this.active].dateCloture, this.simulationActif.assures[this.active].dateCloture, "jour")
          this.utilsService.recupererDuree(dureeJour);
          this.recupererCompagniePrix();
          this.loadingCtrl.dismiss();
        }
        else {
          this.simulationActif = JSON.parse(window.sessionStorage.getItem(simulationActifenSession));


          if (this.simulationActif.dateSimulation == null) {
            this.loadingCtrl.dismiss();
            this.navCtrl.navigateForward(LIENSIMDECLAOBJET);

          }
          else {
            this.onDeclarationForm.get('debut').value = this.simulationActif.assures[this.active].dateEffet;
            this.onDeclarationForm.get('fin').value = this.simulationActif.assures[this.active].dateCloture;

            let dureeEnJour = this.utilsService.diffdate(this.simulationActif.assures[this.active].dateCloture, this.simulationActif.assures[this.active].dateEffet, "jour")
            this.valueDuree = this.utilsService.recupererDuree(dureeEnJour);

            this.recupererListSimFlotteDeLaSimActive().then(
              () => {
                console.log("listeSimFlotte", this.listeSimFlotte)

                this.recupererCompagniePrix();

              }
            )
          }
        }
        //dev à faire au niveau du back
        //pour chaque compagnie verifiez sil respecte les garanties
        //por chaque garanti et compagnie assurance...
        //retourner un pojo de la classe compagniePrixProduit

        break;///
      case this.typeRoutineRenewContrat:
      case this.typeRoutineNewContrat:
        {
          this.contratActif = JSON.parse(window.sessionStorage.getItem(contratActifenSession));
          this.objetActif = JSON.parse(window.sessionStorage.getItem(objetActifenSession));
          this.operationActif = JSON.parse(window.sessionStorage.getItem(operationActifenSession));
          this.assureContratActif = JSON.parse(window.sessionStorage.getItem(assureContratActifenSession))
          console.log('assureContrat Actif', this.assureContratActif)
          if (this.contratActif == null) {
            this.loadingCtrl.dismiss();
            this.navCtrl.navigateForward(LIENMESCONTRATS);

          }
          else {
            console.log("aaaa")
            //////console.log(this.contratActif.ca)
            if (this.typeOrigineRoutine == typeRoutineRenewContrat)
              this.caActif = this.contratActif.myCA;
            this.typeGaranties = [];

            // this.operationActif.garanties.forEach(element => {
            //   this.typeGaranties.push(element.typeGarantie)
            // });
            //Nouveau Changement
            this.assureContratActif.garanties.forEach(element => {
              //this.typeGaranties.push(element.garantie.typegarantie.typegarantie)
              this.typeGaranties.push(element)
            });
            this.listeRoutineContrat = JSON.parse(sessionStorage.getItem(listeRoutineContratSession))
            if (this.listeRoutineContrat == null)
              this.listeRoutineContrat = [];
            console.group("listeContrat")
            console.log(this.listeRoutineContrat)
            //////console.log(this.typeGaranties) ;
            //recupererCompagniePrix prend en paramtre soit la solution,les contrats et à définir
            this.onDeclarationForm.get('debut').value = this.operationActif.dateEffet;
            this.onDeclarationForm.get('fin').value = this.operationActif.dateCloture;
            let dureeEnJour = this.utilsService.diffdate(this.operationActif.dateCloture, this.operationActif.dateEffet, "jour")
            this.valueDuree = this.utilsService.recupererDuree(dureeEnJour);
            this.recupererCompagniePrix();
          }
          //dev à faire au niveau du back
          //pour chaque compagnie verifiez sil respecte les types garanties
          //por chaque type garanti et compagnie assurance...
          //retourner un pojo de la classe compagniePrixProduit

          break;
        }
      // default :this.effacerDonnees() ;

    }

    //fin

  }
  /**
   * Cette fonction permet de recuperer la durée à partir de la date de début et de la date de fin
   */

  //on va envoyer au serveur les types de garanties utilisés et on va recuperer la liste des cople ca montan
  //pour contrat METTRE LES PARAMETRES§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§
  async recupererCompagniePrix() {
    this.typeOrigineRoutine = JSON.parse(sessionStorage.getItem(typeRoutine));
    // switch(this.typeOrigineRoutine){
    //   case typeRoutineSimulation :
    //   //////console.log(this.simulationActif.typeGaranties) ;
    //    if(this.simulationActif.typeGaranties == null || this.simulationActif.typeGaranties.length ==0)
    //   this.loadingCtrl.dismiss() ;
    //                                 this.utilsService.alertNotification("Desolé une erreur est survenue veuillez recommencer") ;
    //                                 this.navCtrl.navigateForward("/choiceoption");

    //                                 break ;
    //   case typeRoutineRenewContrat :
    //   case typeRoutineNewContrat :
    //   //////console.log(this.operationActif.garanties)
    //    if( this.operationActif.garanties.length ==0)
    //   this.loadingCtrl.dismiss() ;
    //   this.utilsService.alertNotification("Desolé une erreur est survenue veuillez recommencer") ;
    //   this.navCtrl.navigateForward("/choiceoption");
    //    break ;


    // }
    let compagnie1 = {
      id: 1,
      nom: "AXA",
      typecompagnie: null,
      ninea: "445sd5e5",
      registreCommerce: '455511',
      adresse: "Medina",
      telephone: "338550266",
      email: "",
      siteweb: "",
      tauxfraisTransac: 0,
      forfaitTransac: 0,
      logo: "",
      adresse2: "",
      telephone2: "",
      fax: '',
      codepostal: '',
      pays: null,
      ville: null,
      coeffPenalitefract: 0.0,
      tableCoeff: null,
      tableMontant: null,
      actif: null,
      contacts: [],
      compteutilisateurs: [],
      communicationCommerciales: [],
      branches: []
    }
    let compagnie2 = {
      id: 2,
      nom: "ALLIANZ",
      typecompagnie: null,
      ninea: "445sd5e5",
      registreCommerce: '455511',
      adresse: "Medina",
      telephone: "338550266",
      email: "",
      siteweb: "",
      tauxfraisTransac: 0,
      forfaitTransac: 0,
      logo: "",
      adresse2: "",
      telephone2: "",
      fax: '',
      codepostal: '',
      pays: null,
      ville: null,
      coeffPenalitefract: 0.0,
      tableCoeff: null,
      tableMontant: null,
      actif: null,
      contacts: [],
      compteutilisateurs: [],
      communicationCommerciales: [],
      branches: []
    }
    let compagnie3 = {
      id: 3,
      nom: "AMSA",
      typecompagnie: null,
      ninea: "445sd5e5",
      registreCommerce: '455511',
      adresse: "Medina",
      telephone: "338550266",
      email: "",
      siteweb: "",
      tauxfraisTransac: 0,
      forfaitTransac: 0,
      logo: "",
      adresse2: "",
      telephone2: "",
      fax: '',
      codepostal: '',
      pays: null,
      ville: null,
      coeffPenalitefract: 0.0,
      tableCoeff: null,
      tableMontant: null,
      actif: null,
      contacts: [],
      compteutilisateurs: [],
      communicationCommerciales: [],
      branches: []
    }
    let compagnie4 = {
      id: 4,
      nom: "ASKIA",
      typecompagnie: null,
      ninea: "445sd5e5",
      registreCommerce: '455511',
      adresse: "Medina",
      telephone: "338550266",
      email: "",
      siteweb: "",
      tauxfraisTransac: 0,
      forfaitTransac: 0,
      logo: "",
      adresse2: "",
      telephone2: "",
      fax: '',
      codepostal: '',
      pays: null,
      ville: null,
      coeffPenalitefract: 0.0,
      tableCoeff: null,
      tableMontant: null,
      actif: null,
      contacts: [],
      compteutilisateurs: [],
      communicationCommerciales: [],
      branches: []
    }
    let caObjetPrix = new Array<CompagnieObjetPrix>();
    //alerte garder le meme classement que sur la listesimFlotte pour pouvoir utiliser l'index
    let index;
    //console.log(this.listeSimFlotte)
    this.listeSimFlotte.forEach(simulation => {
      //console.log("a")
      index = this.getIndex(simulation.numeroDansFlotte);
      //console.log(index)
      if (index >= 0) {
        this.listeSimFlotte[index].montantUniqueObjet = 5000 * (index + 1);
        caObjetPrix.push(new CompagnieObjetPrix(this.listeSimFlotte[index].assure, this.listeSimFlotte[index].montantUniqueObjet));
      }

    });
    //console.log(caObjetPrix)
    this.listeCompagniePrix = new Array<CompagniePrix>();
    //on simule les total en attedant de mettre en place lagorithme de calcule prix
    this.totalSimuler = 0;
    for (let index = 0; index < this.listeSimFlotte.length; index++) {
      this.totalSimuler += 5000 * (index + 1)

    }

    let indexcompagnie = 10;
    await this.listeCompagniePrix.push(new CompagniePrix(compagnie1, this.totalSimuler + (indexcompagnie-- * 1000), caObjetPrix), new CompagniePrix(compagnie2, this.totalSimuler + ((indexcompagnie - 2) * 1000), caObjetPrix), new CompagniePrix(compagnie3, this.totalSimuler + (indexcompagnie-- * 1000), caObjetPrix), new CompagniePrix(compagnie4, this.totalSimuler, caObjetPrix))
    //Removes checkbox from array when you uncheck it
    //////console.log("typeOrigneRoutine")
    if (this.typeOrigineRoutine == typeRoutineNewContrat || this.typeOrigineRoutine == typeRoutineRenewContrat) {
      //////console.log("comparaison") ;
      this.totalPrixRoutineContrat = 0;
      for (let index = 0; index < this.listeRoutineContrat.length; index++) {
        this.totalPrixRoutineContrat += 450000 * (index + 1)

      }
      //   this.listeCompagniePrix.forEach(element => {

      //     if (element.ca.id == this.caActif.id) {
      //       let index = this.listeCompagniePrix.indexOf(element);
      //       this.compagniePrixActif = this.listeCompagniePrix[index];
      //       this.listeCompagniePrix.splice(index, 1);

      //     }


      //   });

    }


    //console.log(this.listeSimFlotte)
    this.initialiseItems();
  }

  selectionBranche() {
    this.utilsService.getTypeBas().subscribe((data: TypeBA2[]) => {
      this.utilsService.selectionBrancheSimulation2(OriginePageGesSim, data);
    })
  }


  //on recuperera les branches à partir de l'api
  recupererBranches() {
    return this.utilsService.recupererTypeBranches();


  }



  //selectiond des simulations
  async selectionSimulation() {
    var options: AlertOptions = {
      header: 'Simulations',
      message: 'Veuillez choisir parmi ces simulations',
      buttons: [
        {
          text: 'Cancel',
          role: 'cancel',
          handler: () => {
            //////console.log('Cancel clicked');
          }
        },
        {
          text: 'Ok',
          handler: data => {
            //////console.log(data)
            if (data) {
              //////console.log("data") ;
              //////console.log(data) ;

              sessionStorage.setItem(simulationActifenSession, JSON.stringify(data));
              this.simulationActif = data;
              this.recupererListSimFlotteDeLaSimActive().then(
                () => {
                  console.log(this.listeSimFlotte)
                  window.location.reload();
                }
              )
              //



            }


          }
        }
      ]
    };

    options.inputs = [];

    // Now we add the radio buttons
    this.recuperationSimulationSession();
    for (let i = 0; i < this.listeSimulations.length; i++) {
      options.inputs.push({ name: 'Simulation numero ' + i, value: this.listeSimulations[i], label: 'Simulation numero ' + this.listeSimulations[i].numeroDansSession, type: 'radio' });
      //////console.log(this.listeSimulations[i]) ;
    }

    // Create the alert with the options
    let alert = this.alertCtrl.create(options);
    (await alert).present();
  }





  //selectin
  //recuperer simulation
  recuperationSimulationSession() {
    this.listeSimulations = JSON.parse(window.sessionStorage.getItem(ListeSimulationEnSession));
    ////console.log(this.listeSimulations)
    if (this.listeSimulations == null)
      this.listeSimulations = new Array<Simulation>();



  }


  validerSimulation(compagnie, montant, type) {

    this.typeOrigineRoutine = JSON.parse(window.sessionStorage.getItem(typeRoutine));
    switch (this.typeOrigineRoutine) {
      case typeRoutineSimulation:
        //////console.log(compagnie) ;
        sessionStorage.setItem(typeRenouvellement, JSON.stringify(typeRenouvellementChangementDeCA));

        this.simulationActif.assures[this.active].montantTTCCompagnie = montant;


        this.validerCAFlotteSimulation(compagnie).then(
          () => {
            sessionStorage.setItem(simulationActifenSession, JSON.stringify(this.simulationActif));
            console.log(this.listeSimFlotte)
            sessionStorage.setItem(listeSimulationDansFlotteValide, JSON.stringify(this.listeSimFlotte));
            sessionStorage.setItem(listeSimulationDansFlotte, JSON.stringify(this.listeSimFlotte));
            let origine = this.simulationActif.origineRoutine;


            switch (origine) {
              case OriginePageMonObjet:
                if (this.listeSimFlotte != null && this.listeSimFlotte.length == 1)
                  this.navCtrl.navigateForward(LIENECRANRECAPITULATIF);
                if (this.listeSimFlotte != null && this.listeSimFlotte.length > 1) {

                  this.navCtrl.navigateForward(LIENASSURINFOVEVEHICULEFLOTTE); break;
                }

                break;
              default:
                if (this.listeSimFlotte != null && this.listeSimFlotte.length == 1)
                  this.navCtrl.navigateForward("/assurinfove");
                //console.log("babs");

                if (this.listeSimFlotte != null && this.listeSimFlotte.length > 1) {

                  this.navCtrl.navigateForward(LIENASSURINFOVEVEHICULEFLOTTE); break;
                }

                break;


            }
          }
        )


        break;
      case typeRoutineRenewContrat:
      case typeRoutineNewContrat:
        if (this.typeOrigineRoutine == typeRoutineNewContrat)
          sessionStorage.setItem(typeRenouvellement, JSON.stringify(typeRenouvellementChangementDeCA));
        else {
          switch (type) {
            case true: sessionStorage.setItem(typeRenouvellement, JSON.stringify(typeRenouvellementMemeCa)); break;
            case false: sessionStorage.setItem(typeRenouvellement, JSON.stringify(typeRenouvellementChangementDeCA)); break;
          }
        }
        //////console.log(compagnie) ;
        ///test verification
        this.utilsService.verifierUniciteDansFlotte(this.listeRoutineContrat).then(
          res => {
            if (!res) {
              this.utilsService.alertNotification("Un objet ne peut pas figurer deux fois dans un même contrat")
            }
            else {
              this.validerCAFlotteRoutineContrat(compagnie).then(
                () => {
                  let paiement = new Paiement();
                  paiement.montant = montant;
                  this.operationActif.paiement = paiement;
                  console.log("Contrat Actif", this.contratActif);
                  window.sessionStorage.setItem(contratActifenSession, JSON.stringify(this.contratActif))
                  window.sessionStorage.setItem(operationActifenSession, JSON.stringify(this.operationActif))
                  window.sessionStorage.setItem(objetActifenSession, JSON.stringify(this.objetActif));
                  sessionStorage.setItem(listeRoutineContratSession, JSON.stringify(this.listeRoutineContrat))
                  this.navCtrl.navigateForward(LIENECRANRECAPITULATIF);
                }
              );
              //   this.utilsService.getDate().subscribe(
              //     dateFormatMobile => {
              //       this.testIfEachVehiculeCanHaveContrat(dateFormatMobile).then(
              //         res => {
              //           forkJoin(this.observablesTest).subscribe(
              //             () => {
              //               console.log(this.resultatGeneralTestALLPeriode)
              //               if (this.resultatGeneralTestALLPeriode) {
              //                 this.validerCAFlotteRoutineContrat(compagnie).then(
              //                   () => {
              //                     let paiement = new Paiement();
              //                     paiement.montant = montant;
              //                     this.operationActif.paiement = paiement;

              //                     window.sessionStorage.setItem(contratActifenSession, JSON.stringify(this.contratActif))
              //                     window.sessionStorage.setItem(operationActifenSession, JSON.stringify(this.operationActif))
              //                     window.sessionStorage.setItem(objetActifenSession, JSON.stringify(this.objetActif));
              //                     sessionStorage.setItem(listeRoutineContratSession, JSON.stringify(this.listeRoutineContrat))
              //                     this.navCtrl.navigateForward(LIENECRANRECAPITULATIF);



              //                   }
              //                 )

              //               }

              //             }
              //           )
              //         }
              //       )
              //     },
              //     error => {

              //     }
              //   )
            }
          }
        )

        //fint test verification



        break;
      //  default :this.effacerDonnees() ;


    }
  }
  seConnecter() {
    this.authService.logout()
    this.loadingCtrl.dismiss();
    this.navCtrl.navigateForward(LIENLOGIN);
  }

  effacerDonnees() {
    this.navCtrl.back();
  }
  enregistrer() {
    this.utilsService.enregistrerSimulation(this.listeSimFlotte);
  }
  remplacerSimulation(listeSimulation) {
    listeSimulation.forEach(simulation => {


      if (simulation.numeroDansSession == this.simulationActif.numeroDansSession)
        simulation = this.simulationActif;

      listeSimulation[simulation.index] = this.simulationActif


    });


  }
  chercherIndexCompagnie(caActif): number {

    this.listeCompagniePrix.forEach(compagniePrix => {
      //////console.log(compagniePrix.ca.id+" "+caActif.id)
      if (compagniePrix.ca.id == caActif.id)
        return this.listeCompagniePrix.indexOf(compagniePrix)

    });
    return

  }
  // precedent(){
  //   sessionStorage.setItem(retourFlotte,JSON.stringify(true)) ;
  // }

  async recupererListSimFlotteDeLaSimActive() {

    let liste = JSON.parse(window.sessionStorage.getItem(listeSimulationDansFlotte));
    console.log(liste)
    this.listeSimFlotte = [];
    liste.forEach(simulation => {
      for (let i = 0; i < this.simulationActif.assures.length; i++) {
        if (simulation.numeroDansFlotte == this.simulationActif.assures[i].numeroDansFlotte) {
          this.listeSimFlotte.push(simulation)
        }
      }
    });
    ////console.log("before")
    //console.log(this.listeSimFlotte)

    return true;

  }
  /**
   * cette fonction permet de changer l'aspacet de 'input losque l'erreur est corrigée
   */
  ajustInput(nom) {

    switch (nom) {
      case "debut":
        this.dateDebutError = false;
        break;
      case "fin":

        ////console.log(this.inputFin.value)
        this.dateFinError = false; break;
    }
  }
  recupererNumeroSessionPivot(): number {

    ////console.log("fai")
    let numeroPivot = this.listeSimFlotte[0].numeroDansFlotte;
    for (let index = 1; index < this.listeSimFlotte.length; index++) {
      if (this.listeSimFlotte[index].numeroDansFlotte < numeroPivot)
        numeroPivot = this.listeSimFlotte[index].numeroDansFlotte;


    }
    console.log(numeroPivot)
    return numeroPivot;

  }
  precedent() {
    sessionStorage.setItem(simulationActifenSession, JSON.stringify(this.listeSimFlotte[0]))
    sessionStorage.setItem(numeroPivot, JSON.stringify(this.recupererNumeroSessionPivot()));
    sessionStorage.setItem(resultatFlotteOrigineGesSimulation, JSON.stringify(false));
    ////console.log("after")
    ////console.log(this.listeSimFlotte)
  }
  /**
   * permet de modifier la routine de simulation d'un objet dans une flotte
   */
  editSimulation(simulation: AssureSimulation) {
    this.active = simulation.numeroDansFlotte - 1;
    sessionStorage.setItem("active", this.active.toString())
    sessionStorage.setItem(simulationActifenSession, JSON.stringify(this.simulationActif))
    sessionStorage.setItem(numeroPivot, JSON.stringify(this.recupererNumeroSessionPivot()));
    this.navCtrl.navigateForward(LIENSIMDECLAOBJET)
  }
  editContrat(routineContrat) {
    window.sessionStorage.setItem(contratActifenSession, JSON.stringify(routineContrat.contrat));
    window.sessionStorage.setItem(operationActifenSession, JSON.stringify(routineContrat.operation));
    window.sessionStorage.setItem(objetActifenSession, JSON.stringify(routineContrat.objet));
    /*on mets la routine à modifier en session pour la remplacer lorsque l'utilisateur a validé l'objet et
     les options dans la routine de modification
    */
    console.log(this.listeRoutineContrat.indexOf(routineContrat))
    window.sessionStorage.setItem(indexRoutineContratEditingSession, JSON.stringify(this.listeRoutineContrat.indexOf(routineContrat)));

    this.navCtrl.navigateForward(LIENASSURINFOVE)

  }





  /**
   * permet de supprimer un objet de la flotte
   *
   */
  async deleteSimulation(numeroFlotte) {
    let simulation = JSON.parse(window.sessionStorage.getItem(simulationActifenSession));
    // this.simulation.assures = []
    for (let index = 0; index < this.listeSimFlotte.length; index++) {

      if (this.listeSimFlotte[index].numeroDansFlotte == numeroFlotte) {
        this.listeSimFlotte.splice(index, 1)
        // On mise à jour la liste de flotte 
        for (let i = 0; i < this.listeSimFlotte.length; i++) {
          this.listeSimFlotte[i].numeroDansFlotte = i + 1;
        }
        // console.log("listeSimFlotte", this.listeSimFlotte)
        //on introduit la modification de liste en session qui regrouve tous les flottes
        // this.liste = JSON.parse(sessionStorage.getItem(listeSimulationDansFlotte))
        this.misAjourListe();
        // sessionStorage.setItem(listeSimulationDansFlotte, JSON.stringify(this.liste))
        const loader = await this.loadingCtrl.create({
          message: 'Calcul en cours...',
        });
        this.recupererCompagniePrix();
        let flotte = [];
        this.listeSimFlotte.forEach(element => {
          flotte.push(element)
        });
        simulation.assures = flotte;
        // console.log(simulation);
        //on introduit la modification de liste en session qui regrouve tous les flottes
        sessionStorage.setItem(listeSimulationDansFlotte, JSON.stringify(flotte))
        sessionStorage.setItem(simulationActifenSession, JSON.stringify(simulation))
        sessionStorage.setItem("active", "0")
        window.location.reload()
        return;

      }


    }

  }
  /*
  Cette fonction va nous permettre de retirer un objet de la flotte lors de la creation d'un nouveau contrat
  */
  deleteRoutineContrat(indexRoutine) {
    this.listeRoutineContrat.splice(indexRoutine, 1);
    sessionStorage.setItem(listeRoutineContratSession, JSON.stringify(this.listeRoutineContrat))
    window.location.reload();

  }
  async misAjourListe() {
    // const loader = await this.loadingCtrl.create({
    //   message: '',
    // });

    let index = this.getIndexListePrincipale(this.simulationActif.assures[this.active].numeroDansFlotte);
    console.log(index)
    if (index >= 0) {
      this.supprimerListePrincipale(index).then(
        () => {
          sessionStorage.setItem(listeSimulationDansFlotte, JSON.stringify(this.liste))
          this.recupererCompagniePrix();
          console.log(this.liste);
        }
      )


    }



  }
  async supprimerListePrincipale(index) {
    this.liste.splice(index, 1)

  }
  getIndex(numeroFlotte): number {
    for (let index = 0; index < this.listeSimFlotte.length; index++) {

      if (this.listeSimFlotte[index].numeroDansFlotte == numeroFlotte) {
        //console.log(index)
        return index;

      }


    }
    return -1;

  }
  getIndexListePrincipale(numeroFlotte) {
    for (let index = 0; index < this.liste.length; index++) {

      if (this.liste[index].numeroDansFlotte == numeroFlotte) {
        //console.log(index)
        return index;

      }


    }
    return -1;

  }
  ///ajout
  addVehicule() {
    this.typeOrigineRoutine = JSON.parse(window.sessionStorage.getItem(typeRoutine));
    switch (this.typeOrigineRoutine) {
      case typeRoutineSimulation:
        //////console.log(compagnie) ;
        this.debutFlotte();

        break;


        break;
      case typeRoutineRenewContrat:
      case typeRoutineNewContrat:
        this.debutFlotteContrat();
        break;
      //  default :this.effacerDonnees() ;


    }


  }
  debutFlotte() {
    ////////console.log(this.listeSimFlotte) ;

    sessionStorage.setItem(numeroPivot, JSON.stringify(this.recupererNumeroSessionPivot()));
    sessionStorage.setItem(flotte, JSON.stringify(true));
    // this.simulationActif.dateDebut = null ;
    let simFlotte: AssureSimulation = new AssureSimulation();


    //reference : string;

    // sim.client = this.simulationActif.client
    // sim.dateSimulation = new Date(this.simulationActif.dateSimulation)
    //relations à ajoutéé


    //type de garanties pour uniformiser dans le choice option

    simFlotte.dateEffet = new Date(this.utilsService.displayDateFormatInitialise(this.simulationActif.assures[this.active].dateEffet))
    // ////////console.log(this.utils.displayDate(this.simulationActif.dateDebut)) ;
    ////////console.log(this.utilsService.displayDate(sim.dateDebut)) ;
    simFlotte.dateCloture = new Date(this.simulationActif.assures[this.active].dateCloture)
    // simFlotte.origineRoutine = this.simulationActif.origineRoutine
    // simFlotte.numeroDansSession = this.simulationActif.numeroDansSession
    //sim.ca =this.simulationActif.ca
    simFlotte.montantTTCCompagnie = this.simulationActif.assures[this.active].montantTTCCompagnie
    simFlotte.assure = new ObjetAssures2();
    simFlotte.garanties = [];
    this.active = 0;
    this.simulationActif.assures.forEach(sim => {
      if (sim.assure)
        this.active++
    });
    simFlotte.numeroDansFlotte = this.active + 1;
    sessionStorage.setItem("active", this.active.toString())
    this.simulationActif.assures.push(simFlotte)
    sessionStorage.setItem(simulationActifenSession, JSON.stringify(this.simulationActif));

    this.navCtrl.navigateForward(LIENSIMDECLAOBJET);

  }
  ///fin ajout
  async misAjourListFirstStep() {
    var p1 = new Promise(function (resolve, reject) {
      resolve("Succès !");
      // ou
      // reject("Erreur !");
    });
    this.liste = JSON.parse(sessionStorage.getItem(listeSimulationDansFlotte));
    ////console.log(this.liste)
    for (let index = 0; index < this.liste.length; index++) {

      if (this.liste[index].numeroDansFlotte == this.simulationActif.assures[index].numeroDansFlotte) {
        this.liste.splice(index, 1);
        ////console.log(o)
        ////console.log(this.liste)
      }

    }

    return p1;

  }
  async misAjourSecondStep() {
    var p1 = new Promise(function (resolve, reject) {
      resolve("Succès !");
      // ou
      // reject("Erreur !");
    });
    console.log(this.simulationActif.assures[this.active].numeroDansFlotte)
    this.listeSimFlotte.forEach(element => {
      if (element.numeroDansFlotte != this.simulationActif.assures[this.active].numeroDansFlotte)
        this.liste.push(element)
    });
    return p1;

  }

  /**
   * cette fonction permer de calculer la date de fin a partir de la date de debut et de la duree
   */
  ajustInputSecondeSession() {
    this.validerDate = true;
    ////console.log(this.inputFin.value)
    this.dateFinError = false;
    this.finOuDureeFirstOption();
    this.checkBeforeValidationFirstOption()
  }
  async finOuDureeFirstOption() {

    if (this.checkDureeFirstOption()) { // On vérifie si on a saisie une date

      // this.dateFin = new Date(this.inputDebut.value);
      ////console.log(this.dateFin)
      ////console.log(this.inputDuree.value)
      if (this.duree == 15)
        this.dateFin.setDate(this.dateFin.getDate() + 15);
      else {
        // console.log("dateFin",this.dateFin)
        // console.log("inputDuree",this.inputDuree.value)
        this.dateFin.setMonth(this.dateFin.getMonth() + Number(this.duree));
        // console.log("dateFin",this.dateFin)

      }

      ////console.log(this.dateFin)

      // await this.utilsService.delay(500)
      // this.inputFin.value = this.utilsService.displayDateFormatInput(this.dateFin);
      // this.onDeclarationForm.get('fin').setValue(this.dateFin) // on ajoute la valeur de la date au formulaire
    }


  }
  checkDureeFirstOption(): Boolean {
    // this.duree = Number(this.inputDuree.value);
    if (this.duree == null) {
      this.utilsService.alertNotification("veuillez indiquer une durée")
      return false;
    }

    if (isNaN(this.duree) || this.duree < 0) {
      this.utilsService.alertNotification("veuillez indiquer une durée correcte")
      return false;

    }

    if (this.duree == 0) {
      this.utilsService.alertNotification("veuillez indiquer une durée d'au mininum 1 jour")
      return false;
    }

    return true;


  }
  async validerChangementDate() {
    this.ifAddNewVehicule = false;
    this.modifierDate = false;
    console.log("changement date", this.modifierDate)
    // this.checkBeforeValidationFirstOption()
    const modal = await this.modalController.create({
      component: ModalDateSimulationPage,
      componentProps: {
        'dateDebut': this.simulationActif.assures[this.active].dateEffet,
        'dateFin': this.simulationActif.assures[this.active].dateCloture,
        'duree': this.valueDuree
      },
      cssClass: "custom-modal-css"
    });
    modal.onDidDismiss().then((form) => {
      console.log(form.data);
      if (form.data) {
        this.dateDebut = new Date(form.data.date_effet)
        this.dateFin = new Date(form.data.date_effet)
        this.duree = Number(form.data.duree)
        this.ajustInputSecondeSession()
      }
    });
    return await modal.present();
  }


  async validerCAFlotteSimulation(compagnie) {

    this.simulationActif.ca = compagnie;
    //    ON A DEJA LA FLOTTE DANS LA SIMULATION
    // if (this.listeSimFlotte != null) {
    //   this.listeSimFlotte.forEach(simulation => {
    //     simulation.ca = compagnie;

    //   });
    // }

  }
  /**
   * ceette fonction permet d'affecter à chaque contrat de la flotee la compagnie choisie
   * routine contrat
   * @param compagnie
   */
  async validerCAFlotteRoutineContrat(compagnie: CA) {

    this.contratActif.myCA = compagnie;
    if (this.listeRoutineContrat != null) {
      this.listeRoutineContrat.forEach(routine => {
        routine.contrat.myCA = compagnie;

      });
    }

  }

  async checkBeforeValidationFirstOption() {


    if (this.dateFinReference == null) {
      if (this.checkDureeFirstOption()) {

        //valider changement heure
        this.valider()
      }
      else {

      }
    }
    else
      this.valider();


  }
  valider() {

    // let reactiveDebut = this.onDeclarationForm.get('debut').value;
    // let reactiveFin = this.onDeclarationForm.get('fin').value;

    // console.log(reactiveDebut, reactiveFin);
    if (!this.dateDebut || !this.dateFin) {
      // if (this.onDeclarationForm.get('fin').hasError('required'))
      this.dateFinError = true;
      // if (this.onDeclarationForm.get('debut').hasError('required'))
      this.dateDebutError = true;

      this.utilsService.alertNotification("Veuillez indiquer toutes les dates avant de continuer");

    }
    else {
      // this.dateFin = new Date(reactiveFin);
      // this.dateDebut = new Date(reactiveDebut);
      console.log(this.dateDebut, this.dateFin)
      this.typeOrigineRoutine = JSON.parse(sessionStorage.getItem(typeRoutine));
      switch (this.typeOrigineRoutine) {
        case typeRoutineSimulation: this.trouverDateMinimum(this.simulationActif.assures[this.active].assure); break;
        case typeRoutineRenewContrat:
        case typeRoutineNewContrat: this.nextEtape(); break;
      }


      // ////console.log(this.dateDebut+" "+this.displayDate(this.dateDebut)) ;
      // ////console.log(this.dateFin+" "+this.displayDate(this.dateFin)) ;
      // ////console.log(this.checked);

    }

  }
  trouverDateMinimum(objet) {
    ////console.log("debutFonction")
    //recupere la liste des contrats delobjet
    //let contrats = this.utilsService.filtrerContrat(objet.contrats)
    //
    let contrats = objet.contrats;
    let date = this.utilsService.getDate().subscribe(
      dateFormatMobile => {


        let dat = new Date(dateFormatMobile.date);
        dat.setDate(dat.getDate() + 1)

        this.dateDuLendemain = new Date(dat);

        //
        if (contrats == null || contrats.length == 0) {


          this.dateDebutMinimum = dat;
          this.dateFinMaximumBeforeOther = null;
          //  console.log("date minimum"+this.utilsService.displayDate(this.dateFinMaximumBeforeOther))
          this.verifierDate();
          return;
          ////console.log(this.dateDebutMinimum) ;
        }
        else {
          console.log(contrats[0])
          contrats[0].dateEffet = new Date(contrats[0].dateEffet)
          let datBeforeOther = new Date(contrats[0].dateEffet);
          console.log("date minimum" + this.utilsService.displayDate(datBeforeOther))

          contrats.forEach(contrat => {
            //boucle sur operation

            contrat.operations.forEach(operation => {
              if (this.utilsService.testOperationActif(operation)) {
                operation.dateCloture = new Date(operation.dateCloture);
                if (this.utilsService.isinferior(dat, operation.dateCloture)) {
                  if (operation.dateCloture != null && this.utilsService.isinferior(dat, operation.dateCloture)) {
                    dat = new Date(operation.dateCloture);

                  }
                  operation.dateEffet = new Date(operation.dateEffet)
                  if (operation.dateEffet != null && this.utilsService.isinferior(operation.dateEffet, datBeforeOther)) {
                    datBeforeOther = new Date(operation.dateEffet);
                    console.log("date minimum" + this.utilsService.displayDate(datBeforeOther))

                  }


                }


              }

            });

            //finboucle sur operation



          });



          if (dat != null) {

            this.dateDebutMinimum = new Date(dat);
            this.dateDebutMinimum.setDate(this.dateDebutMinimum.getDate() + 1)



          }
          if (datBeforeOther != null) {
            this.dateFinMaximumBeforeOther = new Date(datBeforeOther);
            this.dateFinMaximumBeforeOther.setDate(this.dateFinMaximumBeforeOther.getDate() - 1)
            console.log("date minimum" + this.utilsService.displayDate(this.dateFinMaximumBeforeOther))
          }
          this.verifierDate();

        }
      },
      error => {

        console.log("1")
        let message = "Oups le serveur est introuvable verfiez votre connexion et recommencez svp";
        this.utilsService.alertNotification(message);



      }

    );

  }
  verifierDate() {
    this.dateFinError = false;
    this.dateDebutError = false;
    console.log(this.dateDebut)
    if (this.dateDebut != null) {
      this.dateDebut = new Date(this.dateDebut);
      ////console.log("dateDebutVerif "+this.utilsService.displayDate(this.dateDebut))
    }

    if (this.dateFin != null) {
      this.dateFin = new Date(this.dateFin);

      ////console.log("dateFintVerif "+this.utilsService.displayDate(this.dateFin))
    }


    ////console.log("dateMinmimumVerif "+this.utilsService.displayDate(this.dateDebutMinimum))


    if (this.dateDebut != null && this.dateFin != null) {
      // this.trouverdateDebutMinimum() ;
      this.dateDebut.setHours(0, 0, 0, 0);
      this.dateFin.setHours(0, 0, 0, 0);
      this.dateDebutMinimum.setHours(0, 0, 0, 0);

      if (this.utilsService.isinferior(this.dateFin, this.dateDebut)) {

        ////console.log("date debut > fin")
        this.dateFin = null;
        this.dateDebut = null;

        this.dateFinError = true;
        this.utilsService.alertNotification("la date de fin ne doit pas être inférieure à la date de début");
        return;
      }
      let ok = this.verifierIfInFirstIntervalle(this.dateDebut, this.dateFin);
      console.log("console " + ok)


      if (!this.verifierIfInFirstIntervalle(this.dateDebut, this.dateFin)) {
        console.log("2 false")
        if (!this.verifierIfInSecondIntervalle()) {
          return;
        }

      }


    }
    this.nextEtape()
  }
  verifierIfInFirstIntervalle(dateDebut, dateFin) {
    var p1;
    console.log(dateDebut)
    if (this.dateFinMaximumBeforeOther != null) {


      console.log("date Debut" + this.utilsService.displayDate(dateDebut))
      console.log("date Lendemain" + this.utilsService.displayDate(this.dateDuLendemain))
      console.log("date Fin" + this.utilsService.displayDate(dateFin))
      console.log("date debut minimum" + this.utilsService.displayDate(this.dateDebutMinimum))
      console.log("date fin maximumBefore other" + this.utilsService.displayDate(this.dateFinMaximumBeforeOther))
      console.log(!this.utilsService.isinferior(dateDebut, this.dateDuLendemain));
      console.log(!this.utilsService.isinferior(this.dateFinMaximumBeforeOther, dateFin));
      if ((!this.utilsService.isinferior(dateDebut, this.dateDuLendemain) && !this.utilsService.isinferior(this.dateFinMaximumBeforeOther, dateFin))) {


        console.log("in da building")
        //  this.dateFin = null ;
        //  this.dateDebut =null ;
        //  this.dateDebutError =true ;
        //  this.utilsService.alertNotification("le contrat doit finir au plus tard le le "+this.utilsService.displayDate(this.dateFinMaximumBeforeOther)) ;
        return true
      }
      else
        return false


    }
    else return false
  }

  verifierIfInSecondIntervalle() {
    console.log("not in first")
    if (this.utilsService.isinferior(this.dateDebut, this.dateDebutMinimum)) {


      console.log("date fin" + this.utilsService.displayDate(this.dateFin))
      ////console.log("date debut < minimum")
      this.dateFin = null;
      this.dateDebut = null;
      this.dateDebutError = true;
      if (this.dateFinMaximumBeforeOther != null)
        this.utilsService.alertNotification(
          "le contrat peut soit commencer apres le " + this.utilsService.displayDate(this.dateDebutMinimum) +
          "\n ou soit debuter apres le " + this.utilsService.displayDate(this.dateDuLendemain) + " se terminer avant le" + this.utilsService.displayDate(this.dateFinMaximumBeforeOther));
      else
        this.utilsService.alertNotification("le contrat ne peut pas commencer avant le " + this.utilsService.displayDate(this.dateDebutMinimum));
      return false;
    }
    else
      return true;

  }

  nextEtape() {
    //dispatching
    this.typeOrigineRoutine = JSON.parse(window.sessionStorage.getItem(typeRoutine));
    switch (this.typeOrigineRoutine) {
      case typeRoutineSimulation:
        let date = this.utilsService.getDate().subscribe(
          dateFormatMobile => {
            this.simulationActif.dateSimulation = new Date(dateFormatMobile.date)
            this.misAjourListePrincipale().then(
              () => {
                sessionStorage.setItem(simulationActifenSession, JSON.stringify(this.simulationActif))
                sessionStorage.setItem(listeSimulationDansFlotte, JSON.stringify(this.listeAllSimFlotte))
                console.log("after")
                console.log(this.listeAllSimFlotte)
                window.location.reload()

              }
            )
          },
          error => {
            let message = "Oups le serveur est introuvable verfiez votre connexion et recommencez svp";
            this.utilsService.alertNotification(message);

            sessionStorage.setItem(loginToPage, JSON.stringify(this.lienSimbranauto))
            this.navCtrl.navigateForward(LIENLOGIN);
          }
        );

        break;
      case typeRoutineNewContrat:
      case typeRoutineRenewContrat:
        {
          this.modifierDateRoutine().then(
            () => {
              console.log(this.listeRoutineContrat)
              sessionStorage.setItem(listeRoutineContratSession, JSON.stringify(this.listeRoutineContrat));
              sessionStorage.setItem(operationActifenSession, JSON.stringify(this.operationActif));
              window.location.reload()
            }

          )
        }
      default:
      //this.effacerDonnees() ;

    }

    //fin dispatching




  }
  async misAjourListePrincipale() {
    this.simulationActif.assures[this.active].dateEffet = this.dateDebut;

    this.simulationActif.assures[this.active].dateEffet.setHours(0, 0, 0);
    this.simulationActif.assures[this.active].dateEffet.setHours(0, 0, 0);

    this.simulationActif.assures[this.active].dateCloture = this.dateFin;
    this.simulationActif.assures[this.active].dateCloture.setHours(23, 59, 59);


    this.listeAllSimFlotte = JSON.parse(sessionStorage.getItem(listeSimulationDansFlotte))

    this.listeAllSimFlotte.forEach(simulation => {
      if (simulation.numeroDansFlotte == this.simulationActif.assures[this.active].numeroDansFlotte) {
        simulation.dateEffet = new Date(this.dateDebut);

        simulation.dateEffet.setHours(0, 0, 0);

        simulation.dateCloture = this.dateFin;
        simulation.dateCloture.setHours(23, 59, 59);
      }

    });

  }
  montrerDetails(item) {
    item.zipped = false;
    console.log(item.compagnie);
    this.garantiesCa = [];
    this.listeSimFlotte.forEach(simulation => {
      this.CaService.getBrancheCA(simulation.assure.typeba.id, item.compagnie.id).subscribe(branchCa => {
        if (branchCa) {
          // console.log("brancheca" ,branchCa)
          simulation.garanties.forEach(garantie => {
            this.garantieService.getGarantieCA(branchCa.id, garantie.id).subscribe(garantieCa => {
              console.log("Garantie Ca", garantieCa);
              this.garantiesCa.push(garantieCa)
            })
          })
        }
      })
    })
  }
  cacherDetails(item) {
    item.zipped = true;
  }


  /**
   * session gestion flotte contrat
   */

  /**
   * Cette methode nous permet d'ajouter un nouveau vehicule à la flotte pour la routine nouveau contrat
   */
  debutFlotteContrat() {

    this.brancheId = sessionStorage.getItem('typeBrancheId');
    sessionStorage.setItem(flotte, JSON.stringify(true));
    // this.simulationActif.dateDebut = null ;
    let newOperation: Operation = new Operation();
    newOperation.paiement = new Paiement();
    let newContrat: Contrat2 = new Contrat2();

    let obj;

    newOperation.dateEffet = new Date(this.utilsService.displayDateFormatInitialise(this.operationActif.dateEffet))
    // //console.log(this.utils.displayDate(this.simulationActif.dateDebut)) ;

    newOperation.dateCloture = new Date(this.utilsService.displayDateFormatInitialise(this.operationActif.dateCloture))

    newContrat.myCA = this.contratActif.myCA

    obj = new ObjetAssures();

    //newOperation.garanties = [];
    newOperation.details = [];
    sessionStorage.setItem(contratActifenSession, JSON.stringify(newContrat));
    sessionStorage.setItem(objetActifenSession, JSON.stringify(obj));
    sessionStorage.setItem(operationActifenSession, JSON.stringify(newOperation));
    if (this.brancheId) {
      console.log('this.brancheAutoId', this.brancheId);
      this.listeValeurService.getUsages(this.brancheId).subscribe((data: TypeBas[]) => {
        console.log(data);
        if (data) {
          this.utilsService.getTypeBasAndSousBrancheAttributsDTO(this.brancheId)
            .subscribe((attributsParents) => {
              console.log(attributsParents);
              this.utilsService.selectionSousBrancheObjetForRoutineContrat(data, "AUTOMOBILE", attributsParents, this.brancheId);
            })
        }

      });
    }
    //this.navCtrl.navigateForward(LIENASSURINFOVE);
    //this.navCtrl.navigateForward(LIENMESCONTRATS)

  }
  /**
   *
   * @param dateFormatMobile
   * cette fonction nous permet  de verifier si chaque vehicule respecte l'unicité operation
   */
  async testIfEachVehiculeCanHaveContrat(dateFormatMobile) {
    this.resultatGeneralTestALLPeriode = true;

    this.observablesTest = []

    console.log("test if each")


    this.listeRoutineContrat.forEach(
      routine => {
        this.observablesTest.push(
          /*
          Premiere methode ne prend pas en compte des intervalles au centre
          mais on a vue sur les intervalles "libre"
         voir assurinfove vehicule
            */

          /**seconde methode prend en compte de avant,apres et centre mais
            * ne definit pas les messages pour linstant
            * on a pas de vue sur les intervalles libre mais sur les intervalles occupés
            * teste si le vehicule respecte la contrainte d'unicité des contrats
            * les messages  sont affiches au niveau de la fonction testerSiNewContratIntervalleDisponible de la classe utils service
            *
            */
          //   this.utilsService.recupererIntervalleNonDisponibleObjet(routine.objet.contrats).then(
          //     liste => {

          //       console.log(routine.objet)
          //       console.log(liste)
          //       this.utilsService.testerSiNewContratIntervalleDisponible(routine.objet, routine.operation.dateEffet, routine.operation.dateCloture, liste).then(
          //         resultat => {
          //           if (resultat == false)
          //             this.resultatGeneralTestALLPeriode = false;

          //         }
          //       );
          //     }
          //   )
        )

      }
    )





  }
  /**
   * Cette fonction nous  permet de modifier les dates de tous les objets de la flotte dans la routine contrat
   */
  async modifierDateRoutine() {
    this.listeRoutineContrat.forEach(routine => {
      routine.operation.dateEffet = new Date(this.inputDebut.value);
      routine.operation.dateCloture = new Date(this.inputFin.value);

    });
    this.operationActif.dateEffet = new Date(this.inputDebut.value);
    this.operationActif.dateCloture = new Date(this.inputFin.value);

  }

  calculerSimulation() {
    if (window.sessionStorage.getItem(simulationActifenSession)) {
      let sim: SimulationClient = JSON.parse(window.sessionStorage.getItem(simulationActifenSession));
      delete sim.ca
      delete sim.dateSimulation
      delete sim.eligiblePeriode
      delete sim.numeroDansSession
      delete sim.origineRoutine
      let simFlotte: SimulationClient2 = new SimulationClient2();
      simFlotte = sim;
      simFlotte.assures = sim.assures.map(ob => {
        delete ob.montantTTCCompagnie
        delete ob.montantUniqueObjet
        delete ob.numeroDansFlotte
        return ob
      })
      simFlotte.assures.forEach(element => {
        element.assure.typeba.attributs.forEach((element: any) => {
          if (element != null) {
            delete element.listevaleur;
          }
        });
        element.assure.typeba.parent.attributs.forEach((element: any) => {
          if (element != null) {
            delete element.listevaleur;
          }
        });
        element.assure.attributs.forEach((element: any) => {
          if (element.attribut != null) {
            delete element.attribut.listevaleur;
          }
        });
      });
      console.log("calcule", simFlotte);

      this.calculSimulation.getSimulationClient(simFlotte).subscribe(data => {
        console.log("simulation client reçue", data);
        this.simulationActif.contrats = data.contrats
      })
    }
  }
  /**GESTION LOGIQUE TRANSLATION DES LANGUES */
  ionViewDidEnter(): void {
    this.getDeviceLanguage()
  }

  public changeLanguage(): void {
    this._translateLanguage();
  }

  _translateLanguage(): void {
    this._translate.use(this.language);
  }

  _initTranslate(language) {
    // Set the default language for translation strings, and the current language.
    this._translate.setDefaultLang('fr-FR');
    if (language) {
      this.language = language;
    }
    else {
      // Set your language here
      this.language = 'fr-FR';
    }
    this._translateLanguage();
  }

  getDeviceLanguage() {
    if (!this.platform.is('android') || !this.platform.is('ios')) {
      //alert('Navigateur')
      console.log('Navigateur langage', navigator.language)
      this._initTranslate(navigator.language);
    } else {
      this.globalization.getPreferredLanguage()
        .then(res => {
          this._initTranslate(res.value)
        })
        .catch(e => { console.log(e); });
    }
  }
  /**END TRANSLATION */
}

import { Component, OnInit } from '@angular/core';
import { AuthService } from 'src/app/services/auth.service';
import { NavController, Platform } from '@ionic/angular';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { UtilsService } from 'src/app/services/utils.service';
import { Simulation } from 'src/app/models/simulation';
import { simulationActifenSession, typeRoutine, typeRoutineSimulation, flotte, UsageTransportProduit, UsageMotoPersonnelEtTourisme, UsagepersonnelEtTourisme, constMinNombrePlace, constMaxNombrePlace, constMaxAnnee, constMinAnnee, UsageTransportPayantProduit, UsageTransportVoyageur, typeSousBranche } from 'src/environments/environment';
import { ListeValeur } from 'src/app/models/liste-valeur';
import { ListeValeurService } from 'src/app/services/liste-valeur.service';
import { Globalization } from '@ionic-native/globalization/ngx';
import { TranslateService } from '@ngx-translate/core';
import { TypeBas } from 'src/app/models/type-bas';
import { ObjetAssures2 } from 'src/app/models/objet-assures';
import { AttributAussureBranche } from 'src/app/models/attribut-assure-branche';
import { AttributAussure } from 'src/app/models/attribut-assure';
import { AttributAussureBrancheDto } from 'src/app/Dto/AttributAussureBrancheDto';
import createNumberMask from 'text-mask-addons/dist/createNumberMask';
import { AssureSimulation } from 'src/app/models/assure-simulation';
import { SimulationClient } from 'src/app/models/simulation-client';

@Component({
  selector: 'app-sim-decla-objet',
  templateUrl: './sim-decla-objet.page.html',
  styleUrls: ['./sim-decla-objet.page.scss'],
})
export class SimDeclaObjetPage implements OnInit {
  UsageMotoPersonnelEtTourisme = UsageMotoPersonnelEtTourisme
  UsageTransportVoyageur = UsageTransportVoyageur
  UsagepersonnelEtTourisme = UsagepersonnelEtTourisme
  UsageTransportProduit = UsageTransportProduit
  UsageTransportPayantProduit = UsageTransportPayantProduit
  usage;
  usages: TypeBas[] = [];
  monUsages: TypeBas[] = new Array(5); // variable de teste pour la traduction
  energies: ListeValeur[] = [];
  remorque: ListeValeur[] = [];
  categorieMoto: ListeValeur[] = [];
  allListeValeur: ListeValeur[] = [];
  monCategorieMoto: ListeValeur[] = [];
  listeVehiculeMoto: ListeValeur[] = [];
  monListeVehiculeMoto: ListeValeur[] = [];
  utilisationMoto: ListeValeur[] = [];
  monUtilisationMoto: ListeValeur[] = [];
  valuePuissance: number = 0;
  minNombrePlace: number = constMinNombrePlace;
  maxNombrePlace: number = constMaxNombrePlace;
  public onDeclarationForm: FormGroup;
  isFlotte = JSON.parse(window.sessionStorage.getItem(flotte));
  public simulationActif: SimulationClient;
  sousBranche;
  tabAttributs: AttributAussureBrancheDto[];
  formInvalide: boolean = false;
  minAnnee: number = constMinAnnee;
  maxAnnee: number = new Date().getFullYear();
  carosseries: ListeValeur[];
  monCarosseries: ListeValeur[]; // variable de teste pour la traduction
  dismiss: string;
  language: string;
  //cette variable permet de récupérer l'id de la branche en session
  brancheId: string;
  //cette variable permet de récupérer l'id de l'usage sélectionner
  choosenUsageId: number;
  carburant: string;
  valRemorque: string;
  objetAssure: ObjetAssures2;
  TabAttributs = [];
  sousBrancheParentAttributs = [];
  autofocus: boolean = false;
  valeurDeclaree: string;
  nombrePlace: string;
  annee: string;
  numberMask = createNumberMask({ prefix: '', thousandsSeparatorSymbol: ',', allowDecimal: true, decimalSymbol: '.' });
  assureSimulation: AssureSimulation;
  active: number = 0;
  constructor(private authService: AuthService,
    private navCtrl: NavController,
    private utilsService: UtilsService,
    private listeValeurService: ListeValeurService,
    private platform: Platform,
    private globalization: Globalization,
    private _translate: TranslateService,
    private formBuilder: FormBuilder) {
    this.maxAnnee = new Date().getFullYear();
  }

  ionViewWillEnter() {
    this.dismiss = this._translate.instant("dismiss")
  }

  ngOnInit() {
    if (sessionStorage.getItem('typeBrancheId')) {
      // Récupérer id brache depuis sessionStorage
      this.brancheId = sessionStorage.getItem('typeBrancheId');
      //console.log('Data User', data);
      console.log("brancheId", this.brancheId);
    }

    this.listeValeurService.getUsages(this.brancheId).subscribe((data) => {
      this.usages = data;
      console.log("usages", this.usages);

      const indice = data.length; // variable pour l'arrangement du tableau
      let monOrdre = data
      // Arrangement des éléments du tableau
      for (let i = 0; i < indice; i++) {
        if (i == 1) {
          this.monUsages[i] = monOrdre[i + 2];
          continue;
        }
        if (i == 3) {
          this.monUsages[i] = monOrdre[i - 2];
          continue;
        }
        this.monUsages[i] = monOrdre[i];
      }
      if (sessionStorage.getItem("active")) {
        this.active = parseInt(sessionStorage.getItem("active"))
      } else {
        this.active = 0
      }
      this.simulationActif = JSON.parse(window.sessionStorage.getItem(simulationActifenSession));
      if (this.simulationActif.assures && this.simulationActif.assures[this.active] && this.simulationActif.assures[this.active].assure.intitule) {
        this.usage = this.simulationActif.assures[this.active].assure.typeba.libelle;
        console.log(this.usage)
        this.sousBranche = this.simulationActif.assures[this.active].assure.typeba;
        // this.sousBranche.libelle = this.sousBranche.libelle
        this.choosenUsageId = this.sousBranche.id;
        let attributs = [];
        this.sousBranche.parent.attributs.forEach(att => {
          attributs.push(att)
        });
        //recuperation fils
        this.sousBranche.attributs.forEach(att => {
          attributs.push(att)
        });
        this.initForm(attributs)
        this.getUsage(this.sousBranche, true)
      }
    }, (error) => {
      console.log(error);
    })
    // On initialise le formulaire à vide et on attend les donnnées
    this.onDeclarationForm = this.formBuilder.group({})
  }

  onFocus() {
    this.autofocus = true;
  }

  initForm(attributs) {
    let control = new Object();
    attributs.forEach(att => {
      if (att.affichageEtape == 1 || att.affichageEtape == -1) {
        control[att.code] = [null, Validators.required]
        if (att.code == "valDate1ereCirculation")
          control[att.code] = [null, Validators.compose([Validators.required, Validators.min(this.minAnnee), Validators.max(this.maxAnnee), Validators.pattern('[1-2]{1}[0-9]{3}')])]
        if (att.code == "valNombrePlace")
          control[att.code] = [null, Validators.compose([Validators.required, Validators.min(this.minNombrePlace), Validators.max(this.maxNombrePlace)])]
      }
    })
    this.onDeclarationForm = this.formBuilder.group(control)
  }
  onInputNumber(e, code) {
    let chaine = e.target.value;
    chaine = chaine.split(",").join("").split(".").join("")
    if (isNaN(chaine) && e.target.value.length > 1) {
      chaine = e.target.value.slice(-1)
    }
    if (isNaN(chaine) == true && chaine.length == 1)
      chaine = "";
    if (chaine.indexOf("0") == 0) {
      chaine = chaine.slice(1)
    }
    if (code == "valDate1ereCirculation" && chaine.length > 4) {  // Pour l'année de circulation
      chaine = chaine.slice(0, 4);
    }
    if (code != "valDate1ereCirculation") {
      chaine = this.currencyFormat(chaine)
    }
    e.target.value = chaine;
    // console.log("e.target.value", e.target.value)
  }
  /**
   * Cette méthode permet de formater un chaine en format monétaire
   * @param value: any
   * @returns value formated Ex: 12,000,000
   */
  currencyFormat(chaine: any) {
    chaine = chaine.toString().split(",").join("").split(".").join("")
    let fin = "";
    if (chaine.length > 3) {
      fin = "," + chaine.slice(-3)
      if (chaine.length > 9 && chaine.length <= 12) {
        fin = "," + chaine.slice(-6, -3) + fin
        chaine = chaine.slice(0, -3)
      }
      if (chaine.length > 12 && chaine.length <= 15) {
        fin = "," + chaine.slice(-9, -6) + "," + chaine.slice(-6, -3) + fin
        chaine = chaine.slice(0, -6)
      }
      if (chaine.length > 15) {
        chaine = "000000000000000000"
        fin = ""
      }
      chaine = chaine.slice(0, -3)
    }

    let valeur = chaine.slice(0, -3);
    if (valeur.length == 1 || valeur.length == 2 || valeur.length == 3) {
      chaine = this.formatValue(chaine)
    }
    chaine += fin
    return chaine;
  }
  formatValue(valeur: string) {
    let fin = valeur.slice(-3)
    if (valeur.length == 4 || valeur.length == 5 || valeur.length == 6)
      valeur = valeur.slice(0, -3) + "," + fin
    return valeur
  }

  /**
   * Cette méthode permet de convertir une valeur au format monétaire en nombre
   * @param value : string
   * @returns value de type number; EX: 1000000
   */
  reformatValue(value) {
    value = value.split(",").join("").split(".").join("")
    value = parseInt(value)
    console.log("value", value);
    return value;
  }
  onChangeRange(e) {
    this.valuePuissance = e.detail.value;
  }

  effacerDonnees() {
    this.navCtrl.back();
  }
  // onSelectListeVal(onSelectListeVal: string) {
  //   return new Promise((resolve, reject) => {
  //     this.listeValeurService.getAllListeValeur(onSelectListeVal).subscribe(data => {
  //       resolve(data)
  //     }, (error) => {
  //       reject(error)
  //     })
  //   })
  // }
  seConnecter() {
    this.authService.logout()
    this.navCtrl.navigateRoot('/login');
  }
  verifierInformations() {
    this.verificationControlForm(this.usage)
    if (this.onDeclarationForm.get('valNombrePlace') && this.onDeclarationForm.get('valNombrePlace').value)
      this.nombrePlace = "valNombrePlace";

    if (this.onDeclarationForm.get('valDate1ereCirculation') && this.onDeclarationForm.get('valDate1ereCirculation').value)
      this.annee = "valDate1ereCirculation"
    // On enlève les virgules dans la valeur
    this.onDeclarationForm.get('mntValeurDeclaree').setValue(this.onDeclarationForm.get('mntValeurDeclaree').value.split(".").join(""))
    this.onDeclarationForm.get('mntValeurNeuve').setValue(this.onDeclarationForm.get('mntValeurNeuve').value.split(".").join(""))
    // On vérifie si la valeur initiale est supérieure à la valeur déclarée
    if (this.reformatValue(this.onDeclarationForm.get('mntValeurNeuve').value) <= this.reformatValue(this.onDeclarationForm.get('mntValeurDeclaree').value)) {
      this.utilsService.alertNotification("La valeur déclarée doit être inférieure à la valeur neuve");
      this.valeurDeclaree = "mntValeurDeclaree";
      console.log(this.reformatValue(this.onDeclarationForm.get('mntValeurNeuve').value), this.reformatValue(this.onDeclarationForm.get('mntValeurDeclaree').value));
    }
    else if (this.onDeclarationForm.invalid || this.usage == null) {
      console.log("form", this.onDeclarationForm.value);
      this.formInvalide = true;
      this.utilsService.alertNotification("Veuillez remplir tous les champs correctement pour pouvoir passer à l'étape suivante");
    }
    else if (this.onDeclarationForm.valid) {
      console.log("formulaire", this.onDeclarationForm.value);
      // Création d'un nouvel objet assuré
      this.assureSimulation = new AssureSimulation()
      this.assureSimulation.assure = new ObjetAssures2();
      if (this.sousBranche) {
        // this.sousBranche.libelle = this.sousBranche.libelle; // Pour enlever le "libeller." dans libellé
        sessionStorage.setItem(typeSousBranche, JSON.stringify(this.sousBranche));
        this.assureSimulation.branche = this.assureSimulation.assure.typeba = this.sousBranche;
        this.assureSimulation.assure.attributs = [];
        this.tabAttributs.forEach((att) => {
          switch (att.affichageEtape) {
            case 1:
              if (att.code == "mntValeurNeuve" || att.code == "mntValeurDeclaree")
                this.onDeclarationForm.get(att.code).setValue(this.reformatValue(this.onDeclarationForm.get(att.code).value))
              this.assureSimulation.assure.attributs.push(new AttributAussure(att, this.onDeclarationForm.get(att.code).value))
              break;
            case -1:
              this.assureSimulation.assure.attributs.push(new AttributAussure(att, this.onDeclarationForm.get(att.code).value))
              break;
          }
        });
      }
      this.assureSimulation.assure.intitule = this.onDeclarationForm.get("immatriculation").value;

      // on vérifie dabors si on est en flotte
      console.log("active valider", this.active);

      if (this.simulationActif.assures && this.simulationActif.assures.length > 0) {
        this.simulationActif.assures[this.active].branche = this.assureSimulation.branche;
        this.simulationActif.assures[this.active].assure = this.assureSimulation.assure;
      } else {
        this.simulationActif.assures = [];
        this.simulationActif.assures.push(this.assureSimulation);
      }
      console.log(this.simulationActif)
      window.sessionStorage.setItem(simulationActifenSession, JSON.stringify(this.simulationActif));
      window.sessionStorage.setItem(typeRoutine, JSON.stringify(typeRoutineSimulation));
      sessionStorage.setItem("choosenUsageId", this.choosenUsageId.toString());
      this.navCtrl.navigateRoot("/choiceoption")
    }
  }

  verificationControlForm(usage) {
    if (usage == UsagepersonnelEtTourisme || usage == UsageTransportVoyageur) {
      this.onDeclarationForm.removeControl('valTypeCarosserieAuto')
      this.onDeclarationForm.removeControl('valUtilisationVehiculeMoto')
      this.onDeclarationForm.removeControl('valTypeVehicule')
      this.onDeclarationForm.removeControl('valCylindree')
    }
    else if (usage == UsageMotoPersonnelEtTourisme) {
      this.onDeclarationForm.removeControl('valTypeEnergie')
      this.onDeclarationForm.removeControl('valTypeCarosserieAuto')
      this.onDeclarationForm.removeControl('valPuissance')
      this.onDeclarationForm.removeControl('valNombrePlace')
    }
    else if (usage == UsageTransportProduit || usage == UsageTransportPayantProduit) {
      this.onDeclarationForm.removeControl('valUtilisationVehiculeMoto')
      this.onDeclarationForm.removeControl('valTypeVehicule')
      this.onDeclarationForm.removeControl('valCylindree')
    }
  }

  getCarburant(carburant) {
    this.carburant = carburant;
  }

  getUsage(usageSelected, reload = false) {
    this.onFocus()
    if (sessionStorage.getItem('AttributsParent')) {
      usageSelected.parent.attributs = JSON.parse(sessionStorage.getItem('AttributsParent'));
    }
    this.utilsService.getTypeBasAndSousBrancheAttributsDTO(usageSelected.id).subscribe((data) => {
      console.log("Attributs fils", data)
      usageSelected.attributs = data;
      if (this.usage != usageSelected.libelle) {
        this.usage = usageSelected.libelle; // substr(8) pour enlever "libelle." ajouter à la chaine dans la variable de test!
        this.choosenUsageId = usageSelected.id;
        this.sousBranche = usageSelected;
        console.log("usage selected", usageSelected);

        let attributs = []
        usageSelected.parent.attributs.forEach(att => {
          attributs.push(att)
        });
        //recuperation fils
        usageSelected.attributs.forEach(att => {
          attributs.push(att)
        });
        this.initForm(attributs)
        // On remove les valeurs des champs déjà renseignés
        if (this.simulationActif.assures && this.simulationActif.assures[this.active]) {
          attributs.forEach(att => {
            switch (att.affichageEtape) {
              case 1:
                this.onDeclarationForm.get(att.code).setValue(null)
                if (att.code == 'valPuissance' || att.code == 'valCylindree') this.valuePuissance = 0;
                if (att.code == 'valRemorque') this.valRemorque = null;
                if (att.code == 'valTypeEnergie') this.carburant = null;
                break;
            }
          })
        }
        // on renseigne le champs CatégorieVéhicule par défaut
        usageSelected.parent.attributs.forEach(element => {
          if (element.affichageEtape == - 1) {
            element.listevaleur.forEach(listevaleur => {
              if (listevaleur.code == usageSelected.codeFront)
                this.onDeclarationForm.get("valCategorieVehicule").setValue(listevaleur.libelle)

            });
          }
        });
        this.tabAttributs = attributs;
      }
      else if (this.usage == usageSelected.libelle && reload) {
        this.choosenUsageId = usageSelected.id;
        this.sousBranche = usageSelected;
        console.log("usage selected", usageSelected);
        this.simulationActif.assures[this.active].assure.attributs.forEach(att => {
          switch (att.attribut.code) {
            case att.attribut.code:
              if (att.attribut.code == "mntValeurNeuve" || att.attribut.code == "mntValeurDeclaree") // Pour le forma monétaire des montants
                att.valeur = this.currencyFormat(att.valeur)
              this.onDeclarationForm.get(att.attribut.code).setValue(att.valeur)
              if (att.attribut.code == 'valPuissance' || att.attribut.code == 'valCylindree') this.valuePuissance = Number(att.valeur);
              if (att.attribut.code == 'valRemorque') this.valRemorque = att.valeur;
              if (att.attribut.code == 'valTypeEnergie') this.carburant = att.valeur;
              break;
          }
        })
        this.tabAttributs = [];
        usageSelected.parent.attributs.forEach(att => {
          this.tabAttributs.push(att)
        });
        //recuperation fils
        usageSelected.attributs.forEach(att => {
          this.tabAttributs.push(att)
        });

        // on renseigne le champs CatégorieVéhicule par défaut
        usageSelected.parent.attributs.forEach(element => {
          if (element.affichageEtape == - 1) {
            element.listevaleur.forEach(listevaleur => {
              if (listevaleur.code == usageSelected.codeFront)
                this.onDeclarationForm.get("valCategorieVehicule").setValue(listevaleur.libelle)
            });
          }
        });
      }
    })
  }

  /**GESTION LOGIQUE TRANSLATION DES LANGUES */
  ionViewDidEnter(): void {
    this.getDeviceLanguage()
  }

  public changeLanguage(): void {
    this._translateLanguage();
  }

  _translateLanguage(): void {
    this._translate.use(this.language);
  }

  _initTranslate(language) {
    // Set the default language for translation strings, and the current language.
    this._translate.setDefaultLang('fr-FR');
    if (language) {
      this.language = language;
    }
    else {
      // Set your language here
      this.language = 'fr-FR';
    }
    this._translateLanguage();
  }

  getDeviceLanguage() {
    if (!this.platform.is('android') || !this.platform.is('ios')) {
      //alert('Navigateur')
      console.log('Navigateur langage', navigator.language)
      this._initTranslate(navigator.language);
    } else {
      this.globalization.getPreferredLanguage()
        .then(res => {
          this._initTranslate(res.value)
        })
        .catch(e => { console.log(e); });
    }
  }
  /**END TRANSLATION */

}
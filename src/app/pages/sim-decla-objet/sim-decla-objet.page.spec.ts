import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SimDeclaObjetPage } from './sim-decla-objet.page';

describe('SimDeclaObjetPage', () => {
  let component: SimDeclaObjetPage;
  let fixture: ComponentFixture<SimDeclaObjetPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SimDeclaObjetPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SimDeclaObjetPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MonservicePage } from './monservice.page';

describe('MonservicePage', () => {
  let component: MonservicePage;
  let fixture: ComponentFixture<MonservicePage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MonservicePage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MonservicePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

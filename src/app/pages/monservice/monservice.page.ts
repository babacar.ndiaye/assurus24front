import { Component, OnInit } from '@angular/core';
import { UrlApiLogo, UrlApiLogoReseau, statutDemandeEnCoursDeTraitement, statutDemandeAffectee, statutDemandePriseEnCharge, statutDemandeCloturee, PrestationServiceSession } from 'src/environments/environment';
import { PrestationService } from 'src/app/models/prestationService';
import { NavController, LoadingController, ModalController } from '@ionic/angular';
import { UtilsService } from 'src/app/services/utils.service';
import { FormBuilder } from '@angular/forms';
import { CompteService } from 'src/app/services/compte.service';

@Component({
  selector: 'app-monservice',
  templateUrl: './monservice.page.html',
  styleUrls: ['./monservice.page.scss'],
})
export class MonservicePage implements OnInit {

  service  ;
  urlApiLogo= UrlApiLogo;
  urlApiLogoReseau= UrlApiLogoReseau;
 
  
  
  
  date :Date = new Date(null) ;
  titreStatutVisible : boolean ;
  infoServVisible : boolean ;
  docServVisible : boolean ;
  infoPrestataireVisible  : boolean ;
  messageVisible : boolean ;
 
  titreStatutParam = "titreStatu" 
  infoServParam = "infoServParam" 
  docServParam = "docServParam" 
  infoPrestataireParam = "infoPtopParam"  
  messageParam = "messageParam" 
  
  statutDemandeEnCoursDeTraitementLoc =statutDemandeEnCoursDeTraitement   
  statutDemandeAffecteeLoc = statutDemandeAffectee
  statutDemandePriseEnChargeLoc = statutDemandePriseEnCharge
  statutDemandeClotureeLoc = statutDemandeCloturee  
  colonneReseau:boolean=false;
  
  constructor(private navCtrl : NavController,
    
    public utilsService :UtilsService,
    private formBuilder: FormBuilder,
    private loadingCtrl: LoadingController,
    private serviceCompte:CompteService,
    private modalController: ModalController) {
      this.service = new PrestationService() ;

      this.chargerInfos() ;
      
      
      
    }
    
    ngOnInit() {
      
    }
    
    
    
    
    
    
    
    
    
    
    ionViewWillEnter(){
      this.initialiserSectiontoInvisible()
      this.chargerInfos() ;
    }
    chargerInfos(){
      
      
      this.service = JSON.parse(window.sessionStorage.getItem(PrestationServiceSession) );
      
      if( this.service ==null)
      {
        this.utilsService.alertNotification("La session a expiré veuillez vous reconnecter") ;
        this.navCtrl.navigateRoot("/login") ;
        
      }
     
      this.utilsService.getDate().subscribe(
        dateFormatMobile => {this.date= new Date(dateFormatMobile.date)},
        error =>{
          let message ="Oups le serveur est introuvable verfiez votre connexion et recommencez svp" ;
          this.utilsService.alertNotification(message) ;
          this.navCtrl.navigateRoot("/login") ;
        }) ;
      
        
       
        } 
      
      
       
     
        
      
        
        
        initialiserSectiontoInvisible(){
          this.titreStatutVisible = true ;
          this.infoServVisible = true  ;
          this.docServVisible = false  ;
          this.infoPrestataireVisible = false ;
          this.messageVisible = false;
         
          
        }
        affichageDetails(section,visible:boolean)
        {
          switch(section){
            case this.titreStatutParam: 
            this.titreStatutVisible=visible
            break ;
            case this.infoServParam:
            this.infoServVisible=visible  
            break ;
            case this.docServParam:
            this.docServVisible=visible  
            break ;
            case this.infoPrestataireParam:
            this.infoPrestataireVisible= visible
            break ;
            case this.messageParam:
            this.messageVisible=visible ;
            break ;
           
          }
        }
       
       
      }
      
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { AssurinfoveVehiculeFlottePage } from './assurinfove-vehicule-flotte.page';
import { ComponentsModule } from 'src/app/components/components.module';

//import { ArianeComponent } from 'src/app/components/ariane/ariane.component';

const routes: Routes = [
  {
    path: '',
    component: AssurinfoveVehiculeFlottePage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
   
    ComponentsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [AssurinfoveVehiculeFlottePage]
})
export class AssurinfoveVehiculeFlottePageModule {}

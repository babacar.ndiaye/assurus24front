import { Contrat2 } from 'src/app/models/contrat';
import { Component, OnInit, ViewChild } from '@angular/core';
import { listeSimulationDansFlotteValide, typeRoutineSimulation, typeRoutineNewContrat, typeRoutineRenewContrat, typeRoutine, UrlApi, simulationActifenSession, flotte, OriginePageGesSim, OriginePageMonObjet, listeSimulationDansFlotte, typeRenouvellement, typeRenouvellementChangementDeCA, typeRenouvellementMemeCa, contratActifenSession, operationActifenSession, objetActifenSession, CONDITIONSPATHNAME, downloadConditionsGenerales, ListeSimulationEnSession, numeroPivot, editOptions, devise, UrlApiLogo, listeRoutineContratSession, indexRoutineContratEditingSession, listeFlotteOrigineGestionSimulation, LIENLOGIN, LIENASSURINFOVE, editObjet, LIENSIMDECLAOBJET, LIENCHOICEOPTION, LIENECRANRECAPITULATIF, LIENMESCONTRATS, LIENASSURINFOVEVEHICULEFLOTTE, typeSousBranche } from 'src/environments/environment';
import { LoadingController, NavController, AlertController, IonSlides } from '@ionic/angular';
import { Automobile } from 'src/app/models/automobile';
import { ControleTechnique } from 'src/app/models/controle-technique';
import { UtilsService } from 'src/app/services/utils.service';
import { Simulation } from 'src/app/models/simulation';
import { CompteService } from 'src/app/services/compte.service';
import { FormBuilder, Validators } from '@angular/forms';
import { AuthService } from 'src/app/services/auth.service';
import { CompagniePrix } from 'src/app/pojos/compagnie-prix';
import { Paiement } from 'src/app/models/paiement';
import { Operation } from 'src/app/models/operation';
import { ConditionGenerale } from 'src/app/models/condition-generale';
import { CompagnieObjetPrix } from 'src/app/pojos/compagnie-objet-prix';
import { AlertOptions } from '@ionic/core';
import { ObjetAssures, ObjetAssures2 } from 'src/app/models/objet-assures';
import { resolve } from 'dns';
import { DateFormatMobile } from 'src/app/pojos/date-format-mobile';
import { Observable, forkJoin, observable } from 'rxjs';
import { RoutineContrat } from 'src/app/pojos/routine-contrat';

@Component({
    selector: 'app-assurinfove-vehicule-flotte',
    templateUrl: './assurinfove-vehicule-flotte.page.html',
    styleUrls: ['./assurinfove-vehicule-flotte.page.scss'],
})
export class AssurinfoveVehiculeFlottePage implements OnInit {
    listeSimFlotte: any = [];
    typeOrigineRoutine;
    logoCA
    deviseMonnaie = devise;
    typeRoutineSimulation = typeRoutineSimulation;
    typeRoutineNewContrat = typeRoutineNewContrat;
    typeRoutineRenewContrat = typeRoutineRenewContrat;
    @ViewChild('fin') inputFin;
    @ViewChild('debut') inputDebut;
    @ViewChild('jours') inputJours;
    @ViewChild('mois') inputMois;
    @ViewChild('annees') inputAnnees;
    @ViewChild('duree') inputDuree;
    classisActive: boolean = false;
    public simulationActif: Simulation = new Simulation();
    public contratActif: Contrat2 = new Contrat2
    public operationActif: Operation = new Operation
    public objetActif: Automobile = new Automobile
    endChargement = false;


    public items: any = [];
    listeCompagniePrix;
    compagniePrixActif;
    caActif;

    duree: number
    valueDuree;
    listeSimulations: any;

    listeAllSimFlotte: Array<Simulation> = [];
    dateDebutError: boolean = false;
    dateFinError: boolean = false;
    dateDebut: Date;
    dateFin: Date;
    typeGaranties = [];
    resultatGeneralTestALLPeriode: boolean;
    observablesTest;
    listeRoutineContrat: Array<RoutineContrat> = []
    isFlotte = JSON.parse(window.sessionStorage.getItem(flotte));
    cachebouton = true;
    originePageMonObjet = OriginePageMonObjet;
    origineGesSimultation = OriginePageGesSim;
    liste = [];
    onDeclarationForm: any;
    dateFinReference: Date = null;
    ifAddNewVehicule: boolean = false;
    dureeRef: number;
    dateDuLendemain: Date;
    dateDebutMinimum = new Date(null);
    dateFinMaximumBeforeOther = new Date(null);
    @ViewChild('slideWithNav') slideWithNav: IonSlides;
    infosValide: boolean = true;

    sliderOne: any;


    @ViewChild('mySlider') mySlider: IonSlides;

    ionViewDidLoad() {
        this.mySlider.lockSwipes(true);
    }
    ionViewWillLeave() {
        this.loadingCtrl.dismiss();
    }
    //Configuration for each Slider
    slideOptsOne = {
        initialSlide: 0,
        slidesPerView: 1,

        spaceBetween: 0,
        //  autoplay: true,
        speed: 1000


    };
    slideChanged(slideView) {
        switch (slideView) {

            //  case "infos" : this.slideWithNav.startAutoplay();break ;
        }
    }
    swipeNext() {
        this.slideWithNav.slideNext();

    }
    swipeBack() {
        this.slideWithNav.slidePrev();
    }

    constructor(private loadingCtrl: LoadingController,

        public utilsService: UtilsService,

        private serviceCompte: CompteService,


        private navCtrl: NavController,
        private alertCtrl: AlertController,
        public authService: AuthService,
        private formBuilder: FormBuilder, ) {
        this.listeCompagniePrix = new Array<CompagniePrix>();
        this.items = []
    }


    //hhhh
    async recupererListSimFlotteDeLaSimActive() {
        this.typeOrigineRoutine = JSON.parse(window.sessionStorage.getItem(typeRoutine));
        ///console.log(this.typeOrigineRoutine)
        switch (this.typeOrigineRoutine) {
            case this.typeRoutineSimulation:
                this.listeSimFlotte = JSON.parse(window.sessionStorage.getItem(listeSimulationDansFlotteValide));
                this.listeSimFlotte.forEach((element, index) => {
                    console.log(element.objet.infosValide, index);

                    if(element.objet.infosValide == undefined){
                        this.infosValide=true;
                    }else{
                        this.infosValide=false;
                    }
                    console.log(this.infosValide);
                    //alert("ci biir")
                });
                console.log("listeSimFlotte dans flotte",this.listeSimFlotte)
                if (this.listeSimFlotte == null) {
                    this.loadingCtrl.dismiss();
                    this.navCtrl.navigateRoot(LIENSIMDECLAOBJET);
                }
                else
                    if (this.listeSimFlotte[0].ca != null) {
                        this.caActif = this.listeSimFlotte[0].ca
                        this.logoCA = UrlApiLogo + this.listeSimFlotte[0].ca.id
                    }
                break;
        }
    }

    verifierInfosVehiculesManquants(infosAutomobile) {
        //a completer
    }
    editInfos(simulation) {
        sessionStorage.setItem(typeSousBranche,JSON.stringify(simulation.objet.typeba));
        sessionStorage.setItem(simulationActifenSession, JSON.stringify(simulation))
        sessionStorage.setItem("pagePrecedent", LIENASSURINFOVEVEHICULEFLOTTE)
        this.navCtrl.navigateForward(LIENASSURINFOVE)
    }
    /**
     * Ctte fonction permet de modifier uniquement les garanties d'un objet lors d'ue routine de simulaton ou routine de contrat
     * @param objet = simulation ou routine
     */
    modifierOptions(typeRoutine, objet) {
        sessionStorage.setItem(editOptions, JSON.stringify(true))

        switch (typeRoutine) {
            case typeRoutineSimulation:
                sessionStorage.setItem(simulationActifenSession, JSON.stringify(objet))
                break;
            case typeRoutineNewContrat:
            case typeRoutineRenewContrat:
                window.sessionStorage.setItem(contratActifenSession, JSON.stringify(objet.contrat));
                window.sessionStorage.setItem(operationActifenSession, JSON.stringify(objet.operation));
                window.sessionStorage.setItem(objetActifenSession, JSON.stringify(objet.objet));
                window.sessionStorage.setItem(indexRoutineContratEditingSession, JSON.stringify(this.listeRoutineContrat.indexOf(objet)));
                break;

        }


        /*on mets la routine à modifier en session pour la remplacer lorsque l'utilisateur a validé l'objet et
        les options dans la routine de modification
        */



        this.navCtrl.navigateForward(LIENCHOICEOPTION)




    }
    delete(numeroFlotte) {
        for (let index = 0; index < this.listeSimFlotte.length; index++) {

            if (this.listeSimFlotte[index].numeroDansFlotte == numeroFlotte) {
                this.listeSimFlotte.splice(index, 1)
                //on introduit la modification de liste en session qui regrouve tous les flottes
                this.liste = JSON.parse(sessionStorage.getItem(listeSimulationDansFlotte))
                this.misAjourListe();
                sessionStorage.setItem(listeSimulationDansFlotteValide, JSON.stringify(this.listeSimFlotte));
                window.location.reload()
                return;

            }


        }

    }

    validerFlotte(compagnie, montant, type) {
        this.typeOrigineRoutine = JSON.parse(window.sessionStorage.getItem(typeRoutine));
        console.log(this.typeOrigineRoutine)
        switch (this.typeOrigineRoutine) {
            case typeRoutineSimulation:
                this.verifierIfFlotteValide().then(
                    ok => {
                        if (!ok) {
                            this.utilsService.alertNotification("veuillez remplir les informations de tous les vehicules de la flotte ")
                        }
                        else {
                            this.utilsService.verifierUniciteDansFlotte(this.listeSimFlotte).then(
                                res => {
                                    if (!res) {
                                        this.utilsService.alertNotification("Un véhicule ne peut pas figurer deux fois dans un même contrat")
                                    }
                                    else {
                                        this.utilsService.getDate().subscribe(
                                            dateFormatMobile => {
                                                this.testIfEachVehiculeCanHaveContrat(dateFormatMobile).then(
                                                    res => {
                                                        forkJoin(this.observablesTest).subscribe(
                                                            () => {
                                                                console.log(this.resultatGeneralTestALLPeriode)
                                                                if (this.resultatGeneralTestALLPeriode) {
                                                                    this.validerCAEtMontant(compagnie, montant).then(
                                                                        async () => {
                                                                            sessionStorage.setItem(simulationActifenSession, JSON.stringify(this.simulationActif));
                                                                            console.log(this.listeSimFlotte)
                                                                            sessionStorage.setItem(listeSimulationDansFlotteValide, JSON.stringify(this.listeSimFlotte));
                                                                            console.log("ras")
                                                                            const loader = await this.loadingCtrl.create({
                                                                                message: "ajout en cours",
                                                                            });
                                                                            loader.present()
                                                                            this.navCtrl.navigateForward(LIENECRANRECAPITULATIF)
                                                                        }
                                                                    )

                                                                }

                                                            }
                                                        )
                                                    }
                                                )
                                            },
                                            error => {

                                            }
                                        )
                                    }
                                }
                            )


                        }
                    }
                )
                break;
            case typeRoutineRenewContrat:
            case typeRoutineNewContrat:
                if (this.typeOrigineRoutine == typeRoutineNewContrat)
                    sessionStorage.setItem(typeRenouvellement, JSON.stringify(typeRenouvellementChangementDeCA));
                else {
                    switch (type) {
                        case true: sessionStorage.setItem(typeRenouvellement, JSON.stringify(typeRenouvellementMemeCa)); break;
                        case false: sessionStorage.setItem(typeRenouvellement, JSON.stringify(typeRenouvellementChangementDeCA)); break;
                    }
                }
                //////console.log(compagnie) ;
                ///test verification
                this.utilsService.verifierUniciteDansFlotte(this.listeRoutineContrat).then(
                    res => {
                        if (!res) {
                            this.utilsService.alertNotification("Un véhicule ne peut pas figurer deux fois dans un même contrat")
                        }
                        else {
                            this.utilsService.getDate().subscribe(
                                dateFormatMobile => {
                                    this.testIfEachVehiculeCanHaveContrat(dateFormatMobile).then(
                                        res => {
                                            forkJoin(this.observablesTest).subscribe(
                                                () => {
                                                    console.log(this.resultatGeneralTestALLPeriode)
                                                    if (this.resultatGeneralTestALLPeriode) {
                                                        this.validerCAFlotteRoutineContrat(compagnie).then(
                                                            () => {
                                                                let paiement = new Paiement();
                                                                paiement.montant = montant;
                                                                this.operationActif.paiement = paiement;

                                                                window.sessionStorage.setItem(contratActifenSession, JSON.stringify(this.contratActif))
                                                                window.sessionStorage.setItem(operationActifenSession, JSON.stringify(this.operationActif))
                                                                window.sessionStorage.setItem(objetActifenSession, JSON.stringify(this.objetActif));
                                                                sessionStorage.setItem(listeRoutineContratSession, JSON.stringify(this.listeRoutineContrat))
                                                                this.navCtrl.navigateForward(LIENECRANRECAPITULATIF);



                                                            }
                                                        )

                                                    }

                                                }
                                            )
                                        }
                                    )
                                },
                                error => {

                                }
                            )
                        }
                    }
                )

                //fint test verification



                break;






        }
    }
    verifierIfFlotteValide(): Promise<Boolean> {
        let ok = true;
        var p1;
        this.listeSimFlotte.forEach(simulation => {
            console.log("verifierIfFlotteValide",simulation);

            if (!simulation.objet.infosValide) {
                ok = false;
                p1 = new Promise<Boolean>(function (resolve, reject) {
                    resolve(ok);
                    // ou
                    // reject("Erreur !");
                });
                return p1;


            }
        });
        p1 = new Promise<Boolean>(function (resolve, reject) {
            resolve(ok);
            // ou
            // reject("Erreur !");
        });
        return p1;

    }
    //fin gestion flotte


    initialiseItems() {
        this.items = []
        //console.log(this.listeCompagniePrix)
        if (this.listeCompagniePrix.length == 0) {
            window.sessionStorage.removeItem(simulationActifenSession);
            this.utilsService.alertNotification("Vos critères de sélection ne correspondent à aucune Offre d’assurance. Merci de modifier vos critères.")
            this.loadingCtrl.dismiss();
            this.navCtrl.navigateRoot(LIENSIMDECLAOBJET);


        }
        console.log(this.compagniePrixActif)
        //   let item =   {   expanded: false,
        //     name: 'Risques Couverts',
        //     zipped:true,
        //     prix : this.compagniePrixActif.prix,
        //     compagnie:this.compagniePrixActif.ca ,
        //     id: this.compagniePrixActif.ca.id  ,
        //     caActuel : true,
        //     logo : UrlApi+"/file/downloadLogo/"+this.compagniePrixActif.ca.id ,

        // }
        // this.items = []
        //  this.items.push(item) ;
        console.log(this.items)



        this.listeCompagniePrix.sort((a, b) => a.prix - b.prix);
        this.listeCompagniePrix.forEach(compagniePrix => {
            let item = {
                expanded: false,
                name: 'Risques Couverts',
                zipped: true,
                prix: compagniePrix.prix,
                compagnie: compagniePrix.ca,
                id: compagniePrix.ca.id,
                caActuel: false,
                logo: UrlApiLogo + compagniePrix.ca.id,
                objetsPrix: compagniePrix.caObjetPrix,

            }
            this.items.push(item);

        });
        console.log(this.items)  //console.log("items")
        //console.log(this.items)
        //console.log(this.listeSimFlotte)
        this.insererGondole();
        this.endChargement = true;

        this.loadingCtrl.dismiss();
    }
    displayDate(date: Date): String {

        date = new Date(date);
        let jour, mois;
        let dd = date.getDate();

        let mm = date.getMonth() + 1;
        let yyyy = date.getFullYear();
        if (dd < 10) {
            jour = '0' + dd;
        }
        else
            jour = dd;

        if (mm < 10) {
            mois = '0' + mm;
        }
        else
            mois = mm;
        return jour + "/" + mois + "/" + yyyy;
    }
    ngOnInit() {
        this.onDeclarationForm = this.formBuilder.group({
            'debut': [null, Validators.compose([
                Validators.required
            ])],
            'fin': [null, Validators.compose([
                Validators.required
            ])],
        })

    }

    async ionViewWillEnter() {
        const loader = await this.loadingCtrl.create({
            message: 'chargement en cours',
        });

        loader.present();
        // console.log(this.choice.dateDebut)
        this.chargerInfoSimSession();
        this.cachebouton = false;
        console.log(this.compagniePrixActif)
        //////console.log(this.simulationActif) ;

    }
    ionViewDidLeave() {
        this.cachebouton = true;
    }

    captureEventName(event: any) {
        //////console.log("ok");
    }



    expandItem(item): void {
        item.zipped = !item.zipped;
        /*for (let i = 1; i < 9; i++) {
        if (i.toString() === item.id) { continue; }
        document.getElementById(i.toString()).setAttribute(name, 'remove');
        }*/
        if (item.expanded) {
            item.expanded = false;
        } else {
            this.items.map(listItem => {
                if (item === listItem) {
                    listItem.expanded = !listItem.expanded;
                }
                return listItem;
            });
        }
    }
    recupererConditionsGarantie(garantie, ca): ConditionGenerale {
        //on va recuperer les conditions de la garantie  proposé par la compagnie d'assurnace
        let conditionsGenerales: ConditionGenerale = {

            id: 1,

            conditionsSpeciales: ["Les garanties sont valables pour la traversée de la gambie", "les garanties sont valables pour la casamance", "les garanties sont valables pour la casamance", "les garanties sont valables pour la casamance", "les garanties sont valables pour la casamance", "les garanties sont valables pour la casamance", "les garanties sont valables pour la casamance", "les garanties sont valables pour la casamance"],
            garantie: null,

        }

        return conditionsGenerales;
    }
    async chargerConditionsGenerales(id) {
        const loader = await this.loadingCtrl.create({
            message: '',
        });

        loader.present();
        // To download the PDF file
        this.serviceCompte.download(CONDITIONSPATHNAME, id, downloadConditionsGenerales);
    }
    async chargerInfoSimSession() {
        this.loadingCtrl.dismiss();
        //debut
        this.typeOrigineRoutine = JSON.parse(window.sessionStorage.getItem(typeRoutine));
        ///console.log(this.typeOrigineRoutine)
        switch (this.typeOrigineRoutine) {
            case this.typeRoutineSimulation:
                this.simulationActif = JSON.parse(window.sessionStorage.getItem(simulationActifenSession));
                if (this.simulationActif == null) {
                    this.loadingCtrl.dismiss();
                    this.navCtrl.navigateRoot(LIENSIMDECLAOBJET);

                }
                else {
                    this.onDeclarationForm.get('debut').value = this.simulationActif.dateDebut;
                    this.onDeclarationForm.get('fin').value = this.simulationActif.dateFin;
                    let dureeEnJour = this.utilsService.diffdate(this.simulationActif.dateFin, this.simulationActif.dateDebut, "jour")
                    this.valueDuree = this.utilsService.recupererDuree(dureeEnJour);

                    this.recupererListSimFlotteDeLaSimActive().then(
                        () => {
                            console.log(this.listeSimFlotte)

                            this.recupererCompagniePrix();
                        }
                    )


                }
                //dev à faire au niveau du back
                //pour chaque compagnie verifiez sil respecte les garanties
                //por chaque garanti et compagnie assurance...
                //retourner un pojo de la classe compagniePrixProduit

                break;///
            case this.typeRoutineRenewContrat:
            case this.typeRoutineNewContrat:
                {
                    this.contratActif = JSON.parse(window.sessionStorage.getItem(contratActifenSession));
                    this.objetActif = JSON.parse(window.sessionStorage.getItem(objetActifenSession));
                    this.operationActif = JSON.parse(window.sessionStorage.getItem(operationActifenSession));

                    //////console.log("garanties recus resultats")
                    //////console.log(this.operationActif.garanties)


                    if (this.contratActif == null) {
                        this.loadingCtrl.dismiss();
                        this.navCtrl.navigateRoot(LIENMESCONTRATS);

                    }
                    else {

                        //////console.log(this.contratActif.ca)
                        if (this.typeOrigineRoutine == typeRoutineRenewContrat)
                            this.caActif = this.contratActif.myCA;
                        this.typeGaranties = [];

                        this.operationActif.details.forEach(element => {
                            //////console.log(element)
                            this.typeGaranties.push(element.garantie.typegarantie)


                        });
                        //////console.log(this.typeGaranties) ;
                        //recupererCompagniePrix prend en paramtre soit la solution,les contrats et à définir
                        this.listeRoutineContrat = JSON.parse(sessionStorage.getItem(listeRoutineContratSession))
                        if (this.listeRoutineContrat == null)
                            this.listeRoutineContrat = [];
                        console.group("listeContrat")
                        console.log(this.listeRoutineContrat)
                        //////console.log(this.typeGaranties) ;
                        //recupererCompagniePrix prend en paramtre soit la solution,les contrats et à définir
                        this.onDeclarationForm.get('debut').value = this.operationActif.dateEffet;
                        this.onDeclarationForm.get('fin').value = this.operationActif.dateCloture;
                        let dureeEnJour = this.utilsService.diffdate(this.operationActif.dateCloture, this.operationActif.dateEffet, "jour")
                        this.valueDuree = this.utilsService.recupererDuree(dureeEnJour);
                        this.recupererCompagniePrix();
                    }
                    //dev à faire au niveau du back
                    //pour chaque compagnie verifiez sil respecte les types garanties
                    //por chaque type garanti et compagnie assurance...
                    //retourner un pojo de la classe compagniePrixProduit

                    break;
                }
            // default :this.effacerDonnees() ;

        }

        //fin

    }

    //on va envoyer au serveur les types de garanties utilisés et on va recuperer la liste des cople ca montan
    //pour contrat METTRE LES PARAMETRES§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§
    async recupererCompagniePrix() {
        console.log("calculer prix")
        this.typeOrigineRoutine = JSON.parse(sessionStorage.getItem(typeRoutine));
        // switch(this.typeOrigineRoutine){
        //   case typeRoutineSimulation :
        //   //////console.log(this.simulationActif.typeGaranties) ;
        //    if(this.simulationActif.typeGaranties == null || this.simulationActif.typeGaranties.length ==0)
        //   this.loadingCtrl.dismiss() ;
        //                                 this.utilsService.alertNotification("Desolé une erreur est survenue veuillez recommencer") ;
        //                                 this.navCtrl.navigateRoot("/choiceoption");

        //                                 break ;
        //   case typeRoutineRenewContrat :
        //   case typeRoutineNewContrat :
        //   //////console.log(this.operationActif.garanties)
        //    if( this.operationActif.garanties.length ==0)
        //   this.loadingCtrl.dismiss() ;
        //   this.utilsService.alertNotification("Desolé une erreur est survenue veuillez recommencer") ;
        //   this.navCtrl.navigateRoot("/choiceoption");
        //    break ;


        // }
        let compagnie1 = {
            id: 1,

            nom: "AXA",

            registreCommerce: "hfhfh",

            compte: null,

            communicationCommerciales: [],

            bas: [],

            listeNoire: null,

            reseaux: [],


            compteCredits: []
        }
        let compagnie2 = {
            id: 2,

            nom: "ALLIANZ",

            registreCommerce: "hfhfh",

            compte: null,

            communicationCommerciales: [],

            bas: [],

            listeNoire: null,

            reseaux: [],


            compteCredits: []
        }
        let compagnie3 = {
            id: 3,

            nom: "AMSA",

            registreCommerce: "hfhfh",

            compte: null,

            communicationCommerciales: [],

            bas: [],

            listeNoire: null,

            reseaux: [],


            compteCredits: []
        }
        let compagnie4 = {
            id: 4,

            nom: "ASKIA",

            registreCommerce: "hfhfh",

            compte: null,

            communicationCommerciales: [],

            bas: [],

            listeNoire: null,

            reseaux: [],


            compteCredits: []
        }
        let caObjetPrix = new Array<CompagnieObjetPrix>();
        //alerte garder le meme classement que sur la listesimFlotte pour pouvoir utiliser l'index
        let index;
        //console.log(this.listeSimFlotte)
        this.listeSimFlotte.forEach(simulation => {
            //console.log("a")
            index = this.getIndex(simulation.numeroDansFlotte);
            //console.log(index)
            if (index >= 0)
                caObjetPrix.push(new CompagnieObjetPrix(this.listeSimFlotte[index].objet, 5000 * (index + 1)));

        });
        //console.log(caObjetPrix)
        this.listeCompagniePrix = new Array<CompagniePrix>();
        //on simule les total en attedant de mettre en place lagorithme de calcule prix
        let totalSimuler = (5000 * (this.listeSimFlotte.length + 1));
        let indexcompagnie = 10;
        await this.listeCompagniePrix.push(new CompagniePrix(compagnie1, totalSimuler + (indexcompagnie-- * 1000), caObjetPrix), new CompagniePrix(compagnie2, totalSimuler + ((indexcompagnie - 2) * 1000), caObjetPrix), new CompagniePrix(compagnie3, totalSimuler + (indexcompagnie-- * 1000), caObjetPrix), new CompagniePrix(compagnie4, totalSimuler, caObjetPrix))
        //Removes checkbox from array when you uncheck it
        //////console.log("typeOrigneRoutine")

        //////console.log("comparaison") ;
        this.listeCompagniePrix.forEach(element => {

            //////console.log(element.ca.id+" "+this.caActif.id)
            console.log(this.caActif)
            if (this.caActif != null && element.ca.id == this.caActif.id) {
                let index = this.listeCompagniePrix.indexOf(element);
                console.log(index)
                this.compagniePrixActif = this.listeCompagniePrix[index];
                this.listeCompagniePrix.splice(index, 1);

                //console.log(this.compagniePrixActif) ;
                //////console.log(this.listeCompagniePrix) ;

            }


        });



        this.initialiseItems();
        //console.log(this.listeSimFlotte)

    }



















    async selectionBranche() {
        this.utilsService.selectionBrancheSimulation(OriginePageGesSim);
    }


    //on recuperera les branches à partir de l'api
    recupererBranches() {
        return this.utilsService.recupererTypeBranches();


    }

    //selectiond des simulations
    async selectionSimulation() {
        var options: AlertOptions = {
            header: 'Simulations',
            message: 'Veuillez choisir parmi ces simulations',
            buttons: [
                {
                    text: 'Cancel',
                    role: 'cancel',
                    handler: () => {
                        //////console.log('Cancel clicked');
                    }
                },
                {
                    text: 'Ok',
                    handler: data => {
                        //////console.log(data)
                        if (data) {
                            //////console.log("data") ;
                            //////console.log(data) ;

                            sessionStorage.setItem(simulationActifenSession, JSON.stringify(data));
                            this.simulationActif = data;
                            this.recupererListSimFlotteDeLaSimActive().then(
                                () => {
                                    console.log(this.listeSimFlotte)
                                    this.recupererCompagniePrix();
                                }
                            )
                            //window.location.reload() ;



                        }


                    }
                }
            ]
        };

        options.inputs = [];

        // Now we add the radio buttons
        this.recuperationSimulationSession();
        for (let i = 0; i < this.listeSimulations.length; i++) {
            options.inputs.push({ name: 'Simulation numero ' + i, value: this.listeSimulations[i], label: 'Simulation numero ' + this.listeSimulations[i].numeroDansSession, type: 'radio' });
            //////console.log(this.listeSimulations[i]) ;
        }

        // Create the alert with the options
        let alert = this.alertCtrl.create(options);
        (await alert).present();
    }





    //selectin
    //recuperer simulation
    recuperationSimulationSession() {
        this.listeSimulations = JSON.parse(window.sessionStorage.getItem(ListeSimulationEnSession));
        ////console.log(this.listeSimulations)
        if (this.listeSimulations == null)
            this.listeSimulations = new Array<Simulation>();



    }



    seConnecter() {
        this.authService.logout()
        this.loadingCtrl.dismiss();
        this.navCtrl.navigateRoot(LIENLOGIN);
    }

    enregistrer() {
        this.utilsService.enregistrerSimulation(this.listeSimFlotte);
    }
    remplacerSimulation(listeSimulation) {
        listeSimulation.forEach(simulation => {


            if (simulation.numeroDansSession == this.simulationActif.numeroDansSession)
                simulation = this.simulationActif;

            listeSimulation[simulation.index] = this.simulationActif


        });


    }
    chercherIndexCompagnie(caActif): number {

        this.listeCompagniePrix.forEach(compagniePrix => {
            //////console.log(compagniePrix.ca.id+" "+caActif.id)
            if (compagniePrix.ca.id == caActif.id)
                return this.listeCompagniePrix.indexOf(compagniePrix)

        });
        return

    }
    // precedent(){
    //   sessionStorage.setItem(retourFlotte,JSON.stringify(true)) ;
    // }


    ajustInput(nom) {

        switch (nom) {
            case "debut":
                this.dateDebutError = false;
                break;
            case "fin":

                ////console.log(this.inputFin.value)
                this.dateFinError = false; break;
        }
    }
    recupererNumeroSessionPivot(): number {

        ////console.log("fai")
        let numeroPivot = this.listeSimFlotte[0].numeroDansFlotte;
        for (let index = 1; index < this.listeSimFlotte.length; index++) {
            if (this.listeSimFlotte[index].numeroDansFlotte < numeroPivot)
                numeroPivot = this.listeSimFlotte[index].numeroDansFlotte;


        }
        console.log(numeroPivot)
        return numeroPivot;

    }
    precedent() {
        sessionStorage.setItem(simulationActifenSession, JSON.stringify(this.listeSimFlotte[0]))
        sessionStorage.setItem(numeroPivot, JSON.stringify(this.recupererNumeroSessionPivot()));
        ////console.log("after")
        ////console.log(this.listeSimFlotte)
    }




    getIndex(numeroFlotte): number {
        for (let index = 0; index < this.listeSimFlotte.length; index++) {

            if (this.listeSimFlotte[index].numeroDansFlotte == numeroFlotte) {
                //console.log(index)
                return index;

            }


        }
        return -1;

    }
    getIndexListePrincipale(numeroFlotte) {
        for (let index = 0; index < this.liste.length; index++) {

            if (this.liste[index].numeroDansFlotte == numeroFlotte) {
                //console.log(index)
                return index;

            }


        }
        return -1;

    }
    ///ajout
    addVehicule() {
        this.typeOrigineRoutine = JSON.parse(window.sessionStorage.getItem(typeRoutine));
        switch (this.typeOrigineRoutine) {
            case typeRoutineSimulation:
                //////console.log(compagnie) ;
                this.debutFlotte();

                break;


                break;
            case typeRoutineRenewContrat:
            case typeRoutineNewContrat:
                this.debutFlotteContrat();




                break;
            //  default :this.effacerDonnees() ;


        }

    }
    debutFlotte() {
        ////////console.log(this.listeSimFlotte) ;

        sessionStorage.setItem(numeroPivot, JSON.stringify(this.recupererNumeroSessionPivot()));
        sessionStorage.setItem(flotte, JSON.stringify(true));
        // this.simulationActif.dateDebut = null ;
        let sim: Simulation = new Simulation();


        //reference : string;

        sim.client = this.simulationActif.client
        sim.dateSimulation = new Date(this.simulationActif.dateSimulation)
        //relations à ajoutéé


        //type de garanties pour uniformiser dans le choice option

        sim.dateDebut = new Date(this.utilsService.displayDateFormatInitialise(this.simulationActif.dateDebut))
        // ////////console.log(this.utils.displayDate(this.simulationActif.dateDebut)) ;
        ////////console.log(this.utilsService.displayDate(sim.dateDebut)) ;
        sim.dateFin = new Date(this.simulationActif.dateFin)
        sim.origineRoutine = this.simulationActif.origineRoutine
        sim.numeroDansSession = this.simulationActif.numeroDansSession
        //sim.ca =this.simulationActif.ca
        sim.montantTTCCompagnie = this.simulationActif.montantTTCCompagnie
        sim.objet = new ObjetAssures2();

        sim.typeGaranties = [];
        sessionStorage.setItem(simulationActifenSession, JSON.stringify(sim));

        this.navCtrl.navigateRoot(LIENSIMDECLAOBJET);

    }
    ///fin ajout
    async misAjourListFirstStep() {
        var p1 = new Promise(function (resolve, reject) {
            resolve("Succès !");
            // ou
            // reject("Erreur !");
        });
        this.liste = JSON.parse(sessionStorage.getItem(listeSimulationDansFlotte));
        ////console.log(this.liste)
        for (let index = 0; index < this.liste.length; index++) {

            if (this.liste[index].numeroDansSession == this.simulationActif.numeroDansSession) {
                this.liste.splice(index, 1);
                ////console.log(o)
                ////console.log(this.liste)
            }

        }

        return p1;

    }
    async misAjourSecondStep() {
        var p1 = new Promise(function (resolve, reject) {
            resolve("Succès !");
            // ou
            // reject("Erreur !");
        });
        console.log(this.simulationActif.numeroDansFlotte)
        this.listeSimFlotte.forEach(element => {
            if (element.numeroDansFlotte != this.simulationActif.numeroDansFlotte)
                this.liste.push(element)
        });
        return p1;

    }
    ajustInputSecondeSession() {


        ////console.log(this.inputFin.value)
        this.dateFinError = false;
        this.finOuDureeFirstOption();

    }
    async finOuDureeFirstOption() {

        if (this.checkDureeFirstOption()) {

            this.dateFin = new Date(this.inputDebut.value);;
            ////console.log(this.dateFin)
            ////console.log(this.inputDuree.value)
            if (this.inputDuree.value == 15)
                this.dateFin.setDate(this.dateFin.getDate() + 15);
            else {
                ////console.log(this.dateFin)
                ////console.log(this.inputDuree.value)
                this.dateFin.setMonth(this.dateFin.getMonth() + Number(this.inputDuree.value));
                ////console.log(this.dateFin)

            }


            ;
            ////console.log(this.dateFin)

            // await this.utilsService.delay(500)
            if (this.dateFin != null)
                this.inputFin.value = this.utilsService.displayDateFormatInput(this.dateFin);
        }


    }
    checkDureeFirstOption(): Boolean {
        this.duree = Number(this.inputDuree.value);
        if (this.duree == null) {
            this.utilsService.alertNotification("veuillez indiquer une durée")
            return false;
        }

        if (isNaN(this.duree) || this.duree < 0) {
            this.utilsService.alertNotification("veuillez indiquer une durée correcte")
            return false;

        }

        if (this.duree == 0) {
            this.utilsService.alertNotification("veuillez indiquer une durée d'au mininum 1 jour")
            return false;
        }

        return true;


    }
    validerChangementDate() {
        this.validerEtape()

    }
    async validerCAEtMontant(compagnie, montant) {

        this.simulationActif.ca = compagnie;
        if (this.listeSimFlotte != null) {
            this.listeSimFlotte.forEach(simulation => {
                simulation.ca = compagnie;
                simulation.montantTTCCompagnie = montant

            });
        }

    }
    validerEtape() {
        //this.isFlotte = false ;
        this.ifAddNewVehicule = false;
        this.checkBeforeValidationFirstOption()
    }
    async checkBeforeValidationFirstOption() {


        if (this.dateFinReference == null) {
            if (this.checkDureeFirstOption()) {

                // let debut = new Date(this.onDeclarationForm.get('debut').value)
                // this.dateFin =debut ;;

                //             this.dateFin.setMonth(debut.getMonth()+this.duree);


                //             ////console.log(this.dateFin) ;

                //             this.inputFin.value = this.utilsService.displayDateFormatInput(this.dateFin)       ;
                //             await this.utilsService.delay(500)
                this.valider()
            }
            else {

            }
        }
        else
            this.valider();


    }
    valider() {


        let reactiveDebut = this.onDeclarationForm.get('debut').value;
        let reactiveFin = this.onDeclarationForm.get('fin').value;

        // ////console.log(reactiveDebut) ;
        if (!this.onDeclarationForm.valid) {
            if (this.onDeclarationForm.get('fin').hasError('required'))
                this.dateFinError = true;
            if (this.onDeclarationForm.get('debut').hasError('required'))
                this.dateDebutError = true;

            this.utilsService.alertNotification("Veuillez indiquer toutes les dates avant de continuer");

        }
        else {





            this.dateFin = new Date(reactiveFin);
            this.dateDebut = new Date(reactiveDebut);


            this.typeOrigineRoutine = JSON.parse(sessionStorage.getItem(typeRoutine));
            switch (this.typeOrigineRoutine) {
                case typeRoutineSimulation: this.trouverDateMinimum(this.simulationActif.objet); break;
                case typeRoutineRenewContrat:
                case typeRoutineNewContrat: this.trouverDateMinimum(this.objetActif); break;


            }


            // ////console.log(this.dateDebut+" "+this.displayDate(this.dateDebut)) ;
            // ////console.log(this.dateFin+" "+this.displayDate(this.dateFin)) ;
            // ////console.log(this.checked);

        }

    }
    trouverDateMinimum(objet) {
        ////console.log("debutFonction")
        //recupere la liste des contrats delobjet
        //let contrats = this.utilsService.filtrerContrat(objet.contrats)

        let contrats = objet.contrats;
        //
        let date = this.utilsService.getDate().subscribe(
            dateFormatMobile => {


                let dat = new Date(dateFormatMobile.date);
                dat.setDate(dat.getDate() + 1)

                this.dateDuLendemain = new Date(dat);

                //
                if (contrats == null || contrats.length == 0) {


                    this.dateDebutMinimum = dat;
                    this.dateFinMaximumBeforeOther = null;
                    //  console.log("date minimum"+this.utilsService.displayDate(this.dateFinMaximumBeforeOther))
                    this.verifierDate();
                    return;
                    ////console.log(this.dateDebutMinimum) ;






                }
                else {
                    console.log(contrats[0])
                    contrats[0].dateEffet = new Date(contrats[0].dateEffet)
                    let datBeforeOther = new Date(contrats[0].dateEffet);
                    console.log("date minimum" + this.utilsService.displayDate(datBeforeOther))

                    contrats.forEach(contrat => {
                        //boucle sur operation

                        contrat.operations.forEach(operation => {
                            if (this.utilsService.testOperationActif(operation)) {
                                operation.dateCloture = new Date(operation.dateCloture);
                                if (this.utilsService.isinferior(dat, operation.dateCloture)) {
                                    if (operation.dateCloture != null && this.utilsService.isinferior(dat, operation.dateCloture)) {
                                        dat = new Date(operation.dateCloture);

                                    }
                                    operation.dateEffet = new Date(operation.dateEffet)
                                    if (operation.dateEffet != null && this.utilsService.isinferior(operation.dateEffet, datBeforeOther)) {
                                        datBeforeOther = new Date(operation.dateEffet);
                                        console.log("date minimum" + this.utilsService.displayDate(datBeforeOther))

                                    }


                                }

                            }



                        });

                        //finboucle sur operation



                    });



                    if (dat != null) {

                        this.dateDebutMinimum = new Date(dat);
                        this.dateDebutMinimum.setDate(this.dateDebutMinimum.getDate() + 1)



                    }
                    if (datBeforeOther != null) {
                        this.dateFinMaximumBeforeOther = new Date(datBeforeOther);
                        this.dateFinMaximumBeforeOther.setDate(this.dateFinMaximumBeforeOther.getDate() - 1)
                        console.log("date minimum" + this.utilsService.displayDate(this.dateFinMaximumBeforeOther))
                    }
                    this.verifierDate();

                }
            }
        );
    }
    verifierDate() {
        this.dateFinError = false;
        this.dateDebutError = false;
        console.log(this.dateDebut)
        if (this.dateDebut != null) {
            this.dateDebut = new Date(this.dateDebut);
            ////console.log("dateDebutVerif "+this.utilsService.displayDate(this.dateDebut))



        }

        if (this.dateFin != null) {
            this.dateFin = new Date(this.dateFin);

            ////console.log("dateFintVerif "+this.utilsService.displayDate(this.dateFin))
        }


        ////console.log("dateMinmimumVerif "+this.utilsService.displayDate(this.dateDebutMinimum))


        if (this.dateDebut != null && this.dateFin != null) {
            // this.trouverdateDebutMinimum() ;
            this.dateDebut.setHours(0, 0, 0, 0);
            this.dateFin.setHours(0, 0, 0, 0);
            this.dateDebutMinimum.setHours(0, 0, 0, 0);

            if (this.utilsService.isinferior(this.dateFin, this.dateDebut)) {

                ////console.log("date debut > fin")
                this.dateFin = null;
                this.dateDebut = null;

                this.dateFinError = true;
                this.utilsService.alertNotification("la date de fin ne doit pas être inférieure à la date de début");
                return;
            }
            let ok = this.verifierIfInFirstIntervalle(this.dateDebut, this.dateFin);
            console.log("console " + ok)


            if (!this.verifierIfInFirstIntervalle(this.dateDebut, this.dateFin)) {
                console.log("2 false")
                if (!this.verifierIfInSecondIntervalle()) {
                    return;
                }

            }


        }

        this.nextEtape()




    }
    verifierIfInFirstIntervalle(dateDebut, dateFin) {
        var p1;
        console.log(dateDebut)
        if (this.dateFinMaximumBeforeOther != null) {


            console.log("date Debut" + this.utilsService.displayDate(dateDebut))
            console.log("date Lendemain" + this.utilsService.displayDate(this.dateDuLendemain))
            console.log("date Fin" + this.utilsService.displayDate(dateFin))
            console.log("date debut minimum" + this.utilsService.displayDate(this.dateDebutMinimum))
            console.log("date fin maximumBefore other" + this.utilsService.displayDate(this.dateFinMaximumBeforeOther))
            console.log(!this.utilsService.isinferior(dateDebut, this.dateDuLendemain));
            console.log(!this.utilsService.isinferior(this.dateFinMaximumBeforeOther, dateFin));
            if ((!this.utilsService.isinferior(dateDebut, this.dateDuLendemain) && !this.utilsService.isinferior(this.dateFinMaximumBeforeOther, dateFin))) {


                console.log("in da building")
                //  this.dateFin = null ;
                //  this.dateDebut =null ;
                //  this.dateDebutError =true ;
                //  this.utilsService.alertNotification("le contrat doit finir au plus tard le le "+this.utilsService.displayDate(this.dateFinMaximumBeforeOther)) ;
                return true
            }
            else
                return false


        }
        else return false
    }

    verifierIfInSecondIntervalle() {
        console.log("not in first")
        if (this.utilsService.isinferior(this.dateDebut, this.dateDebutMinimum)) {


            console.log("date fin" + this.utilsService.displayDate(this.dateFin))
            ////console.log("date debut < minimum")
            this.dateFin = null;
            this.dateDebut = null;
            this.dateDebutError = true;
            if (this.dateFinMaximumBeforeOther != null)
                this.utilsService.alertNotification(
                    "le contrat peut soit commencer apres le " + this.utilsService.displayDate(this.dateDebutMinimum) +
                    "\n ou soit debuter apres le " + this.utilsService.displayDate(this.dateDuLendemain) + " se terminer avant le" + this.utilsService.displayDate(this.dateFinMaximumBeforeOther));
            else
                this.utilsService.alertNotification("le contrat ne peut pas commencer avant le " + this.utilsService.displayDate(this.dateDebutMinimum));
            return false;
        }
        else
            return true;

    }

    nextEtape() {
        //dispatching
        this.typeOrigineRoutine = JSON.parse(window.sessionStorage.getItem(typeRoutine));
        "next"
        switch (this.typeOrigineRoutine) {
            case typeRoutineSimulation:
                let date = this.utilsService.getDate().subscribe(
                    dateFormatMobile => {
                        this.simulationActif.dateSimulation = new Date(dateFormatMobile.date)




                        this.misAjourListeValide().then(
                            () => {
                                sessionStorage.setItem(simulationActifenSession, JSON.stringify(this.simulationActif))
                                sessionStorage.setItem(listeSimulationDansFlotte, JSON.stringify(this.listeAllSimFlotte))
                                sessionStorage.setItem(listeSimulationDansFlotteValide, JSON.stringify(this.listeSimFlotte))
                                console.log("after")
                                console.log(this.listeAllSimFlotte)
                                window.location.reload();

                            }
                        )






                    },
                    error => {
                        let message = "Oups le serveur est introuvable verfiez votre connexion et recommencez svp";
                        this.utilsService.alertNotification(message);


                        this.navCtrl.navigateRoot(LIENLOGIN);
                    }
                );

                break;
            case typeRoutineNewContrat:
            case typeRoutineRenewContrat:
                {

                    // let operation =  new Operation() ;
                    // ////console.log("avant mis a jour")
                    // ////console.log(this.checked)

                    // ////console.log("apres mis a jour")
                    // ////console.log(this.checked)
                    // //ici on affecte un tableau de type  garantie  à garantie pour uniformiser avec les autres type de routine

                    // operation.dateEffet =this.dateDebut ;
                    // operation.dateCloture =this.dateFin ;
                    // this.operationActif = operation;
                    // ////console.log(this.operationActif) ;
                    // ////console.log(this.contratActif) ;
                    // ////console.log(JSON.parse(window.sessionStorage.getItem(objetActifenSession))) ;

                    // window.sessionStorage.setItem(contratActifenSession,JSON.stringify(this.contratActif))
                    // window.sessionStorage.setItem(operationActifenSession,JSON.stringify(this.operationActif))
                    // ////console.log("garanties donnees") ;
                    // ////console.log(this.operationActif.garanties)

                    // this.navCtrl.navigateRoot('/simbranauto') ;


                    // break ;
                }
            default:
            //this.effacerDonnees() ;

        }

        //fin dispatching




    }
    async misAjourListeValide() {
        this.simulationActif.dateDebut = this.dateDebut;

        this.simulationActif.dateDebut.setHours(0, 0, 0);

        this.simulationActif.dateFin = this.dateFin;
        this.simulationActif.dateFin.setHours(23, 59, 59);


        this.listeAllSimFlotte = JSON.parse(sessionStorage.getItem(listeSimulationDansFlotte))

        this.listeAllSimFlotte.forEach(simulation => {
            if (simulation.numeroDansSession == this.simulationActif.numeroDansSession) {
                simulation.dateDebut = new Date(this.dateDebut);

                simulation.dateDebut.setHours(0, 0, 0);

                simulation.dateFin = new Date(this.dateFin);
                simulation.dateFin.setHours(23, 59, 59);

            }
        });
        this.listeSimFlotte = JSON.parse(sessionStorage.getItem(listeSimulationDansFlotteValide))

        this.listeSimFlotte.forEach(simulation => {

            simulation.dateDebut = this.dateDebut;

            simulation.dateDebut.setHours(0, 0, 0);

            simulation.dateFin = this.dateFin;
            simulation.dateFin.setHours(23, 59, 59);


        });


    }
    insererGondole() {
        this.items.push(null);
        let suivant
        for (let index = this.items.length - 2; index >= 0; index--) {
            suivant = index + 1
            this.items[suivant] = this.items[index];

        }
        let item = {
            expanded: false,
            name: 'Risques Couverts',
            zipped: true,
            prix: this.compagniePrixActif.prix,
            compagnie: this.compagniePrixActif.ca,
            id: this.compagniePrixActif.ca.id,
            caActuel: false,
            logo: UrlApiLogo + this.compagniePrixActif.ca.id,
            objetsPrix: this.compagniePrixActif.caObjetPrix,
        }
        this.items[0] = item;
        console.log(this.items)
    }
    async misAjourListe() {
        const loader = await this.loadingCtrl.create({
            message: '',
        });

        let index = this.getIndexListePrincipale(this.simulationActif.numeroDansFlotte);
        console.log(index)
        if (index >= 0) {
            this.supprimerListePrincipale(index).then(
                () => {
                    sessionStorage.setItem(listeSimulationDansFlotte, JSON.stringify(this.liste))
                    //this.recupererCompagniePrix();
                    window.location.reload();
                    console.log(this.liste);
                }
            )


        }



    }
    async supprimerListePrincipale(index) {
        this.liste.splice(index, 1)

    }
    /**
    * teste si le vehicule respecte la contrainte d'unicité des contrats
    * les messages  sont affiches au niveau de la fonction testerSiNewContratIntervalleDisponible de la classe utils service
    *
    */
    async testIfEachVehiculeCanHaveContrat(dateFormatMobile) {
        this.resultatGeneralTestALLPeriode = true;

        this.observablesTest = []

        console.log("test if each")

        this.typeOrigineRoutine = JSON.parse(window.sessionStorage.getItem(typeRoutine));
        ///console.log(this.typeOrigineRoutine)
        switch (this.typeOrigineRoutine) {
            case this.typeRoutineSimulation:

                this.listeSimFlotte.forEach(
                    simulation => {
                        this.observablesTest.push(
                            /*
                            Premiere methode ne prend pas en compte des intervalles au centre
                            mais on a vue sur les intervalles "libre"

                            */

                            // this.utilsService.objetEligiblePeriode(dateFormatMobile,simulation.dateDebut,simulation.dateFin,simulation.objet).then(
                            //   res=>{
                            //     console.log(res)
                            //     simulation.eligiblePeriode =res;
                            //     if(res== true)
                            //     {
                            //       console.log(simulation.objet.matricule+"est eligible")


                            //     }
                            //     else
                            //     {
                            //       console.log(simulation.objet.matricule+"non eligible")
                            //       this.resultatGeneralTestALLPeriode = false;
                            //       console.log(this.resultatGeneralTestALLPeriode)
                            //     }

                            //   }
                            //   )
                            /**seconde methode prend en compte de avant,apres et centre mais
                            * ne definit pas les messages pour linstant
                            * on a pas de vue sur les intervalles libre mais sur les intervalles occupés
                            *
                            */
                            this.utilsService.recupererIntervalleNonDisponibleObjet(simulation.objet.contrats).then(
                                liste => {

                                    console.log(simulation.objet)
                                    console.log(liste)
                                    this.utilsService.testerSiNewContratIntervalleDisponible(simulation.objet, simulation.dateDebut, simulation.dateFin, liste).then(
                                        resultat => {
                                            simulation.objetEligiblePeriode = resultat
                                            if (resultat == false) {
                                                this.resultatGeneralTestALLPeriode = false;
                                            }

                                        }
                                    );
                                }
                            )
                        )

                    }
                )
                break;

            case typeRoutineNewContrat:
            case typeRoutineRenewContrat:
                this.listeRoutineContrat.forEach(
                    routine => {
                        this.observablesTest.push(
                            /*
                            Premiere methode ne prend pas en compte des intervalles au centre
                            mais on a vue sur les intervalles "libre"
                           voir assurinfove vehicule
                              */

                            /**seconde methode prend en compte de avant,apres et centre mais
                              * ne definit pas les messages pour linstant
                              * on a pas de vue sur les intervalles libre mais sur les intervalles occupés
                              * teste si le vehicule respecte la contrainte d'unicité des contrats
                              * les messages  sont affiches au niveau de la fonction testerSiNewContratIntervalleDisponible de la classe utils service
                              *
                              */
                             //A revoir
                            // this.utilsService.recupererIntervalleNonDisponibleObjet(routine.objet.contrats).then(
                            //     liste => {

                            //         console.log(routine.objet)
                            //         console.log(liste)
                            //         this.utilsService.testerSiNewContratIntervalleDisponible(routine.objet, routine.operation.dateEffet, routine.operation.dateCloture, liste).then(
                            //             resultat => {
                            //                 if (resultat == false)
                            //                     this.resultatGeneralTestALLPeriode = false;

                            //             }
                            //         );
                            //     }
                            // )
                        )

                    }
                )






                break;
        }


    }
    montrerDetails(item) {
        item.zipped = false;
    }
    cacherDetails(item) {
        item.zipped = true;
    }
    debutFlotteContrat() {


        sessionStorage.setItem(flotte, JSON.stringify(true));
        // this.simulationActif.dateDebut = null ;
        let newOperation: Operation = new Operation();
        newOperation.paiement = new Paiement();
        let newContrat: Contrat2 = new Contrat2();

        let obj;

        newOperation.dateEffet = new Date(this.utilsService.displayDateFormatInitialise(this.operationActif.dateEffet))
        // //console.log(this.utils.displayDate(this.simulationActif.dateDebut)) ;

        newOperation.dateCloture = new Date(this.utilsService.displayDateFormatInitialise(this.operationActif.dateCloture))

        newContrat.myCA = this.contratActif.myCA

        obj = new ObjetAssures();

        //newOperation.garanties = [];
        //nouveau changement
        newOperation.details = [];
        sessionStorage.setItem(contratActifenSession, JSON.stringify(newContrat));
        sessionStorage.setItem(objetActifenSession, JSON.stringify(obj));
        sessionStorage.setItem(operationActifenSession, JSON.stringify(newOperation));

        this.navCtrl.navigateForward(LIENASSURINFOVE);

    }
    editContrat(routineContrat) {
        sessionStorage.setItem(editObjet, JSON.stringify(true))
        window.sessionStorage.setItem(contratActifenSession, JSON.stringify(routineContrat.contrat));
        window.sessionStorage.setItem(operationActifenSession, JSON.stringify(routineContrat.operation));
        window.sessionStorage.setItem(objetActifenSession, JSON.stringify(routineContrat.objet));
        /*on mets la routine à modifier en session pour la remplacer lorsque l'utilisateur a validé l'objet et
        les options dans la routine de modification
        */
        console.log(this.listeRoutineContrat.indexOf(routineContrat))
        window.sessionStorage.setItem(indexRoutineContratEditingSession, JSON.stringify(this.listeRoutineContrat.indexOf(routineContrat)));

        this.navCtrl.navigateForward(LIENASSURINFOVE)

    }
    async deleteRoutineContrat(indexRoutine) {
        this.listeRoutineContrat.splice(indexRoutine, 1);
        sessionStorage.setItem(listeRoutineContratSession, JSON.stringify(this.listeRoutineContrat))
        const loader = await this.loadingCtrl.create({
            message: 'chargement en cours',
        });
        window.location.reload();

    }
    /**
    * ceette fonction permet d'affecter à chaque contrat de la flotee la compagnie choisie
    * routine contrat
    * @param compagnie
    */
    async validerCAFlotteRoutineContrat(compagnie) {

        this.contratActif.myCA = compagnie;
        if (this.listeRoutineContrat != null) {
            this.listeRoutineContrat.forEach(routine => {
                routine.contrat.myCA = compagnie;

            });
        }

    }
}

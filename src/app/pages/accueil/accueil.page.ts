import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { NavController } from '@ionic/angular';
import { FingerprintAIO, FingerprintOptions} from '@ionic-native/fingerprint-aio/ngx';


@Component({
  selector: 'app-accueil',
  templateUrl: './accueil.page.html',
  styleUrls: ['./accueil.page.scss'],
})
export class AccueilPage implements OnInit {
  public accueilForm: FormGroup;
  fingerprintOptions : FingerprintOptions;
  constructor(private formBuilder: FormBuilder, public navCtrl: NavController, private fingerAuth: FingerprintAIO) { }

  ngOnInit() {
    this.accueilForm = this.formBuilder.group({
      'lieu': [null, Validators.compose([
        Validators.required
      ])],
      'langue': [null, Validators.compose([
        Validators.required
      ])]
    });
  }
  goToHome() {
    this.navCtrl.navigateRoot('/home-results');
  }
  goToLogin() {
    this.navCtrl.navigateRoot('/login');
  }
  /*public showFingerprintAuthDlg() {
    this.fingerprintOptions = {
        clientId: 'fingerprint-Demo',
        clientSecret: 'password', // Only necessary for Android
        disableBackup: true  // Only for Android(optional)
    };
    this.fingerAuth.isAvailable().then(result => {
    if (result === 'OK') {
        this.fingerAuth.show(this.fingerprintOptions)
        // tslint:disable-next-line: no-shadowed-variable
        .then((result: any) => console.log(result))
        .catch((error: any) => console.log(error));
    }
    });
}*/

public showFingerprintAuthDlg(){
    this.fingerprintOptions = {
      title: 'Biometric Authentication', // (Android Only) | optional | Default: "<APP_NAME> Biometric Sign On"
      subtitle: 'Coolest Plugin ever', // (Android Only) | optional | Default: null
      description: 'Please authenticate', // optional | Default: null
      fallbackButtonTitle: 'Use Backup', // optional | When disableBackup is false defaults to "Use Pin".
                                         // When disableBackup is true defaults to "Cancel"
      disableBackup:true,  // optional | default: false
    }
    this.fingerAuth.isAvailable().then(result =>{
    if(result === "OK")
    {
        this.fingerAuth.show(this.fingerprintOptions)
        .then((result: any) => console.log(result))
        .catch((error: any) => console.log(error));
    }
    });
}

}

import { Component, OnInit } from '@angular/core';
import {MyprofilecomPage} from '../myprofilecom/myprofilecom.page';

@Component({
  selector: 'app-tabs',
  templateUrl: './tabs.page.html',
  styleUrls: ['./tabs.page.scss'],
})
export class TabsPage implements OnInit {
tab1Root  : any = MyprofilecomPage;
animateBtnTabsLogin: Boolean = false;
animateBtnTabsContact: Boolean = false
  constructor() { }

  ngOnInit() {
  }

  onClickBtnTabs(type){
      if(type=== 'login'){
          this.animateBtnTabsLogin = !this.animateBtnTabsLogin;
      }else{
          this.animateBtnTabsContact = !this.animateBtnTabsContact;
      }

  }

}

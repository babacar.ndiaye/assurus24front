import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { TabsPage } from './tabs.page';

const routes: Routes = [
  {
    path: '',
    component: TabsPage,
    children: [
      { path: '', loadChildren: './../epocno/epocno.module#EpocnoPageModule' }
    ]
  },
  { path: 'login', loadChildren: './../login/login.module#LoginPageModule' },
  { path: 'contactez-nous', loadChildren: './../contactez-nous/contactez-nous.module#ContactezNousPageModule' },
];



@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [TabsPage]
})
export class TabsPageModule {}

import { Component, OnInit, ViewChild } from '@angular/core';
import { Md5 } from 'ts-md5/dist/md5';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NavController, ToastController, AlertController, LoadingController, Platform } from '@ionic/angular';
import { ContactClient } from 'src/app/models/contact-client';
import { CompteService } from 'src/app/services/compte.service';
import { MailLien } from 'src/app/models/mail-lien';
import { Compte } from 'src/app/models/compte';
import { CODE, CONTACTSESSION, LOGIN, NBESSAISAUTORISES, CODECONFIRMATION, IDINSCRIPTION, typeRoutineProfil, routineInscription, routineUpdateProfil, LIENLOGIN, LIENSELECTIONPROFIL, LIENCONTACT, LIENUPLOADDOC, LIENMYPROFILE, CLIENTPARTICULIERSESSION, CLIENTPROFESSIONNELSESSION, SessionCompte, SessionCompteUpdate, routineResetPassword, routineInscriptionPro } from 'src/environments/environment';
import { UtilsService } from 'src/app/services/utils.service';
import { ClientParticulier } from 'src/app/models/client-particulier';
import { ClientParticulierService } from 'src/app/services/client-particulier.service';
import { ClientProfessionnel } from 'src/app/models/client-professionnel';
import { ClientProfessionnelService } from 'src/app/services/client-professionnel.service';
import { Globalization } from '@ionic-native/globalization/ngx';
import { TranslateService } from '@ngx-translate/core';




@Component({
    selector: 'app-identification',
    templateUrl: './identification.page.html',
    styleUrls: ['./identification.page.scss'],
})
export class IdentificationPage implements OnInit {
    language: string;
    LIENCONTACT;
    public onIdentificationForm: FormGroup;
    typeRoutineProfilLoc = JSON.parse(sessionStorage.getItem(typeRoutineProfil))
    routineUpdateProfilLoc = routineUpdateProfil
    routineResetLoc = routineResetPassword
    clientParticuler: ClientParticulier;
    clientProfessionnel: ClientProfessionnel;
    nombreEssais: number;
    login: String;
    passwordVisible = false;
    oldPasswordVisible = false;
    confirmPasswordVisible = false;
    @ViewChild('password') inputPassword;
    @ViewChild('confirm') inputConfirm;
    @ViewChild('oldPassword') inputOldPassword;
    formInvalide: boolean = false;
    compteUpdate: Compte;
    constructor(private formBuilder: FormBuilder,
        public navCtrl: NavController,
        public alertCtrl: AlertController,
        public toastCtrl: ToastController,
        public loadingCtrl: LoadingController,
        public serviceCompte: CompteService,
        public serviceClientParticulier: ClientParticulierService,
        public serviceClientProfessionnel: ClientProfessionnelService,
        private utilsService: UtilsService,
        private platform: Platform,
        private globalization: Globalization,
        private _translate: TranslateService
    ) {
        this.compteUpdate = new Compte();

        this.nombreEssais = 0;



    }
    ionViewWillEnter() {


        if (window.sessionStorage.getItem(CLIENTPARTICULIERSESSION) == null && window.sessionStorage.getItem(CLIENTPROFESSIONNELSESSION) == null) {
            let messageInfo = 'Les informations du contact ne sont pas renseignés ';

            this.utilsService.alertNotification(messageInfo);
            switch (this.typeRoutineProfilLoc) {
                case routineUpdateProfil:
                    this.navCtrl.navigateRoot(LIENMYPROFILE);
                    break;
                case routineResetPassword:
                    this.navCtrl.navigateRoot(LIENLOGIN);
                    break;
                case routineInscription:
                    this.navCtrl.navigateRoot(LIENCONTACT);
                    break;
                case routineInscriptionPro:
                    this.navCtrl.navigateRoot(LIENCONTACT);
                    break;

            }

        }
        if (this.typeRoutineProfilLoc == routineInscription || this.typeRoutineProfilLoc == routineResetPassword || this.typeRoutineProfilLoc == routineInscriptionPro)
            this.onIdentificationForm.get('oldCode').setValue(" ")
        this.login = this.getLogin();
        if (this.login.indexOf('"') >= 0)
            this.login = this.login.substring(1, this.login.length - 1)
        this.nombreEssais = 0;
    }

    ngOnInit() {
        this.onIdentificationForm = this.formBuilder.group({
            'oldCode': [null, Validators.compose([
                Validators.required
            ])],

            'code': [null, Validators.compose([
                Validators.required
            ])],
            'confirmCode': [null, Validators.compose([
                Validators.required
            ])]
        });
    }
    goToHome() {
        this.navCtrl.navigateRoot('/');
    }
    async alertRecupererCode(message = "Veuillez saisir le code de Validation envoyé par sms (le code est valable 6 mn)") {
        const changeLocation = await this.alertCtrl.create({
            header: 'Code de confirmation',
            message: message,
            cssClass: 'alertCss',
            inputs: [
                {
                    name: 'codeconfirmation',
                    placeholder: 'Entrer le code de confirmation',
                    type: 'number'
                },
            ],
            buttons: [
                {
                    text: 'Cancel',
                    handler: data => {
                        console.log('Cancel clicked');
                    }
                },
                {
                    text: 'ok',
                    handler: async (data) => {


                        this.verifierCode(data.codeconfirmation);
                        //code enregistrement client et contact client si code confirlmation correct

                    }
                }
            ]
        });
        changeLocation.present();
    }

    getLogin(): string {
        return window.sessionStorage.getItem(LOGIN);
    }
    async verifierCode(code) {
        const loader = await this.loadingCtrl.create({
            message: '',
        });

        loader.present();
        let codeStocke = window.localStorage.getItem(CODECONFIRMATION);
        if (codeStocke == null) {
            loader.dismiss();
            let messageInfo = 'Desolé le code a expiré veuillez recommencer il vous reste encore ' + (NBESSAISAUTORISES - this.nombreEssais) + ' essais';
            this.utilsService.alertNotification(messageInfo);

        }
        else
            if (codeStocke == code) {
                loader.dismiss();
                const toast = await this.toastCtrl.create({
                    message: 'Code de confirmation correct',
                    duration: 3000,
                    position: 'top',
                    closeButtonText: 'OK',
                    showCloseButton: true
                });
                this.sauvegarder();

                toast.present();
            }
            else {
                this.nombreEssais++;
                if (this.nombreEssais == NBESSAISAUTORISES) {
                    loader.dismiss();
                    //votre compte a été bloqué apres a definir comment on bloque une personne
                    let messageInfo = "Vous  avez épuisé le nombre d'essais autorisés veuillez recommencer";
                    this.utilsService.alertNotification(messageInfo);
                    window.sessionStorage.removeItem(CLIENTPARTICULIERSESSION);
                    window.sessionStorage.removeItem(LOGIN);
                    window.sessionStorage.removeItem(CODE);
                    this.navCtrl.navigateRoot(LIENSELECTIONPROFIL);
                }
                else {
                    loader.dismiss();
                    let messageInfo = 'code incorrect veuillez réessayer ,il vous reste ' + (NBESSAISAUTORISES - this.nombreEssais) + ' essais';
                    this.alertRecupererCode(messageInfo);
                }
            }
    }

    async envoyerCode() {
        let telephone = this.getLogin()


        const loader = await this.loadingCtrl.create({
            message: 'Envoi en cours...',
        });

        loader.present();

        let mailLien = new MailLien();
        let CodeConfirmation = this.utilsService.definirNombreAleatoire();
        console.log(CodeConfirmation);
        mailLien.mail = telephone;
        mailLien.code = CodeConfirmation;
        window.localStorage.setItem(CODECONFIRMATION, String(CodeConfirmation));

        this.serviceCompte.envoyerSms(mailLien).subscribe(async resultat => {
            loader.dismiss();
            if (resultat) {

                this.alertRecupererCode();
                await this.delay(360000);
                window.localStorage.removeItem(CODECONFIRMATION);

            }
            else {

                let messageInfo = 'lenvoi a échoué veuillez recommencer';
                this.utilsService.alertNotification(messageInfo);

            }

        },
            erreur => {
                loader.dismiss();
                let messageInfo = 'Oups problème de connexion,veuillez recommencer';
                this.utilsService.alertNotification(messageInfo);

            }
        );
    }

    delay(ms: number) {
        return new Promise(resolve => setTimeout(resolve, ms));
    }
    voirPassword() {
        this.inputPassword.type = 'text';
        this.passwordVisible = true;

    }
    voirOldPassword() {
        this.inputOldPassword.type = 'text';
        this.oldPasswordVisible = true;

    }

    cacherOldPassword() {
        this.inputOldPassword.type = 'password';
        this.oldPasswordVisible = false;
    }
    voirConfirm() {
        this.inputConfirm.type = 'text';
        this.confirmPasswordVisible = true;

    }
    cacherPassword() {
        this.inputPassword.type = 'password';
        this.passwordVisible = false;
    }
    cacherConfirm() {
        this.inputConfirm.type = 'password';
        this.confirmPasswordVisible = false;
    }

    validerInscription() {
        if (!this.onIdentificationForm.valid) {
            this.utilsService.alertNotification("tous les champs ne sont pas remplis correctement")
            this.formInvalide = true;
        }
        else {
            if (window.sessionStorage.getItem(CLIENTPARTICULIERSESSION) == null && window.sessionStorage.getItem(CLIENTPROFESSIONNELSESSION) == null) {
                let messageInfo = 'Les informations du contact ne sont pas renseignés ';

                this.utilsService.alertNotification(messageInfo);
                this.navCtrl.navigateRoot(LIENCONTACT);
            }
            else {
                if (this.onIdentificationForm.get('code').value != this.onIdentificationForm.get('confirmCode').value) {
                    let messageInfo = 'Les codes ne sont pas conformes';

                    this.utilsService.alertNotification(messageInfo);

                }
                else {
                    this.typeRoutineProfilLoc = JSON.parse(sessionStorage.getItem(typeRoutineProfil))
                    switch (this.typeRoutineProfilLoc) {
                        case routineInscription:
                            window.sessionStorage.setItem(CODE, this.onIdentificationForm.get('code').value);
                            this.envoyerCode();
                            break;
                        case routineInscriptionPro:
                            window.sessionStorage.setItem(CODE, this.onIdentificationForm.get('code').value);
                            this.envoyerCode();
                            break;
                        case routineUpdateProfil:
                            let ancienCompte = JSON.parse(window.sessionStorage.getItem(SessionCompte));
                            let oldcodEncode = Md5.hashStr(this.onIdentificationForm.get('oldCode').value)
                            if (oldcodEncode != ancienCompte.motDePasse) {
                                console.log(ancienCompte.motDePasse)
                                console.log(oldcodEncode)
                                let messageInfo = "L' ancien code est incorrect";

                                this.utilsService.alertNotification(messageInfo);

                            }
                            else
                                if (this.onIdentificationForm.get('code').value != this.onIdentificationForm.get('confirmCode').value) {
                                    let messageInfo = 'Les codes ne sont pas conformes';

                                    this.utilsService.alertNotification(messageInfo);

                                }
                                else {

                                    this.modifierIdentification();
                                }
                            break;
                        case routineResetPassword:
                            this.modifierIdentification();
                            break;

                    }

                }
            }

        }

    }
    async sauvegarder() {
        const loader = await this.loadingCtrl.create({
            message: 'Inscription en cours...',
        });

        loader.present();
        //this.sauvegarderDansCompte() ;
        this.inscription();
        loader.dismiss();
    }

    inscription() {
        let code = window.sessionStorage.getItem(CODE);
        let login = window.sessionStorage.getItem(LOGIN);
        if (code != null && login != null) {
            let compte = new Compte;
            compte.telephone = login;
            compte.motDePasse = code;
            if (this.typeRoutineProfilLoc == routineInscription) {
                this.clientParticuler = JSON.parse(window.sessionStorage.getItem(CLIENTPARTICULIERSESSION));
                console.log("compte + clientParticulier")
                console.log(compte);
                console.log(this.clientParticuler);

                this.serviceClientParticulier.inscription(compte, this.clientParticuler).subscribe(
                    async clientParticulier => {
                        
                        if (clientParticulier) {
                            const toast = await this.toastCtrl.create({
                                message: 'Felicitations ' + clientParticulier.prenom + ' votre compte a bien été créé',
                                duration: 5000,
                                position: 'top',
                                closeButtonText: 'OK',
                                showCloseButton: true
                            });
                            toast.present();
                            window.sessionStorage.setItem(IDINSCRIPTION, String(clientParticulier.id));
                            window.sessionStorage.removeItem(CLIENTPARTICULIERSESSION);
                            window.sessionStorage.removeItem(LOGIN);
                            window.sessionStorage.removeItem(CODE);

                            this.navCtrl.navigateRoot(LIENUPLOADDOC);


                        }
                        else {
                            let messageInfo = 'Erreur connexion veuillez réessayer';

                            this.utilsService.alertNotification(messageInfo);

                        }

                    },
                    erreur => {
                        let messageInfo = 'Erreur connexion veuillez réessayer';

                        this.utilsService.alertNotification(messageInfo);

                    }
                );
            }
            if (this.typeRoutineProfilLoc == routineInscriptionPro) {
                this.clientProfessionnel = JSON.parse(window.sessionStorage.getItem(CLIENTPROFESSIONNELSESSION));
                console.log("compte + clientProfessionnel")
                console.log(compte);
                console.log(this.clientProfessionnel);
                console.log('ggggggggggggggggggggggggggggggggg');
                if(this.clientProfessionnel.contacts) {
                    console.log(this.clientProfessionnel.contacts.length)
                    for (let i = 0; i < this.clientProfessionnel.contacts.length; i++) {
                        console.log(this.clientProfessionnel.contacts[i].prenom)
                    }
                }

                this.serviceClientProfessionnel.inscription(compte, this.clientProfessionnel).subscribe(
                    async clientProfessionnel => {
                        console.log('clientPro retourner',clientProfessionnel);
                        
                        if (clientProfessionnel) {
                            const toast = await this.toastCtrl.create({
                                message: 'Felicitations ' + clientProfessionnel.nom + ' votre compte a bien été créé',
                                duration: 5000,
                                position: 'top',
                                closeButtonText: 'OK',
                                showCloseButton: true
                            });
                            toast.present();
                            window.sessionStorage.setItem(IDINSCRIPTION, String(clientProfessionnel.id));
                            window.sessionStorage.removeItem(CLIENTPROFESSIONNELSESSION);
                            window.sessionStorage.removeItem(LOGIN);
                            window.sessionStorage.removeItem(CODE);

                            this.navCtrl.navigateRoot(LIENUPLOADDOC);


                        }
                        else {
                            let messageInfo = 'Erreur connexion veuillez réessayer';

                            this.utilsService.alertNotification(messageInfo);

                        }

                    },
                    erreur => {
                        let messageInfo = 'Erreur connexion veuillez réessayer';

                        this.utilsService.alertNotification(messageInfo);

                    }
                );
            }
        }

    }
    modifierIdentification() {
        this.compteUpdate = JSON.parse(window.sessionStorage.getItem(SessionCompte));
        if (this.compteUpdate == null) {
            let message = "Oups la session a  expiré veuillez recommencer";
            this.utilsService.alertNotification(message);
            this.typeRoutineProfilLoc = JSON.parse(sessionStorage.getItem(typeRoutineProfil))
            switch (this.typeRoutineProfilLoc) {
                case routineUpdateProfil:
                    this.navCtrl.navigateRoot(LIENMYPROFILE);
                    break;
                case routineResetPassword:
                    this.navCtrl.navigateRoot(LIENLOGIN);
                    break;

            }
        }

        this.compteUpdate.motDePasse = this.onIdentificationForm.get('code').value;

        window.sessionStorage.setItem(SessionCompteUpdate, JSON.stringify(this.compteUpdate));
        console.log(this.compteUpdate);
        this.validerUpdate();
    }

    chargerContact() {
        this.compteUpdate = JSON.parse(window.sessionStorage.getItem(SessionCompteUpdate));
        if (this.compteUpdate == null)
            this.compteUpdate = JSON.parse(window.sessionStorage.getItem(SessionCompte));
        if (this.compteUpdate == null) {
            let message = "Oups la session a  expiré veuillez vous reconnecter";
            this.utilsService.alertNotification(message);
            this.navCtrl.navigateRoot(LIENLOGIN);
        }
        this.login = this.compteUpdate.telephone;


    }
    validerUpdate() {


        if (this.compteUpdate != null) {
            let cp = JSON.parse(window.sessionStorage.getItem(CLIENTPARTICULIERSESSION));
            this.compteUpdate.client = null;
            cp.compte = this.compteUpdate;
            this.serviceClientParticulier.updateClient(cp).subscribe(async compte => {
                if (compte) {
                    const toast = await this.toastCtrl.create({
                        message: 'Vos modifications sont validées ',
                        duration: 5000,
                        position: 'top',
                        closeButtonText: 'OK',
                        showCloseButton: true
                    });

                    toast.present();
                    this.compteUpdate = compte;
                    window.sessionStorage.setItem(SessionCompte, JSON.stringify(this.compteUpdate));
                    window.sessionStorage.removeItem(SessionCompteUpdate);
                    this.loadingCtrl.dismiss();
                    this.typeRoutineProfilLoc = JSON.parse(sessionStorage.getItem(typeRoutineProfil))
                    switch (this.typeRoutineProfilLoc) {
                        case routineUpdateProfil:
                            this.navCtrl.navigateRoot(LIENMYPROFILE);
                            break;
                        case routineResetPassword:
                            this.navCtrl.navigateRoot(LIENLOGIN);
                            break;
                    }



                }
                else {
                    this.loadingCtrl.dismiss();
                    let messageInfo = 'Erreur connexion veuillez réessayer';

                    this.utilsService.alertNotification(messageInfo);

                }

            },
                erreur => {
                    let messageInfo = 'Erreur connexion veuillez réessayer';

                    this.utilsService.alertNotification(messageInfo);

                }
            );
        }

    }

    /**GESTION LOGIQUE TRANSLATION DES LANGUES */
  ionViewDidEnter(): void {
    this._translateLanguage()
  }
  ionViewDidLoad() {
    this._translateLanguage();
  }

  public changeLanguage(): void {
    this._translateLanguage();
  }

  _translateLanguage(): void {
    this._translate.use(this.language);
  }
  _initTranslate(language) {
    // Set the default language for translation strings, and the current language.
    this._translate.setDefaultLang('fr-FR');
    if (language) {
      this.language = language;
    }
    else {
      // Set your language here
      this.language = 'fr-FR';
    }
    this._translateLanguage();
  }

  getDeviceLanguage() {
    if (!this.platform.is('android') || !this.platform.is('ios')) {
      //alert('Navigateur')
      this._initTranslate(navigator.language);
    } else {
      this.globalization.getPreferredLanguage()
        .then(res => {
          this._initTranslate(res.value)
        })
        .catch(e => { console.log(e); });
    }
  }
  /**END TRANSLATION */


}

import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ModalMarquePage } from './modal-marque.page';

describe('ModalMarquePage', () => {
  let component: ModalMarquePage;
  let fixture: ComponentFixture<ModalMarquePage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ModalMarquePage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ModalMarquePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import { Component, OnInit } from '@angular/core';
import { ModalController } from '@ionic/angular';

@Component({
  selector: 'app-modal-marque',
  templateUrl: './modal-marque.page.html',
  styleUrls: ['./modal-marque.page.scss'],
})
export class ModalMarquePage implements OnInit {

  constructor(private modalController: ModalController) { }

  ngOnInit() {
  }
  dismiss(){
    this.modalController.dismiss() ;
  }
}

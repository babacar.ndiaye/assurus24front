import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { SimplepageaccueilPage } from './simplepageaccueil.page';

const routes: Routes = [
  {
    path: '',
    component: SimplepageaccueilPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [SimplepageaccueilPage]
})
export class SimplepageaccueilPageModule {}

import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SimplepageaccueilPage } from './simplepageaccueil.page';

describe('SimplepageaccueilPage', () => {
  let component: SimplepageaccueilPage;
  let fixture: ComponentFixture<SimplepageaccueilPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SimplepageaccueilPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SimplepageaccueilPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import {
    LIENACCUEILCOMMERCIAL,
    LIENASSURINFOVE,
    LIENEPOCNO,
} from "./../../../environments/environment";
import { ResetpasswordPage } from "./../resetpassword/resetpassword.page";

import { Compte } from "./../../models/compte";
import { CompteService } from "./../../services/compte.service";
import { AuthService } from "./../../services/auth.service";
import { Component, OnInit, ViewChild } from "@angular/core";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import {
    NavController,
    MenuController,
    ToastController,
    AlertController,
    LoadingController,
    ModalController,
    Platform,
} from "@ionic/angular";
import {
    FingerprintAIO,
    FingerprintOptions,
} from "@ionic-native/fingerprint-aio/ngx";
import { EmailComposer } from "@ionic-native/email-composer/ngx";
import {
    idSessionUser,
    NBESSAISAUTORISES,
    CODERESTORE,
    SessionCompte,
    loginToPage,
    routineInscription,
    typeRoutineProfil,
    LIENACCUEILCLIENTLOGGE,
    LIENSELECTIONPROFIL,
    routineUpdateProfil,
    routineResetPassword,
    LIENIDENTIFICATION,
    CLIENTPARTICULIERSESSION,
    LIENLOGIN,
} from "src/environments/environment";
import { MailLien } from "src/app/models/mail-lien";
import { Globalization } from "@ionic-native/globalization/ngx";
import { TranslateService } from "@ngx-translate/core";

@Component({
    selector: "app-login",
    templateUrl: "./login.page.html",
    styleUrls: ["./login.page.scss"],
})
export class LoginPage implements OnInit {
    public onLoginForm: FormGroup;
    checkConnexion: boolean = false;
    passwordVisible = false;
    compte: Compte;
    @ViewChild("password") inputPassword;
    @ViewChild("indicatif") selectIndicatif;
    @ViewChild("telephone") inputTelephone;
    nombreEssais: number;
    nbEnvoiMail: number;
    id: number;
    email: String;
    fingerprintOptions: FingerprintOptions;
    language: string;
    lienepocnoloc = LIENEPOCNO;
    //variable pour verifier le type de client
    typeClient: string = "";
    constructor(
        public navCtrl: NavController,
        public menuCtrl: MenuController,
        public toastCtrl: ToastController,
        public alertCtrl: AlertController,
        public loadingCtrl: LoadingController,
        private formBuilder: FormBuilder,
        private faio: FingerprintAIO,
        private auth: AuthService,
        private emailComposer: EmailComposer,
        private serviceCompte: CompteService,
        private modalController: ModalController,
        private platform: Platform,
        private globalization: Globalization,
        private _translate: TranslateService
    ) {}

    /* public showFingerprintAuthDlg(){
      this.fingerprintOptions = {
        clientId: 'fingerprint-user',
        clientSecret: 'password', //Only necessary for Android
        disableBackup:true  //Only for Android(optional)
      }
      this.faio.isAvailable().then(result =>{
        if(result === "OK")
        {
          this.faio.show(this.fingerprintOptions)
          .then((result: any) => console.log(result))
          .catch((error: any) => console.log(error));
        }
      });
    } */
    ionViewWillEnter() {
        this.menuCtrl.enable(false);
        this.nombreEssais = 0;
        this.nbEnvoiMail = 0;
    }

    ngOnInit() {
        this.onLoginForm = this.formBuilder.group({
            indicatif: [null],
            telephone: [
                null,
                Validators.compose([
                    Validators.required,
                    Validators.maxLength(20),
                ]),
            ],
            code: [
                null,
                Validators.compose([
                    Validators.required,
                    // Validators.maxLength(4)
                ]),
            ],
        });
    }

    ionViewWillLeave() {
        sessionStorage.removeItem(loginToPage);
    }
    async erreurInput() {
        this.nombreEssais++;
        let changeLocation;

        if (this.nombreEssais >= NBESSAISAUTORISES) {
            let message =
                "Cliquer sur le bouton valider pour restaurer un nouveau mot de passe à l’adresse suivante : ";
            this.recupererMail(message);
        } else {
            changeLocation = await this.alertCtrl.create({
                header: "Erreur connexion",
                message:
                    "L’identifiant ou le mot de passe est incorrect. Merci de réessayer il vous reste " +
                    (NBESSAISAUTORISES - this.nombreEssais) +
                    " essais",

                buttons: [
                    {
                        text: "ok",
                        handler: (data) => {
                            this.inputPassword.value = "";
                        },
                    },
                ],
            });
        }
        changeLocation.present();
    }

    // // //
    goToRegister() {
        this.navCtrl.navigateRoot(LIENSELECTIONPROFIL);
    }

    /**
     * * Cette fonction permet de lancer la routine
     * * de connexion
     */
    connect() {
        console.log("connect");
        //code provisoire pour tester fonctionnalité avant
        let login = this.getLogin();
        this.login(login, this.onLoginForm.get("code").value);
    }
    voirPassword() {
        this.inputPassword.type = "text";
        this.passwordVisible = true;
    }
    cacherPassword() {
        this.inputPassword.type = "password";
        this.passwordVisible = false;
    }
    async envoyerMail() {
        let telephone = this.getLogin();

        if (this.email != null) {
            const loader = await this.loadingCtrl.create({
                message: "Envoi en cours...",
            });

            loader.present();

            let mailLien = new MailLien();
            let codeRestore = this.definirNombreAleatoire();
            console.log(codeRestore);
            mailLien.mail = this.email;
            mailLien.code = codeRestore;
            window.localStorage.setItem(CODERESTORE, String(codeRestore));

            this.serviceCompte.envoyerMail(mailLien).subscribe(
                async (resultat) => {
                    loader.dismiss();
                    if (resultat) {
                        this.AlertRecupererCode();
                        await this.delay(180000);
                        window.localStorage.removeItem(CODERESTORE);
                    } else {
                        let messageInfo =
                            "l'envoi a échoué veuillez recommencer";
                        this.alertNotification(messageInfo);
                    }
                },
                (erreur) => {
                    loader.dismiss();
                    let messageInfo =
                        "Oups problème de connexion,veuillez recommencer";
                    this.alertNotification(messageInfo);
                }
            );
        }
    }

    async login(telephone, code) {
        const loader = await this.loadingCtrl.create({
            message: "Connexion en cours",
        });

        loader.present();
        this.compte = new Compte();
        this.compte.telephone = telephone;
        this.compte.motDePasse = code;
        console.log(this.compte);
        this.serviceCompte.seConnecter(this.compte).subscribe(
            (compte) => {
                loader.dismiss();
                console.log("Compte exist", compte);
                if (compte != null) {
                    window.sessionStorage.setItem(
                        idSessionUser,
                        String(compte.id)
                    );
                    this.auth.authenticationState = true;
                    console.log("connecter");
                    this.recupererCompte(compte.id, compte);
                } else {
                    this.erreurInput();
                }
            },
            (erreur) => {
                loader.dismiss();
                let messageInfo =
                    "Oups problème de connexion,veuillez recommencer";
                this.alertNotification(messageInfo);
            }
        );
    }
    forgotCode() {
        let message =
            " Cliquer sur le bouton valider pour restaurer un nouveau mot de passe à l’adresse suivante : ";
        this.recupererMail(message);
    }

    async alertNotification(message) {
        let changeLocation;

        changeLocation = await this.alertCtrl.create({
            header: "Notification",
            message: message,

            buttons: [
                {
                    text: "ok",
                    handler: (data) => {},
                },
            ],
        });

        changeLocation.present();
    }

    delay(ms: number) {
        return new Promise((resolve) => setTimeout(resolve, ms));
    }
    definirNombreAleatoire() {
        let nombre = "";

        for (let i = 0; i < 5; i++) {
            nombre = nombre.concat(this.entierAleatoire(1, 15));
        }
        return Number(nombre);
    }
    async AlertRecupererCode() {
        let alert = await this.alertCtrl.create({
            header: "Lien envoyé avec succès",
            message:
                "Veuillez saisir le code envoyé dans votre mail.(le code est valable 3 mn)",
            inputs: [
                {
                    name: "code",
                    type: "number",
                },
            ],
            buttons: [
                {
                    text: "Annuler",
                    handler: (data) => {},
                },
                {
                    text: "Valider",
                    handler: (data) => {
                        this.verifierCode(data.code);
                    },
                },
            ],
        });
        alert.present();
    }
    async verifierCode(code) {
        const loader = await this.loadingCtrl.create({
            message: "",
        });

        loader.present();
        let codeStocke = window.localStorage.getItem(CODERESTORE);
        if (codeStocke == null) {
            loader.dismiss();
            let messageInfo =
                "Desolé le code a expiré veuillez recommencer il vous reste encore " +
                (NBESSAISAUTORISES - this.nbEnvoiMail) +
                " essais";
            this.alertNotification(messageInfo);
        } else if (codeStocke == code) {
            loader.dismiss();
            this.resetPassword();
        } else {
            loader.dismiss();
            this.nbEnvoiMail++;
            if (this.nbEnvoiMail >= NBESSAISAUTORISES) {
                //votre compte a été bloqué apres a definir comment on bloque une personne
                let messageInfo = "Votre compte a été bloqué";
                this.alertNotification(messageInfo);
            } else {
                let messageInfo =
                    "code incorrect veuillez cliquez sur mot de passe oublié pour génerer un nouveau code il vous reste " +
                    (NBESSAISAUTORISES - this.nbEnvoiMail) +
                    " essais";
                this.alertNotification(messageInfo);
            }
        }
    }

    async resetPassword() {
        const loader = await this.loadingCtrl.create({
            message: "",
        });

        loader.present();
        let compte = new Compte();

        compte.telephone = this.getLogin();
        this.serviceCompte.recupererIdByTel(compte).subscribe(
            async (compte) => {
                console.log(compte);
                loader.dismiss();
                if (compte.id == 0) {
                    let messageInfo = "Erreur interne veuillez recommencer";
                    this.alertNotification(messageInfo);
                } else {
                    this.id = compte.id;
                    this.nombreEssais = 0;
                    this.nbEnvoiMail = 0;
                    console.log(this.id);
                    let login = this.getLogin();
                    sessionStorage.setItem(
                        CLIENTPARTICULIERSESSION,
                        JSON.stringify(compte.client)
                    );
                    sessionStorage.setItem(
                        typeRoutineProfil,
                        JSON.stringify(routineResetPassword)
                    );
                    sessionStorage.setItem(
                        SessionCompte,
                        JSON.stringify(compte)
                    );
                    this.navCtrl.navigateRoot(LIENIDENTIFICATION);
                    /*const modal = await this.modalController.create({
                    component: ResetpasswordPage,
                    componentProps: {
                        'id': this.id,
                        'telephone': login
                    }
                });

                return await modal.present();*/
                }
            },
            (erreur) => {
                let messageInfo =
                    "Oups problème de connexion,veuillez recommencer";
                this.alertNotification(messageInfo);
            }
        );
    }
    async recupererMail(message) {
        //forgot code

        //forgot code
        this.email = null;
        if (this.inputTelephone.value == "") {
            let messageInfo =
                "Le champ téléphone est vide veuillez le renseigner pour recuperer votre code";
            this.alertNotification(messageInfo);
        } else {
            const loader = await this.loadingCtrl.create({
                message: "",
            });

            loader.present();
            let compte = new Compte();
            compte.telephone = this.getLogin();
            this.serviceCompte.recupererMailByTel(compte).subscribe(
                async (client) => {
                    console.log(client.email);
                    if (client.email == null) {
                        loader.dismiss();
                        let messageInfo =
                            "Vous n'avez pas encore de compte veuillez creer un compte";
                        this.alertNotification(messageInfo);
                    }
                    this.email = client.email;
                    if (this.email != null) {
                        const alert = await this.alertCtrl.create({
                            header: "Code Oublié?",
                            message:
                                message +
                                " " +
                                this.email.substring(0, 3) +
                                "*****************@******" +
                                this.email.substring(this.email.length - 4),

                            buttons: [
                                {
                                    text: "Annuler",
                                    handler: (data) => {},
                                },
                                {
                                    text: "Valider",
                                    handler: (data) => {
                                        this.inputPassword.value = "";
                                        this.envoyerMail();
                                    },
                                },
                            ],
                        });

                        await alert.present();
                    }
                    loader.dismiss();
                },
                (erreur) => {
                    loader.dismiss();
                    let messageInfo =
                        "Oups problème de connexion,veuillez recommencer";
                    this.alertNotification(messageInfo);
                }
            );
        }
    }

    getLogin(): string {
        return this.selectIndicatif.value.concat(
            this.onLoginForm.get("telephone").value
        );
    }

    entierAleatoire(min, max) {
        return Math.floor(Math.random() * (max - min + 1)) + min;
    }
    recupererCompte(id: number, compte: Compte) {
        if (id == null) {
            let message = "Erreur de connexion veuillez recommencer";
            this.alertNotification(message);
        } else {
            console.log(id);
            console.log("compte", compte);

            window.sessionStorage.setItem(
                SessionCompte,
                JSON.stringify(compte)
            );
            //alert("Set user session to storage");
            if (sessionStorage.getItem(SessionCompte)) {
                // Récupérer des données depuis sessionStorage
                let data = JSON.parse(sessionStorage.getItem(SessionCompte));

                //console.log('Data User', data);
                console.log("Type client", data.myProfil.codeGroupe);
                this.typeClient = data.myProfil.codeGroupe;

                if (
                    this.typeClient == "userClientParticulier" ||
                    this.typeClient == "userClientProfessionnel"
                ) {
                    console.log("Connected UserPart ou UserPro");
                    let page = sessionStorage.getItem("pagePrecedent");
                    window.sessionStorage.setItem(
                        SessionCompte,
                        JSON.stringify(compte)
                    );
                    if (page) {
                        this.navCtrl.navigateRoot(page);
                    } else {
                        this.navCtrl.navigateRoot(LIENACCUEILCLIENTLOGGE);
                    }
                } else {
                    console.log("Connected Commercial");
                    this.navCtrl.navigateRoot(LIENACCUEILCOMMERCIAL);
                }
            }
            // this.serviceCompte.getCompte(id).subscribe(
            //     (compte) => {
            //         if (compte) {
            //             console.log("compte", compte);

            //             window.sessionStorage.setItem(
            //                 SessionCompte,
            //                 JSON.stringify(compte)
            //             );
            //             //alert("Set user session to storage");
            //             if (sessionStorage.getItem(SessionCompte)) {
            //                 // Récupérer des données depuis sessionStorage
            //                 let data = JSON.parse(
            //                     sessionStorage.getItem(SessionCompte)
            //                 );

            //                 //console.log('Data User', data);
            //                 console.log(
            //                     "Type client",
            //                     data.myProfil.codeGroupe
            //                 );
            //                 this.typeClient = data.myProfil.codeGroupe;

            //                 if (
            //                     this.typeClient == "userClientParticulier" ||
            //                     this.typeClient == "userClientProfessionnel"
            //                 ) {
            //                     console.log("Connected UserPart ou UserPro");
            //                     let page = sessionStorage.getItem(
            //                         "pagePrecedent"
            //                     );
            //                     window.sessionStorage.setItem(
            //                         SessionCompte,
            //                         JSON.stringify(compte)
            //                     );
            //                     if (page) {
            //                         this.navCtrl.navigateRoot(page);
            //                     } else {
            //                         this.navCtrl.navigateRoot(
            //                             LIENACCUEILCLIENTLOGGE
            //                         );
            //                     }

            //                 } else {
            //                     console.log("Connected Commercial");
            //                     this.navCtrl.navigateRoot(
            //                         LIENACCUEILCOMMERCIAL
            //                     );
            //                 }
            //             }
            //         } else {
            //             let message =
            //                 "Erreur de connexion veuillez recommencer";
            //             this.alertNotification(message);
            //         }
            //     },
            //     (erreur) => {
            //         let message = "Erreur de connexion veuillez recommencer";
            //         this.alertNotification(message);
            //     }
            // );
        }
    }

    /**GESTION LOGIQUE TRANSLATION DES LANGUES */
    ionViewDidEnter(): void {
        this.getDeviceLanguage();
    }

    public changeLanguage(): void {
        this._translateLanguage();
    }

    _translateLanguage(): void {
        this._translate.use(this.language);
    }

    _initTranslate(language) {
        // Set the default language for translation strings, and the current language.
        this._translate.setDefaultLang("fr-FR");
        if (language) {
            this.language = language;
        } else {
            // Set your language here
            this.language = "fr-FR";
        }
        this._translateLanguage();
    }

    getDeviceLanguage() {
        if (!this.platform.is("android") || !this.platform.is("ios")) {
            //alert('Navigateur')
            console.log("Navigateur langage", navigator.language);
            this._initTranslate(navigator.language);
        } else {
            this.globalization
                .getPreferredLanguage()
                .then((res) => {
                    this._initTranslate(res.value);
                })
                .catch((e) => {
                    console.log(e);
                });
        }
    }
    /**END TRANSLATION */
}

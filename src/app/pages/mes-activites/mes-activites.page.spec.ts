import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MesActivitesPage } from './mes-activites.page';

describe('MesActivitesPage', () => {
  let component: MesActivitesPage;
  let fixture: ComponentFixture<MesActivitesPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MesActivitesPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MesActivitesPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import { GarantieService } from './../../services/garantie.service';
import { assureContratActifenSession, SessionCompte } from './../../../environments/environment';
import { DetailOperation } from './../../models/DetailOperation';
import { Sinistre } from './../../models/sinistre';
import { ContratService } from './../../services/contrat.service';
import { CaService } from './../../services/ca.service';
import { Contrat2 } from './../../models/contrat';
import { Component, OnInit, ViewChild } from "@angular/core";
import {
    simulationActifenSession,
    UrlApi,
    CONDITIONSPATHNAME,
    downloadConditionsGenerales,
    typeRoutine,
    typeRoutineSimulation,
    typeRoutineNewContrat,
    contratActifenSession,
    objetActifenSession,
    operationActifenSession,
    operationSimActifenSession,
    typeRoutineRenewContrat,
    typeRenouvellement,
    typeRenouvellementMemeCa,
    typeRenouvellementChangementDeCA,
    OriginePageGesSim,
    OrigineClientNonLogue,
    OriginePageMonObjet,
    OriginePageGesSimReseau,
    flotte,
    listeSimulationDansFlotte,
    listeOperationFlotte,
    listeObjetFlotte,
    listeContratFlotte,
    listeSimulationDansFlotteValide,
    UrlApiLogo,
    statutContratActif,
    listeRoutineContratSession,
    MessagePaiementErreurConnexion,
    listeAssureContratFlotte,
} from "src/environments/environment";
import { Simulation } from "src/app/models/simulation";
import { UtilsService } from "src/app/services/utils.service";
import { NavController, LoadingController, IonSlides, Platform } from "@ionic/angular";
import { CA } from "src/app/models/ca";
import { Automobile } from "src/app/models/automobile";
import { Garantie } from "src/app/models/garantie";
import { ConditionGenerale } from "src/app/models/condition-generale";
import { CompteService } from "src/app/services/compte.service";
import { Operation } from "src/app/models/operation";
import { TypeGarantie } from "src/app/models/type-garantie";
import { Paiement } from "src/app/models/paiement";
import { timingSafeEqual } from "crypto";
import { ObjetAssures, ObjetAssures2 } from "src/app/models/objet-assures";
import { AuthService } from "src/app/services/auth.service";
import { RoutineContrat } from "src/app/pojos/routine-contrat";
import { Commande } from "src/app/pojos/commande";
import { isPlatformBrowser } from "@angular/common";
import { Globalization } from "@ionic-native/globalization/ngx";
import { TranslateService } from "@ngx-translate/core";
import { Reseau } from 'src/app/models/reseau';
import { Avenant } from 'src/app/models/avenant';
import { reseauxervice } from 'src/app/services/reseau.service';
import { Client } from 'src/app/models/client';
import { TypeBA2 } from 'src/app/models/type-ba';
import { TypeSinistre } from 'src/app/models/TypeSinistre';
import { AssureContrat } from 'src/app/models/assure-contrat';
import { TarifGarantie } from 'src/app/models/tarif-garantie';
import { TableCoeffGarantie } from 'src/app/models/TableCoeffGarantie';
import { GarantieCA } from 'src/app/models/garantie-ca';
import { AttributGarantie } from 'src/app/models/AttributGarantie';
import { ObjetService } from 'src/app/services/objet.service';
declare var PayTech;
@Component({
    selector: "app-ecran-recapitulatif",
    templateUrl: "./ecran-recapitulatif.page.html",
    styleUrls: ["./ecran-recapitulatif.page.scss"],
})
export class EcranRecapitulatifPage implements OnInit {
    language: string;
    largeur = screen.width;
    longueur = screen.height;
    simulationActif: Simulation = new Simulation();
    public contratActif: Contrat2 = new Contrat2();
    public operationActif: Operation = new Operation();
    public objetActif: ObjetAssures2 = new ObjetAssures2();
    public assureContratActif: AssureContrat = new AssureContrat();
    //operation finale pour toutes kes routines
    operationSimActif: Operation = new Operation();
    logo: string;
    TabtypeGaranties: any[] = [];
    connectedClient: Client;
    isFlotte = JSON.parse(window.sessionStorage.getItem(flotte));
    operationProvisoire = new Operation();
    typeRoutineSimulation = typeRoutineSimulation;
    typeRoutineNewContrat = typeRoutineNewContrat;
    typeRoutineRenewContrat = typeRoutineRenewContrat;
    typeOrigineRoutine: any = JSON.parse(
        window.sessionStorage.getItem(typeRoutine)
    );
    listeSimFlotte: Array<Simulation> = JSON.parse(
        window.sessionStorage.getItem(listeSimulationDansFlotteValide)
    );
    listeOpFlotte: Array<Operation> = JSON.parse(
        window.sessionStorage.getItem(listeOperationFlotte)
    );
    listeObjFlotte: Array<ObjetAssures2> = JSON.parse(
        window.sessionStorage.getItem(listeObjetFlotte)
    );
    listeConFlotte: Array<Contrat2> = JSON.parse(
        window.sessionStorage.getItem(listeContratFlotte)
    );
    listeAssConFlotte: Array<AssureContrat> = JSON.parse(
        window.sessionStorage.getItem(listeAssureContratFlotte)
    );
    listeRoutineContrat: Array<RoutineContrat> = [];
    @ViewChild("slideWithNav") slideWithNav: IonSlides;

    sliderOne: any;

    @ViewChild("mySlider") mySlider: IonSlides;

    refCommande = 2015;
    //Configuration for each Slider
    slideOptsOne = {
        initialSlide: 0,
        slidesPerView: 1,

        spaceBetween: 20,
        autoplay: true,
        speed: 1000,
    };
    slideChanged(slideView) {
        switch (slideView) {
            case "infos":
                this.slideWithNav.startAutoplay();
                break;
        }
    }
    swipeNext() {
        this.slideWithNav.slideNext();
    }
    swipeBack() {
        this.slideWithNav.slidePrev();
    }

    constructor(
        public utils: UtilsService,
        private navCtrl: NavController,
        public authService: AuthService,
        private loadingCtrl: LoadingController,
        private platform: Platform,
        private globalization: Globalization,
        private _translate: TranslateService,
        private serviceCompte: CompteService,
        public utilsService: UtilsService,
        private reseauService: reseauxervice,
        private CaService: CaService,
        private contratService: ContratService,
        private garantieService: GarantieService,
        private serviceObjet: ObjetService,
    ) {
        this.simulationActif = new Simulation();
        this.simulationActif.ca = new CA();
        this.simulationActif.objet = new ObjetAssures2();
        this.simulationActif.typeGaranties = new Array<TypeGarantie>();
        this.operationActif = new Operation();
        this.operationActif.paiement = new Paiement();
        this.contratActif = new Contrat2();
        this.assureContratActif = new AssureContrat();
        this.contratActif.myCA = new CA();
    }
    ngOnInit(): void {
        //Called after the constructor, initializing input properties, and the first call to ngOnChanges.
        //Add 'implements OnInit' to the class.
        this.LoadClientConnectedFromSession();
    }
    ionViewWillEnter() {
        this.chargerInfoSimSession();
        console.log("ecran récapitulatif:", this.listeSimFlotte);
    }
    chargerInfoSimSession() {
        //debut
        this.typeOrigineRoutine = JSON.parse(
            window.sessionStorage.getItem(typeRoutine)
        );

        switch (this.typeOrigineRoutine) {
            case this.typeRoutineSimulation:
                this.simulationActif = JSON.parse(
                    window.sessionStorage.getItem(simulationActifenSession)
                );
                this.logo = UrlApiLogo + this.simulationActif.ca.id;

                if (this.simulationActif == null) {
                    this.utils.alertNotification(
                        "veuillez lancer d'abord lancer  une simulation"
                    );

                    this.navCtrl.navigateRoot("/sim-decla-objet");
                }

                this.listeSimFlotte = JSON.parse(
                    window.sessionStorage.getItem(
                        listeSimulationDansFlotteValide
                    )
                );
                console.log("simulation valide", this.listeSimFlotte[0])
                //console.log(JSON.parse(window.sessionStorage.getItem(listeSimulationDansFlotteValide)))
                if (this.listeSimFlotte == null) this.listeSimFlotte = [];
                //

                this.remplirGarantiesFlotte();

                //console.log(this.listeSimFlotte) ;
                //console.log(this.simulationActif) ;

                this.lancerContratFromSimulation();
                break;
            case this.typeRoutineRenewContrat:
            case this.typeRoutineNewContrat: {
                this.listeRoutineContrat = JSON.parse(
                    sessionStorage.getItem(listeRoutineContratSession)
                );
                let typeObjet = window.sessionStorage.getItem('TypeObjet');
                 console.log("TypeObjet",typeObjet);
                if (this.listeRoutineContrat.length == 1){
                    console.group("listeContrat length= 1");
                    console.log(this.listeRoutineContrat);
                    this.contratActif = JSON.parse(
                    window.sessionStorage.getItem(contratActifenSession)
                    );
                    this.objetActif = JSON.parse(
                        window.sessionStorage.getItem(objetActifenSession)
                    );
                    this.operationActif = JSON.parse(
                        window.sessionStorage.getItem(operationActifenSession)
                    );
                    this.assureContratActif = JSON.parse(window.sessionStorage.getItem(assureContratActifenSession))
                    this.logo = UrlApiLogo + this.contratActif.myCA.id;
                    this.contratActif.objetAssures = [];
                    this.contratActif.objetAssures.push(this.assureContratActif);
                    this.contratActif.dateEffet= this.assureContratActif.dateEffet;
                    this.contratActif.dateCloture= this.assureContratActif.dateCloture;
                     console.log("contratActif",this.contratActif);
                     console.log("objetActif",this.objetActif);
                    //  this.TabtypeGaranties = this.assureContratActif.garanties;
                    //  console.log("TabtypeGaranties",this.TabtypeGaranties);
                    // if (this.contratActif == null) {
                    //     this.loadingCtrl.dismiss();
                    //     this.navCtrl.navigateRoot("/mes-contrats");
                    // }
                }else{
                     console.group("listeContrat length>1");
                    console.log(this.listeRoutineContrat);
                    this.listeOpFlotte = [];
                    this.listeObjFlotte = [];
                    this.listeConFlotte = [];
                    this.listeAssConFlotte = [];
                    this.listeRoutineContrat.forEach((routine) => {
                        this.listeOpFlotte.push(routine.operation);
                        this.listeObjFlotte.push(routine.objet);
                        this.listeConFlotte.push(routine.contrat);
                        this.listeAssConFlotte.push(routine.assureContrat);
                    })
                }

                // this.listeOpFlotte = JSON.parse(
                //     window.sessionStorage.getItem(listeOperationFlotte)
                // );
                // this.listeObjFlotte = JSON.parse(
                //     window.sessionStorage.getItem(listeObjetFlotte)
                // );
                // this.listeAssConFlotte = JSON.parse(
                //     window.sessionStorage.getItem(listeAssureContratFlotte)
                // );
                // if (this.listeOpFlotte == null) {
                //     this.listeOpFlotte = [];
                //     this.listeObjFlotte = [];
                //     this.listeConFlotte = [];
                //     this.listeAssConFlotte = [];
                //     //
                //     this.remplirGarantiesContrat().then(() => {
                //         this.listeOpFlotte.push(this.operationActif);
                //         this.listeObjFlotte.push(this.objetActif);
                //         this.listeConFlotte.push(this.contratActif);
                //     });
                // } else
                //     this.remplirGarantiesContrat().then(() => {
                //         this.listeOpFlotte.push(this.operationActif);
                //         this.listeObjFlotte.push(this.objetActif);
                //         this.listeConFlotte.push(this.contratActif);
                //     });

                //this.lancerContratFromRoutine();
                break;
            }
            // default :this.effacerDonnees() ;
        }

        //fin
    }
    recupererConditionsGarantie(garantie, ca): ConditionGenerale {
        //on va recuperer les conditions de la garantie  proposé par la compagnie d'assurnace
        let conditionsGenerales: ConditionGenerale = {
            id: 1,

            conditionsSpeciales: [
                "Les garanties sont valables pour la traversée de la gambie",
                "les garanties sont valables pour la casamance",
                "les garanties sont valables pour la casamance",
                "les garanties sont valables pour la casamance",
                "les garanties sont valables pour la casamance",
                "les garanties sont valables pour la casamance",
                "les garanties sont valables pour la casamance",
                "les garanties sont valables pour la casamance",
            ],
            garantie: null,
        };

        return conditionsGenerales;
    }
    async chargerConditionsGenerales(id) {
        const loader = await this.loadingCtrl.create({
            message: "",
        });

        loader.present();
        // To download the PDF file
        this.serviceCompte.download(
            CONDITIONSPATHNAME,
            id,
            downloadConditionsGenerales
        );
    }
    displayDate(date: Date): String {
        date = new Date(date);
        let jour, mois;
        let dd = date.getDate();

        let mm = date.getMonth() + 1;
        let yyyy = date.getFullYear();
        if (dd < 10) {
            jour = "0" + dd;
        } else jour = dd;

        if (mm < 10) {
            mois = "0" + mm;
        } else mois = mm;
        return jour + "/" + mois + "/" + yyyy;
    }
    lancerContratFromSimulation() {
        this.operationSimActif = JSON.parse(
            window.sessionStorage.getItem(operationSimActifenSession)
        );
        if (this.operationSimActif == null) {
            this.operationSimActif = new Operation();
            //this.operationSimActif.garanties = new Array<Garantie>();
            this.operationSimActif.details = new Array<DetailOperation>();
            this.operationSimActif.dateEffet = new Date(
                this.simulationActif.dateDebut
            );
            this.operationSimActif.dateCloture = new Date(
                this.simulationActif.dateFin
            );
            this.simulationActif.typeGaranties.forEach((typeGarantie) => {
                let garantie = this.recupererGarantie(
                    typeGarantie,
                    this.simulationActif.ca
                );
                //this.operationSimActif.garanties.push(garantie);
                this.operationSimActif.details.forEach((item) => {
                    item.garantie = garantie;
                });
            });
        }
        let typeRenouvel = JSON.parse(
            sessionStorage.getItem(typeRenouvellement)
        );
        if (typeRenouvel == typeRenouvellementChangementDeCA) {
            let contrat = this.verifierExistContratwithAuto(
                this.simulationActif.ca,
                this.simulationActif.objet
            );
            if (contrat == null) {
                this.operationSimActif.intitule = "Souscription";

                //format date MM-dd-YYYY
                this.creerNouveauContrat();
            } else {
                this.operationSimActif.intitule = "Renouvellement";

                this.contratActif = contrat;
            }
        } else {
            switch (this.simulationActif.origineRoutine) {
                case OriginePageGesSim:
                    this.navCtrl.navigateRoot("/sim-decla-objet");
                    break;
                case OrigineClientNonLogue:
                    this.navCtrl.navigateRoot("/sim-decla-objet");
                    break;
                case OriginePageMonObjet:
                    this.navCtrl.navigateRoot("/monobjet");
                    break;
                case OriginePageGesSimReseau:
                    this.navCtrl.navigateRoot("/EcranPageSimulation");
                    break;
                default:
                    this.navCtrl.navigateRoot("/sim-decla-objet");
            }
            this.simulationActif.typeGaranties = [];
            window.sessionStorage.setItem(
                simulationActifenSession,
                JSON.stringify(this.simulationActif)
            );
        }
    }
    lancerContratFromRoutine() {
        this.operationSimActif = JSON.parse(
            window.sessionStorage.getItem(operationSimActifenSession)
        );
        if (this.operationSimActif == null) {
            this.operationSimActif = new Operation();
        }
       // this.operationSimActif.garanties = new Array<Garantie>();
        this.operationSimActif.details = new Array<DetailOperation>();

        /**
         * !A revoir pour récuperer les garanties
         * !par rapport à une CA
         */
        // this.operationActif.garanties.forEach((typeGarantie) => {
        //     let garantie = this.recupererGarantie(
        //         typeGarantie,
        //         this.contratActif.myCA
        //     );
        //     this.operationSimActif.garanties.push(garantie);
        //     this.operationSimActif.details.forEach((item) => {
        //             item.garantie = garantie;
        //         });
        // });
        this.operationSimActif.dateEffet = this.operationActif.dateEffet;
        this.operationSimActif.dateCloture = this.operationActif.dateCloture;

        let typeRenouvel = JSON.parse(
            sessionStorage.getItem(typeRenouvellement)
        );

        switch (typeRenouvel) {
            case typeRenouvellementMemeCa:
                this.operationSimActif.intitule = "Renouvellement";
                break;
            case typeRenouvellementChangementDeCA:
                let contrat = this.verifierExistContratwithAuto(
                    this.contratActif.myCA,
                    this.objetActif
                );
                if (contrat == null) {
                    //console.log("souscription") ;
                    this.operationSimActif.intitule = "Souscription";

                    this.creerNouveauContrat();
                } else {
                    //console.log("renouvellement") ;
                    this.operationSimActif.intitule = "Renouvellement";
                    this.contratActif = contrat;
                }
                break;
        }
        //on cree nouveau contrat ou on met a jour ancien contrat
    }
    creerNouveauContrat() {
        ///creer
        // this.contratActif =  new Contrat() ;
        //this.contratActif.operations = [];
    }
    //ici je fais un appel à lapi pour recuperzer la garantie correspondante en fournissant le ca et typeGarantie
    recupererGarantie(typeGarantie, ca: CA): Garantie {
        //simuleer baase
        //console.log(ca)
        // Comment
        /*let garantie = {
            id: typeGarantie.id,
            nom: ca.nom + " " + typeGarantie.nom,

            offrePromos: [],

            typeGarantie: {
                id: 1,
                nom: typeGarantie.nom,
                typeBA: null,
                garanties: [],
                obligatoire: null,
                general: null,
                resume: null,

                description: null,
            },
            ba: null,

            operations: [],
        };*/
        return null;
    }
    verifierExistContratwithAuto(ca, objet) {
        //ici on va verifie s'il existait un contrat entre la compagnie et lobjet et retourn un objet de type contrat ou null
        ///test first case
        let sinistre:Sinistre = {
                id: 1,
                date: new Date(),
                numSinistre: "SIN0355",
                typeSinistre: new TypeSinistre(),
                lieu:"Dakar",
                assure: new AssureContrat(),
                contrat: new Contrat2()
            };
        // let assureContrat: AssureContrat = {
        //     id: null,
        //     contrat: new Contrat2(),
        //     assure: objet,
        //     dateEffet: this.operationSimActif.dateEffet,
        //     dateCloture: this.operationSimActif.dateCloture,
        //     garanties:[],
        //     montantTarif: 15050,
        // }
        let contrat: Contrat2 = {
            id: 1,
            police: "488-4445-552",
            dateEffet: this.operationSimActif.dateEffet,
            dateCloture: this.operationSimActif.dateCloture,
            etat: null,
            myCA: ca,
            reseau: new Reseau(),
            client: new Client(),
            //branche: null,
            objetAssures: [],
            avenants: [],
            sinistres: [new Sinistre],
            operations: [
                // {
                //     id: 2,

                //     intitule: "souscription",

                //     codePin: 5412544,

                //     dateEffet: new Date("05-02-2018"),
                //     dateCloture: new Date("05-05-2020"),
                //     contrat: null,

                //     paiement: null,

                //     garanties: [],
                //     statut: statutContratActif,

                //     sinistres: [],
                // },
            ],
            validationCA: true,
            validationOP: true,
            montantBrut: 0,
            montant: 0,
            montantPromo: 0,
            montantFrais: 0,
            montantPromoFrais: 0,
        };

        return contrat;
    }
    valider() {
        this.contratActif.operations.push(this.operationSimActif);

        //console.log(this.contratActif)
    }

    addVehicule() {
        // //pour tester une date fe fin inferieur à 15 jours test
        // this.simulationActif.dateFin=new Date(this.simulationActif.dateFin)
        // this.simulationActif.dateFin.setDate(this.simulationActif.dateFin.getDate()-1)
        // //fin test
        let dureeJour = this.utils.diffdate(
            this.simulationActif.dateFin,
            this.simulationActif.dateDebut,
            "jour"
        );
        let dat = new Date(dureeJour * 24 * 60 * 60 * 1000);
        let datRef = new Date(null);
        dureeJour = dat.getDate() - datRef.getDate() - 1;
        let dureeMois = dat.getMonth() - datRef.getMonth();

        let dureeAnnee = dat.getFullYear() - datRef.getFullYear();

        if (dureeMois == 0 && dureeAnnee == 0 && dureeJour < 15) {
            this.utils.alertNotification(
                "Vous ne pouvez pas ajouter un nouveau véhicule il reste moins de 15 jours avant la fin du contrat"
            );
        } else {
            ///repetitition car sinon le traitement est effectué avant l'ajout à corriger

            this.debutFlotte();
        }
    }
    addVehiculeContrat() {
        // //pour tester une date fe fin inferieur à 15 jours test
        // this.simulationActif.dateFin=new Date(this.simulationActif.dateFin)
        // this.simulationActif.dateFin.setDate(this.simulationActif.dateFin.getDate()-1)
        // //fin test
        let dureeJour = this.utils.diffdate(
            this.operationActif.dateEffet,
            this.operationActif.dateCloture,
            "jour"
        );
        let dat = new Date(dureeJour * 24 * 60 * 60 * 1000);
        let datRef = new Date(null);
        dureeJour = dat.getDate() - datRef.getDate() - 1;
        let dureeMois = dat.getMonth() - datRef.getMonth();

        let dureeAnnee = dat.getFullYear() - datRef.getFullYear();

        if (dureeMois == 0 && dureeAnnee == 0 && dureeJour < 15) {
            this.utils.alertNotification(
                "Vous ne pouvez pas ajouter un nouveau véhicule il reste moins de 15 jours avant la fin du contrat"
            );
        } else {
            ///repetitition car sinon le traitement est effectué avant l'ajout à corriger

            this.debutFlotteContrat();
        }
    }

    debutFlotte() {
        //console.log(this.listeSimFlotte) ;
        sessionStorage.setItem(
            listeSimulationDansFlotteValide,
            JSON.stringify(this.listeSimFlotte)
        );
        sessionStorage.setItem(flotte, JSON.stringify(true));
        // this.simulationActif.dateDebut = null ;
        let sim: Simulation = new Simulation();

        //reference : string;

        sim.client = this.simulationActif.client;
        sim.dateSimulation = new Date(this.simulationActif.dateSimulation);
        //relations à ajoutéé

        //type de garanties pour uniformiser dans le choice option

        sim.dateDebut = new Date(
            this.utils.displayDateFormatInitialise(
                this.simulationActif.dateDebut
            )
        );
        // console.log(this.utils.displayDate(this.simulationActif.dateDebut)) ;
        // console.log(this.utils.displayDate(sim.dateDebut));
        sim.dateFin = new Date(this.simulationActif.dateFin);
        sim.origineRoutine = this.simulationActif.origineRoutine;
        sim.numeroDansSession = this.simulationActif.numeroDansSession;
        sim.ca = this.simulationActif.ca;
        sim.montantTTCCompagnie = this.simulationActif.montantTTCCompagnie;
        sim.objet = new ObjetAssures2();

        sim.typeGaranties = [];
        sessionStorage.setItem(simulationActifenSession, JSON.stringify(sim));
        this.navCtrl.navigateRoot("/sim-decla-objet");
    }
    debutFlotteContrat() {
        //console.log(this.listeOpFlotte) ;
        sessionStorage.setItem(
            listeOperationFlotte,
            JSON.stringify(this.listeOpFlotte)
        );
        sessionStorage.setItem(
            listeObjetFlotte,
            JSON.stringify(this.listeObjFlotte)
        );
        sessionStorage.setItem(
            listeContratFlotte,
            JSON.stringify(this.listeConFlotte)
        );
        sessionStorage.setItem(flotte, JSON.stringify(true));
        // this.simulationActif.dateDebut = null ;
        let op: Operation = new Operation();
        op.paiement = new Paiement();
        let contrat: Contrat2 = new Contrat2();
        let obj;

        //reference : string;

        //type de garanties pour uniformiser dans le choice option

        op.dateEffet = new Date(
            this.utils.displayDateFormatInitialise(
                this.operationActif.dateEffet
            )
        );
        // //console.log(this.utils.displayDate(this.simulationActif.dateDebut)) ;

        op.dateCloture = new Date(this.operationActif.dateCloture);

        contrat.myCA = this.contratActif.myCA;
        op.paiement.montant = this.operationActif.paiement.montant;
        obj = new ObjetAssures();

        //op.garanties = [];
        //op.details = [];
        sessionStorage.setItem(contratActifenSession, JSON.stringify(contrat));
        sessionStorage.setItem(operationActifenSession, JSON.stringify(op));
        sessionStorage.setItem(objetActifenSession, JSON.stringify(obj));
        this.navCtrl.navigateRoot("/sim-decla-objet");
    }

    // lancerContratFromSimulationFlotte(){
    //   this.operationSimActif= JSON.parse(window.sessionStorage.getItem(operationSimActifenSession)) ;
    //   if(this.operationSimActif == null)
    //   {
    //     this.operationSimActif = new Operation() ;
    //     this.operationSimActif.garanties = new Array<Garantie>() ;
    //     this.operationSimActif.dateEffet = new Date(this.simulationActif.dateDebut) ;
    //     this.operationSimActif.dateCloture = new Date(this.simulationActif.dateFin) ;
    //     this.simulationActif.typeGaranties.forEach(typeGarantie=> {
    //       let garantie =this.recupererGarantie(typeGarantie,this.simulationActif.ca) ;
    //       this.operationSimActif.garanties.push(garantie) ;

    //     });

    //   }
    //   let typeRenouvel = JSON.parse(sessionStorage.getItem(typeRenouvellement)) ;
    //   if(typeRenouvel == typeRenouvellementChangementDeCA)
    //   {
    //     let contrat = this.verifierExistContratwithAuto(this.simulationActif.ca,this.simulationActif.objet) ;
    //     if(contrat == null)
    //     {
    //       this.operationSimActif.intitule ="Souscription" ;

    //  //format date MM-dd-YYYY
    //       this.creerNouveauContrat() ;

    //     }
    //     else{
    //       this.operationSimActif.intitule ="Renouvellement" ;

    //       this.contratActif = contrat ;
    //     }

    //   }
    //   else{

    //     switch(this.simulationActif.origineRoutine)
    //     {
    //       case OriginePageGesSim : this.navCtrl.navigateRoot("/sim-decla-objet") ;break ;
    //       case OrigineClientNonLogue : this.navCtrl.navigateRoot("/sim-decla-objet") ;break ;
    //       case OriginePageMonObjet: this.navCtrl.navigateRoot("/monobjet") ;break ;
    //       case OriginePageGesSimReseau : this.navCtrl.navigateRoot("/EcranPageSimulation") ;break ;
    //       default : this.navCtrl.navigateRoot("/sim-decla-objet") ;
    //     }
    //     this.simulationActif.typeGaranties = [] ;
    //     window.sessionStorage.setItem(simulationActifenSession,JSON.stringify(this.simulationActif))

    //   }

    // }
    remplirGarantiesFlotte() {
        var p1 = new Promise(function (resolve, reject) {
            resolve("Succès !");
            // ou
            // reject("Erreur !");
        });
        this.listeSimFlotte.forEach((simulation) => {
            //console.log(simulation)
            simulation.garanties = [];
            simulation.typeGaranties.forEach((typeGarantie) => {
                let garantie = this.recupererGarantie(
                    typeGarantie,
                    simulation.ca
                );
                simulation.garanties.push(garantie);
            });
        });

        return p1;
    }
    remplirGarantiesContrat() {
        var p1 = new Promise(function (resolve, reject) {
            resolve("Succès !");
            // ou
            // reject("Erreur !");
        });
        let op = new Operation();
        /**
         * !A REVOIR POUR RECUPERER
         * !LES GARANTIES CA
         */
        // let garanties = this.operationActif.garanties;
        // this.operationActif.garanties = [];
        // garanties.forEach((typeGarantie) => {
        //     let garantie = this.recupererGarantie(
        //         typeGarantie,
        //         this.simulationActif.ca
        //     );
        //     this.operationActif.garanties.push(garantie);
        // });
        return p1;
    }

    defaultHref() {
        this.typeOrigineRoutine = JSON.parse(
            window.sessionStorage.getItem(typeRoutine)
        );

        switch (this.typeOrigineRoutine) {
            case this.typeRoutineSimulation:
                return "assurinfove-vehicule-flotte";
                break;
            case typeRoutineNewContrat:
            case typeRoutineRenewContrat:
                return "simbranauto";
                break;
        }
    }
    paytech() {
        if (PayTech != null) {
            console.log("paytech");
            let commande = new Commande();
            commande.refCommande = "f5f54f54f";
            new PayTech({
                item_name: "Nouvelle Souscription",
                item_price: "16000",
                ref_command: this.refCommande++,
                command_name: "HBZZYZVUzeuxxZZZV",
                env: "test",
                ipn_url: "https://localhost:8100/Assurus24/api//paiement/receptionIPN",
                success_url: "https://paytech.sn/mobile/success",
                cancel_url: "https://paytech.sn/mobile/cancel",
            }).withOption({
                requestTokenUrl: UrlApi + "/paiement/demande",
                method: "POST",
                headers: {
                    Accept: "text/html",
                },
                presentationMode: PayTech.OPEN_IN_POPUP,
                didPopupClosed: function (
                    is_completed,
                    success_url,
                    cancel_url
                ) {
                    console.log(is_completed);
                    window.location.href =
                        is_completed === true ? success_url : cancel_url;
                },
                willGetToken: function () {
                    console.log("Je me prepare a obtenir un token");
                },
                didGetToken: function (token, redirectUrl) {
                    console.log(
                        "Mon token est : " +
                        token +
                        " et url est " +
                        redirectUrl
                    );
                },
                didReceiveError: function (error) {
                    console.log("Erreur", error);
                },
                didReceiveNonSuccessResponse: function (jsonResponse) {
                    console.log("non success response ", jsonResponse);
                    alert(jsonResponse.errors);
                },
            }).send();
        } else {
            this.utils.alertNotification(MessagePaiementErreurConnexion);
        }
    }
    //paytech postpaiement

    /**
     * * Cette fonction permet de récuperer
     * * le client connecté en session
     */
    LoadClientConnectedFromSession() {
        if (window.sessionStorage.getItem(SessionCompte)) {
            let data = JSON.parse(window.sessionStorage.getItem(SessionCompte));
            this.connectedClient = data.client;
            console.log("Client connected", this.connectedClient);
        }
    }

    /**
     * *Cette fonction permet de former une garantie
     * @param assureContrat
     * @param garantieCa
     * @returns garantie
     */
    async createGarantieAssureContrat(assureContrat, garantieCa){
        let garantie: Garantie= {
            id: null,
            code: "" ,
            codeExterne: "",
            libelle:"" ,
            description :"" ,
            taux: 100,
            minimum: 0,
            franchise : 0,
            agemin : 0,
            agemax : 0,
            attributmontant :"" ,
            codeFormule: new TarifGarantie,
            tablecoeff: new TableCoeffGarantie,
            pack: false,
            typegarantie: garantieCa,
            primepure: 0.0, // montant prime pure
            primecommerciale: 0.0, // montant primce commerciale
            montantchargement: 0.0, // montant chargement
            primeht: 0.0, // montant hors taxe
            montanttaxe: 0.0,// montant montant taxe
            primettc:0.0, // montant TTC
            montantTarif:0, // montant à payer sur la garantie
            assure: new AssureContrat,
            attributs:[new AttributGarantie]
        }

        return garantie;
    }
    createContratTest(){
        let typeObjet = window.sessionStorage.getItem('TypeObjet');
         this.contratActif.police= "9689-9452-203",
         this.contratActif.etat= 1;
         this.contratActif.client= this.connectedClient;
         this.contratActif.avenants= [];
         this.contratActif.sinistres= [],
         this.contratActif.operations = [];
         this.contratActif.validationCA= true;
         this.contratActif.validationOP= true;
         this.contratActif.montantBrut= 0;
         this.contratActif.montantPromo= 0;
         this.contratActif.montantFrais= 0;
         this.contratActif.montantPromoFrais= 0;
         if(typeObjet == 'Existant'){
            this.enregistrementContrat();
         }else{
             this.addNewObjetAssureAndContrat(this.objetActif,this.contratActif);
         }


    }
    /**
     * Cette fonction permet de creer d'abord un objet
     * assure ensuite de creer un contrat
     * @param objetAssure
     * @param contrat
     */
    async addNewObjetAssureAndContrat(objetAssure, contrat) {
        objetAssure.attributs = objetAssure.attributs.filter(
            function (el) {
                return el != null;
            }
        );
        objetAssure.attributs.forEach((element: any) => {
            if (element.attribut != null) {
                delete element.attribut.listevaleur;
            }
            //element.typeba.parent
        });

        objetAssure.typeba.attributs.forEach((element: any) => {
            if (element != null) {
                delete element.listevaleur;
            }
            //element.typeba.parent
        });
        console.log("object", objetAssure);
        this.serviceObjet.addObjetAssures(objetAssure).subscribe(
            (data) => {
                if (data) {

                    // this.loadingCtrl.dismiss();
                    // this.utilsService.alertNotification(
                    //     "Enregistré avec succes"
                    // );
                    // //api post objet
                    // console.log(
                    //     "enregistrement de l'objet au niveu de la base"
                    // );

                    console.log("Objet saved", data);
                    this.enregistrementContrat(data);

                }
            },
            (error) => {
                console.log(error);
            }
        );
    }

    /**
     * *Cette fonction permet d'enregistrer
     * *un contrat en fonction du type objet
     * *et de la routine
     */
    enregistrementContrat(objet?){
        let typeObjet = window.sessionStorage.getItem('TypeObjet');
         this.reseauService.getReseau(2).subscribe((reseau)=>{
                console.log("reseau" ,reseau);
                if(reseau){
                this.contratActif.reseau = reseau;
                let count = 0;
                //Ici on parcours chaque assureContrat pour récupèrer
                //la brancheCa ensuite on parcours les type de garanties
                //pour récuperer la garantieCa de chaque type
                //et former l'objet garantie
                this.contratActif.objetAssures.forEach((assure) => {
                    let TabtypeGarantiesChoosen:any[] = assure.garanties;
                        assure.garanties = [];
                        if(typeObjet == 'Nouveau'){
                            assure.assure = objet;
                        }
                        delete assure.assure.TabAtrributs;
                        assure.assure.proprietaire = this.connectedClient;
                    this.CaService.getBrancheCA(assure.assure.typeba.id,this.contratActif.myCA.id).subscribe((brancheca)=>{
                        if(brancheca){
                            console.log("brancheca" ,brancheca)
                                assure.brancheCA = brancheca;
                                let countTypeGarantie = 0;
                                TabtypeGarantiesChoosen.forEach((typegarantie: TypeGarantie) => {
                                    console.log("typegarantie", typegarantie.id);
                                    this.garantieService.getGarantieCA(brancheca.id,typegarantie.id)
                                    .subscribe((garantieCa) => {
                                        console.log("Garantie Ca", garantieCa);
                                        if(garantieCa){
                                            this.createGarantieAssureContrat(assure,garantieCa)
                                            .then((garantie)=>{
                                                console.log("garantie created", garantie);
                                                assure.garanties.push(garantie);
                                                countTypeGarantie +=1;
                                                if(countTypeGarantie === TabtypeGarantiesChoosen.length){
                                                count += 1
                                                console.log("count", count);
                                                }
                                                if(count === this.contratActif.objetAssures.length){
                                                    console.log("ContratActif before save" ,this.contratActif);
                                                    this.contratService.postContrat(this.contratActif).subscribe(contrat => {
                                                        console.log("saved contrat" ,contrat);
                                                    })
                                                }
                                            })

                                        }
                                    })//Fin appel service garantie
                                })//End foreach
                        }

                    });
                });
                }

            });
    }

    /**GESTION LOGIQUE TRANSLATION DES LANGUES */
    ionViewDidEnter(): void {
        this.getDeviceLanguage()
    }

    public changeLanguage(): void {
        this._translateLanguage();
    }

    _translateLanguage(): void {
        this._translate.use(this.language);
    }

    _initTranslate(language) {
        // Set the default language for translation strings, and the current language.
        this._translate.setDefaultLang('fr-FR');
        if (language) {
            this.language = language;
        }
        else {
            // Set your language here
            this.language = 'fr-FR';
        }
        this._translateLanguage();
    }

    getDeviceLanguage() {
        if (!this.platform.is('android') || !this.platform.is('ios')) {
            //alert('Navigateur')
            console.log('Navigateur langage', navigator.language)
            this._initTranslate(navigator.language);
        } else {
            this.globalization.getPreferredLanguage()
                .then(res => {
                    this._initTranslate(res.value)
                })
                .catch(e => { console.log(e); });
        }
    }
    /**END TRANSLATION */
}

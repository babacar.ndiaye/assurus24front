import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ChoicesteponePage } from './choicestepone.page';

describe('ChoicesteponePage', () => {
  let component: ChoicesteponePage;
  let fixture: ComponentFixture<ChoicesteponePage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ChoicesteponePage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ChoicesteponePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

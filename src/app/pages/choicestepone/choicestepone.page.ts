import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-choicestepone',
  templateUrl: './choicestepone.page.html',
  styleUrls: ['./choicestepone.page.scss'],
})
export class ChoicesteponePage implements OnInit  {
  public onSimulationForm: FormGroup;

  public technologies : Array<{ name: string, description: string, isChecked: boolean }> = [
    {
       name : 'Responsabilité Civile',
       description : '<table><tr><td><B>Compagnie d\'assurance: </B> Axa</td><td><B>Catégorie:</B> Véhicule</td></tr><tr><td><B>Branche: Auto</B> </td><td><B>Avenant:</B> 00002-00158</td></tr><tr><td><B>N police:</B>501/21-012356</td></tr></table>',
       isChecked: false
    },
    {
       name : 'Défense / Recours',
       description : 'Latest cutting edge front-end development framework - can be enabled as an option for Ionic development',
       isChecked: false
      },
    {
       name : 'Recours anticipé',
       description : 'Popular front-end development framework from Facebook- can be enabled as an option for Ionic development',
       isChecked: false
    },
    {
       name : 'Individuel accident',
       description : 'Superset of JavaScript that provides class based object oriented programming and strict data typing',
       isChecked: false
    },
    {
       name : 'Assistance',
       description : 'Apache Cordova compatible plugins that allow native device API\'s to be utilised',
       isChecked: false
    },
    {
       name : 'Bris de glace',
       description : 'Plugins for Progressive Web App and hybrid app development',
       isChecked: false
    },
    {
       name : 'Incendie',
       description : 'Custom web component development framework',
       isChecked: false
    },
    {
       name : 'vol',
       description : 'CSS pre-processor development library',
       isChecked: false
    },
    {
       name : 'Assurance tous risques',
       description : 'Markup language and front-end API support',
       isChecked: false
    }
    ,
    {
       name : 'Messages',
       description : 'Markup language and front-end API support',
       isChecked: false
    },
    {
       name : 'Carte grise',
       description : 'Markup language and front-end API support',
       isChecked: false
    },
    {
       name : 'Control Technique',
       description : 'Markup language and front-end API support',
       isChecked: false
    }
  ];



  constructor(private formBuilder: FormBuilder) {}


  /**
   * Captures and console logs the value emitted from the user depressing the accordion component's <ion-button> element
   * @public
   * @method captureName
   * @param {any}		event 				The captured event
   * @returns {none}
   */
  public captureName(event: any) : void
  {
     console.log(`Captured name by event value: ${event}`);
  }
  ngOnInit() {
    this.onSimulationForm = this.formBuilder.group({
      'rs': [null, Validators.compose([
        Validators.required
      ])],
      'dr': [null, Validators.compose([
        Validators.required
      ])],
    
    });
  }

}

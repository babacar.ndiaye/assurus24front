import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';


import { IonicModule } from '@ionic/angular';
import { CUSTOM_ELEMENTS_SCHEMA }  from '@angular/core';

import { ChoicesteponePage } from './choicestepone.page';
import { AccordionchecklistComponent } from './../../components/accordionchecklist/accordionchecklist.component';

const routes: Routes = [
  {
    path: '',
    component: ChoicesteponePage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [ChoicesteponePage,AccordionchecklistComponent],
  schemas: [ CUSTOM_ELEMENTS_SCHEMA ]
})
export class ChoicesteponePageModule {}

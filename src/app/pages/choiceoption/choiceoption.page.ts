import { PackTypeGarantieDto } from './../../Dto/PackTypeGarantieDto';
import { PackTypeGarantie } from './../../models/PackTypeGarantie';
import { assureContratActifenSession } from './../../../environments/environment';
import { AssureContratService } from 'src/app/services/assure-contrat.service';
import { Component, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { NavController, ToastController, AlertController, LoadingController, Platform } from '@ionic/angular';
import { AuthService } from 'src/app/services/auth.service';
import { Garantie } from 'src/app/models/garantie';

import { UtilsService } from 'src/app/services/utils.service';
import { Simulation } from 'src/app/models/simulation';
import { simulationActifenSession, ListeSimulationEnSession, OriginePageGesSim, OrigineClientNonLogue, OriginePageMonObjet, debutRoutine, typeRoutine, typeRoutineSimulation, typeRoutineNewContrat, contratActifenSession, operationActifenSession, objetActifenSession, typeRoutineRenewContrat, startRenouvellement, checkParam, initParam, simRoutine, contRoutine, OriginePageGesSimReseau, flotte, listeSimulationDansFlotte, retourFlotte, numeroPivot, editOptions, MessageChargementEnCours, loginToPage, listeRoutineContratSession, indexRoutineContratEditingSession, LIENLOGIN, LIENEPOCNO, LIENASSURINFOVEVEHICULEFLOTTE, LIENSIMDECLAOBJET, LIENASSURINFOVE, LIENSIMBRANAUTO, LIENECRANPAGESIMULATION, LIENMONOBJET, LIENCHOICEOPTION, CONTRATSESSION } from 'src/environments/environment';
import { Session } from 'protractor';
import { Contrat2 } from 'src/app/models/contrat';
import { Operation } from 'src/app/models/operation';
import { ObjetAssures, ObjetAssures2 } from 'src/app/models/objet-assures';
import { Observable } from 'rxjs';
import { AlertOptions } from '@ionic/core';
import { timingSafeEqual } from 'crypto';
import { CA } from 'src/app/models/ca';
import { containsTree } from '@angular/router/src/url_tree';
import { RoutineContrat } from 'src/app/pojos/routine-contrat';
import { Globalization } from '@ionic-native/globalization/ngx';
import { TranslateService } from '@ngx-translate/core';
import { TypeGarantieService } from 'src/app/services/type-garantie.service';
import { TypeGarantie } from 'src/app/models/type-garantie';
import { AssureContrat } from 'src/app/models/assure-contrat';
import { OperationService } from 'src/app/services/operation.service';
import { AssureSimulation } from 'src/app/models/assure-simulation';
import { SimulationClient, SimulationClient2 } from 'src/app/models/simulation-client';
import { CalculSimulationClient } from 'src/app/services/calcul-simulation-client';

@Component({
    selector: 'app-choiceoption',
    templateUrl: './choiceoption.page.html',
    styleUrls: ['./choiceoption.page.scss'],
})
export class ChoiceoptionPage implements OnInit {

    public simulationActif: SimulationClient;
    public contratActif: Contrat2;
    isEditOptions = false;
    public operationActif: Operation = new Operation
    public objetActif: ObjetAssures2 = new ObjetAssures2();
    public assureContratActif: AssureContrat = new AssureContrat();

    largeur = screen.width;
    longueur = screen.height;
    @ViewChild('fin') inputFin;
    @ViewChild('debut') inputDebut;
    @ViewChild('jours') inputJours;
    @ViewChild('mois') inputMois;
    @ViewChild('annees') inputAnnees;
    @ViewChild('duree') inputDuree;


    garanties;
    garantiesFormat;

    checked = [];
    typeOrigineRoutine: any = JSON.parse(window.sessionStorage.getItem(typeRoutine));
    dateFinReference: Date = null;
    dureeRef: number;
    dateDuLendemain: Date;
    mois: string;
    indiceCOL: TypeGarantie;
    indiceCPL: TypeGarantie;
    isFlotte = JSON.parse(window.sessionStorage.getItem(flotte));

    listeSimFlotte: Array<AssureSimulation> = JSON.parse(window.sessionStorage.getItem(listeSimulationDansFlotte));
    listeActif: Array<AssureSimulation> = new Array<AssureSimulation>();
    dateDebut: Date;
    dateFin: Date;
    dureeJour: number;
    test = "test";
    duree: number
    dureeMois: number;
    dureeAnnee: number;
    dateDebutError: boolean = false;
    dateFinError: boolean = false;
    dateDebutMinimum = new Date(null);
    dateFinMaximumBeforeOther = new Date(null);
    obligatoireChecked: boolean = true;
    toutchecked = false;
    dateduJour = new Date(null);
    typeRoutineSimulation = typeRoutineSimulation;
    typeRoutineNewContrat = typeRoutineNewContrat;
    typeRoutineRenewContrat = typeRoutineRenewContrat;
    onDeclarationForm: FormGroup;
    definitionByDate: boolean = true;
    definitionByDuree: boolean = false;
    ifAddNewVehicule: boolean = false;
    numeroPivot: number;
    firstChangeDuree = true;
    lienSimDeclaAuto = LIENSIMDECLAOBJET;
    language: string;
    dismiss: string;
    jours: string;
    moiss: string;
    an: string;
    dureePlaceholder: string;
    //cette variable permet de récupérer l'id de la branche en session
    brancheId: string;
    //cette variable permet de récupérer l'id de l'usage en session
    choosenUsageId: string;
    annee: number;
    active: number;

    slideOpts = {
        slidesPerView: 2,
        //initialSlide: 0,
        speed: 400,
    };
    TabPack = [
        {
            libelle: "Pack 1",
            slogan: "Xeweul",
            open: false,
            garantiesPack: [
                {
                    libelle: "Responsabilité civile"
                },
                {
                    libelle: "Vol"
                }
            ]
        },
        {
            libelle: "Pack 2",
            slogan: "Falaw",
            open: false,
            garantiesPack: [
                {
                    libelle: "Responsabilité civile"
                },
                {
                    libelle: "Vol"
                },
                {
                    libelle: "Incendie"
                }

            ]
        },
        {
            libelle: "Pack 3",
            slogan: "Tawfekh",
            open: false,
            garantiesPack: [
                {
                    libelle: "Responsabilité civile"
                },
                {
                    libelle: "Vol"
                },
                {
                    libelle: "Incendie"
                },
                {
                    libelle: "Brise Glace"
                }

            ]
        },
        {
            libelle: "Pack 4",
            slogan: "Mega",
            open: false,
            garantiesPack: [
                {
                    libelle: "Responsabilité civile"
                },
                {
                    libelle: "Vol"
                },
                {
                    libelle: "Incendie"
                },
                {
                    libelle: "Brise Glace"
                },
                {
                    libelle: "Assistance"
                }

            ]
        }
    ];
    packChecked = false;
    TabPackTypeGarantie: PackTypeGarantie[] = [];
    TabPackTypeGarantieDto: PackTypeGarantieDto[] = [];
    testP = 'cat1BA-VOL walloo';
    showGarantie: boolean;
    showPack: boolean;
    ExludePacke = false;
    conditionGaranties = "Si vous prenez une garantie unitaire vous n'avez plus la possiblité de choisir un pack et inversement !";
    constructor(
        public navCtrl: NavController,
        public alertCtrl: AlertController,
        public toastCtrl: ToastController,
        public utilsService: UtilsService,
        private typeGarantieService: TypeGarantieService,
        private assurecontratService: AssureContratService,
        private operationService: OperationService,
        public formBuilder: FormBuilder,
        private platform: Platform,
        private globalization: Globalization,
        private _translate: TranslateService,
        public loadingCtrl: LoadingController,
        public authService: AuthService,
        private calculSimulation: CalculSimulationClient
    ) {
        this.garanties = new Array<TypeGarantie>();
        this.garantiesFormat = new Array<TypeGarantie>();
        this.simulationActif = new SimulationClient();
        this.contratActif = new Contrat2();
        this.objetActif = new ObjetAssures2();

    }

    /**Gestion des pack */

    /**
     * *Cette fonction permet de gerer
     * *le choix des packs ou des garanties
     * *
     */
    togglePackDetails(event, index, TabGarantiePack, pack) {
        console.log("index", index);
        console.log("event", event.target.checked);
        console.log("pack choosen", pack);

        //Ceci permet de gerer l'evenement checked
        if (event.target.checked) {
            //this.TabPackTypeGarantieDto[index].checked = !this.TabPackTypeGarantieDto[index].checked;
            this.packChecked = true;
            let count = 0;
            //Avant d'appliquer les exclusions pour un pack on rend d'abord toutes les garanties actifs
            this.garantiesFormat.forEach((element: TypeGarantie) => {
                element.inPack = false;
                count += 1;
                if (count == this.garantiesFormat.length) {
                    this.TabPackTypeGarantieDto
                        .filter((item) => {
                            if (item.id == pack.id) {
                                item.checked = true;

                                this.typeGarantieService.getEclusionbyTypeGarantiePrincipale(pack.id)
                                    .subscribe((exclusionTypeGarantie) => {
                                        if (exclusionTypeGarantie.length >= 1) {
                                            console.log("exclusionTypeGarantie", exclusionTypeGarantie);
                                            let countExlusion = 0;
                                            exclusionTypeGarantie.forEach((exclusion) => {
                                                this.garantiesFormat.forEach((element: TypeGarantie) => {
                                                    //Ici on applique les exclusions
                                                    if (exclusion.garantie2.code == element.code) {
                                                        element.inPack = true;
                                                    }
                                                    countExlusion += 1;
                                                    //Si on termine les exclusions on récupère le
                                                    //pack choisit et la garantie obligatoire
                                                    if (countExlusion == exclusionTypeGarantie.length) {
                                                        //Pour récuperer le pack choisit
                                                        this.checked = [];
                                                        this.garanties.forEach(element => {
                                                            if (pack.libelle == element.libelle) {
                                                                console.log('element match', element);
                                                                this.checked.push(element);
                                                            }
                                                            if (element.obligatoire) {
                                                                console.log('element obligatoire', element);
                                                                this.checked.push(element);
                                                            }
                                                            console.log("checked", this.checked);
                                                        });
                                                    }

                                                });

                                                //Exclusion pour pack
                                                this.TabPackTypeGarantieDto.forEach((element: PackTypeGarantieDto) => {
                                                    //Ici on applique les exclusions
                                                    if (exclusion.garantie2.code == element.code) {
                                                        element.ExludePacke = true;
                                                    }
                                                    countExlusion += 1;

                                                });
                                            })
                                        }
                                    });
                            } else {
                                item.checked = false;
                                //item.ExludePacke = true;
                            }
                        });
                }
            });


        } else {
            //this.TabPackTypeGarantieDto[index].checked = false;
            /**
             * Ici on prend en compte le cas ou
             * on selectionne un pack puis le déselectionner
             * et le cas ou selectionne un pack et ensuite
             * choisir un autre pack
             */
            if (pack.checked) {//cas selection puis déselection
                //on garde que la garantie obligatoire et on enlève les autres
                this.checked = this.checked
                .filter((element) => element.obligatoire == true);

                pack.checked = false;
                //Pour verifier si celui qu'on décocche à des exclusions ou pas
                this.typeGarantieService.getEclusionbyTypeGarantiePrincipale(pack.id)
                    .subscribe((exclusionTypeGarantie) => {
                        if (exclusionTypeGarantie.length >= 1) {
                            console.log("exclusionTypeGarantie for unchecked", exclusionTypeGarantie);
                            this.garantiesFormat.forEach((element: TypeGarantie) => {
                                element.inPack = false;
                            });
                            this.TabPackTypeGarantieDto.forEach((element: PackTypeGarantieDto) => {
                                element.ExludePacke = false;
                            });
                        }
                    });
                    console.log("checked", this.checked);
            } else {//cas selectionne pack et ensuite choisir un autre pack
                //console.log("je viens detre delectionner", event.target.checked);
            }
        }

        //ceci permet de gerer le packs à montrer
        // if (this.TabPackTypeGarantieDto[index].checked) {
        //     this.TabPackTypeGarantieDto
        //         .filter((item, itemIndex) => itemIndex != index)
        //         .map(item => item.checked = false);
        // }


    }
    initialiserSectiontoInvisible() {
        this.showGarantie = true;
        this.showPack = false;
    }

    affichageDetails(section, visible: boolean, targetPack) {
        switch (section) {
            case "Garantie":
                this.showGarantie = visible;
                if (this.showPack) {
                    this.showPack = false;
                    this.showGarantie = visible;
                } else {
                    this.showGarantie = visible;
                }
                break;
            case "Pack":
                if (this.showGarantie) {
                    this.showGarantie = false;
                    this.showPack = visible;
                    //this.scroll(targetPack);
                } else {
                    this.showPack = visible;
                }
                break;
        }
    }
    scroll(targetPack) {
        document.querySelector('#targetPack').scrollIntoView({ behavior: 'smooth', block: 'center' });
        //el.scrollIntoView();
    }
    /**End */

    ionViewWillEnter() {
        //Popup infos garanties
        this.alertLocation('Infos Garantie', this.conditionGaranties);
        this.initialiserSectiontoInvisible();
        this.isEditOptions = false;
        this.jours = this._translate.instant('jours')
        this.mois = this._translate.instant('mois')
        this.moiss = this._translate.instant('moiss')
        this.an = this._translate.instant('an')
        this.dismiss = this._translate.instant("dismiss")
        this.dureePlaceholder = this._translate.instant("duree")

        let datBeforeOther = new Date(8640000000000000);
        console.log("date minimum" + this.utilsService.displayDate(datBeforeOther))

        this.recupererNumeroSessionPivot();
        //this.getAllGarantiesByUsageAndBranche();

        console.log("before")
        console.log(this.listeSimFlotte)
        console.log(this.isFlotte)
        console.log(this.typeOrigineRoutine)
        console.log(this.numeroPivot)
        // console.log(this.simulationActif.assures[this.active])
        //console.log(this.simulationActif.numeroDansFlotte)


        this.chargerInfoSimSession();


    }
    ionViewDidLeave() {
        sessionStorage.setItem(editOptions, JSON.stringify(false));
    }


    ngOnInit() {
        if (sessionStorage.getItem("active")) {
            this.active = parseInt(sessionStorage.getItem("active"))
        } else {
            this.active = 0
        }
        this.onDeclarationForm = this.formBuilder.group({
            'debut': [null, Validators.compose([
                Validators.required
            ])],
            'fin': [null, Validators.compose([
                Validators.required
            ])]
        })

        if (sessionStorage.getItem("active")) {
            this.active = parseInt(sessionStorage.getItem("active"))
        } else {
            this.active = 0
        }
        console.log(this.active);

    }
    goToHome() {
        this.navCtrl.navigateRoot('/home-results');
    }

    async calculerSimulation() {
        var p1 = new Promise((resolve, reject) => {

            if (window.sessionStorage.getItem(simulationActifenSession)) {
                let sim: SimulationClient = JSON.parse(window.sessionStorage.getItem(simulationActifenSession));
                delete sim.ca
                delete sim.dateSimulation
                delete sim.eligiblePeriode
                delete sim.numeroDansSession
                delete sim.origineRoutine
                let simFlotte: SimulationClient2 = new SimulationClient2();
                simFlotte = sim;
                simFlotte.assures = sim.assures.map(ob => {
                    delete ob.montantTTCCompagnie
                    delete ob.montantUniqueObjet
                    delete ob.numeroDansFlotte
                    return ob
                })
                simFlotte.assures.forEach(element => {
                    element.assure.typeba.attributs.forEach((element: any) => {
                        if (element != null) {
                            delete element.listevaleur;
                        }
                    });
                    element.assure.typeba.parent.attributs.forEach((element: any) => {
                        if (element != null) {
                            delete element.listevaleur;
                        }
                    });
                    element.assure.attributs.forEach((element: any) => {
                        if (element.attribut != null) {
                            delete element.attribut.listevaleur;
                        }
                    });
                });
                console.log("calcule", simFlotte);

                this.calculSimulation.getSimulationClient(simFlotte).subscribe(data => {
                    // console.log("simulation client reçue", data);
                    resolve(data)
                })
            }
        });
        return p1;
    }

    async alertLocation(header, message) {
        const param1 = this._translate.instant(header) //
        const param2 = this._translate.instant(message) //

        const changeLocation = await this.alertCtrl.create({
            header: param1,
            message: param2,
            cssClass: 'alertCssDescription',

            buttons: [

                {
                    text: 'ok',
                    handler: async (data) => {

                    }
                }
            ]
        });
        changeLocation.present();
    }

    getAllGarantiesByUsageAndBranche() {
        //ici on va recuperer les garanties liées a la branche & usage
        if (sessionStorage.getItem('typeBrancheId') && sessionStorage.getItem('choosenUsageId')) {
            // Récupérer id branche & usage depuis sessionStorage
            this.brancheId = sessionStorage.getItem('typeBrancheId');
            this.choosenUsageId = sessionStorage.getItem('choosenUsageId');
            //console.log('Data User', data);
            console.log("brancheId", this.brancheId);
            console.log("choosenUsageId", this.choosenUsageId);
        }
        if (this.choosenUsageId && this.brancheId) {
            // Pour récupérer l'année de mise en circulation
            if (sessionStorage.getItem(simulationActifenSession)) {
                let sim = JSON.parse(sessionStorage.getItem(simulationActifenSession))
                sim.assures[this.active].assure.attributs.forEach(att => {
                    if (att.attribut.code == "valDate1ereCirculation")
                        this.annee = att.valeur;
                });
            }
            // Pour récupérer l'année de mise en circulation cas routine contrat
            if (sessionStorage.getItem(objetActifenSession)) {
                JSON.parse(sessionStorage.getItem(objetActifenSession)).attributs.forEach(att => {
                    if (att.attribut.code == "valDate1ereCirculation")
                        this.annee = att.valeur;
                });
            }
            // console.log(this.annee);

            this.typeGarantieService.getTypeGarantieByUsageAndBranche(this.choosenUsageId, this.brancheId).subscribe((data) => {
                this.garanties = data;
                console.log("Garanties before controle datecirculation", data);
                console.log("annee", this.annee);
                let tabTest = []; // variable de test  pour le tableau garanties
                let count = 0;//variable pour compter le nombre de parcours
                /**
                 * *Ici on recupère la garantie obligatoire
                 * *On enlève les garanties tierce collision & tierce complète
                 * *pour les objets assurees dont la comparaison entre
                 * *la date d'aujourd'hui et la date de 1ere circulation
                 * *n'est supérieur à 3ans
                 */
                data.forEach(el => {
                    if (el.code == "Cat1BA-RC Walloo")
                        this.addGarantyOfChecked(el)    // on ajoute la garantie RESPONSABILITE CIVILE
                    if ((el.code != "cat1 T.COL walloo" && el.code != "cat1 T.CPL walloo")) {
                        tabTest.push(el)
                    }
                    if ((new Date().getFullYear() - this.annee) <= 3 && (el.code == "cat1 T.COL walloo" || el.code == "cat1 T.CPL walloo")) {
                        tabTest.push(el)
                        if (el.code == "cat1 T.COL walloo")
                            this.indiceCOL = el;
                        if (el.code == "cat1 T.CPL walloo")
                            this.indiceCPL = el;
                    }
                    count += 1;//on incremente à chaque element
                });
                /**
                 * *A la fin du parcours on affecte
                 * *le nouveau tableau
                 * *Puis on récupère le pack de chaque
                 * *type de garantie qui est à true
                 */
                if (count === data.length) {
                    this.garantiesFormat = tabTest;
                    console.log("Garanties after controle datecirculation", tabTest)
                    this.garantiesFormat.forEach(element => {
                        if (element.pack === true) {

                            this.TabPackTypeGarantieDto = [];
                            this.typeGarantieService.getPackTypeGarantie(element.id)
                                .subscribe((packTypeGarantie) => {
                                    if (packTypeGarantie.length >= 1) {
                                        console.log("pa", packTypeGarantie)
                                        //this.TabPackTypeGarantie.push(packTypeGarantie);
                                        let pack: PackTypeGarantieDto = new PackTypeGarantieDto();
                                        pack.id = packTypeGarantie[0].garantieprincipale.id;
                                        pack.libelle = packTypeGarantie[0].garantieprincipale.libelle;
                                        pack.code = packTypeGarantie[0].garantieprincipale.code;
                                        pack.description = packTypeGarantie[0].garantieprincipale.description;
                                        pack.slogan = packTypeGarantie[0].garantieprincipale.slogan;
                                        pack.typeGaranties = []
                                        packTypeGarantie.forEach((p) => {
                                            console.log("p", p.garantieprincipale.libelle)

                                            pack.typeGaranties.push(p.garantiesecondaire);

                                        });
                                        this.TabPackTypeGarantieDto.push(pack);
                                        console.log('TabPackTypeGarantie', this.TabPackTypeGarantieDto);
                                        //pour enlever les packs dans les types de garanties
                                        this.garantiesFormat = this.garantiesFormat.filter((item) => {
                                            return item.pack != true;
                                        })
                                    }
                                })
                        }
                    });
                }

            }, (error) => {
                console.log(error);
            });
        }


    }


    traitementGarantiesObligatoires() {

    }
    async checkBeforeValidation() {
        if (this.definitionByDuree) {
            if (this.checkDuree()) {
                this.definitionByDate = true;
                this.definitionByDuree = false;
                let debut = new Date(this.onDeclarationForm.get('debut').value)
                this.dateFin = debut;;
                this.dateFin.setDate(debut.getDate() + this.dureeJour - 1);
                this.dateFin.setMonth(debut.getMonth() + this.dureeMois);
                this.dateFin.setFullYear(debut.getFullYear() + this.dureeAnnee);

                ////console.log(this.dateFin) ;

                this.inputFin.value = this.utilsService.displayDateFormatInput(this.dateFin);
                await this.utilsService.delay(500)
                this.valider()
            }
            else {

            }
        }
        else {
            this.valider();
        }
    }

    valider() {


        let reactiveDebut = this.onDeclarationForm.get('debut').value;
        let reactiveFin = this.onDeclarationForm.get('fin').value;

        // ////console.log(reactiveDebut) ;
        if (this.checked.length <= 0) {
            this.utilsService.alertNotification("Veuillez choisir une garantie pour passer à l'étape suivante");
        } else {
            if (!this.onDeclarationForm.valid) {
                if (this.onDeclarationForm.get('fin').hasError('required'))
                    this.dateFinError = true;
                if (this.onDeclarationForm.get('debut').hasError('required'))
                    this.dateDebutError = true;
                this.loadingCtrl.dismiss();
                this.utilsService.alertNotification("Veuillez indiquer toutes les dates avant de continuer");
            } else {
                this.dateFin = new Date(reactiveFin);
                this.dateDebut = new Date(reactiveDebut);

                this.typeOrigineRoutine = JSON.parse(sessionStorage.getItem(typeRoutine));
                switch (this.typeOrigineRoutine) {
                    case typeRoutineSimulation: this.trouverDateMinimum(this.simulationActif.assures[this.active].assure); break;
                    case typeRoutineRenewContrat:
                    case typeRoutineNewContrat: this.nextEtape(); break;

                }
                // ////console.log(this.dateDebut+" "+this.displayDate(this.dateDebut)) ;
                // ////console.log(this.dateFin+" "+this.displayDate(this.dateFin)) ;
                // ////console.log(this.checked);

            }
        }

    }

    addCheckbox(event, checkbox) {
        // if (checkbox == this.indiceCOL) {
        //     if (this.removeCheckedFromArray(this.indiceCPL) > -1)
        //         this.removeGarantyFromChecked(this.indiceCPL)
        // }
        // if (checkbox == this.indiceCPL) {
        //     if (this.removeCheckedFromArray(this.indiceCOL) > -1)
        //         this.removeGarantyFromChecked(this.indiceCOL)
        // }
        console.log("checkbox", checkbox);
        if (event.target.checked) {
            // console.log(event.target.checked, checkbox)
            this.addGarantyOfChecked(checkbox)
            console.log(this.checked);
            this.typeGarantieService.getEclusionbyTypeGarantiePrincipale(checkbox.id)
                .subscribe((exclusionTypeGarantie) => {
                    if (exclusionTypeGarantie.length >= 1) {
                        console.log("exclusionTypeGarantie", exclusionTypeGarantie);
                        exclusionTypeGarantie.forEach((exclusion) => {
                            this.garantiesFormat.forEach((element: TypeGarantie) => {
                                if (exclusion.garantie2.code == element.code) {
                                    element.inPack = true;
                                    //pour enlever le checked si ils étaient déjà selectionner
                                    this.checked = this.checked
                                        .filter((c) => c.code != exclusion.garantie2.code);
                                }
                            });
                        })
                    }
                });
        } else {
            this.removeGarantyFromChecked(checkbox)

            //Pour verifier si celui qu'on décocche à des exclusions ou pas
            this.typeGarantieService.getEclusionbyTypeGarantiePrincipale(checkbox.id)
                .subscribe((exclusionTypeGarantie) => {
                    if (exclusionTypeGarantie.length >= 1) {
                        this.garantiesFormat.forEach((element: TypeGarantie) => {
                            element.inPack = false;

                        });
                    }
                });


            this.garantiesFormat.forEach((el) => {
                if (el.obligatoire) {
                    this.checked.push(el);
                }
            });

            console.log(this.checked);

        }
    }

    addGarantyOfChecked(checkbox) {
        if (this.removeCheckedFromArray(checkbox) < 0) {
            this.checked.push(checkbox);
            if (checkbox.general) {
                this.toutchecked = true;
                // console.log(this.checked)
                this.cocherGeneral();
                // console.log(this.checked)
                this.garanties.forEach(garantie => {
                    if (garantie.obligatoire) {
                        this.obligatoireChecked = false;
                        let index = this.removeCheckedFromArray(garantie);
                        ////console.log(index) ;
                        if (index > -1)
                            this.checked.splice(index, 1);
                        ////console.log(this.checked) ;
                    }

                });

            }
        }
    }
    removeGarantyFromChecked(checkbox) {
        let index = this.removeCheckedFromArray(checkbox);
        console.log("index", index)
        this.checked.splice(index, 1);
        if (checkbox.general) {
            this.checked = [];
            this.garanties.forEach(garantie => {

                this.toutchecked = false;
                if (garantie.obligatoire) {
                    this.obligatoireChecked = true;
                    ////console.log(this.testGarantieChecked(garantie)) ;
                    if (!this.testGarantieChecked(garantie))
                        this.checked.push(garantie);

                }

            });
        }
    }
    //Removes checkbox from array when you uncheck it
    removeCheckedFromArray(checkbox) {
        return this.checked.findIndex((category) => {
            return category.id == checkbox.id;
        })
    }


    initialiseChecked() {
        this.garanties.forEach(garantie => {
            ////console.log(this.removeCheckedFromArray(garantie)+" RC") ;
            if (garantie.obligatoire && this.removeCheckedFromArray(garantie) == -1 && !this.testGarantieChecked(garantie))
                this.checked.push(garantie);
        });
        this.misAjourBegin();
    }
    verifierDate() {
        this.dateFinError = false;
        this.dateDebutError = false;
        console.log(this.dateDebut)
        if (this.dateDebut != null) {
            this.dateDebut = new Date(this.dateDebut);
            ////console.log("dateDebutVerif "+this.utilsService.displayDate(this.dateDebut))



        }

        if (this.dateFin != null) {
            this.dateFin = new Date(this.dateFin);

            ////console.log("dateFintVerif "+this.utilsService.displayDate(this.dateFin))
        }


        ////console.log("dateMinmimumVerif "+this.utilsService.displayDate(this.dateDebutMinimum))


        if (this.dateDebut != null && this.dateFin != null) {
            // this.trouverdateDebutMinimum() ;
            this.dateDebut.setHours(0, 0, 0, 0);
            this.dateFin.setHours(0, 0, 0, 0);
            this.dateDebutMinimum.setHours(0, 0, 0, 0);

            if (this.isInferior(this.dateFin, this.dateDebut)) {

                ////console.log("date debut > fin")
                this.dateFin = null;
                this.dateDebut = null;

                this.dateFinError = true;
                this.utilsService.alertNotification("la date de fin ne doit pas être inférieure à la date de début");
                return;
            }
            let ok = this.verifierIfInFirstIntervalle(this.dateDebut, this.dateFin);
            console.log("console " + ok)


            if (!this.verifierIfInFirstIntervalle(this.dateDebut, this.dateFin)) {
                console.log("2 false")
                if (!this.verifierIfInSecondIntervalle()) {
                    return;
                }

            }


        }

        this.nextEtape()




    }


    displayDate(date: Date): String {

        date = new Date(date);
        let jour, mois;
        let dd = date.getDate();

        let mm = date.getMonth() + 1;
        let yyyy = date.getFullYear();
        if (dd < 10) {
            jour = '0' + dd;
        }
        else
            jour = dd;

        if (mm < 10) {
            mois = '0' + mm;
        }
        else
            mois = mm;
        return yyyy + "-" + mois + "-" + jour;
    }
    diffdate(d1, d2, u) {
        ////console.log("diffDate") ;
        ////console.log(d2 ) ;
        d1 = new Date(this.utilsService.displayDateFormatInitialise(d1))
        d2 = new Date(this.utilsService.displayDateFormatInitialise(d2))
        ////console.log(d1);
        ////console.log(d2) ;
        let div = 1
        switch (u) {
            case 'seconde': div = 1000;
                break;
            case 'minute': div = 1000 * 60
                break;
            case 'heure': div = 1000 * 60 * 60
                break;
            case 'jour': div = 1000 * 60 * 60 * 24
                break;
        }

        var Diff = d1.getTime() - d2.getTime() + 1;
        //////console.log(Diff/div+ "jours")

        return Math.ceil((Diff / div))
    }
    isInferior(d1, d2) {
        d1 = new Date(d1);
        d2 = new Date(d2);
        if (d1.getFullYear() != d2.getFullYear())
            return d1.getFullYear() < d2.getFullYear()
        else
            if (d1.getMonth() != d2.getMonth())
                return d1.getMonth() < d2.getMonth()
            else
                if (d1.getDate() != d2.getDate())
                    return d1.getDate() < d2.getDate()
                else
                    return false;



    }

    ajustInput(nom) {

        switch (nom) {
            case "debut":
                this.dateDebutError = false;
                break;
            case "fin":

                ////console.log(this.inputFin.value)
                this.dateFinError = false; break;
        }
    }
    async chargerInfoSimSession() {
        this.typeOrigineRoutine = JSON.parse(window.sessionStorage.getItem(typeRoutine));
        switch (this.typeOrigineRoutine) {
            case typeRoutineSimulation:
                //Pour charger la liste des garanties par rapport à la branche et sous branche(usage)
                this.getAllGarantiesByUsageAndBranche();
                this.simulationActif = JSON.parse(window.sessionStorage.getItem(simulationActifenSession));
                if (this.simulationActif == null) {
                    this.navCtrl.navigateRoot(LIENSIMDECLAOBJET);
                }
                else {
                    this.isEditOptions = JSON.parse(window.sessionStorage.getItem(editOptions));
                    this.isFlotte = JSON.parse(window.sessionStorage.getItem(flotte));
                    if (!this.isEditOptions) {
                        if (this.isFlotte) {

                            this.initialiserDatesFlotte();
                        }
                        else {
                            this.initialiserDates();
                        }
                    }
                    if (this.simulationActif.assures[this.active].garanties != null) {
                        this.checked = this.simulationActif.assures[this.active].garanties;
                        this.verifierGeneralCocher()

                    }
                    //console.log("ddd") ;
                    // this.recupererNumeroSessionPivot() ;
                    //console.log(this.numeroPivot)
                    //console.log(this.listeSimFlotte)
                    this.initialiseChecked();
                    ////console.log("this.checked en Session") ;
                    ////console.log(this.checked) ;



                }

                break;
            case typeRoutineNewContrat:
                //alert('routine new contrat')
                //Pour charger la liste des garanties par rapport à la branche et sous branche(usage)
                this.getAllGarantiesByUsageAndBranche();
            case typeRoutineRenewContrat: {
                console.log("nex")
                this.contratActif = JSON.parse(window.sessionStorage.getItem(contratActifenSession));
                this.operationActif = JSON.parse(window.sessionStorage.getItem(operationActifenSession));
                this.objetActif = JSON.parse(window.sessionStorage.getItem(objetActifenSession));
                if (this.contratActif == null) {
                    this.navCtrl.navigateRoot(LIENASSURINFOVE);
                } else {
                    this.isEditOptions = JSON.parse(window.sessionStorage.getItem(editOptions));
                    if (this.operationActif != null) {
                        if (this.isFlotte) {
                            this.initialiserDatesFlotte();
                        }
                        //this.operationActif.garanties != null
                        if (this.operationActif.details != null) {
                            let renouvellement = JSON.parse(sessionStorage.getItem(startRenouvellement))
                            if (renouvellement) {
                                this.operationActif.details.forEach(d => {
                                    //this.checked.push(garantie.typeGarantie);
                                    this.checked.push(d.garantie.typegarantie.typegarantie);

                                });
                                sessionStorage.setItem(startRenouvellement, JSON.stringify(false));
                            }
                            else
                                //this.checked = this.operationActif.garanties;
                                this.operationActif.details.forEach(d => {
                                    //this.checked.push(garantie.typeGarantie);
                                    this.checked.push(d.garantie);

                                });
                            //this.checked = this.operationActif.details;
                            this.verifierGeneralCocher();
                        }
                        else {
                            this.checked = [];
                            this.garanties.forEach(element => {
                                if (element.obligatoire)
                                    this.checked.push(element);
                            });
                        }
                        this.initialiseChecked();
                    }
                    else {
                        this.operationActif = new Operation();
                        this.recupererDateClotureSurObjet(this.objetActif, contRoutine, initParam);
                        //this.operationActif.garanties = [];
                        this.operationActif.details = [];
                        this.checked = [];
                        this.garanties.forEach(element => {
                            if (element.obligatoire)
                                this.checked.push(element);
                        });
                    }
                }
                break;
            }

            default: this.effacerDonnees();

        }

    }

    async nextEtape() {
        //dispatching
        this.typeOrigineRoutine = JSON.parse(window.sessionStorage.getItem(typeRoutine));
        switch (this.typeOrigineRoutine) {
            case typeRoutineSimulation:
                const loader = await this.loadingCtrl.create({
                    message: MessageChargementEnCours,
                });
                let date = this.utilsService.getDate().subscribe(
                    dateFormatMobile => {
                        this.simulationActif.dateSimulation = new Date(dateFormatMobile.date)
                        loader.dismiss();
                        this.misAjourObligatoire();
                        this.simulationActif.assures[this.active].garanties = this.checked;
                        this.simulationActif.assures[this.active].dateEffet = this.dateDebut;

                        this.simulationActif.assures[this.active].dateEffet.setHours(0, 0, 0);

                        this.simulationActif.assures[this.active].dateCloture = this.dateFin;
                        this.simulationActif.assures[this.active].dateCloture.setHours(23, 59, 59);

                        ////console.log(this.simulationActif) ;
                        ////console.log(this.simulationActif.dateDebut.getHours()+" "+this.simulationActif.dateDebut.getMinutes()+" "+this.simulationActif.dateDebut.getSeconds())
                        let listeSimulation = JSON.parse(window.sessionStorage.getItem(ListeSimulationEnSession));
                        if (listeSimulation == null)
                            listeSimulation = new Array<AssureSimulation>();
                        if (this.simulationActif.numeroDansSession == null) {
                            this.simulationActif.numeroDansSession = listeSimulation.length + 1;

                            listeSimulation = [];
                            ////console.log(listeSimulation)
                            sessionStorage.setItem(ListeSimulationEnSession, JSON.stringify(listeSimulation));
                        }
                        else {
                            ////console.log("replacer")
                            this.remplacerSimulation(listeSimulation);
                        }

                        //console.log("liste simulation ")
                        //console.log(listeSimulation)

                        // this.listeSimFlotte = JSON.parse(window.sessionStorage.getItem(listeSimulationDansFlotte))
                        this.listeSimFlotte = this.simulationActif.assures;
                        if (this.listeSimFlotte.length == 1) {
                            console.log("numéro dans flotte", 1, this.active);
                            this.simulationActif.assures[this.active].numeroDansFlotte = 1
                            // window.sessionStorage.setItem(simulationActifenSession, JSON.stringify(this.simulationActif))
                            // this.listeSimFlotte = this.simulationActif.assures
                            // sessionStorage.setItem(listeSimulationDansFlotte, JSON.stringify(this.listeSimFlotte));
                        }
                        else {
                            console.log("active dans numero dans flotte", this.active);

                            if (this.simulationActif.assures[this.active].numeroDansFlotte == null) {
                                ////console.log("numero null")
                                this.incrementerNumeroFlotte().then(

                                    num => {
                                        console.log("numéro dans flotte", num);
                                        this.simulationActif.assures[this.active].numeroDansFlotte = num
                                        // window.sessionStorage.setItem(simulationActifenSession, JSON.stringify(this.simulationActif))
                                        // this.listeSimFlotte = this.simulationActif.assures
                                        // sessionStorage.setItem(listeSimulationDansFlotte, JSON.stringify(this.listeSimFlotte));
                                    })
                            }
                            else {
                                console.log("numero non null", this.active, this.listeSimFlotte)
                                this.remplacerSimulationFlotte(this.listeSimFlotte);
                            }
                        }



                        ////console.log(this.listeSimFlotte) ;
                        ////console.log(this.simulationActif) ;

                        if (this.ifAddNewVehicule) {
                            this.debutFlotte();
                        }
                        else {
                            //console.log("liste flotte ")
                            //console.log(this.listeSimFlotte)

                            //console.log("simulation Actif")
                            //console.log(this.simulationActif)
                            console.log("after")
                            console.log(this.listeSimFlotte)
                            // let contrat = new Contrat2();
                            // let assureContrat = new AssureContrat();
                            // assureContrat.assure = this.simulationActif.assures[this.active].assure;
                            // assureContrat.dateEffet = contrat.dateEffet = this.simulationActif.assures[this.active].dateEffet;
                            // assureContrat.dateCloture = contrat.dateCloture = this.simulationActif.assures[this.active].dateCloture;
                            // console.log("assureContrat", assureContrat);
                            // if (!contrat.objetAssures)
                            //     contrat.objetAssures = []
                            // contrat.objetAssures.push(assureContrat)
                            // console.log("contrat", contrat);
                            // sessionStorage.setItem(CONTRATSESSION, JSON.stringify(contrat))
                            this.calculerSimulation().then((data: SimulationClient) => {
                                console.log("simulation client reçue", data);
                                if (data.contrats.length >= 1) {
                                    this.simulationActif.contrats = data.contrats
                                    window.sessionStorage.setItem(simulationActifenSession, JSON.stringify(this.simulationActif))
                                    this.listeSimFlotte = this.simulationActif.assures
                                    sessionStorage.setItem(simulationActifenSession, JSON.stringify(this.simulationActif))
                                    this.navCtrl.navigateRoot(LIENSIMBRANAUTO);
                                } else {
                                    this.alertLocation('Pas de Résultat', "0 résultat pour cette recherche !");
                                }
                            })
                        }


                    },
                    error => {
                        loader.dismiss();
                        console.log("1")
                        let message = "Oups le serveur est introuvable verfiez votre connexion et recommencez svp";
                        this.utilsService.alertNotification(message);


                        sessionStorage.setItem(loginToPage, JSON.stringify(LIENCHOICEOPTION))
                        this.navCtrl.navigateRoot(LIENLOGIN);
                    }
                );

                break;
            case typeRoutineNewContrat:
                alert('Fonction nextEtape Routine New contrat');
                let typeObjet = window.sessionStorage.getItem('TypeObjet');
                let operation = new Operation();
                let assure = new AssureContrat();
                this.misAjourObligatoire();
                console.log("apres mis a jour")
                console.log(this.checked);
                //ici on affecte un tableau de type  garantie  à garantie pour uniformiser avec les autres type de routine
                //operation.garanties = this.checked;
                assure.garanties = this.checked;
                assure.dateEffet = this.dateDebut;
                assure.dateCloture = this.dateFin;
                operation.dateEffet = this.dateDebut;
                operation.dateCloture = this.dateFin;
                this.operationActif = operation;
                this.assureContratActif = assure;
                if (typeObjet === 'Existant') {
                    this.assureContratActif.assure = this.objetActif;
                } else {
                    this.assureContratActif.assure = new ObjetAssures2();
                }

                console.log(this.operationActif);
                console.log(this.contratActif);
                console.log("objetActif", this.objetActif);
                console.log("assureContratActif", this.objetActif);
                console.log("TypeObjet", typeObjet);
                window.sessionStorage.setItem(contratActifenSession, JSON.stringify(this.contratActif));
                window.sessionStorage.setItem(operationActifenSession, JSON.stringify(this.operationActif));
                window.sessionStorage.setItem(assureContratActifenSession, JSON.stringify(this.assureContratActif))
                let routineContrat = new RoutineContrat();
                routineContrat.contrat = this.contratActif
                routineContrat.objet = this.objetActif;
                routineContrat.operation = this.operationActif
                routineContrat.assureContrat = this.assureContratActif;
                console.log("routineContrat", routineContrat);
                this.remplacerRoutineContrat(routineContrat).then(
                    () => {
                        this.navCtrl.navigateForward(LIENSIMBRANAUTO);
                    }
                );
                /**
                 * *Ici on vérifie d'abord si c'est un objet déjà
                 * *existant en base pour pouvoir controller
                 * *si il a des contrats dont la date effet et
                 * *cloture des opérations ne chevauche pas
                 * *avec les dates choisis
                 * !Sinon si c'est un nouveau objet on
                 * !a aura pas besoin de verifier car
                 * !il n'a pas encore de contrat
                 */
                // if(typeObjet === 'Existant'){
                //     this.checkIfObjetHasContrat(this.objetActif.id).then((Assurecontrats) => {
                //         console.log('Assurecontrats', Assurecontrats);
                //         if(Assurecontrats.length >= 1){
                //             let count = 0;
                //             let tailleTab = Assurecontrats.length;
                //             Assurecontrats.forEach((assurecontrat) => {
                //                 this.operationService.getOperationByContrat(assurecontrat.contrat.id)
                //                     .subscribe((operations) => {
                //                         let resultat = null;
                //                         console.log("operations",operations);
                //                         if (operations.length >= 1) {
                //                             let countOp = 0;
                //                             operations.forEach((op) => {
                //                                 let operationDateDebut = new Date(op.dateEffet).toLocaleDateString();
                //                                 let operationDateFin = new Date(op.dateCloture).toLocaleDateString();
                //                                 let choosenDateEffet = new Date(this.operationActif.dateEffet).toLocaleDateString();
                //                                 let choosenDateCloture = new Date(this.operationActif.dateCloture).toLocaleDateString();
                //                                 console.log("choosenDateEffet", choosenDateEffet);
                //                                 console.log("choosenDateCloture", choosenDateCloture);
                //                                 console.log("operationDateDebut", operationDateDebut);
                //                                 console.log("operationDateFin", operationDateFin);
                //                                 resultat = this.CheckIfObjetContratHasOverlap(op, this.operationActif);
                //                                 console.log("resultat", resultat);
                //                                 if (resultat) {
                //                                     this.utilsService.alertNotification(`Vous avez un contrat en cours pour cet objet pour la période du ${operationDateDebut} au ${operationDateFin} à 23h:59`);
                //                                     //break;
                //                                 }else{
                //                                     window.sessionStorage.setItem(contratActifenSession, JSON.stringify(this.contratActif))
                //                                     window.sessionStorage.setItem(operationActifenSession, JSON.stringify(this.operationActif))
                //                                     let routineContrat = new RoutineContrat();
                //                                     routineContrat.contrat = this.contratActif
                //                                     routineContrat.objet = this.objetActif;
                //                                     routineContrat.operation = this.operationActif
                //                                     console.log("routineContrat", routineContrat);

                //                                     this.remplacerRoutineContrat(routineContrat).then(
                //                                         () => {
                //                                             this.navCtrl.navigateForward(LIENSIMBRANAUTO);
                //                                         }
                //                                     );
                //                                 }


                //                             });
                //                         }

                //                     });
                //                     // count += 1;
                //                     // if (count == tailleTab){
                //                     //     console.log("Dernier élément")
                //                     // }
                //                 });

                //                 // if(count === contrats.length){
                //                 //     if(hasOneContratValid){
                //                 //         this.utilsService.alertNotification("Cet objet ")
                //                 //     }
                //                 // }

                //         }else{
                //             console.log("Cette objet n'a aucun contrat");
                //             window.sessionStorage.setItem(contratActifenSession, JSON.stringify(this.contratActif))
                //             window.sessionStorage.setItem(operationActifenSession, JSON.stringify(this.operationActif))
                //             let routineContrat = new RoutineContrat();
                //             routineContrat.contrat = this.contratActif
                //             routineContrat.objet = this.objetActif;
                //             routineContrat.operation = this.operationActif
                //             console.log("routineContrat", routineContrat);
                //             this.remplacerRoutineContrat(routineContrat).then(
                //                 () => {
                //                     this.navCtrl.navigateForward(LIENSIMBRANAUTO);
                //                 }
                //             );
                //         }
                //     });
                // }else{
                //     window.sessionStorage.setItem(contratActifenSession, JSON.stringify(this.contratActif))
                //     window.sessionStorage.setItem(operationActifenSession, JSON.stringify(this.operationActif))
                //     let routineContrat = new RoutineContrat();
                //     routineContrat.contrat = this.contratActif
                //     routineContrat.objet = this.objetActif;
                //     routineContrat.operation = this.operationActif
                //     console.log("routineContrat", routineContrat);
                //     this.remplacerRoutineContrat(routineContrat).then(
                //         () => {
                //             this.navCtrl.navigateForward(LIENSIMBRANAUTO);
                //         }
                //     );
                // }
                break;
            case typeRoutineRenewContrat:
                {

                    let operation = new Operation();
                    ////console.log("avant mis a jour")
                    ////console.log(this.checked)
                    this.misAjourObligatoire();
                    ////console.log("apres mis a jour")
                    ////console.log(this.checked)
                    //ici on affecte un tableau de type  garantie  à garantie pour uniformiser avec les autres type de routine
                    //operation.garanties = this.checked;
                    operation.details = this.checked;
                    operation.dateEffet = this.dateDebut;
                    operation.dateCloture = this.dateFin;
                    this.operationActif = operation;
                    ////console.log(this.operationActif) ;
                    ////console.log(this.contratActif) ;
                    ////console.log(JSON.parse(window.sessionStorage.getItem(objetActifenSession))) ;

                    window.sessionStorage.setItem(contratActifenSession, JSON.stringify(this.contratActif))
                    window.sessionStorage.setItem(operationActifenSession, JSON.stringify(this.operationActif))
                    let routineContrat = new RoutineContrat();
                    routineContrat.contrat = this.contratActif
                    routineContrat.objet = this.objetActif;
                    routineContrat.operation = this.operationActif


                    this.remplacerRoutineContrat(routineContrat).then(
                        () => {
                            if (this.typeOrigineRoutine == typeRoutineNewContrat)
                                this.navCtrl.navigateForward(LIENSIMBRANAUTO);
                            if (this.typeOrigineRoutine == typeRoutineRenewContrat)
                                this.navCtrl.navigateForward(LIENASSURINFOVEVEHICULEFLOTTE)

                        }
                    )



                    break;
                }

            default: this.effacerDonnees();

        }

        //fin dispatching




    }

    /**
     * *Cette fonction permet de vérifier si un
     * *objet existant à un contrat dont les opérations
     * *date Effet et de cloture ne chevauche
     * *pas avec les dates choisies au niveau
     * *de la page du choix des garanties
     */
    CheckIfObjetContratHasOverlap(operationContrat: Operation, ChoosenOperation: Operation): Boolean {
        let hasOverlap: Boolean = false;
        if (new Date(operationContrat.dateEffet).getTime() <= new Date(ChoosenOperation.dateCloture).getTime()
            && new Date(operationContrat.dateCloture).getTime() >= new Date(ChoosenOperation.dateEffet).getTime()) {
            return hasOverlap = true;
        }
        return hasOverlap;
    }



    /**
     * *Cette fonction permet de verifier
     * *si un objet a des contrats
     * *en retournant un tableau
     * *d'assureContrat
     * @param objetId
     */
    checkIfObjetHasContrat(objetId) {
        return new Promise<AssureContrat[]>((resolve, reject) => {
            this.assurecontratService
                .getContratByObjetAssure(objetId)
                .subscribe((data: AssureContrat[]) => {

                    resolve(data);
                },
                    (error) => {
                        resolve(error);
                    });
        });

    }
    effacerDonnees() {
        this.typeOrigineRoutine = JSON.parse(window.sessionStorage.getItem(typeRoutine));
        switch (this.typeOrigineRoutine) {
            case typeRoutineSimulation:

                switch (this.simulationActif.origineRoutine) {
                    case OriginePageGesSim: this.navCtrl.navigateRoot(LIENSIMDECLAOBJET); break;
                    case OriginePageGesSimReseau: this.navCtrl.navigateRoot(LIENECRANPAGESIMULATION); break;
                    case OrigineClientNonLogue: this.navCtrl.navigateRoot(LIENSIMDECLAOBJET); break;
                    case OriginePageMonObjet: this.navCtrl.navigateRoot(LIENMONOBJET); break;
                    default: this.navCtrl.navigateRoot(LIENSIMDECLAOBJET);
                }
                this.simulationActif.assures[this.active].garanties = [];
                window.sessionStorage.setItem(simulationActifenSession, JSON.stringify(this.simulationActif))


                break;
            case typeRoutineNewContrat:
                this.navCtrl.navigateRoot("/assurinfove")
                //this.operationActif.garanties = [];
                this.operationActif.details = [];
                window.sessionStorage.setItem(operationActifenSession, JSON.stringify(this.operationActif));

                break;



        }



    }
    remplacerSimulation(listeSimulation) {
        ////console.log("remplacer simulation")
        ////console.log(listeSimulation)


        for (let index = 0; index < listeSimulation.length; index++) {


            if (listeSimulation[index].numeroDansSession == this.simulationActif.numeroDansSession) {
                ////console.log("egal")
                //simulation=this.simulationActif ;
                listeSimulation[index] = this.simulationActif;
                sessionStorage.setItem(ListeSimulationEnSession, JSON.stringify(listeSimulation));
                ////console.log(listeSimulation)

            }


        };


    }
    remplacerSimulationFlotte(listeSimFlotte: AssureSimulation[]) {
        console.log("remplacerSimulationFlotte()", listeSimFlotte)
        let numeroDansFlotte = this.simulationActif.assures[this.active].numeroDansFlotte
        for (let index = 0; index < listeSimFlotte.length; index++) {

            if (listeSimFlotte[index].numeroDansFlotte == numeroDansFlotte) {
                //simulation=this.simulationActif ;
                if (numeroDansFlotte == this.numeroPivot) {
                    listeSimFlotte.forEach(simulation => {
                        if (simulation.numeroDansFlotte == numeroDansFlotte)
                            simulation.dateCloture = new Date(this.simulationActif.assures[this.active].dateCloture);

                    });
                }
                listeSimFlotte[index] = this.simulationActif.assures[this.active]
                sessionStorage.setItem(listeSimulationDansFlotte, JSON.stringify(listeSimFlotte));
            }


        };


    }
    misAjourObligatoire() {
        this.checked.forEach(checkbox => {
            if (checkbox.general) {
                ;
                this.checked.forEach(garantie => {
                    if (garantie.obligatoire) {
                        this.obligatoireChecked = false;
                    }
                    let index = this.removeCheckedFromArray(garantie);
                    ////console.log("index "+index)
                    if (index >= 0) !
                        this.checked.splice(index, 1);
                    ////console.log(this.checked) ;
                });
            }
        });
        this.verifierDoublon();
    }
    misAjourBegin() {
        this.checked.forEach(checkbox => {

            if (checkbox.general) {
                this.obligatoireChecked = false;
                return;
            }

        });

    }
    testGarantieChecked(garantie) {

        this.checked.forEach(gar => {
            ////console.log(gar.id+" "+garantie.id)
            if (gar.id == garantie.id)
                return true

        });
        return false

    }
    async recupererDateClotureSurObjet(objet, type, mode) {
        //recupere la liste des contrats delobjet
        // let contrats = this.utilsService.filtrerContrat(objet.contrats)
        let contrats = objet.contrats;
        let dat = new Date();
        if (contrats == null) {
            const loader = await this.loadingCtrl.create({
                message: MessageChargementEnCours,
            });
            let date = this.utilsService.getDate().subscribe(
                dateFormatMobile => {
                    loader.dismiss();
                    if (mode == initParam) {

                        if (this.operationActif.dateEffet != null) {
                            this.dateDebut = this.operationActif.dateEffet;
                            this.dateFin = this.operationActif.dateCloture;


                        }
                        else {
                            this.dateDebut = new Date(dateFormatMobile.date);
                            this.dateDebut.setDate(this.dateDebut.getDate() + 1);
                            this.dateFin = this.dateDebut;

                            this.inputFin.value = this.utilsService.displayDateFormatInput(this.dateFin);
                            this.inputDebut.value = this.utilsService.displayDateFormatInput(this.dateDebut);


                        }

                    }


                },
                error => {
                    loader.dismiss();
                    console.log("2")
                    let message = "Oups le serveur est introuvable verfiez votre connexion et recommencez  svp";
                    this.utilsService.alertNotification(message);
                    sessionStorage.setItem(loginToPage, JSON.stringify(LIENCHOICEOPTION))
                    this.navCtrl.navigateRoot(LIENLOGIN);
                }
            );


        }
        else {
            contrats.forEach(contrat => {
                //boucle sur operations

                contrat.operations.forEach(operation => {
                    if (this.utilsService.testOperationActif(operation)) {
                        operation.dateCloture = new Date(operation.dateCloture);
                        if (this.isInferior(dat, operation.dateCloture)) {
                            dat = new Date(operation.dateCloture);

                        }

                    }
                });


                //fin boucle sur operation



            });


            switch (type) {
                case simRoutine:
                    if (dat != null) {
                        if (mode == initParam) {
                            // this.simulationActif.dateDebut= new Date(dat)   ;
                            // this.simulationActif.dateDebut.setDate(this.simulationActif.dateDebut.getDate() +1) ;
                            // this.simulationActif.dateFin  =this.simulationActif.dateDebut;
                            if (this.simulationActif.assures[this.active].dateEffet != null) {
                                this.dateDebut = this.simulationActif.assures[this.active].dateEffet;
                                this.dateFin = this.simulationActif.assures[this.active].dateCloture;
                                this.inputFin.value = this.utilsService.displayDateFormatInput(this.dateFin);
                                this.inputDebut.value = this.utilsService.displayDateFormatInput(this.dateDebut);

                            }
                            else {
                                this.dateDebut = new Date(dat);
                                this.dateDebut.setDate(this.dateDebut.getDate() + 1);
                                this.dateFin = this.dateDebut;

                                this.inputFin.value = this.utilsService.displayDateFormatInput(this.dateFin);
                                this.inputDebut.value = this.utilsService.displayDateFormatInput(this.dateDebut);

                            }

                        }

                    }
                    else {
                        const loader = await this.loadingCtrl.create({
                            message: MessageChargementEnCours,
                        });
                        let date = this.utilsService.getDate().subscribe(
                            dateFormatMobile => {
                                loader.dismiss();
                                if (mode == initParam) {
                                    if (this.simulationActif.assures[this.active].dateEffet != null) {
                                        this.dateDebut = this.simulationActif.assures[this.active].dateEffet;
                                        this.dateFin = this.simulationActif.assures[this.active].dateCloture;
                                        this.inputFin.value = this.utilsService.displayDateFormatInput(this.dateFin);
                                        this.inputDebut.value = this.utilsService.displayDateFormatInput(this.dateDebut);


                                    }
                                    else {
                                        this.dateDebut = new Date(dateFormatMobile.date);
                                        this.dateDebut.setDate(this.dateDebut.getDate() + 1);
                                        this.dateFin = this.dateDebut;

                                        this.inputFin.value = this.utilsService.displayDateFormatInput(this.dateFin);
                                        this.inputDebut.value = this.utilsService.displayDateFormatInput(this.dateDebut);

                                    }

                                }

                            },
                            error => {
                                loader.dismiss()
                                console.log("3")
                                let message = "Oups le serveur est introuvable verfiez votre connexion et recommencez svp";
                                this.utilsService.alertNotification(message);
                                if (this.simulationActif.origineRoutine == OrigineClientNonLogue)
                                    this.navCtrl.navigateRoot(this.lienSimDeclaAuto)
                                else {
                                    sessionStorage.setItem(loginToPage, JSON.stringify(LIENCHOICEOPTION))
                                    this.navCtrl.navigateRoot(LIENLOGIN);
                                }
                            }
                        );

                    }

                    break;
                case contRoutine:
                    if (dat != null) {
                        if (mode == initParam) {
                            // this.operationActif.dateEffet= new Date(dat)   ;
                            // this.operationActif.dateEffet.setDate(this.operationActif.dateEffet.getDate() +1) ;
                            // this.operationActif.dateCloture  =this.operationActif.dateEffet ;
                            this.dateDebut = new Date(dat);
                            this.dateDebut.setDate(this.dateDebut.getDate() + 1);
                            this.dateFin = this.dateDebut;
                            this.inputFin.value = this.utilsService.displayDateFormatInput(this.dateFin);
                            this.inputDebut.value = this.utilsService.displayDateFormatInput(this.dateDebut);

                        }

                    }
                    else {
                        const loader = await this.loadingCtrl.create({
                            message: MessageChargementEnCours,
                        });
                        let date = this.utilsService.getDate().subscribe(
                            dateFormatMobile => {
                                loader.dismiss();
                                if (mode == initParam) {
                                    this.dateDebut = new Date(dateFormatMobile.date);
                                    this.dateDebut.setDate(this.dateDebut.getDate() + 1);
                                    this.dateFin = this.dateDebut;
                                    this.inputFin.value = this.utilsService.displayDateFormatInput(this.dateFin);
                                    this.inputDebut.value = this.utilsService.displayDateFormatInput(this.dateDebut);


                                }


                            },
                            error => {
                                loader.dismiss();
                                console.log("4")
                                let message = "Oups le serveur est introuvable verfiez votre connexion et recommencez svp";
                                this.utilsService.alertNotification(message);
                                sessionStorage.setItem(loginToPage, JSON.stringify(LIENCHOICEOPTION))
                                this.navCtrl.navigateRoot(LIENLOGIN);
                            }
                        );

                    }

                    break;

            }


        }


    }

    verifierDoublon() {
        this.checked.forEach(element => {
            if (element.obligatoire) {
                let index = this.removeCheckedFromArray(element);
                for (let i = 0; i < this.checked.length; i++) {
                    if (i != index) {
                        if (this.checked[i].id == this.checked[index].id)
                            this.checked.splice(i, 1);
                    }

                }
            }


        });
    }
    async trouverDateMinimum(objet) {
        ////console.log("debutFonction")
        //recupere la liste des contrats delobjet
        // let contrats = this.utilsService.filtrerContrat(objet.contrats)
        let contrats = objet.contrats;
        const loader = await this.loadingCtrl.create({
            message: MessageChargementEnCours,
        });
        let date = this.utilsService.getDate().subscribe(
            dateFormatMobile => {

                loader.dismiss();
                let dat = new Date(dateFormatMobile.date);
                dat.setDate(dat.getDate() + 1)

                this.dateDuLendemain = new Date(dat);

                //
                if (contrats == null || contrats.length == 0) {


                    this.dateDebutMinimum = dat;
                    this.dateFinMaximumBeforeOther = null;
                    //  console.log("date minimum"+this.utilsService.displayDate(this.dateFinMaximumBeforeOther))
                    this.verifierDate();
                    return;
                    ////console.log(this.dateDebutMinimum) ;






                }
                else {
                    console.log(contrats[this.active])
                    // contrats[this.active].operations[this.active].dateEffet = new Date(contrats[this.active].operations[this.active].dateEffet)
                    // let datBeforeOther =  new Date(contrats[this.active].operations[this.active].dateEffet) ;
                    let datBeforeOther = new Date(8640000000000000);
                    console.log("date minimum" + this.utilsService.displayDate(datBeforeOther))


                    contrats.forEach(contrat => {
                        //boucle sur operation

                        contrat.operations.forEach(operation => {
                            if (this.utilsService.testOperationActif(operation)) {
                                operation.dateCloture = new Date(operation.dateCloture);
                                if (this.isInferior(dat, operation.dateCloture)) {
                                    if (operation.dateCloture != null && this.isInferior(dat, operation.dateCloture)) {
                                        dat = new Date(operation.dateCloture);

                                    }
                                    operation.dateEffet = new Date(operation.dateEffet)
                                    if (operation.dateEffet != null && this.isInferior(operation.dateEffet, datBeforeOther)) {
                                        datBeforeOther = new Date(operation.dateEffet);
                                        console.log("date minimum" + this.utilsService.displayDate(datBeforeOther))

                                    }


                                }

                            }


                        });

                        //finboucle sur operation



                    });



                    if (dat != null) {

                        this.dateDebutMinimum = new Date(dat);
                        this.dateDebutMinimum.setDate(this.dateDebutMinimum.getDate() + 1)



                    }
                    if (datBeforeOther != null) {
                        this.dateFinMaximumBeforeOther = new Date(datBeforeOther);
                        this.dateFinMaximumBeforeOther.setDate(this.dateFinMaximumBeforeOther.getDate() - 1)
                        console.log("date minimum" + this.utilsService.displayDate(this.dateFinMaximumBeforeOther))
                    }
                    this.verifierDate();

                }
            }
        );
    }
    cocherGeneral() {
        // this.checked = [] ;
        this.garanties.forEach(garantie => {
            //////console.log(garantie.nom)
            if (garantie.general) {
                this.checked.push(garantie);

            }

        });
        ////console.log(this.checked)
    }
    verifierGeneralCocher() {
        this.checked.forEach(garantie => {
            if (garantie.general) {
                this.cocherGeneral()
                this.toutchecked = true;


            }

        });
    }
    async finOuDuree() {
        if (this.definitionByDuree) {
            this.definitionByDuree = false;
            this.definitionByDate = true;
            if (this.checkDuree()) {
                let debut = new Date(this.onDeclarationForm.get('debut').value)
                this.dateFin = debut;;
                this.dateFin.setDate(debut.getDate() + this.dureeJour - 1);
                this.dateFin.setMonth(debut.getMonth() + this.dureeMois);
                this.dateFin.setFullYear(debut.getFullYear() + this.dureeAnnee);
                ;

                // await this.utilsService.delay(500)
                this.inputFin.value = this.utilsService.displayDateFormatInput(this.dateFin);
            }

        }
        else {
            this.definitionByDuree = true;
            this.definitionByDate = false;
            if (!this.onDeclarationForm.get('debut').hasError('required') && !this.onDeclarationForm.get('fin').hasError('required')) {
                this.dureeJour = this.diffdate(this.onDeclarationForm.get('fin').value, this.onDeclarationForm.get('debut').value, "jour")
                let dat = new Date(this.dureeJour * 24 * 60 * 60 * 1000)
                let datRef = new Date(null);
                this.dureeJour = dat.getDate() - datRef.getDate();
                this.dureeMois = dat.getMonth() - datRef.getMonth();
                this.dureeAnnee = dat.getFullYear() - datRef.getFullYear();
                ////console.log(this.diffdate(this.onDeclarationForm.get('fin').value,this.onDeclarationForm.get('debut').value,"jour"))
            }

        }
    }

    checkDuree(): Boolean {
        this.dureeJour = Number(this.inputJours.value);
        this.dureeMois = Number(this.inputMois.value);
        this.dureeAnnee = Number(this.inputAnnees.value);
        if (this.dureeJour == null && this.dureeMois == null && this.dureeAnnee == null) {
            this.utilsService.alertNotification("veuillez indiquer une durée")
            return false;
        }
        ////console.log(isNaN(this.dureeJour))
        if (isNaN(this.dureeJour) || this.dureeJour < 0) {
            this.utilsService.alertNotification("veuillez indiquer un nombre de jour correct")
            return false;

        }
        if (isNaN(this.dureeMois) || this.dureeMois < 0) {
            this.utilsService.alertNotification("veuillez indiquer un mobre de mois correct")
            return false;

        }
        if (isNaN(this.dureeAnnee) || this.dureeAnnee < 0) {
            this.utilsService.alertNotification("veuillez indiquer un mobre d'années correctes")
            return false;

        }
        if (this.dureeJour == 0 && this.dureeMois == 0 && this.dureeAnnee == 0) {
            this.utilsService.alertNotification("veuillez indiquer une durée d'au mininum 1 jour")
            return false;
        }

        return true;


    }
    checkDureeFirstOption(input = ""): Boolean {
        if (this.inputDuree != null)
            this.duree = Number(this.inputDuree.value);
        console.log(input)
        if (this.duree == null && input == "duree") {
            this.loadingCtrl.dismiss();
            this.utilsService.alertNotification("veuillez indiquer une durée")
            return false;
        }

        if (isNaN(this.duree) || this.duree < 0) {
            console.log("isNanDuree")
            this.loadingCtrl.dismiss();
            this.utilsService.alertNotification("veuillez indiquer une durée correcte")
            return false;

        }

        if (this.duree == 0) {
            this.loadingCtrl.dismiss();
            this.utilsService.alertNotification("veuillez indiquer une durée d'au mininum 1 jour")
            return false;
        }

        return true;


    }
    async finOuDureeFirstOption(input) {

        if (this.checkDureeFirstOption(input)) {

            this.dateFin = new Date(this.inputDebut.value);;
            ////console.log(this.dateFin)
            ////console.log(this.inputDuree.value)
            if (this.inputDuree.value == 15)
                this.dateFin.setDate(this.dateFin.getDate() + 15);
            else {
                ////console.log(this.dateFin)
                ////console.log(this.inputDuree.value)
                this.dateFin.setMonth(this.dateFin.getMonth() + Number(this.inputDuree.value));
                ////console.log(this.dateFin)

            }


            ;
            ////console.log(this.dateFin)

            // await this.utilsService.delay(500)
            this.inputFin.value = this.utilsService.displayDateFormatInput(this.dateFin);
        }


    }
    async validerEtape() {
        const loader = await this.loadingCtrl.create({
            message: MessageChargementEnCours,
        });
        //this.isFlotte = false ;
        this.ifAddNewVehicule = false;
        console.log(this.isEditOptions)
        if (this.isEditOptions) {
            this.validerChangementOptions();
        }
        else
            this.checkBeforeValidationFirstOption()
    }
    /**
     * cette fonction nous permet de tester si la duree est correcte pour calcuer la date de fin
     */
    async checkBeforeValidationFirstOption() {
        console.log("checkBefore")
        console.log(this.utilsService.displayDate(this.operationActif.dateEffet))
        console.log(this.dateFinReference)
        if (this.dateFinReference == null) {
            if (this.checkDureeFirstOption()) {

                this.valider()
            }
            else {

            }
        }
        else
            this.valider();


    }
    ajustInputSecondeSession(input) {
        if (!this.firstChangeDuree) {
            ////console.log(this.inputFin.value)
            this.dateFinError = false;
            this.finOuDureeFirstOption(input);
        }
        else
            this.firstChangeDuree = false

    }


    addVehicule() {

        // //pour tester une date fe fin inferieur à 15 jours test
        // this.simulationActif.dateFin=new Date(this.simulationActif.dateFin)
        // this.simulationActif.dateFin.setDate(this.simulationActif.dateFin.getDate()-1)
        // //fin test
        this.ifAddNewVehicule = true;
        ////console.log(this.inputDebut.value) ;
        ////console.log(this.inputFin.value)
        let dureeJour = this.utilsService.diffdate(this.inputFin.value, this.inputDebut.value, "jour")
        let dat = new Date(dureeJour * 24 * 60 * 60 * 1000)
        let datRef = new Date(null);
        dureeJour = dat.getDate() - datRef.getDate() - 1;
        let dureeMois = dat.getMonth() - datRef.getMonth();

        let dureeAnnee = dat.getFullYear() - datRef.getFullYear();

        if (dureeMois == 0 && dureeAnnee == 0 && dureeJour < 15) {
            this.utilsService.alertNotification("Vous ne pouvez pas ajouter un nouveau véhicule il reste moins de 15 jours avant la fin du contrat")

        }
        else {

            ///


            ///
            this.dateDebut = this.inputDebut.value;
            this.dateFin = this.inputFin.value;
            this.isFlotte = true;

            this.checkBeforeValidationFirstOption();






        }

    }
    debutFlotte() {
        console.log("ListeSimFlotte", this.listeSimFlotte);


        sessionStorage.setItem(flotte, JSON.stringify(this.isFlotte));
        // this.simulationActif.dateDebut = null ;
        let sim: SimulationClient = new SimulationClient();


        //reference : string;

        sim.client = this.simulationActif.client
        sim.dateSimulation = new Date(this.simulationActif.dateSimulation)
        //relations à ajoutéé


        //type de garanties pour uniformiser dans le choice option

        sim.assures[this.active].dateEffet = new Date(this.utilsService.displayDateFormatInitialise(this.simulationActif.assures[this.active].dateEffet))
        // ////console.log(this.utils.displayDate(this.simulationActif.dateDebut)) ;
        ////console.log(this.utilsService.displayDate(sim.dateDebut)) ;
        sim.assures[this.active].dateCloture = new Date(this.simulationActif.assures[this.active].dateCloture)
        sim.origineRoutine = this.simulationActif.origineRoutine
        sim.numeroDansSession = this.simulationActif.numeroDansSession
        //sim.ca =this.simulationActif.ca
        sim.assures[this.active].montantTTCCompagnie = this.simulationActif.assures[this.active].montantTTCCompagnie
        sim.assures[this.active].assure = new ObjetAssures2();

        sim.assures[this.active].garanties = [];
        sessionStorage.setItem(simulationActifenSession, JSON.stringify(sim));

        this.navCtrl.navigateRoot(LIENSIMDECLAOBJET);

    }
    remplirGaranties() {
        var p1 = new Promise(function (resolve, reject) {
            resolve("Succès !");
            // ou
            // reject("Erreur !");
        });
        // this.simulationActif.garanties = [];
        // this.simulationActif.garanties.forEach(typeGarantie => {
        //     console.log("typeGarantie forEach", typeGarantie);

        //     let garantie = this.recupererGarantie(typeGarantie, this.simulationActif.ca);
        //     this.simulationActif.garanties.push(garantie);

        // });
        return p1;
    }

    recupererGarantie(typeGarantie: TypeGarantie, ca: CA): Garantie {
        //simuleer baase
        // let garantie = {
        //     id: typeGarantie.id,
        //     nom: ca.nom + " " + typeGarantie.nom,


        //     offrePromos: [],

        //     typeGarantie: {
        //         id: 1,
        //         nom: typeGarantie.nom,
        //         typeBA: null,
        //         garanties: [],
        //         obligatoire: null,
        //         general: null,
        //         resume: null,

        //         description: null,
        //     },
        //     ba: null,


        //     operations: [],
        // }
        let garantie = new Garantie();
        garantie.id = typeGarantie.id;
        garantie.libelle = ca.nom + " " + typeGarantie.libelle;
        //garantie.offrePromos = [];
        let newtypeGarantie = new TypeGarantie();
        newtypeGarantie.libelle = typeGarantie.libelle;
        garantie.typegarantie.typegarantie = newtypeGarantie;
        //garantie.ba = null;
        //garantie.operations = [];

        return garantie;
    }
    recupererNumeroSessionPivot() {

        this.numeroPivot = JSON.parse(sessionStorage.getItem(numeroPivot))



    }
    creationDeLaListe() {
        this.listeActif = [];

        this.listeActif = this.listeSimFlotte

        //console.log(this.listeActif)
        var p1 = new Promise(function (resolve, reject) {
            resolve("Succès !");
            // ou
            // reject("Erreur !");
        });
        return p1;

    }
    incrementerNumeroFlotte(): Promise<number> {

        let ancienNumeroFlotte = 0;
        this.listeSimFlotte.forEach(simulation => {
            if (simulation.numeroDansFlotte == this.simulationActif.assures[this.active].numeroDansFlotte && simulation.numeroDansFlotte > ancienNumeroFlotte)
                ancienNumeroFlotte = simulation.numeroDansFlotte
        });

        var p1 = new Promise<number>(function (resolve, reject) {
            resolve(++ancienNumeroFlotte);
            // ou
            // reject("Erreur !");
        });
        return p1;
    }


    verifierIfInFirstIntervalle(dateDebut, dateFin) {
        var p1;
        console.log(dateDebut)
        if (this.dateFinMaximumBeforeOther != null) {


            console.log("date Debut" + this.utilsService.displayDate(dateDebut))
            console.log("date Lendemain" + this.utilsService.displayDate(this.dateDuLendemain))
            console.log("date Fin" + this.utilsService.displayDate(dateFin))
            console.log("date debut minimum" + this.utilsService.displayDate(this.dateDebutMinimum))
            console.log("date fin maximumBefore other" + this.utilsService.displayDate(this.dateFinMaximumBeforeOther))
            console.log(!this.isInferior(dateDebut, this.dateDuLendemain));
            console.log(!this.isInferior(this.dateFinMaximumBeforeOther, dateFin));
            if ((!this.isInferior(dateDebut, this.dateDuLendemain) && !this.isInferior(this.dateFinMaximumBeforeOther, dateFin))) {


                console.log("in da building")
                //  this.dateFin = null ;
                //  this.dateDebut =null ;
                //  this.dateDebutError =true ;
                //  this.utilsService.alertNotification("le contrat doit finir au plus tard le le "+this.utilsService.displayDate(this.dateFinMaximumBeforeOther)) ;
                return true
            }
            else
                return false


        }
        else return false
    }

    verifierIfInSecondIntervalle() {
        console.log("not in first")
        if (this.isInferior(this.dateDebut, this.dateDebutMinimum)) {


            console.log("date fin" + this.utilsService.displayDate(this.dateFin))
            ////console.log("date debut < minimum")
            this.dateFin = null;
            this.dateDebut = null;
            this.dateDebutError = true;
            if (this.dateFinMaximumBeforeOther != null)
                this.utilsService.alertNotification(
                    "le contrat peut soit commencer apres le " + this.utilsService.displayDate(this.dateDebutMinimum) +
                    "\n ou soit debuter apres le " + this.utilsService.displayDate(this.dateDuLendemain) + " se terminer avant le" + this.utilsService.displayDate(this.dateFinMaximumBeforeOther));
            else
                this.utilsService.alertNotification("le contrat ne peut pas commencer avant le " + this.utilsService.displayDate(this.dateDebutMinimum));
            return false;
        }
        else
            return true;

    }
    /**
     * Cette fonction nous permet d'initialiser les dates de debut et de fin du contrat
     */
    async initialiserDates() {
        if (this.simulationActif.origineRoutine == OriginePageMonObjet) {

            this.recupererDateClotureSurObjet(this.simulationActif.assures[this.active].assure, simRoutine, initParam);

        }
        else {
            if (this.simulationActif.assures[this.active].dateEffet == null && this.simulationActif.assures[this.active].dateCloture == null) {
                ////console.log("ils sont nulls")
                const loader = await this.loadingCtrl.create({
                    message: MessageChargementEnCours,
                });
                let date = this.utilsService.getDate().subscribe(

                    dateFormatMobile => {
                        loader.dismiss();
                        this.dateDebut = new Date(dateFormatMobile.date);
                        this.dateDebut.setDate(this.dateDebut.getDate() + 1);
                        this.dateFin = this.dateDebut;
                        this.inputFin.value = this.utilsService.displayDateFormatInput(this.dateFin);
                        this.inputDebut.value = this.utilsService.displayDateFormatInput(this.dateDebut);



                    },
                    error => {
                        loader.dismiss();
                        console.log("5")
                        let message = "Oups le serveur est introuvable verfiez votre connexion et recommencez svp";
                        this.utilsService.alertNotification(message);
                        if (this.simulationActif.origineRoutine == OrigineClientNonLogue) {
                            this.navCtrl.navigateRoot(this.lienSimDeclaAuto)
                        }

                        else {
                            sessionStorage.setItem(loginToPage, JSON.stringify(LIENCHOICEOPTION))
                            this.navCtrl.navigateRoot(LIENLOGIN);

                        }

                    }
                )

            }
            else {
                this.dateDebut = this.simulationActif.assures[this.active].dateEffet;
                this.dateFin = this.simulationActif.assures[this.active].dateCloture;
                this.inputFin.value = this.utilsService.displayDateFormatInput(this.dateFin);
                this.inputDebut.value = this.utilsService.displayDateFormatInput(this.dateDebut);

                ////console.log(this.dateFin)
                ////console.log(this.inputFin.value)
                ///
                let dureeJour = this.diffdate(this.dateFin, this.dateDebut, "jour")
                ////console.log(this.inputFin.value)
                let dat = new Date(dureeJour * 24 * 60 * 60 * 1000)
                let datRef = new Date(null);
                dureeJour = dat.getDate() - datRef.getDate() - 1;
                let dureeMois = dat.getMonth() - datRef.getMonth();

                let dureeAnnee = dat.getFullYear() - datRef.getFullYear();
                ////console.log(dureeJour+" "+dureeMois+" "+dureeAnnee)
                if (this.inputDuree != null) {
                    if (dureeMois == 0 && dureeAnnee == 0) {
                        this.inputDuree.value = "15"

                    }

                    else
                        if (dureeAnnee == 0) {
                            this.inputDuree.value = dureeMois + "";
                            this.dureeRef = dureeMois;


                        }

                        else {
                            this.inputDuree.value = "12";
                            this.dureeRef = 12;

                        }
                    ////console.log(this.inputFin.value)

                }


                ///


            }



        }


        ///
        //

    }
    /**
     * cette fonction nous permet dinitialiser la dates de la routin selon la flotte dans
     * laquelle il se trouve
     */
    async initialiserDatesFlotte() {
        this.typeOrigineRoutine = JSON.parse(sessionStorage.getItem(typeRoutine));
        switch (this.typeOrigineRoutine) {
            case typeRoutineSimulation: this.dateFinReference = new Date(this.simulationActif.assures[this.active].dateCloture)
                break;
            case typeRoutineRenewContrat:
            case typeRoutineNewContrat: this.dateFinReference = new Date(this.operationActif.dateCloture)
                console.log(this.utilsService.displayDate(this.operationActif.dateCloture))
                break;

        }
        const loader = await this.loadingCtrl.create({
            message: MessageChargementEnCours,
        });
        let date = this.utilsService.getDate().subscribe(
            dateFormatMobile => {
                loader.dismiss();
                this.dateDebut = new Date(dateFormatMobile.date);
                this.dateDebut.setDate(this.dateDebut.getDate() + 1)
                this.dateFin = this.simulationActif.assures[this.active].dateCloture;

                let dureeJour = this.diffdate(this.dateFinReference, this.dateDebut, "jour")
                let dat = new Date(dureeJour * 24 * 60 * 60 * 1000)
                let datRef = new Date(null);
                dureeJour = dat.getDate() - datRef.getDate() - 1;
                let dureeMois = dat.getMonth() - datRef.getMonth();

                let dureeAnnee = dat.getFullYear() - datRef.getFullYear();
                ////console.log(dureeJour+" "+dureeMois+" "+dureeAnnee)
                if (this.inputDuree != null) {
                    if (dureeMois == 0 && dureeAnnee == 0)
                        this.inputDuree.value = "15"
                    else
                        if (dureeAnnee == 0) {
                            this.inputDuree.value = dureeMois + "";
                            this.dureeRef = dureeMois;


                        }

                        else {
                            this.inputDuree.value = "12";
                            this.dureeRef = 12;

                        }

                }



                this.inputDebut.value = this.utilsService.displayDateFormatInput(this.dateDebut);
                if (this.dateFinReference != null)
                    this.inputFin.value = this.utilsService.displayDateFormatInput(this.dateFinReference);

                else {
                    let debut = new Date(this.onDeclarationForm.get('debut').value)
                    this.dateFin = debut;;
                    if (this.duree == 15)
                        this.dateFin.setDate(debut.getDate() + 15);
                    else
                        this.dateFin.setMonth(debut.getMonth() + this.duree);

                    ;

                    // await this.utilsService.delay(500)
                    this.inputFin.value = this.utilsService.displayDateFormatInput(this.dateFin);

                }

            },
            error => {
                loader.dismiss();
                console.log("6")
                let message = "Oups le serveur est introuvable verfiez votre connexion et recommencez svp";
                this.utilsService.alertNotification(message);
                if (this.simulationActif.origineRoutine == OrigineClientNonLogue)
                    this.navCtrl.navigateRoot(LIENEPOCNO)
                else
                    sessionStorage.setItem(loginToPage, JSON.stringify(LIENCHOICEOPTION))
                this.navCtrl.navigateRoot(LIENLOGIN);
            }
        )
        ////console.log(this.dateFin)
    }

    /**
     * cette fonction nous permet de valider un changement d'options et de revenir danns la page de recap objet flottes
     */
    validerChangementOptions() {
        console.log("editOptions")
        this.typeOrigineRoutine = JSON.parse(window.sessionStorage.getItem(typeRoutine));

        switch (this.typeOrigineRoutine) {
            case typeRoutineSimulation:
                this.misAjourObligatoire();
                this.simulationActif.assures[this.active].garanties = this.checked;
                this.utilsService.remplacerSimulationFlotteBeforeRecap(this.simulationActif).then(

                    () => {
                        // let contract = new Contrat2();
                        // let assureContrat = new AssureContrat()
                        // assureContrat.assure = this.simulationActif.objet;
                        // assureContrat.dateEffet = this.simulationActif.dateDebut
                        // assureContrat.dateCloture = this.simulationActif.dateFin
                        // assureContrat.garanties = this.simulationActif.garanties;
                        // console.log("assureContrat", assureContrat)
                        // console.log("SimulationActif", this.simulationActif);

                        sessionStorage.setItem(editOptions, JSON.stringify(false))

                        this.navCtrl.navigateRoot(LIENASSURINFOVEVEHICULEFLOTTE)
                    }

                )
                break;
            case typeRoutineNewContrat:
            case typeRoutineRenewContrat:
                let operation = JSON.parse(sessionStorage.getItem(operationActifenSession))
                let contrat = JSON.parse(sessionStorage.getItem(contratActifenSession));
                let objet = JSON.parse(sessionStorage.getItem(objetActifenSession))

                this.misAjourObligatoire();

                operation.garanties = this.checked;

                this.operationActif = operation;
                ////console.log(this.operationActif) ;
                ////console.log(this.contratActif) ;
                ////console.log(JSON.parse(window.sessionStorage.getItem(objetActifenSession))) ;


                window.sessionStorage.setItem(operationActifenSession, JSON.stringify(operation))
                let routineContrat = new RoutineContrat();
                routineContrat.contrat = contrat
                routineContrat.objet = objet;
                routineContrat.operation = operation;


                this.remplacerRoutineContrat(routineContrat).then(
                    () => {

                        this.navCtrl.navigateRoot(LIENASSURINFOVEVEHICULEFLOTTE)

                    }
                )
                break;

        }


    }
    /**
     *
     * @param newRoutine
     * cette fonction nous permet en cas de modification de la routine d'un objet d'une flotte
     * de rendre effectif cette modification au niveau de la listeRoutineContrat
     */
    async remplacerRoutineContrat(newRoutine) {

        this.utilsService.remplacerRoutineContrat(newRoutine);

    }
    defaultHref() {
        this.typeOrigineRoutine = JSON.parse(window.sessionStorage.getItem(typeRoutine));

        switch (this.typeOrigineRoutine) {
            case typeRoutineSimulation:
                return "sim-decla-objet"
                break;
            case typeRoutineNewContrat:
            case typeRoutineRenewContrat:
                return "assurinfove"
                break;
        }

    }
    /**GESTION LOGIQUE TRANSLATION DES LANGUES */
    ionViewDidEnter(): void {
        this._translateLanguage()
    }
    ionViewDidLoad() {
        this._translateLanguage();
    }

    public changeLanguage(): void {
        this._translateLanguage();
    }

    _translateLanguage(): void {
        this._translate.use(this.language);
    }
    _initTranslate(language) {
        // Set the default language for translation strings, and the current language.
        this._translate.setDefaultLang('fr-FR');
        if (language) {
            this.language = language;
        }
        else {
            // Set your language here
            this.language = 'fr-FR';
        }
        this._translateLanguage();
    }

    getDeviceLanguage() {
        if (!this.platform.is('android') || !this.platform.is('ios')) {
            //alert('Navigateur')
            this._initTranslate(navigator.language);
        } else {
            this.globalization.getPreferredLanguage()
                .then(res => {
                    this._initTranslate(res.value)
                })
                .catch(e => { console.log(e); });
        }
    }
    /**END TRANSLATION */

}

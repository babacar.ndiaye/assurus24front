import { Component, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Globalization } from '@ionic-native/globalization/ngx';
import { NavController, LoadingController, ToastController, Platform } from '@ionic/angular';
import { TranslateService } from '@ngx-translate/core';
import { ClientParticulier } from 'src/app/models/client-particulier';
import { ClientProfessionnel } from 'src/app/models/client-professionnel';
import { ClientReseau } from 'src/app/models/client-reseau';
import { Compte } from 'src/app/models/compte';
import { Pays } from 'src/app/models/pays';
import { Region } from 'src/app/models/region';
import { ClientParticulierService } from 'src/app/services/client-particulier.service';
import { ClientProfessionnelService } from 'src/app/services/client-professionnel.service';
import { PaysService } from 'src/app/services/pays.service';
import { UtilsService } from 'src/app/services/utils.service';
import { LIENLOGIN, LIENMYPROFILE, SessionCompte, SessionCompteUpdate, typeRoutineProfil } from 'src/environments/environment';


@Component({
    selector: 'app-edit-profile',
    templateUrl: './edit-profile.page.html',
    styleUrls: ['./edit-profile.page.scss'],
})
export class EditProfilePage implements OnInit {
    contacter: FormGroup;
    language: string;
    formInvalid: boolean = false;
    @ViewChild('selectRegion') selectRegion;
    @ViewChild('pays') selectPays;
    allRegions: Region[] = [];
    allPays: Pays[] = [];
    //Variable pour récuperer l'utilisateur connecter
    userPaConnected: ClientParticulier;
    userProConnected: ClientProfessionnel;
    ClientReseau: ClientReseau;
    //variable pour verifier
    isConnected: boolean;
    //variable pour verifier le type de client
    typeClient: string = '';
    resultat;
    compteUpdate: Compte;

    constructor(
        private formBuilder: FormBuilder,
        private platform: Platform,
        private globalization: Globalization,
        private _translate: TranslateService,
        private loadingCtrl: LoadingController,
        private toastCtrl: ToastController,
        public utilsService: UtilsService,
        public servicePays: PaysService,
        private navCtrl: NavController,
        private clientProService: ClientProfessionnelService,
        private clientParService: ClientParticulierService
    ) { }

    ngOnInit() {
        this.getUserConnectedInfos()
        this.getPays()
        this.onInitForm()
        this.getDeviceLanguage()
    }
    onInitForm() {
        if (this.typeClient == "userClientProfessionnel") {
            this.contacter = this.formBuilder.group({
                nom: ['', [Validators.required]],
                ninea: ['', [Validators.required]],
                email: ['', [Validators.required, Validators.email]],
                ville: ['', [Validators.required]],
                adresse: ['', [Validators.required]],
                pays: ['', [Validators.required]]
            });
        } else {
            this.contacter = this.formBuilder.group({
                nom: ['', [Validators.required]],
                prenom: ['', [Validators.required]],
                email: ['', [Validators.required, Validators.email]],
                ville: ['', [Validators.required]],
                adresse: ['', [Validators.required]],
                pays: ['', [Validators.required]]
            });
        }
    }

    getPays(): void {
        this.servicePays.getPays().subscribe(
            (Pays) => {
                this.allPays = Pays;
            },
            (error) => {
                let message =
                    'Oups le serveur est introuvable verfiez votre connexion et recommencez svp';
                this.utilsService.alertNotification(message);
                this.retourPagePrécédente();
            }
        );
    }
    retourPagePrécédente() {
        this.navCtrl.navigateRoot(LIENLOGIN);
    }
    compareWith = (o1: any, o2: any) => {
        return o1 && o2 ? o1.id === o2.id : o1 === o2;
    };
    cw = this.compareWith;
    async onSubmitForm() {
        if (!this.contacter.valid) {
            this.utilsService.alertNotification('tous les champs ne sont pas remplis correctement');
            this.formInvalid = true;
        } else {
            console.log(this.contacter.value);
            if (this.typeClient == "userClientParticulier") {
                this.userPaConnected.nom = this.contacter.value['nom'];
                this.userPaConnected.email = this.contacter.value['email'];
                this.userPaConnected.adresse = this.contacter.value['adresse'];
                this.userPaConnected.pays = this.contacter.value['pays'];
                this.userPaConnected.ville = this.contacter.value['ville'];
                this.userPaConnected.prenom = this.contacter.value['prenom'];
            }
            if (this.typeClient == "userClientProfessionnel") {
                this.userProConnected.ninea = this.contacter.value['ninea'];
                this.userProConnected.nom = this.contacter.value['nom'];
                this.userProConnected.email = this.contacter.value['email'];
                this.userProConnected.adresse = this.contacter.value['adresse'];
                this.userProConnected.pays = this.contacter.value['pays'];
                this.userProConnected.ville = this.contacter.value['ville'];
            }
            if (this.typeClient == "userClientParticulier") {
                console.log(this.userPaConnected);
                this.compteUpdate = JSON.parse(window.sessionStorage.getItem(SessionCompte));
                this.compteUpdate.client = null;
                this.userPaConnected.compte = this.compteUpdate;
                const loader = await this.loadingCtrl.create({
                    message: 'chargement...',
                });
                loader.present();
                this.clientParService.updateClient(this.userPaConnected)
                    .subscribe(
                        async (compte) => {
                            if (compte) {
                                const toast = await this.toastCtrl.create({
                                    message: this._translate.instant('modifications_validées'),
                                    duration: 5000,
                                    position: 'top',
                                    closeButtonText: 'OK',
                                    showCloseButton: true,
                                });

                                toast.present();

                                window.sessionStorage.setItem(
                                    SessionCompte,
                                    JSON.stringify(compte)
                                );
                                window.sessionStorage.removeItem(
                                    SessionCompteUpdate
                                );
                                window.sessionStorage.setItem(
                                    typeRoutineProfil,
                                    JSON.stringify(null)
                                );
                                this.loadingCtrl.dismiss();
                                this.navCtrl.navigateRoot(LIENMYPROFILE);
                            } else {
                                this.loadingCtrl.dismiss();
                                let messageInfo =
                                    this._translate.instant('erreur_de_connexion');

                                this.utilsService.alertNotification(messageInfo);
                            }
                        },
                        (erreur) => {
                            this.loadingCtrl.dismiss();
                            let messageInfo = this._translate.instant('erreur_de_connexion');

                            this.utilsService.alertNotification(messageInfo);
                        }
                    );
            }
            if (this.typeClient == "userClientProfessionnel") {
                console.log(this.userProConnected);
                this.compteUpdate = JSON.parse(window.sessionStorage.getItem(SessionCompte));
                this.compteUpdate.client = null;
                this.userProConnected.compte = this.compteUpdate;
                const loader = await this.loadingCtrl.create({
                    message: 'chargement...',
                });
                loader.present();
                this.clientProService.updateClient(this.userProConnected)
                    .subscribe(
                        async (compte) => {
                            if (compte) {
                                const toast = await this.toastCtrl.create({
                                    message: this._translate.instant('modifications_validées'),
                                    duration: 5000,
                                    position: 'top',
                                    closeButtonText: 'OK',
                                    showCloseButton: true,
                                });

                                toast.present();

                                window.sessionStorage.setItem(
                                    SessionCompte,
                                    JSON.stringify(compte)
                                );
                                window.sessionStorage.removeItem(
                                    SessionCompteUpdate
                                );
                                window.sessionStorage.setItem(
                                    typeRoutineProfil,
                                    JSON.stringify(null)
                                );
                                this.loadingCtrl.dismiss();
                                this.navCtrl.navigateRoot(LIENMYPROFILE);
                            } else {
                                this.loadingCtrl.dismiss();
                                let messageInfo =
                                    this._translate.instant('erreur_de_connexion');

                                this.utilsService.alertNotification(messageInfo);
                            }
                        },
                        (erreur) => {
                            this.loadingCtrl.dismiss();
                            let messageInfo = this._translate.instant('erreur_de_connexion');

                            this.utilsService.alertNotification(messageInfo);
                        }
                    );
            }

        }
    }

    chargerRegionsduPays() {
        this.selectRegion.value = null;
        this.allRegions = this.contacter.get('pays').value.regions;
        console.log(this.allRegions);
    }

    /**
     *  * Cette fonction permet de récupérer les infos
     *  * de l'utilisateur à partir de la session
     */
    getUserConnectedInfos() {
        if (sessionStorage.getItem(SessionCompte)) {
            this.isConnected = true;
            // Récupérer des données depuis sessionStorage
            let data = JSON.parse(sessionStorage.getItem(SessionCompte));
            console.log("Type client", data.myProfil.codeGroupe);
            this.typeClient = data.myProfil.codeGroupe;
            if (this.typeClient == 'userClientParticulier') {
                this.userPaConnected = data.client;
                console.log('Connected User Part', this.userPaConnected);
            } else if (this.typeClient == 'userReseau') {
                this.ClientReseau = data;
                console.log('Connected User ClientReseau', this.ClientReseau);
            }
            else {
                this.userProConnected = data.client;
                console.log('Connected User Pro', this.userProConnected);
            }
        } else {
            this.isConnected = false;
        }
    }

    /**GESTION LOGIQUE TRANSLATION DES LANGUES */
    ionViewDidEnter(): void {
        this.getDeviceLanguage()
    }

    public changeLanguage(): void {
        this._translateLanguage();
    }

    _translateLanguage(): void {
        this._translate.use(this.language);
    }

    _initTranslate(language) {
        // Set the default language for translation strings, and the current language.
        this._translate.setDefaultLang('fr-FR');
        if (language) {
            this.language = language;
        }
        else {
            // Set your language here
            this.language = 'fr-FR';
        }
        this._translateLanguage();
    }

    getDeviceLanguage() {
        if (!this.platform.is('android') || !this.platform.is('ios')) {
            //alert('Navigateur')
            console.log('Navigateur langage', navigator.language)
            this._initTranslate(navigator.language);
        } else {
            this.globalization.getPreferredLanguage()
                .then(res => {
                    this._initTranslate(res.value)
                })
                .catch(e => { console.log(e); });
        }
    }
    /**END TRANSLATION */
}

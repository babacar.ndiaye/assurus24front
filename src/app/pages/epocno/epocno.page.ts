import { Component, OnInit, ViewChild } from '@angular/core';
import { NavController, AlertController, LoadingController, IonSlides, Platform, MenuController } from '@ionic/angular';
import { UtilsService } from 'src/app/services/utils.service';
import { AlertOptions, createAnimation } from '@ionic/core';
import { simulationActifenSession, OriginePageGesSim, DescAutos, OrigineClientNonLogue, UrlApi, banniereCom, BaniereName, downloadBaniere, typeRoutine, typeRoutineSimulation, MessagePaiementErreurConnexion, LIENACCUEILCOMMERCIAL, utilsGenerale, iab } from 'src/environments/environment';
import { ObjetAssures } from 'src/app/models/objet-assures';
import { Simulation } from 'src/app/models/simulation';
import { BA } from 'src/app/models/ba';
import { TypeBA, TypeBA2 } from 'src/app/models/type-ba';
import { CompteService } from 'src/app/services/compte.service';
import { File, Entry } from "@ionic-native/file/ngx";
import { Commande } from 'src/app/pojos/commande';
import { Capacitor } from '@capacitor/core';
import { InAppBrowser } from '@ionic-native/in-app-browser/ngx';
import { TranslateService } from '@ngx-translate/core';
import { Globalization } from '@ionic-native/globalization/ngx';
//import { InAppBrowser } from '@ionic-native/in-app-browser/ngx';

declare var PayTech;


const MOBILE_CANCEL_URL = "https://paytech.sn/mobile/cancel"
const MOBILE_SUCCESS_URL = "https://paytech.sn/mobile/success"

@Component({
    selector: 'app-epocno',
    templateUrl: './epocno.page.html',
    styleUrls: ['./epocno.page.scss'],
})
export class EpocnoPage implements OnInit {
    largeur = screen.width;
    longueur = screen.height;
    couleur = "../../../assets/img/famille.png";
    banniereComs: Array<banniereCom>;
    urlApi = UrlApi;
    montrerSlogan: boolean = false;
    montrerFleche: boolean = true;
    erreurGif: boolean = false;
    refCommande = 500;

    @ViewChild('slideWithNav') slideWithNav: IonSlides;


    sliderOne: any;

    iabrowser
    @ViewChild('mySlider') mySlider: IonSlides;

    ionViewDidLoad() {
        this.mySlider.lockSwipes(true);
    }
    //Configuration for each Slider
    slideOptsOne = {
        initialSlide: 0,
        slidesPerView: 1,

        // spaceBetween: 20 ,
        autoplay: true,
        speed: 1000


    };
    slideChanged(slideView) {
        switch (slideView) {

            case "infos": this.slideWithNav.startAutoplay(); break;
        }
    }
    directories = [];

    /**TRANSLATION VARIABLE */
    public title: string;
    public title_2: string;
    public description: string;
    public name: string;
    public language: string;
    public flags = [
        { name: 'English', image: 'assets/flags/gb.svg' },
        { name: 'Français', image: 'assets/flags/fr.svg' },
    ]
    public flag: any;
    /**end */
    constructor(private navCtrl: NavController, private alertCtrl: AlertController, private utilsService: UtilsService, private loadingCtrl: LoadingController
        , private serviceCompte: CompteService, private file: File,
        private globalization: Globalization, private _translate: TranslateService,
        private platform: Platform,
        public menuCtrl: MenuController,

    ) {


        this.banniereComs = new Array;
        this.getDeviceLanguage()
    }
    async ionViewWillEnter() {
        //this.download() ;
        // this.serviceCompte.loadDocuments() ;


        // console.log(PayTech)
        this.menuCtrl.enable(true);
        this.recupererBanieresCom();
        console.log(banniereCom)
        await this.utilsService.delay(5000);
        this.montrerSlogan = true;
        await this.utilsService.delay(5500);
        this.montrerFleche = true;
    }


    ngOnInit() {
        let page = localStorage.getItem("pagePrecedent");
        console.log("pagePrecedente", page);

    }
    // selectionBranche() {
    //     this.utilsService.selectionBrancheSimulation(OrigineClientNonLogue);

    // }

    selectionBranche() {
        this.utilsService.getTypeBas().subscribe((data: TypeBA2[]) => {
            this.utilsService.selectionBrancheSimulation2(OriginePageGesSim, data);
        })
    }

    recupererTypeBranches() {
        return this.utilsService.recupererTypeBranches();


    }
    //    const animation :Animation= createAnimation()
    //   .addElement(document.querySelector('.square'))
    //   .duration(1000)
    //   .direction('alternate')
    //   .iterations(Infinity)
    //   .keyframes([
    //     { offset: 0, transform: 'scale(1)', opacity: '1' },
    //     { offset: 1, transform: 'scale(1.5)', opacity: '0.5'
    //  }
    //   ]);

    // animation.play();

    recupererBanieresCom() {
        this.utilsService.recupererBanieresCom().then(
            liste => {
                console.log(liste);
                this.banniereComs = liste;
            }

        )

    }
    erreurLoadGif() {
        console.log("erreur");
        this.erreurGif = true;

    }
    async loadDocuments() {
        let folders = "baniere";
        let directories = [];
        let folder = '';
        this.utilsService.alertNotification(this.file.dataDirectory);

        this.file
            .createDir(
                `${this.file.dataDirectory, ""}`,
                "baniere2",
                true
            )
            .then(async res => {
                this.file.listDir(this.file.dataDirectory, "").then(res => {
                    this.directories = res;

                }
                    ,
                    error => { this.utilsService.alertNotification(error) });
                this.directories.forEach(element => {

                    this.utilsService.alertNotification(element.name);

                });

            },
                error => { this.utilsService.alertNotification(error) });

    }
    async download() {
        const loader = await this.loadingCtrl.create({
            message: '',
        });

        loader.present();
        // To download the PDF file
        this.serviceCompte.download(BaniereName, 1, downloadBaniere);
    }
    // lancerAuto() {
    //     this.utilsService.lancerAuto();

    // }
    paytech() {
        if (PayTech != null) {
            console.log("paytech");
            let commande = new Commande();
            commande.refCommande = "f5f54f54f";
            (new PayTech({
                item_name: "Nouvelle Souscription",
                item_price: "16000",

                ref_command: this.refCommande++,

                command_name: "HBZZYZVUzeuxxZZZV",
                env: "test",
                ipn_url: "https://localhost:8443/Assurus24/api//paiement/receptionIPN",
                success_url: MOBILE_SUCCESS_URL,
                cancel_url: MOBILE_CANCEL_URL
            })).withOption({
                requestTokenUrl: UrlApi + "/paiement/demande",
                method: 'POST',
                headers: {
                    "Accept": "text/html"
                },

                presentationMode: PayTech.DO_NOTHING,

                didPopupClosed: function (is_completed, success_url, cancel_url) {
                    console.log(is_completed)
                    window.location.href = is_completed === true ? success_url : cancel_url;


                },
                willGetToken: function () {
                    console.log("Je me prepare a obtenir un token");

                    //     this.iabrowser = iab.create("https://paytech.sn/payment/",'_self',"hidden=yes")
                    //     this.iabrowser.hide() ;
                    // this.iabrowser.on('loadstart').subscribe(
                    //   event =>{
                    //     if(event.url.includes(MOBILE_SUCCESS_URL))
                    //     {
                    //       console.log("ok")
                    //       this.iabrowser.close() ;
                    //     }
                    //     if(event.url.includes(MOBILE_CANCEL_URL))
                    //     {
                    //       console.log("ko")
                    //       this.iabrowser.close() ;
                    //     }
                    //   }
                    // )



                },
                didGetToken: function (token, redirectUrl) {
                    console.log("Mon token est : " + token + ' et url est ' + redirectUrl);
                    this.iabrowser = iab.create(redirectUrl, '_self', "location=no")

                    this.iabrowser.on('loadstart').subscribe(
                        event => {
                            console.log("event")
                            console.log(event)
                            if (event.url.includes(MOBILE_SUCCESS_URL)) {
                                console.log("ok")
                                alert("ok")
                                this.iabrowser.close();
                            }
                            if (event.url.includes(MOBILE_CANCEL_URL)) {
                                console.log("ko")
                                alert("ko")
                                this.iabrowser.close();
                            }
                        }
                    )



                },
                didReceiveError: function (error) {
                    this.utilsService.alertNotification("erreur au niveau du serveur veuillez recommencer")


                },
                didReceiveNonSuccessResponse: function (jsonResponse) {
                    console.log('non success response ', jsonResponse);
                    alert(jsonResponse.errors);

                }
            }).send();

        }
        else {
            this.utilsService.alertNotification(MessagePaiementErreurConnexion)

        }

    }

    /**GESTION LOGIQUE TRANSLATION DES LANGUES */
    ionViewDidEnter(): void {
        this.getDeviceLanguage()
    }

    _initialiseTranslation(): void {
        this._translate.get('TITLE').subscribe((res: string) => {
            this.title = res;
        });
        this._translate.get('description').subscribe((res: string) => {
            this.description = res;
        });
        this._translate.get('TITLE_2', { value: 'John' }).subscribe((res: string) => {
            this.title_2 = res;
        });
        this._translate.get('data.name', { name_value: 'Marissa Mayer' }).subscribe((res: string) => {
            this.name = res;
        });

    }

    public changeLanguage(): void {
        this._translateLanguage();
    }

    _translateLanguage(): void {
        this._translate.use(this.language);
        this._initialiseTranslation();
    }

    _initTranslate(language) {
        // Set the default language for translation strings, and the current language.
        this._translate.setDefaultLang('fr-FR');
        if (language) {
            this.language = language;
        }
        else {
            // Set your language here
            this.language = 'fr-FR';
        }
        this._translateLanguage();
    }

    getDeviceLanguage() {
        // if (window.Intl && typeof window.Intl === 'object') {
        //     console.log('Naviagateur langage', navigator.language)
        //   this._initTranslate(navigator.language);
        // }
        // else {
        //   this.globalization.getPreferredLanguage()
        //     .then(res => {
        //       this._initTranslate(res.value)
        //     })
        //     .catch(e => {console.log(e);});
        // }
        if (!this.platform.is('android') || !this.platform.is('ios')) {
            //alert('Navigateur')
            console.log('Navigateur langage', navigator.language)
            this._initTranslate(navigator.language);
        } else {
            this.globalization.getPreferredLanguage()
                .then(res => {
                    this._initTranslate(res.value)
                })
                .catch(e => { console.log(e); });
        }
    }
    /**END TRANSLATION */

    goToAccueil() {
        this.navCtrl.navigateRoot('/accueil');
      }
}

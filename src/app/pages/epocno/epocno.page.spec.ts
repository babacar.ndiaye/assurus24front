import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EpocnoPage } from './epocno.page';

describe('EpocnoPage', () => {
  let component: EpocnoPage;
  let fixture: ComponentFixture<EpocnoPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EpocnoPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EpocnoPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { EpocnoPage } from './epocno.page';
import { LazyLoadImageModule } from 'ng-lazyload-image'
import { ComponentsModule } from 'src/app/components/components.module';
import { baniereComModule } from 'src/app/components/banniere-com/banniere-com.module';
import { TranslateLoader, TranslateModule } from '@ngx-translate/core';
import { HttpLoaderFactory } from 'src/app/app.module';
import { HttpClient } from '@angular/common/http';


const routes: Routes = [
  {
    path: '',
    component: EpocnoPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    LazyLoadImageModule,
    ComponentsModule,
    baniereComModule,
    TranslateModule.forChild({
        loader: {
          provide: TranslateLoader,
          useFactory: HttpLoaderFactory,
          deps: [HttpClient]
        },
      }),

    RouterModule.forChild(routes)
  ],
  declarations: [EpocnoPage]
})
export class EpocnoPageModule {}

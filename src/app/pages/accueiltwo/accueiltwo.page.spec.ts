import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AccueiltwoPage } from './accueiltwo.page';

describe('AccueiltwoPage', () => {
  let component: AccueiltwoPage;
  let fixture: ComponentFixture<AccueiltwoPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AccueiltwoPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AccueiltwoPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ListeContactProfessionelPage } from './liste-contact-professionel.page';

describe('ListeContactProfessionelPage', () => {
  let component: ListeContactProfessionelPage;
  let fixture: ComponentFixture<ListeContactProfessionelPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ListeContactProfessionelPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListeContactProfessionelPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import { Component, OnInit } from '@angular/core';
import { ContactClient } from 'src/app/models/contact-client';
import { ListeValeur } from 'src/app/models/liste-valeur';
import { LISTECONTACTCLIENTPROFESSIONNELSESSION, CONTACTDECLIENTPROFESSIONNELSESSION, LIENFORMCONTACTSDEPROFESSIONEL, INDEXCONTACTDECLIENTPROFESSIONNELSESSION, LIENCONTACT } from 'src/environments/environment';
import { NavController, Platform } from '@ionic/angular';
import { Globalization } from '@ionic-native/globalization/ngx';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-liste-contact-professionel',
  templateUrl: './liste-contact-professionel.page.html',
  styleUrls: ['./liste-contact-professionel.page.scss'],
})
export class ListeContactProfessionelPage implements OnInit {
  i = 1
  listeContact: Array<ContactClient> = [];
  LIENCONTACTLOC = LIENCONTACT;
  language: string;
  constructor(
    public navCtrl: NavController,
    private platform: Platform,
    private globalization: Globalization,
    private _translate: TranslateService
  ) { }
  ionViewWillEnter() {
    this.getListeContactProfessionnel();
  }
  ngOnInit() {
  }

  getListeContactProfessionnel() {
    this.listeContact = JSON.parse(sessionStorage.getItem(LISTECONTACTCLIENTPROFESSIONNELSESSION));
    console.log("liste contact pro dans listContactPro au début",this.listeContact)
    /*let contactClient1 = new ContactClient() ;
     contactClient1.id = 1
    contactClient1.nom = "Ndiaye"
     contactClient1.prenom = "Samba"
     contactClient1.dateDeNaissance = new Date("12-05-1995")
     contactClient1.intituleposte="Comptable"
     contactClient1.civilite = new ListeValeur() ;
    contactClient1.civilite.code = "Mr"
    let contactClient2 = new ContactClient() ;
     contactClient2.id = 2
     contactClient2.nom = "Diop"
     contactClient2.prenom = "Bathie"
     contactClient2.dateDeNaissance = new Date("12-05-1995")
     contactClient2.intituleposte="Ressources Humaines"
     contactClient2.civilite = new ListeValeur() ;
     contactClient2.civilite.code = "Mr"
    let contactClient3 = new ContactClient() ;
     contactClient3.id = 3
     contactClient3.nom = "Fall"
     contactClient3.prenom = "Mass"
     contactClient3.dateDeNaissance = new Date("12-05-1995")
     contactClient3.intituleposte="Directeur Général"
     contactClient3.civilite = new ListeValeur() ;
     contactClient3.civilite.code = "Mr"
     this.listeContact.push(contactClient1,contactClient2,contactClient3) ;*/
  }
  ajouterContact() {
    let newContact = new ContactClient();
    sessionStorage.setItem(CONTACTDECLIENTPROFESSIONNELSESSION, JSON.stringify(newContact));
    sessionStorage.removeItem(INDEXCONTACTDECLIENTPROFESSIONNELSESSION);
    this.navCtrl.navigateForward(LIENFORMCONTACTSDEPROFESSIONEL);
  }
  delete(index) {
    console.log(index);

    this.listeContact.splice(index, 1)
    console.log(this.listeContact)
    sessionStorage.setItem(LISTECONTACTCLIENTPROFESSIONNELSESSION, JSON.stringify(this.listeContact))


    //window.location.reload()
    return;

  }
  edit(index) {
    let updateContact = this.listeContact[index];
    console.log(updateContact);
    sessionStorage.setItem(CONTACTDECLIENTPROFESSIONNELSESSION, JSON.stringify(updateContact));
    sessionStorage.setItem(INDEXCONTACTDECLIENTPROFESSIONNELSESSION, JSON.stringify(index))
    this.navCtrl.navigateForward(LIENFORMCONTACTSDEPROFESSIONEL);
  }


  validerListe() {
    sessionStorage.removeItem(INDEXCONTACTDECLIENTPROFESSIONNELSESSION);
    sessionStorage.removeItem(CONTACTDECLIENTPROFESSIONNELSESSION);
    
    let monContact = JSON.parse(sessionStorage.getItem(LISTECONTACTCLIENTPROFESSIONNELSESSION))
    console.log("mon contact dans list-contact-form",monContact);
    this.navCtrl.navigateRoot(LIENCONTACT);

  }

  /**GESTION LOGIQUE TRANSLATION DES LANGUES */
  ionViewDidEnter(): void {
    this._translateLanguage()
  }
  ionViewDidLoad() {
    this._translateLanguage();
  }

  public changeLanguage(): void {
    this._translateLanguage();
  }

  _translateLanguage(): void {
    this._translate.use(this.language);
  }
  _initTranslate(language) {
    // Set the default language for translation strings, and the current language.
    this._translate.setDefaultLang('fr-FR');
    if (language) {
      this.language = language;
    }
    else {
      // Set your language here
      this.language = 'fr-FR';
    }
    this._translateLanguage();
  }

  getDeviceLanguage() {
    if (!this.platform.is('android') || !this.platform.is('ios')) {
      //alert('Navigateur')
      this._initTranslate(navigator.language);
    } else {
      this.globalization.getPreferredLanguage()
        .then(res => {
          this._initTranslate(res.value)
        })
        .catch(e => { console.log(e); });
    }
  }
  /**END TRANSLATION */


}

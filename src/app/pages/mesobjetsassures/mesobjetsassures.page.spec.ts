import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MesobjetsassuresPage } from './mesobjetsassures.page';

describe('MesobjetsassuresPage', () => {
  let component: MesobjetsassuresPage;
  let fixture: ComponentFixture<MesobjetsassuresPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MesobjetsassuresPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MesobjetsassuresPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import { Contrat2 } from 'src/app/models/contrat';
import { OriginePageMesObjets, SessionCompte } from "./../../../environments/environment";
import { Component, OnInit, ViewChild } from "@angular/core";
import { Automobile } from "src/app/models/automobile";
import { ControleTechnique } from "src/app/models/controle-technique";
import { ObjetAssures, ObjetAssures2 } from "src/app/models/objet-assures";
import {
    NomBrancheAuto,
    NomBrancheMaison,
    NomBrancheMoto,
    NomBrancheSante,
    NomBrancheBateau,
    NomBrancheVoyage,
    DescAutos,
    OBJETSESSION,
    statutValideEnCours,
    statutValidationKo,
    typeRoutineNewContrat,
    typeRoutine,
    typeRoutineNewObject,
    statutContratValideNonActif,
    statutContratActif,
    statutContratResilie,
    statutContratEnCoursDeValidation,
    statutContratExpire,
    LIENLOGIN,
    LIENASSURINFOVE,
    LIENMONOBJET,
} from "src/environments/environment";
import { BA } from "src/app/models/ba";
import {
    LoadingController,
    NavController,
    IonSlides,
    Platform,
} from "@ionic/angular";
import { TypeBA, TypeBA2 } from "src/app/models/type-ba";
import { UtilsService } from "src/app/services/utils.service";
import { Proprietaire } from "src/app/models/proprietaire";
import { Operation } from "src/app/models/operation";
import { Reseau } from "src/app/models/reseau";
import { ListeValeur } from "src/app/models/liste-valeur";
import { ObjetService } from "src/app/services/objet.service";
import { Client } from "src/app/models/client";
import { Globalization } from "@ionic-native/globalization/ngx";
import { TranslateService } from "@ngx-translate/core";
import { TypeBas } from "src/app/models/type-bas";
import { ListeValeurService } from "src/app/services/liste-valeur.service";
import { AttributAussureBranche } from "src/app/models/attribut-assure-branche";
import { AttributAussure } from "src/app/models/attribut-assure";

@Component({
    selector: "app-mesobjetsassures",
    templateUrl: "./mesobjetsassures.page.html",
    styleUrls: ["./mesobjetsassures.page.scss"],
})
export class MesobjetsassuresPage implements OnInit {
    //teste le slider
    nbEssaisBranches = [1, 2, 3];
    o1: ObjetAssures;
    o2: ObjetAssures;
    contrat1: Contrat2;
    contrat2: Contrat2;
    contrat3: Contrat2;
    contratFictif: Contrat2;
    p1: Proprietaire;
    typeBranche1: TypeBA;
    controleTechnique1: ControleTechnique;
    controleTechnique2: ControleTechnique;
    objets;

    // babs
    connectedClient: Client;
    objetsAutoBabs: Automobile[] = [];
    objetsAssures: ObjetAssures2[] = [];
    objetsAssuresClone: ObjetAssures2[] = [];
    brancheAutoId;
    brancheSanteId;
    //OldTab
    objetsAuto: ObjetAssures2[] = [];
    objetsSante: ObjetAssures2[] = [];
    objetsMaison: ObjetAssures2[] = [];
    objetsMoto: ObjetAssures2[] = [];
    objetsBateau: ObjetAssures2[] = [];
    objetsVoyage: ObjetAssures2[] = [];
    date: Date = new Date(null);
    compagnie1: {
        id: number;
        nom: string;
        registreCommerce: string;
        compte: any;
        communicationCommerciales: any[];
        bas: BA[];
        listeNoire: any;
        reseaux: any[];
        compteCredits: any[];
    };
    op11: Operation;
    op21: Operation;
    op12: Operation;
    op22: Operation;
    controleTechnique3: { id: number; objet: any; dateValidite: Date };
    critereFiltre;
    valCherche;

    filtreByfin = "filtreByFin";
    filtreByIm = "filtraByIm";
    objetsWhithoutFilter: any;
    @ViewChild("slideWithNav") slideWithNav: IonSlides;

    sliderOne: any;

    @ViewChild("mySlider") mySlider: IonSlides;

    //Configuration for each Slider
    slideOptsOne = {
        initialSlide: 0,
        slidesPerView: 1,

        //spaceBetween: 20 ,
        autoplay: false,
        speed: 1000,
    };
    reseauTest = new Reseau();
    opFictif: Operation;
    opFictif2: Operation;
    swipeNext() {
        this.slideWithNav.slideNext();
    }
    swipeBack() {
        this.slideWithNav.slidePrev();
    }

    //For translation
    language: string;
    //For pagination
    p: number = 1;

    constructor(
        private loadingCtrl: LoadingController,
        private navCtrl: NavController,
        public utilsService: UtilsService,
        private serviceObjet: ObjetService,
        private listeValeurService: ListeValeurService,
        private platform: Platform,
        private globalization: Globalization,
        private _translate: TranslateService
    ) {
        this.utilsService.getDate().subscribe(
            (dateFormatMobile) => {
                this.date = new Date(dateFormatMobile.date);
            },
            (error) => {
                let message =
                    "Oups le serveur est introuvable verfiez votre connexion et recommencez svp";
                this.utilsService.alertNotification(message);
                this.navCtrl.navigateRoot(LIENLOGIN);
            }
        );
        // this.getObjets() ;
    }
    async ionViewWillEnter() {

        await this.utilsService.getDate().subscribe(
            (dateFormatMobile) => {
                this.date = new Date(dateFormatMobile.date);
            },
            (error) => {
                let message =
                    "Oups le serveur est introuvable verfiez votre connexion et recommencez svp";
                this.utilsService.alertNotification(message);
                this.navCtrl.navigateRoot(LIENLOGIN);
            }
        );

        const loader = await this.loadingCtrl.create({
            message: "",
        });
        loader.present();
        // this.getObjets().then(() => {
        //     this.objetsWhithoutFilter = this.objetsAuto;
        //     this.objetsAuto.forEach((objet) => {
        //         this.utilsService
        //             .recupererIntervalleNonDisponibleObjet(objet.contrats)
        //             .then((intervalle) => {
        //                 console.log(intervalle);
        //             });
        //     });
        // });
        loader.dismiss();
    }

    ngOnInit() {
        this.getAllTypeBaId();
        this.LoadClientSavedObject();
        //this.repartitionObjets();
    }


    /**
     * * Cette fonction permet de récuperer
     * * les objets enregistrés du client avec l'api
     */
    LoadClientSavedObject() {
        if (window.sessionStorage.getItem(SessionCompte)) {
            let data = JSON.parse(window.sessionStorage.getItem(SessionCompte));
            this.connectedClient = data.client;
            console.log("Client connected", this.connectedClient);
            if (this.connectedClient) {
                this.serviceObjet
                    .getObjetsAssuresByClient(this.connectedClient.id)
                    .subscribe(
                        (data) => {
                            console.log("data", data);
                            let completedCount = 0;
                            data.forEach((item) => {
                                this.getAttributsObjetAssure(item.id)
                                    .then((tabAttributs) =>{
                                        console.log("promise getAttributs objets", tabAttributs);
                                        item.attributs = tabAttributs;
                                        if(item.attributs.length > 0){

                                            item.TabAtrributs = [];
                                                for (let att of item.attributs) {
                                                    if (
                                                        att.attribut.code == "marqueVehiculeAuto" ||
                                                        att.attribut.code ==
                                                            "appellationCom" ||
                                                        att.attribut.code == "marqueVehiculeMoto" ||
                                                        att.attribut.code ==
                                                                "appellationComMoto" ||
                                                        att.attribut.code == "dateVisiteTech"
                                                    ) {
                                                        item.TabAtrributs.push(att);
                                                    }

                                                }
                                        }
                                        completedCount += 1;
                                        console.log("completedCount",completedCount);

                                        if (completedCount == data.length){
                                            this.objetsAssures = data;
                                            //alert("boucle finish");
                                            this.repartitionObjets();
                                            console.log("Objet Assure",this.objetsAssures)
                                        }
                                    })

                            }
                            )



                            console.log("objetsAssures", this.objetsAssures);
                        },
                        (error) => {
                            console.log(error);
                        }
                    );
            }
        }
    }
    traitementDate() {
        this.objets.forEach((objet) => {
            if (objet.contrats != null && objet.contrats.length != 0) {
                objet.contrats.forEach((element) => {
                    element.dateEffet = new Date(element.dateEffet);
                    element.dateCloture = new Date(element.dateCloture);
                });
            }
        });
    }

    getAllTypeBaId(){
        this.utilsService.getTypeBas().subscribe((data: TypeBA2[]) => {
           data.forEach((element) => {
            switch (element.libelle) {
                case "AUTOMOBILE":
                    this.brancheAutoId = element.id;
                    break;
                case 'SANTE':
                    this.brancheSanteId = element.id;
                    break;
                default:

            }
           });
        })
    }

    /**
     * *Cette fonction permet de récupérer les
     * * attributs en fonction de l'objet
     * @param id
     */
     getAttributsObjetAssure(id) {
        let TabAttributs: AttributAussure[] = [];
        let promise = new Promise<AttributAussure[]>((resolve, reject) => {
            this.serviceObjet
                .getAttributsByObjetAssure(id)
                .subscribe(
                    (data) => {
                        //TabAttributs = [];
                        if (data) {
                            //console.log("OperationsContrat", data);
                            TabAttributs = data;
                            //pour résoudre la promesse
                            resolve(TabAttributs);
                        }
                    },
                    (error) => {
                        //pour rejeter la promesse
                        reject(error);
                    }
                );
        });

        return promise;
    }

    repartitionObjets() {

        //this.traitementDate();
        //alert("In Repartition");
        this.objetsAssures.forEach((objet) => {
            if(objet.typeba.parent == null){
                //alert("Haven't parent");
                switch (objet.typeba.libelle) {
                    case 'AUTOMOBILE':
                        this.objetsAuto.push(objet);
                        this.objetsWhithoutFilter = this.objetsAuto;
                        this.brancheAutoId = objet.typeba.id;
                        break;
                    case 'SANTE':
                        this.objetsSante.push(objet);
                        this.brancheSanteId = objet.typeba.id;
                        break;
                    case NomBrancheMaison:
                        this.objetsMaison.push(objet);
                        break;
                    case NomBrancheMoto:
                        this.objetsMoto.push(objet);
                        break;
                    case NomBrancheBateau:
                        this.objetsBateau.push(objet);
                        break;
                    case NomBrancheVoyage:
                        this.objetsVoyage.push(objet);
                        break;
                }
            }else {
                //alert("Have parent");
                switch (objet.typeba.parent.libelle) {
                    case 'AUTOMOBILE':
                        this.objetsAuto.push(objet);
                        this.objetsWhithoutFilter = this.objetsAuto;
                        this.brancheAutoId = objet.typeba.parent.id;
                        break;
                    case 'SANTE':
                        this.objetsSante.push(objet);
                        this.brancheSanteId = objet.typeba.parent.id;
                        break;
                    case NomBrancheMaison:
                        this.objetsMaison.push(objet);
                        break;
                    case NomBrancheMoto:
                        this.objetsMoto.push(objet);
                        break;
                    case NomBrancheBateau:
                        this.objetsBateau.push(objet);
                        break;
                    case NomBrancheVoyage:
                        this.objetsVoyage.push(objet);
                        break;
                }
            }

        });
        //this.objetsAuto.sort((a, b) => this.compare(a, b));
        console.log("auto");
        console.log(this.objetsAuto);
        console.log("sante");
        console.log(this.objetsSante);
        console.log(this.brancheAutoId);
    }
    // compare(a: ObjetAssures2, b: ObjetAssures2) {
    //     let res;
    //     res = b.statut - a.statut;
    //     if (res == 0) {
    //         if (
    //             a.[a.contrats.length - 1].dateCloture >
    //             b.contrats[b.contrats.length - 1].dateCloture
    //         )
    //             return -1;
    //         else if (
    //             a.contrats[a.contrats.length - 1].dateCloture <
    //             b.contrats[b.contrats.length - 1].dateCloture
    //         )
    //             return 1;
    //         else return 0;
    //     }
    //     return res;
    // }
    voirObjet(objet: ObjetAssures) {
        window.sessionStorage.setItem(OBJETSESSION, JSON.stringify(objet));
        this.navCtrl.navigateForward(LIENMONOBJET);
    }
    voirObjetBabs(objet: ObjetAssures2) {
        window.sessionStorage.setItem(OBJETSESSION, JSON.stringify(objet));
        this.navCtrl.navigateForward(LIENMONOBJET);
        console.log("Babs");
    }
    diffdate(d1, d2, u) {
        let div = 1;
        const d1Format = new Date(d1);
        switch (u) {
            case "seconde":
                div = 1000;
                break;
            case "minute":
                div = 1000 * 60;
                break;
            case "heure":
                div = 1000 * 60 * 60;
                break;
            case "jour":
                div = 1000 * 60 * 60 * 24;
                break;
        }

        var Diff = d1Format.getTime() - d2.getTime() + 1;
        return Math.ceil(Diff / div);
    }
    creerAutomobile() {
        if(this.brancheAutoId){
            console.log('this.brancheAutoId',this.brancheAutoId);
            this.listeValeurService.getUsages(this.brancheAutoId).subscribe((data: TypeBas[]) => {
                if(data){
                    this.utilsService.getTypeBasAndSousBrancheAttributsDTO(this.brancheAutoId)
                    .subscribe((attributsParents) => {
                        this.utilsService.selectionSousBrancheObjet(OriginePageMesObjets, data, "AUTOMOBILE",attributsParents,this.brancheAutoId);
                    })
                }

            })
        }

    }
    onSearchObjet() {
        if (this.critereFiltre == null) {
            this.utilsService.alertNotification(
                "veuillez choisir un critère de recherche"
            );
        } else {
            console.log("TabAuto", this.objetsAuto);
            console.log("objetsWhithoutFilter", this.objetsWhithoutFilter);
            this.objetsAuto = this.objetsWhithoutFilter;
            const val = this.valCherche;
            console.log("filtre");
            console.log(val);
            console.log(this.critereFiltre);
            if (val && val.trim() !== "") {
                console.log("filtre");
                this.objetsAuto = this.objetsAuto.filter((objet) => {
                    switch (this.critereFiltre) {
                        case this.filtreByIm:
                            return (
                                objet.intitule
                                    .toLowerCase()
                                    .indexOf(val.trim().toLowerCase()) > -1
                            );
                        case this.filtreByfin:
                            return objet.attributs.findIndex(
                                (a,index) =>
                                    a.attribut.code=='dateVisiteTech'?this.utilsService.displayDate(new Date(a.valeur)).toLowerCase().indexOf(val.trim().toLowerCase()) != -1:null) > -1
                        default:
                            return this.objetsWhithoutFilter;
                    }
                });
            }
        }
    }

    formatDate(date: string|number|Date) {
        var d = new Date(date),
            month = '' + (d.getMonth() + 1),
            day = '' + d.getDate(),
            year = d.getFullYear();

        if (month.length < 2)
            month = '0' + month;
        if (day.length < 2)
            day = '0' + day;

        return [year, month, day].join('-');
    }

    /**GESTION LOGIQUE TRANSLATION DES LANGUES */
    ionViewDidEnter(): void {
        this.getDeviceLanguage();
    }

    public changeLanguage(): void {
        this._translateLanguage();
    }

    _translateLanguage(): void {
        this._translate.use(this.language);
    }

    _initTranslate(language) {
        // Set the default language for translation strings, and the current language.
        this._translate.setDefaultLang("fr-FR");
        if (language) {
            this.language = language;
        } else {
            // Set your language here
            this.language = "fr-FR";
        }
        this._translateLanguage();
    }

    getDeviceLanguage() {
        if (!this.platform.is("android") || !this.platform.is("ios")) {
            //alert('Navigateur')
            console.log("Navigateur langage", navigator.language);
            this._initTranslate(navigator.language);
        } else {
            this.globalization
                .getPreferredLanguage()
                .then((res) => {
                    this._initTranslate(res.value);
                })
                .catch((e) => {
                    console.log(e);
                });
        }
    }
    /**END TRANSLATION */
}

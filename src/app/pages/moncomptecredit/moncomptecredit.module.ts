import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';
import {ComponentsModule} from '../../components/components.module';
import { IonicModule } from '@ionic/angular';

import { MoncomptecreditPage } from './moncomptecredit.page';
import { TranslateLoader, TranslateModule } from '@ngx-translate/core';
import { HttpLoaderFactory } from 'src/app/app.module';
import { HttpClient } from '@angular/common/http';
const routes: Routes = [
  {
    path: '',
    component: MoncomptecreditPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    TranslateModule.forChild({
        loader: {
          provide: TranslateLoader,
          useFactory: HttpLoaderFactory,
          deps: [HttpClient]
        }
      }),
    RouterModule.forChild(routes),
    ComponentsModule
  ],
  declarations: [MoncomptecreditPage]
})
export class MoncomptecreditPageModule {}

import { Component, OnInit} from '@angular/core';
import { LIENACCUEILCOMMERCIAL, LIENMONCOMPTECREDIT } from 'src/environments/environment';


@Component({
  selector: 'app-moncomptecredit',
  templateUrl: './moncomptecredit.page.html',
  styleUrls: ['./moncomptecredit.page.scss'],
})
export class MoncomptecreditPage implements OnInit {
  lienMonCompteCredit= LIENMONCOMPTECREDIT;
  lienAccueilCommercial = LIENACCUEILCOMMERCIAL ;
  constructor() { }

  ngOnInit() {

  }

}

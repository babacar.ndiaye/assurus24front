import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MoncomptecreditPage } from './moncomptecredit.page';

describe('MoncomptecreditPage', () => {
  let component: MoncomptecreditPage;
  let fixture: ComponentFixture<MoncomptecreditPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MoncomptecreditPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MoncomptecreditPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

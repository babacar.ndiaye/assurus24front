import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SelectionProfilPage } from './selection-profil.page';

describe('SelectionProfilPage', () => {
  let component: SelectionProfilPage;
  let fixture: ComponentFixture<SelectionProfilPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SelectionProfilPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SelectionProfilPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import { ContactClient } from './../../models/contact-client';
import { NavController, Platform } from '@ionic/angular';
import { Component, OnInit } from '@angular/core';
import { typeRoutineProfil, routineInscription, LIENCONTACT, LIENLOGIN, CLIENTPARTICULIERSESSION, CLIENTPROFESSIONNELSESSION, routineInscriptionPro } from 'src/environments/environment';
import { ClientParticulier } from 'src/app/models/client-particulier';
import { ClientProfessionnel } from 'src/app/models/client-professionnel';
import { ListeValeur } from 'src/app/models/liste-valeur';
import { Globalization } from '@ionic-native/globalization/ngx';
import { TranslateService } from '@ngx-translate/core';

const PERSONNEL ="personnel" ;
const PROFESSIONNEL = "professionnel";
const CONTACTSESSION ="contact" ;

@Component({
  selector: 'app-selection-profil',
  templateUrl: './selection-profil.page.html',
  styleUrls: ['./selection-profil.page.scss'],
})


export class SelectionProfilPage implements OnInit {
  infosContact : Map<string,string> =  new Map() ; ;

   clientParticulier = new ClientParticulier  ;
   clientProfessionnel = new ClientProfessionnel
   lienloginloc= LIENLOGIN ;
   typeClientParticulier = "typeClientParticulier" ;
   typeClientProfessionnel="typeClientProfessionnel" ;
   language: string;
  constructor(
      private navCtrl : NavController,
      private platform: Platform,
      private globalization: Globalization,
      private _translate: TranslateService
    ) {
    this.clientParticulier.civilite= new ListeValeur();


   }


  ngOnInit() {
  }
  personnel()
  {
    sessionStorage.setItem(typeRoutineProfil, JSON.stringify(routineInscription))
    this.enregistrerDonnées(this.typeClientParticulier) ;
    this.navCtrl.navigateRoot(LIENCONTACT);
  }
  professionnel(){
    sessionStorage.setItem(typeRoutineProfil, JSON.stringify(routineInscriptionPro))
    this.enregistrerDonnées(this.typeClientProfessionnel) ;
    this.navCtrl.navigateRoot(LIENCONTACT);

  }
  enregistrerDonnées(typeClient){
   switch(typeClient)
   {
       case this.typeClientParticulier :  window.sessionStorage.setItem(CLIENTPARTICULIERSESSION,JSON.stringify(this.clientParticulier)) ;
       break ;
       case this.typeClientProfessionnel :  window.sessionStorage.setItem(CLIENTPROFESSIONNELSESSION,JSON.stringify(this.clientProfessionnel)) ;
       break ;
   }

  }

   /**GESTION LOGIQUE TRANSLATION DES LANGUES */
   ionViewDidEnter(): void {
    this.getDeviceLanguage()
  }

  public changeLanguage(): void {
    this._translateLanguage();
  }

  _translateLanguage(): void {
    this._translate.use(this.language);
  }

  _initTranslate(language) {
    // Set the default language for translation strings, and the current language.
    this._translate.setDefaultLang('fr-FR');
    if (language) {
      this.language = language;
    }
    else {
      // Set your language here
      this.language = 'fr-FR';
    }
    this._translateLanguage();
  }

  getDeviceLanguage() {
    if (!this.platform.is('android') || !this.platform.is('ios')) {
      //alert('Navigateur')
      console.log('Navigateur langage', navigator.language)
      this._initTranslate(navigator.language);
    } else {
      this.globalization.getPreferredLanguage()
        .then(res => {
          this._initTranslate(res.value)
        })
        .catch(e => { console.log(e); });
    }
  }
  /**END TRANSLATION */


}

import { CompteService } from './../../services/compte.service';
import { ContactClient } from './../../models/contact-client';
import { Component, OnInit, ViewChild } from '@angular/core';
import {
    NavController,
    AlertController,
    ToastController,
    LoadingController,
    Platform,
} from '@ionic/angular';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MailLien } from 'src/app/models/mail-lien';
import { Compte } from 'src/app/models/compte';
import {
    CODE,
    CONTACTSESSION,
    INDICATIFDEFAUT,
    LOGIN,
    typeRoutineProfil,
    routineInscription,
    routineInscriptionPro,
    routineUpdateProfil,
    routineUpdateProfilPro,
    NBESSAISAUTORISES,
    CODECONFIRMATION,
    LIENLOGIN,
    routineInscriptionClientReseau,
    IDINSCRIPTION,
    LIENIDENTIFICATION,
    LIENUPLOADDOC,
    LIENACCUEILCLIENTLOGGE,
    LIENMYPROFILE,
    CLIENTPARTICULIERSESSION,
    LIENSELECTIONPROFIL,
    CodeHomme,
    codeFemme,
    SessionCompte,
    SessionCompteUpdate,
    routineResetPassword,
    CLIENTPROFESSIONNELSESSION,
    LIENLISTECONTACTProfessionnel,
    LISTECONTACTCLIENTPROFESSIONNELSESSION,
    LIENMESCLIENTS,
} from 'src/environments/environment';
import { UtilsService } from 'src/app/services/utils.service';
import { Client } from 'src/app/models/client';
import { ClientParticulier } from 'src/app/models/client-particulier';
import { Region } from 'src/app/models/region';
import { Pays } from 'src/app/models/pays';
import { ListeValeur } from 'src/app/models/liste-valeur';
import { PaysService } from 'src/app/services/pays.service';
import { ListeValeurService } from 'src/app/services/liste-valeur.service';
import { ClientParticulierService } from 'src/app/services/client-particulier.service';
import { ClientProfessionnel } from 'src/app/models/client-professionnel';
import { ClientProfessionnelService } from 'src/app/services/client-professionnel.service';
import { TranslateService } from '@ngx-translate/core';
import { Globalization } from '@ionic-native/globalization/ngx';
@Component({
    selector: 'app-contact',
    templateUrl: './contact.page.html',
    styleUrls: ['./contact.page.scss'],
})
export class ContactPage implements OnInit {
    public onContactForm: FormGroup;
    @ViewChild('indicatif') selectIndicatif;
    @ViewChild('telephone') inputTelephone;
    @ViewChild('selectRegion') selectRegion;
    @ViewChild('pays') selectPays;

    clientParticulier: ClientParticulier;
    clientProfessionnel: ClientProfessionnel;

    nombreEssais: number;
    formInvalide: boolean = false;
    typeRoutineProfilLoc = JSON.parse(
        sessionStorage.getItem(typeRoutineProfil)
    );
    routineInscriptionLoc = routineInscription;
    routineInscriptionProLoc = routineInscriptionPro;
    routineUpdateProfilLoc = routineUpdateProfil;
    routineUpdateProfilProLoc = routineUpdateProfilPro;
    routineResetPasswordLoc = routineResetPassword;
    routineInscriptionClientReseauLoc = routineInscriptionClientReseau;
    clientParticulierUpdate: ClientParticulier;
    clientProfessionnelUpdate: ClientProfessionnel;
    compteUpdate: Compte;
    allPays: Pays[] = [];
    allCivilites: ListeValeur[] = [];
    allRegions: Region[] = [];
    // objets civilites masculin  et féminin

    civiliteHomme: ListeValeur = {
        id: 17,
        type: 'LISTE_CIVILITE',
        description: 'Liste des civilités',
        code: 'Mr',
        libelle: 'Monsieur',
        dateSuppression: null,
    };

    civiliteFemme: ListeValeur = {
        id: 18,
        type: 'LISTE_CIVILITE',
        description: 'Liste des civilités',
        code: 'Mme',
        libelle: 'Madame',
        dateSuppression: null,
    };
    ch = 'Mr';
    cf = 'Mme';
    contactClient: ContactClient;
    datenais: string;
    // VARIABLE DES LANGUES
    language: string;
    nom: string;
    YearNow;
    //contactClient: import("c:/Users/bndiaye/Desktop/assurus24front/assurus24theme/src/app/models/contact").Contact;//Error not declared
    constructor(
        public navCtrl: NavController,
        private formBuilder: FormBuilder,
        public alertCtrl: AlertController,
        public toastCtrl: ToastController,
        public loadingCtrl: LoadingController,
        public serviceCompte: CompteService,
        public utilsService: UtilsService,
        public serviceClientParticulier: ClientParticulierService,
        public serviceClientProfessionnel: ClientProfessionnelService,
        public serviceListeValeur: ListeValeurService,
        public servicePays: PaysService,
        private platform: Platform,
        private globalization: Globalization,
        private _translate: TranslateService
    ) {
        //  console.log(JSON.parse(window.sessionStorage.getItem(CONTACTSESSION)));
        this.clientParticulier = new ClientParticulier();
        this.clientParticulier.ville = new Region();
        this.clientParticulier.pays = new Pays();
        this.clientParticulier.civilite = new ListeValeur();
        this.clientParticulierUpdate = new ClientParticulier();
        this.clientProfessionnel = new ClientProfessionnel();
        this.clientProfessionnel.ville = new Region();
        this.clientProfessionnel.pays = new Pays();
        this.compteUpdate = new Compte();
    }
    onChangeValue() {
        if (this.datenais.length == 2) {
            this.datenais = this.datenais+"-";
            console.log("mon date:",this.datenais);

        }
        if (this.datenais.length == 5) {
            this.datenais = this.datenais+"-";
            console.log("mon date:",this.datenais);
        }
        // var newdate = this.datenais.split("-").reverse().join("-");
        // console.log("newdate",newdate);
    }

    getPays(): void {
        this.servicePays.getPays().subscribe(
            (Pays) => {
                this.allPays = Pays;
                console.log(this.allPays);
            },
            (error) => {
                let message =
                    'Oups le serveur est introuvable verfiez votre connexion et recommencez svp';
                this.utilsService.alertNotification(message);
                this.retourPagePrécédente();
            }
        );
    }
    getCivilites(): void {
        this.serviceListeValeur.getListeCivilites().subscribe(
            (civilites) => {
                this.allCivilites = civilites;
                console.log(this.allCivilites);
                //Recuperer les civilités
                this.allCivilites.forEach((civilite) => {
                    if (civilite.code == CodeHomme)
                        this.civiliteHomme = civilite;
                    if (civilite.code == codeFemme)
                        this.civiliteFemme = civilite;
                    //console.log(this.clientParticulier.civilite.code == this.civiliteHomme.code)
                    // console.log(this.clientParticulier.civilite.code == this.civiliteFemme.code)
                });
            },
            (error) => {
                let message =
                    'Oups le serveur est introuvable verfiez votre connexion et recommencez svp';
                this.utilsService.alertNotification(message);
                this.retourPagePrécédente();
            }
        );
    }
    ionViewWillEnter() {
        this.getPays();
        this.typeRoutineProfilLoc = JSON.parse(
            sessionStorage.getItem(typeRoutineProfil)
        );
        //FOR PART
        if (
            this.typeRoutineProfilLoc == routineInscription ||
            this.typeRoutineProfilLoc == routineUpdateProfil ||
            this.typeRoutineProfilLoc == routineResetPassword ||
            this.typeRoutineProfilLoc == routineInscriptionClientReseau
        ) {
            this.getCivilites();
            console.log(this.typeRoutineProfilLoc);
            this.clientParticulier = JSON.parse(
                window.sessionStorage.getItem(CLIENTPARTICULIERSESSION)
            );

            if (this.clientParticulier == null) {
                this.clientParticulier = new ClientParticulier();
                this.clientParticulier.nom = '';
                this.clientParticulier.prenom = '';
                this.clientParticulier.civilite = new ListeValeur();
                this.clientParticulier.adresse = '';
                this.clientParticulier.dateDeNaissance = new Date();
                this.clientParticulier.email = '';
                this.clientParticulier.ville = new Region();
                this.clientParticulier.ville.nom = '';
                this.clientParticulier.pays = new Pays();
                this.clientParticulier.pays.nom = '';
            }
            this.initForm();
            // this.onContactForm
            //     .get('datenais')
            //     .setValue("07-10-1995");
        }

        //FOR PRO
        if (
            this.typeRoutineProfilLoc == routineInscriptionPro ||
            this.typeRoutineProfilLoc == routineUpdateProfilPro
        ) {
            this.clientProfessionnel = JSON.parse(
                window.sessionStorage.getItem(CLIENTPROFESSIONNELSESSION)
            );
            let monContact = JSON.parse(sessionStorage.getItem(LISTECONTACTCLIENTPROFESSIONNELSESSION));
            this.clientProfessionnel.contacts = monContact;

            if (this.clientProfessionnel == null) {
                this.clientProfessionnel = new ClientProfessionnel();
                this.clientProfessionnel.nom = '';
                this.clientProfessionnel.ninea = '';
                this.clientProfessionnel.registreCommerce = '';
                this.clientProfessionnel.adresse = '';
                this.clientProfessionnel.siteweb = '';
                this.clientProfessionnel.fax = '';
                this.clientProfessionnel.email = '';
                this.clientProfessionnel.ville = new Region();
                this.clientProfessionnel.ville.nom = '';
                this.clientProfessionnel.pays = new Pays();
                this.clientProfessionnel.pays.nom = '';
            }
            console.log(this.clientProfessionnel);
        }

        //this.getCiviliteClicked(this.clientParticulier.civilite)
        let login = window.sessionStorage.getItem(LOGIN);
        if (login == null) {
            //this.selectIndicatif.value = INDICATIFDEFAUT;
        } else {
            if (login.indexOf('"') >= 0)
                login = login.substring(1, login.length - 1);
            this.inputTelephone.value = login.substring(4);
            this.selectIndicatif.value = login.substring(0, 4);
        }
    }

    ngOnInit() {
        //ceci permet de récupérer l'année en cours
        this.YearNow = new Date().getFullYear();
        this.getDeviceLanguage()
        this.getPays();
        this.getCivilites();
        this.clientParticulier = new ClientParticulier();
        this.clientParticulier.civilite = new ListeValeur();
        this.clientParticulier.pays = new Pays();
        this.clientParticulier.ville = new Region();

        if (
            this.typeRoutineProfilLoc == routineInscription ||
            this.typeRoutineProfilLoc == routineUpdateProfil ||
            this.typeRoutineProfilLoc == routineResetPassword ||
            this.typeRoutineProfilLoc == routineInscriptionClientReseau
        ) {
            this.onContactForm = this.formBuilder.group({
                nom: [null, Validators.compose([Validators.required])],
                prenom: [null, Validators.compose([Validators.required])],
                civility: [null, Validators.compose([Validators.required])],
                telephone: [null, Validators.compose([Validators.required])],
                datenais: ['', Validators.compose([Validators.required, Validators.pattern('[0-3]{1}[0-9]{1}-[0-1]{1}[0-9]{1}-[1-2]{1}[0-9]{3}')])],
                adresse: [null, Validators.compose([Validators.required])],
                ville: [null, Validators.compose([Validators.required])],
                pays: [null, Validators.compose([Validators.required])],
                email: [
                    null,
                    Validators.compose([Validators.required, Validators.email]),
                ],
            });
        }

        if (
            this.typeRoutineProfilLoc == routineInscriptionPro ||
            this.typeRoutineProfilLoc == routineUpdateProfilPro
        ) {
            this.onContactForm = this.formBuilder.group({
                nom: [null, Validators.compose([Validators.required])],
                ninea: [null, Validators.compose([Validators.required])],
                registre: [null, Validators.compose([Validators.required])],
                telephone: [null, Validators.compose([Validators.required])],
                siteweb: [null, Validators.compose([Validators.required])],
                fax: [null, Validators.compose([Validators.required])],
                adresse: [null, Validators.compose([Validators.required])],
                ville: [null, Validators.compose([Validators.required])],
                pays: [null, Validators.compose([Validators.required])],
                email: [
                    null,
                    Validators.compose([Validators.required, Validators.email]),
                ],
            });
        }
        //this.initForm()
    }
    async continuer() {
        //Pour verification age
        let yearDiff = Number(this.YearNow) - Number(this.onContactForm.get('datenais').value.substr(6,4));
            console.log('year Diff',yearDiff);
        // Contrôle de la date
        if (this.onContactForm.get('datenais').value.substr(0,2) <= 0 || this.onContactForm.get('datenais').value.substr(0,2) > 31 || this.onContactForm.get('datenais').value.substr(3,2) < 1 || this.onContactForm.get('datenais').value.substr(3,2) > 12){
            this.utilsService.alertNotification(this._translate.instant('date_incorrecte'));
            this.formInvalide = true;
        }
        else if (!this.onContactForm.valid) {
            this.utilsService.alertNotification(this._translate.instant('tous_champs_non_remplis'));
            this.formInvalide = true;
        }else if( yearDiff < 18){
            this.utilsService.alertNotification("Vous devez être majeur pour vous inscrire");
        } else {
            let login = this.getLogin();
            let compte = new Compte();
            compte.telephone = login;
            const loader = await this.loadingCtrl.create({
                message: '',
            });

            loader.present();
            this.typeRoutineProfilLoc = JSON.parse(
                sessionStorage.getItem(typeRoutineProfil)
            );
            console.log(this.typeRoutineProfilLoc);
            switch (this.typeRoutineProfilLoc) {
                case routineInscription:
                    this.serviceCompte.validerTelephone(compte).subscribe(
                        async (resultat) => {
                            loader.dismiss();
                            this.typeRoutineProfilLoc = JSON.parse(
                                sessionStorage.getItem(typeRoutineProfil)
                            );
                            if (resultat) {
                                this.remplirInformationsContact().then(() => {
                                    window.sessionStorage.setItem(
                                        CLIENTPARTICULIERSESSION,
                                        JSON.stringify(this.clientParticulier)
                                    );
                                    window.sessionStorage.setItem(LOGIN, login);
                                    console.log(
                                        this.clientParticulier.civilite
                                    );
                                    this.navCtrl.navigateForward(
                                        LIENIDENTIFICATION
                                    );
                                });
                            } else {
                                this.alertTelephoneDejaUtilise();
                            }
                        },
                        (erreur) => {
                            this.erreurConnexion();
                        }
                    );

                    break;
                case routineInscriptionClientReseau:
                    this.serviceCompte
                        .validerTelephoneinReseau(compte, null)
                        .subscribe(
                            async (resultat) => {
                                loader.dismiss();

                                if (resultat) {
                                    this.remplirInformationsContact().then(
                                        () => {
                                            window.sessionStorage.setItem(
                                                CONTACTSESSION,
                                                JSON.stringify(
                                                    this.contactClient
                                                )
                                            );
                                            this.inscriptionClientDansReseau().then(
                                                () => {
                                                    this.navCtrl.navigateForward(
                                                        LIENUPLOADDOC
                                                    );
                                                }
                                            );
                                        }
                                    );
                                } else {
                                    this.alertTelephoneDejaUtilise();
                                }
                            },
                            (erreur) => {
                                this.erreurConnexion();
                            }
                        );

                    break;
                case routineUpdateProfil:
                    let aCompte = JSON.parse(
                        window.sessionStorage.getItem(SessionCompte)
                    );

                    if (aCompte.telephone == login) {
                        this.modifierContact(login);
                    } else {
                        loader.present();
                        this.serviceCompte.validerTelephone(compte).subscribe(
                            async (resultat) => {
                                loader.dismiss();
                                if (resultat) {
                                    this.envoyerCode();
                                } else {
                                    let changeLocation;

                                    changeLocation = await this.alertCtrl.create(
                                        {
                                            header: 'Notification',
                                            message:
                                                this._translate.instant('ce_telephone_a_deja_un_compte'),

                                            buttons: [
                                                {
                                                    text: this._translate.instant('changer_numero'),
                                                    handler: (data) => {},
                                                },
                                                {
                                                    text: this._translate.instant('se_connecter'),
                                                    handler: (data) => {
                                                        window.sessionStorage.removeItem(
                                                            CONTACTSESSION
                                                        );
                                                        window.sessionStorage.removeItem(
                                                            LOGIN
                                                        );
                                                        window.sessionStorage.removeItem(
                                                            CODE
                                                        );
                                                        this.navCtrl.navigateRoot(
                                                            LIENLOGIN
                                                        );
                                                    },
                                                },
                                            ],
                                        }
                                    );

                                    changeLocation.present();
                                }
                            },
                            (erreur) => {
                                loader.dismiss();
                                let messageInfo =
                                    this._translate.instant('erreur_de_connexion');
                                this.utilsService.alertNotification(
                                    messageInfo
                                );
                            }
                        );
                    }

                    break;
            }
        }
    }
    getLogin(): string {
        return this.selectIndicatif.value.concat(
            this.onContactForm.get('telephone').value
        );
    }
    async alertNotification(message) {
        let changeLocation;

        changeLocation = await this.alertCtrl.create({
            header: 'Notification',
            message: message,

            buttons: [
                {
                    text: 'ok',
                    handler: (data) => {},
                },
            ],
        });

        changeLocation.present();
    }
    getCiviliteClicked(civilite) {
        if (this.clientParticulier.civilite == null)
            this.clientParticulier.civilite = new ListeValeur();
        this.clientParticulier.civilite = civilite;
        console.log(this.onContactForm.get('civility').value);
        //this.utilsService.delay(2000)
    }
    async alertTelephoneDejaUtilise() {
        let changeLocation;

        changeLocation = await this.alertCtrl.create({
            header: 'Notification',
            message: this._translate.instant('ce_telephone_a_deja_un_compte'),
            buttons: [
                {
                    text: this._translate.instant('changer_numero'),
                    handler: (data) => {},
                },
                {
                    text: this._translate.instant('se__connecter'),
                    handler: (data) => {
                        this.typeRoutineProfilLoc = JSON.parse(
                            sessionStorage.getItem(typeRoutineProfil)
                        );
                        if (
                            this.typeRoutineProfilLoc == routineInscription ||
                            this.typeRoutineProfilLoc == routineUpdateProfil ||
                            this.typeRoutineProfilLoc == routineResetPassword ||
                            this.typeRoutineProfilLoc ==
                                routineInscriptionClientReseau
                        ) {
                            window.sessionStorage.removeItem(
                                CLIENTPARTICULIERSESSION
                            );
                        }
                        if (
                            this.typeRoutineProfilLoc == routineInscriptionPro
                        ) {
                            window.sessionStorage.removeItem(
                                CLIENTPROFESSIONNELSESSION
                            );
                        }

                        window.sessionStorage.removeItem(LOGIN);
                        window.sessionStorage.removeItem(CODE);
                        this.navCtrl.navigateRoot(LIENLOGIN);
                    },
                },
            ],
        });
        changeLocation.present();
    }
    erreurConnexion() {
        this.loadingCtrl.dismiss();
        let messageInfo = this._translate.instant('erreur_de_connexion');
        this.alertNotification(messageInfo);
    }
    async envoyerCode() {
        let telephone = this.getLogin();

        const loader = await this.loadingCtrl.create({
            message: this._translate.instant('envoi_en_cours'),
        });

        loader.present();

        let mailLien = new MailLien();
        let CodeConfirmation = this.utilsService.definirNombreAleatoire();
        console.log(CodeConfirmation);
        mailLien.mail = telephone;
        mailLien.code = CodeConfirmation;
        window.localStorage.setItem(CODECONFIRMATION, String(CodeConfirmation));

        this.serviceCompte.envoyerSms(mailLien).subscribe(
            async (resultat) => {
                loader.dismiss();
                if (resultat) {
                    this.alertRecupererCode();
                    await this.utilsService.delay(360000);
                    window.localStorage.removeItem(CODECONFIRMATION);
                } else {
                    let messageInfo = this._translate.instant("envoi_echoue_veuillez_reessayer");
                    this.utilsService.alertNotification(messageInfo);
                }
            },
            (erreur) => {
                loader.dismiss();
                let messageInfo =
                    this._translate.instant('erreur_de_connexion');
                this.utilsService.alertNotification(messageInfo);
            }
        );
    }

    async alertRecupererCode(
        message = this._translate.instant('code_de_validation')
    ) {
        const changeLocation = await this.alertCtrl.create({
            header: this._translate.instant('code_de_confirmation'),
            message: message,
            cssClass: 'alertCss',
            inputs: [
                {
                    name: 'codeconfirmation',
                    placeholder: this._translate.instant('entrer_code_de_confirmation'),
                    type: 'number',
                },
            ],
            buttons: [
                {
                    text: 'Cancel',
                    handler: (data) => {
                        console.log('Cancel clicked');
                    },
                },
                {
                    text: 'ok',
                    handler: async (data) => {
                        this.verifierCode(data.codeconfirmation);
                        //code enregistrement client et contact client si code confirlmation correct
                    },
                },
            ],
        });
        changeLocation.present();
    }

    async verifierCode(code) {
        const loader = await this.loadingCtrl.create({
            message: '',
        });

        loader.present();
        let codeStocke = window.localStorage.getItem(CODECONFIRMATION);
        if (codeStocke == null) {
            loader.dismiss();
            let messageInfo =
                this._translate.instant('Desolé le code a expiré veuillez recommencer il vous reste encore') +
                (NBESSAISAUTORISES - this.nombreEssais) +
                 this._translate.instant('essais');
            this.utilsService.alertNotification(messageInfo);
        } else if (codeStocke == code) {
            loader.dismiss();
            const toast = await this.toastCtrl.create({
                message: this._translate.instant('code_de_confirmation_correct'),
                duration: 3000,
                position: 'top',
                closeButtonText: 'OK',
                showCloseButton: true,
            });
            this.modifierContact(this.getLogin());

            toast.present();
        } else {
            this.nombreEssais++;
            if (this.nombreEssais == NBESSAISAUTORISES) {
                loader.dismiss();
                //votre compte a été bloqué apres a definir comment on bloque une personne
                let messageInfo =
                    this._translate.instant("Vous  avez épuisé le nombre d'essais autorisés veuillez recommencer");
                this.utilsService.alertNotification(messageInfo);

                this.navCtrl.navigateRoot(LIENACCUEILCLIENTLOGGE);
            } else {
                loader.dismiss();
                let messageInfo =
                    this._translate.instant('code incorrect veuillez réessayer ,il vous reste ') +
                    (NBESSAISAUTORISES - this.nombreEssais) +
                    this._translate.instant('essais');
                this.alertRecupererCode(messageInfo);
            }
        }
    }
    modifierContact(login) {
        if (this.clientParticulierUpdate == null) {
            let message = this._translate.instant('session_expiree');
            this.utilsService.alertNotification(message);
            this.navCtrl.navigateRoot(LIENLOGIN);
        }
        this.clientParticulierUpdate = this.clientParticulier;

        this.clientParticulierUpdate.nom = this.onContactForm.get('nom').value;
        this.clientParticulierUpdate.prenom = this.onContactForm.get(
            'prenom'
        ).value;

        /**
                *  *  Liste valeur civilité
                *  ! Sans cette manipulation on pourra pas modifier les infos
                *  ? On pose une condition pour vérifier afin de pouvoir créé l'objet nécessaire à la modification
                * // For strikethrough
                * TODO: Pour chaque cas ou la condition est vrai on crée un objet civilité
                */
        this.clientParticulierUpdate.civilite = new ListeValeur();
        if(this.onContactForm.get(
            'civility'
        ).value == 'Mr'){
            this.clientParticulierUpdate.civilite =  {
                id: 17,
                type: 'LISTE_CIVILITE',
                description: 'Liste des civilités',
                code: 'Mr',
                libelle: 'Monsieur',
                dateSuppression: null,
            };
        }else{
            this.clientParticulierUpdate.civilite =  {
                id: 18,
                type: 'LISTE_CIVILITE',
                description: 'Liste des civilités',
                code: 'Mme',
                libelle: 'Madame',
                dateSuppression: null,
            };

        }
        // this.clientParticulierUpdate.civilite = this.onContactForm.get(
        //     'civility'
        // ).value;
        this.clientParticulierUpdate.adresse = this.onContactForm.get(
            'adresse'
        ).value;
        //Formatage date
        let formDate = this.onContactForm.get(
            'datenais'
        ).value;
        console.log("Date Naiis on update before format", formDate);
        var newdate = formDate.split("-").reverse().join("-");
        console.log("Date Naiis on update", newdate);
        this.clientParticulierUpdate.dateDeNaissance = newdate;
        this.clientParticulierUpdate.email = this.onContactForm.get(
            'email'
        ).value;
        this.clientParticulierUpdate.ville = new Region();
        this.clientParticulierUpdate.pays = new Pays();
        this.clientParticulierUpdate.ville = this.onContactForm.get(
            'ville'
        ).value;
        this.clientParticulierUpdate.pays = this.onContactForm.get(
            'pays'
        ).value;

        this.compteUpdate = JSON.parse(
            window.sessionStorage.getItem(SessionCompte)
        );
        this.compteUpdate.telephone = login;
        this.compteUpdate.client = null;
        this.clientParticulierUpdate.compte = this.compteUpdate;

        window.sessionStorage.setItem(
            SessionCompteUpdate,
            JSON.stringify(this.compteUpdate)
        );
        //console.log(this.contactUpdate);
        this.valider();
    }
    modifierContactPro(login) {
        if (this.clientProfessionnelUpdate == null) {
            let message = 'Oups la session a  expiré veuillez vous reconnecter';
            this.utilsService.alertNotification(message);
            this.navCtrl.navigateRoot(LIENLOGIN);
        }
        this.clientProfessionnelUpdate = this.clientProfessionnel;

        this.clientProfessionnelUpdate.nom = this.onContactForm.get(
            'nom'
        ).value;
        this.clientProfessionnelUpdate.ninea = this.onContactForm.get(
            'ninea'
        ).value;
        this.clientProfessionnelUpdate.registreCommerce = this.onContactForm.get(
            'registre'
        ).value;
        this.clientProfessionnelUpdate.adresse = this.onContactForm.get(
            'adresse'
        ).value;
        this.clientProfessionnelUpdate.email = this.onContactForm.get(
            'email'
        ).value;
        this.clientProfessionnelUpdate.fax = this.onContactForm.get(
            'fax'
        ).value;
        this.clientProfessionnelUpdate.siteweb = this.onContactForm.get(
            'siteweb'
        ).value;
        this.clientProfessionnelUpdate.ville = new Region();
        this.clientProfessionnelUpdate.pays = new Pays();
        this.clientProfessionnelUpdate.ville = this.onContactForm.get(
            'ville'
        ).value;
        this.clientProfessionnelUpdate.pays = this.onContactForm.get(
            'pays'
        ).value;

        this.compteUpdate = JSON.parse(
            window.sessionStorage.getItem(SessionCompte)
        );
        this.compteUpdate.telephone = login;
        this.compteUpdate.client = null;
        this.clientProfessionnelUpdate.compte = this.compteUpdate;

        window.sessionStorage.setItem(
            SessionCompteUpdate,
            JSON.stringify(this.compteUpdate)
        );
        //console.log(this.contactUpdate);
        this.validerPro();
    }

    valider() {
        if (this.compteUpdate != null) {
            console.log('Avant modif client Pariculier');
            console.log(this.clientParticulierUpdate);

            // Comment for test before update
            this.serviceClientParticulier
                .updateClient(this.clientParticulierUpdate)
                .subscribe(
                    async (compte) => {
                        if (compte) {
                            const toast = await this.toastCtrl.create({
                                message: this._translate.instant('modifications_validées'),
                                duration: 5000,
                                position: 'top',
                                closeButtonText: 'OK',
                                showCloseButton: true,
                            });

                            toast.present();

                            window.sessionStorage.setItem(
                                SessionCompte,
                                JSON.stringify(compte)
                            );
                            console.log('Apres modif client Pariculier');
                            console.log(compte);

                            window.sessionStorage.removeItem(
                                SessionCompteUpdate
                            );
                            window.sessionStorage.setItem(
                                typeRoutineProfil,
                                JSON.stringify(null)
                            );
                            this.loadingCtrl.dismiss();
                            this.navCtrl.navigateRoot(LIENMYPROFILE);
                        } else {
                            this.loadingCtrl.dismiss();
                            let messageInfo =
                                this._translate.instant('erreur_de_connexion');

                            this.utilsService.alertNotification(messageInfo);
                        }
                    },
                    (erreur) => {
                        this.loadingCtrl.dismiss();
                        let messageInfo = this._translate.instant('erreur_de_connexion');

                        this.utilsService.alertNotification(messageInfo);
                    }
                );
        }
    }
    validerPro() {
        if (this.compteUpdate != null) {
            console.log('avant modif');
            console.log(this.clientProfessionnelUpdate);
            this.serviceClientProfessionnel
                .updateClient(this.clientProfessionnelUpdate)
                .subscribe(
                    async (compte) => {
                        if (compte) {
                            const toast = await this.toastCtrl.create({
                                message: this._translate.instant('modifications_valide'),
                                duration: 5000,
                                position: 'top',
                                closeButtonText: 'OK',
                                showCloseButton: true,
                            });

                            toast.present();

                            window.sessionStorage.setItem(
                                SessionCompte,
                                JSON.stringify(compte)
                            );
                            console.log('apres modif');
                            console.log(compte);

                            window.sessionStorage.removeItem(
                                SessionCompteUpdate
                            );
                            window.sessionStorage.setItem(
                                typeRoutineProfil,
                                JSON.stringify(null)
                            );
                            this.loadingCtrl.dismiss();
                            this.navCtrl.navigateRoot(LIENMYPROFILE);
                        } else {
                            this.loadingCtrl.dismiss();
                            let messageInfo =
                                'Erreur connexion veuillez réessayer';

                            this.utilsService.alertNotification(messageInfo);
                        }
                    },
                    (erreur) => {
                        this.loadingCtrl.dismiss();
                        let messageInfo = this._translate.instant('erreur_connexion');

                        this.utilsService.alertNotification(messageInfo);
                    }
                );
        }
    }
    async remplirInformationsContact() {
        let selectcivilite = this.clientParticulier.civilite;
        this.clientParticulier = JSON.parse(
            window.sessionStorage.getItem(CLIENTPARTICULIERSESSION)
        );
        if (this.clientParticulier == null) {
            this.clientParticulier = new ClientParticulier();
            this.clientParticulier.ville = new Region();
            this.clientParticulier.pays = new Pays();
            this.clientParticulier.civilite = new ListeValeur();
        }
        this.clientParticulier.nom = this.onContactForm.get('nom').value;
        this.clientParticulier.prenom = this.onContactForm.get('prenom').value;
        this.clientParticulier.civilite = new ListeValeur();
        this.clientParticulier.civilite = selectcivilite;
        this.clientParticulier.adresse = this.onContactForm.get(
            'adresse'
        ).value;
        //Formatage date
        let formDate = this.onContactForm.get(
            'datenais'
        ).value;
        var newdate = formDate.split("-").reverse().join("-");
        this.clientParticulier.dateDeNaissance = newdate;
        // this.clientParticulier.dateDeNaissance = this.onContactForm.get(
        //     'datenais'
        // ).value;
        this.clientParticulier.email = this.onContactForm.get('email').value;
        this.clientParticulier.ville = new Region();
        this.clientParticulier.pays = new Pays();
        this.clientParticulier.ville = this.onContactForm.get('ville').value;
        this.clientParticulier.pays = this.onContactForm.get('pays').value;
    }
    async remplirInformationsContactPro() {
        this.clientProfessionnel = JSON.parse(
            window.sessionStorage.getItem(CLIENTPROFESSIONNELSESSION)
        );
        if (this.clientProfessionnel == null) {
            this.clientProfessionnel = new ClientProfessionnel();
            this.clientProfessionnel.ville = new Region();
            this.clientProfessionnel.pays = new Pays();
        }
        this.clientProfessionnel.nom = this.onContactForm.get('nom').value;
        this.clientProfessionnel.ninea = this.onContactForm.get('ninea').value;
        this.clientProfessionnel.registreCommerce = this.onContactForm.get(
            'registre'
        ).value;
        this.clientProfessionnel.adresse = this.onContactForm.get(
            'adresse'
        ).value;
        this.clientProfessionnel.email = this.onContactForm.get('email').value;
        this.clientProfessionnel.siteweb = this.onContactForm.get(
            'siteweb'
        ).value;
        this.clientProfessionnel.fax = this.onContactForm.get('fax').value;
        this.clientProfessionnel.ville = new Region();
        this.clientProfessionnel.pays = new Pays();
        this.clientProfessionnel.ville = this.onContactForm.get('ville').value;
        this.clientProfessionnel.pays = this.onContactForm.get('pays').value;
    }
    /**
     * fonction qui permet d'ajouter le client au reseau
     * fonction à modifier selon la structure du modele pour le vendeurs
     */
    async inscriptionClientDansReseau() {
        let client = new Client();
        client.compte.contact = this.contactClient;
        this.serviceCompte.inscriptionClientReseau(client, null).subscribe(
            async (contact) => {
                if (contact) {
                    const toast = await this.toastCtrl.create({
                        message:
                        this._translate.instant('felicitataion') +
                            contact.prenom +
                            this._translate.instant('votre compte a bien été ajouté comme client'),
                        duration: 5000,
                        position: 'top',
                        closeButtonText: 'OK',
                        showCloseButton: true,
                    });
                    toast.present();
                    console.log(contact.compte.id);
                    window.sessionStorage.setItem(
                        IDINSCRIPTION,
                        String(contact.compte.id)
                    );
                    window.sessionStorage.removeItem(CONTACTSESSION);
                    window.sessionStorage.removeItem(LOGIN);
                    window.sessionStorage.removeItem(CODE);

                    this.navCtrl.navigateRoot(LIENUPLOADDOC);
                } else {
                    let messageInfo = this._translate.instant("erreur_connexion");

                    this.utilsService.alertNotification(messageInfo);
                }
            },
            (erreur) => {
                this.loadingCtrl.dismiss();
                let messageInfo = this._translate.instant("erreur_connexion");

                this.utilsService.alertNotification(messageInfo);
            }
        );
    }
    chargerRegionsduPays() {
        this.selectRegion.value = null;
        this.allRegions = this.onContactForm.get('pays').value.regions;
        console.log(this.allRegions);
    }
    initForm() {
        if (this.clientParticulier.civilite != null) {
            this.onContactForm
                .get('civility')
                .setValue(this.clientParticulier.civilite.code);

            // this.getCiviliteClicked(this.clientParticulier.civilite)
            console.log(this.clientParticulier.civilite);
            console.log(this.onContactForm.get('civility').value);
            console.log("Babs update");
            console.log("Date Naiss", this.clientParticulier.dateDeNaissance)
            let dateUpade = this.clientParticulier.dateDeNaissance
            var convertDate = dateUpade.toString().split("-").reverse().join("-");
            console.log("convertDate",convertDate);
            this.onContactForm
                .get('datenais')
                .setValue(convertDate);
            this.clientParticulier.dateDeNaissance =  this.onContactForm.get(
                'datenais'
            ).value;
        }
    }
    compareWith = (o1: any, o2: any) => {
        return o1 && o2 ? o1.id === o2.id : o1 === o2;
    };
    cw = this.compareWith;

    async continuerpro() {
        if (!this.onContactForm.valid) {
            this.utilsService.alertNotification(
                this._translate.instant("champ_non_remplis")
            );
            this.formInvalide = true;
        } else {
            let login = this.getLogin();
            let compte = new Compte();
            compte.telephone = login;
            const loader = await this.loadingCtrl.create({
                message: '',
            });

            loader.present();
            this.typeRoutineProfilLoc = JSON.parse(
                sessionStorage.getItem(typeRoutineProfil)
            );
            console.log("typeRoutineProfilLoc",this.typeRoutineProfilLoc);
            switch (this.typeRoutineProfilLoc) {
                case routineInscriptionPro:
                    this.serviceCompte.validerTelephone(compte).subscribe(
                        async (resultat) => {
                            loader.dismiss();
                            this.typeRoutineProfilLoc = JSON.parse(
                                sessionStorage.getItem(typeRoutineProfil)
                            );
                            if (resultat) {
                                console.log('telephone valide');

                                this.remplirInformationsContactPro().then(
                                    () => {
                                        console.log('info remplis');
                                        //Ajout des contacts de l'entreprise
                                        let listeContact = JSON.parse(
                                            sessionStorage.getItem(
                                                LISTECONTACTCLIENTPROFESSIONNELSESSION
                                            )
                                        );
                                        console.log("liste des clients:",listeContact);
                                        if (listeContact != null)
                                            //testerrrrrrrrr
                                            this.clientProfessionnel.contacts = listeContact;

                                        window.sessionStorage.setItem(
                                            CLIENTPROFESSIONNELSESSION,
                                            JSON.stringify(
                                                this.clientProfessionnel
                                            )
                                        );
                                        window.sessionStorage.setItem(
                                            LOGIN,
                                            login
                                        );
                                        this.navCtrl.navigateForward(
                                            LIENIDENTIFICATION
                                        );
                                    }
                                );
                            } else {
                                this.alertTelephoneDejaUtilise();
                            }
                        },
                        (erreur) => {
                            this.erreurConnexion();
                        }
                    );

                    break;
                case routineInscriptionClientReseau:
                    this.serviceCompte
                        .validerTelephoneinReseau(compte, null)
                        .subscribe(
                            async (resultat) => {
                                loader.dismiss();

                                if (resultat) {
                                    this.remplirInformationsContact().then(
                                        () => {
                                            window.sessionStorage.setItem(
                                                CONTACTSESSION,
                                                JSON.stringify(
                                                    this.contactClient
                                                )
                                            );
                                            this.inscriptionClientDansReseau().then(
                                                () => {
                                                    this.navCtrl.navigateForward(
                                                        LIENUPLOADDOC
                                                    );
                                                }
                                            );
                                        }
                                    );
                                } else {
                                    this.alertTelephoneDejaUtilise();
                                }
                            },
                            (erreur) => {
                                this.erreurConnexion();
                            }
                        );

                    break;
                case routineUpdateProfilPro:
                    let aCompte = JSON.parse(
                        window.sessionStorage.getItem(SessionCompte)
                    );

                    console.log(aCompte);
                    if (aCompte.telephone == login) {
                        this.modifierContactPro(login);
                    } else {
                        loader.present();
                        this.serviceCompte.validerTelephone(compte).subscribe(
                            async (resultat) => {
                                loader.dismiss();
                                if (resultat) {
                                    this.envoyerCode();
                                } else {
                                    let changeLocation;

                                    changeLocation = await this.alertCtrl.create(
                                        {
                                            header: 'Notification',
                                            message:
                                            this._translate.instant("ce_telephone_a_deja_un_compte"),

                                            buttons: [
                                                {
                                                    text: this._translate.instant('changer_numero'),
                                                    handler: (data) => {},
                                                },
                                                {
                                                    text:  this._translate.instant('se_connecter'),
                                                    handler: (data) => {
                                                        window.sessionStorage.removeItem(
                                                            CONTACTSESSION
                                                        );
                                                        window.sessionStorage.removeItem(
                                                            LOGIN
                                                        );
                                                        window.sessionStorage.removeItem(
                                                            CODE
                                                        );
                                                        this.navCtrl.navigateRoot(
                                                            LIENLOGIN
                                                        );
                                                    },
                                                },
                                            ],
                                        }
                                    );

                                    changeLocation.present();
                                }
                            },
                            (erreur) => {
                                loader.dismiss();
                                let messageInfo =
                                this._translate.instant("erreur_de_connexion");
                                this.utilsService.alertNotification(
                                    messageInfo
                                );
                            }
                        );
                    }

                    break;
            }
        }
    }

    ajouterContactsClient() {
        let login = this.getLogin();
        this.remplirInformationsContactPro().then(() => {
            console.log('info remplis');
            window.sessionStorage.setItem(
                CLIENTPROFESSIONNELSESSION,
                JSON.stringify(this.clientProfessionnel)
            );
            window.sessionStorage.setItem(LOGIN, login);
            if (this.clientProfessionnel.contacts != null) {
                sessionStorage.setItem(
                    LISTECONTACTCLIENTPROFESSIONNELSESSION,
                    JSON.stringify(this.clientProfessionnel.contacts)
                );
            } else {
                let liste = [];
                sessionStorage.setItem(
                    LISTECONTACTCLIENTPROFESSIONNELSESSION,
                    JSON.stringify(liste)
                );
            }
            this.navCtrl.navigateForward(LIENLISTECONTACTProfessionnel);
        });
    }
    retourPagePrécédente() {
        this.typeRoutineProfilLoc = JSON.parse(
            sessionStorage.getItem(typeRoutineProfil)
        );
        console.log(this.typeRoutineProfilLoc);
        switch (this.typeRoutineProfilLoc) {
            case routineInscription:
            case routineInscriptionPro:
                this.navCtrl.navigateRoot(LIENSELECTIONPROFIL);
                break;
            case routineUpdateProfil:
            case routineUpdateProfilPro:
                this.navCtrl.navigateRoot(LIENMYPROFILE);
                break;
            case routineInscriptionClientReseau:
                this.navCtrl.navigateRoot(LIENMESCLIENTS);
                break;
            case routineResetPassword:
                this.navCtrl.navigateRoot(LIENLOGIN);
                break;
            default:
                this.navCtrl.navigateRoot(LIENLOGIN);
        }
    }

     /**GESTION LOGIQUE TRANSLATION DES LANGUES */
     ionViewDidEnter(): void {
        this._translateLanguage()
      }
      ionViewDidLoad() {
        this._translateLanguage();
      }

  public changeLanguage(): void {
    this._translateLanguage();
  }

  _translateLanguage(): void {
    this._translate.use(this.language);
    this._initialiseTranslation()
  }

  _initialiseTranslation() : void
  {
  }
  _initTranslate(language) {
    // Set the default language for translation strings, and the current language.
    this._translate.setDefaultLang('fr-FR');
    if (language) {
      this.language = language;
    }
    else {
      // Set your language here
      this.language = 'fr-FR';
    }
    this._translateLanguage();
  }

  getDeviceLanguage() {
    if (!this.platform.is('android') || !this.platform.is('ios')) {
      //alert('Navigateur')
      this._initTranslate(navigator.language);
    } else {
      this.globalization.getPreferredLanguage()
        .then(res => {
          this._initTranslate(res.value)
        })
        .catch(e => { console.log(e); });
    }
  }
  /**END TRANSLATION */

}

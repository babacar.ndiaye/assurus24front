
import { Component, OnInit, ViewChild } from '@angular/core';
import { NavController, MenuController, AlertController, LoadingController, IonSlides } from '@ionic/angular';
import { ActivatedRoute } from '@angular/router';
import { CONTRATSESSION, OBJETSESSION, UrlApi, ConditionsSpeciale, ConditionsDisposition, messageContrat, CNIPATHNAME, downloadCarteGrise, downloadControleTechnique, CONTROLETECHNIQUEPATHNAME, CARTEGRISEPATHNAME, CONDITIONSPATHNAME, downloadConditionsGenerales, contratActifenSession, typeRoutine, typeRoutineNewContrat, operationActifenSession, typeRoutineRenewContrat, startRenouvellement, objetActifenSession, UrlApiLogo, UrlApiLogoReseau, statutContratEnCoursDeValidation, statutContratActif, statutContratValideNonActif, statutContratSuspendu, statutContratResilie, statutContratExpire, OperationActifDeplie, listeRoutineContratSession, LIENLOGIN, LIENASSURINFOVEVEHICULEFLOTTE, SessionCompte, OriginePageMonObjet } from 'src/environments/environment';
import { Contrat2 } from 'src/app/models/contrat';
import { ObjetAssures } from 'src/app/models/objet-assures';
import { Operation } from 'src/app/models/operation';
import { BA } from 'src/app/models/ba';
import { CompteService } from 'src/app/services/compte.service';
import { Automobile } from 'src/app/models/automobile';
import { ControleTechnique } from 'src/app/models/controle-technique';
import { ConditionGenerale } from 'src/app/models/condition-generale';
import { UtilsService } from 'src/app/services/utils.service';
import { CA } from 'src/app/models/ca';
import { Client } from 'src/app/models/client';
import { RoutineContrat } from 'src/app/pojos/routine-contrat';
import { ContratService } from 'src/app/services/contrat.service';

@Component({
    selector: 'app-contrat',
    templateUrl: './contrat.page.html',
    styleUrls: ['./contrat.page.scss'],
})
export class ContratPage {
    @ViewChild('slideWithNavGaranties') slideWithNavGaranties: IonSlides;

    @ViewChild('slideWithNavMessages') slideWithNavMessages: IonSlides;
    sliderOne: any;



    //Configuration for each Slider
    slideOptsOne = {
        initialSlide: 0,
        slidesPerView: 1,
        autoplay: true,
        speed: 2000


    };

    public itemsOperation: any = [];


    contrat:Contrat2;

    logoCA: String;
    ba: BA;

    listeSinistres: string;
    date: Date;

    urlApi = UrlApi;
    urlApiLogo = UrlApiLogo;
    urlApiLogoReseau = UrlApiLogoReseau;
    titreStatutVisible: boolean;
    infoClientVisible: boolean;
    condDispVisible: boolean;
    infoPropVisible: boolean;
    historiqueVisible: boolean;
    operationVisible: boolean;
    sinistreVisible: boolean;
    titreStatutParam = "titreStatu"
    infoClientParam = "infoClientParam"
    condDispParam = "condDispParam"
    infoPropParam = "infoPtopParam"
    historiqueParam = "historiqueParam"
    operationParam = "operationParams"
    sinistreParam = "sinistreParams"
    zippedDetailOperation = "zippedDetailOperation"
    zippedPeriode = "zippedPeriode"
    zippedConditionsSpeciales = "zippedConditionsSpeciales"
    zippedPaiement = "zippedPaiement"
    zippedDocument = "zippedDocument"
    zippedObjetsAssures = "zippedObjet"
    operationActif = new Operation()
    //liste pour le renouvellement
    listeRoutineContrat = [];
    slideChanged(slideView) {
        switch (slideView) {
            case "messages": this.slideWithNavMessages.startAutoplay(); break;
            case "garanties": this.slideWithNavGaranties.startAutoplay(); break;
        }
    }
    colonneReseau: boolean = false;
    statutContratEnCoursDeValidationLoc = statutContratEnCoursDeValidation
    statutContratActifLoc = statutContratActif
    statutContratValideNonActifLoc = statutContratValideNonActif
    statutContratSuspenduLoc = statutContratSuspendu
    statutContratResilieLoc = statutContratResilie
    statutContratExpireLoc = statutContratExpire
    connectedClient: Client;
    TabOperationsContrat: Operation[] = [];
    constructor(

        private alertCtrl: AlertController,
        private navCtrl: NavController,
        private serviceCompte: CompteService,
        private serviceContrat: ContratService,
        private loadingCtrl: LoadingController,
        public utilsService: UtilsService
    ) {
        this.LoadClientConnectedFromSession();
        this.chargerInfos();
        //  this.contrat.operations=[] ;
        //  this.contrat.ca = new CA() ;
        //  this.contrat.client =  new Client() ;
        //  this.objet.controleTechnique = new ControleTechnique() ;


    }
    ionViewWillEnter() {
        this.LoadClientConnectedFromSession();
        console.log(this.contrat.dateEffet);
        this.chargerInfos().then(() => {
            console.log(this.contrat.operations)
            this.initialiseitemsOperation();
            //this.verifierIfColonneReseau();
            //this.initialiserSectiontoInvisible();
        });


    }

    /**
     * * Cette fonction permet de récuperer
     * * le client connecté en session
     */
    LoadClientConnectedFromSession() {
        if (window.sessionStorage.getItem(SessionCompte)) {
            let data = JSON.parse(window.sessionStorage.getItem(SessionCompte));
            this.connectedClient = data.client;
            console.log("Client connected", this.connectedClient);

            }
    }


    async chargerInfos() {
        this.contrat = new Contrat2();
        this.contrat = JSON.parse(window.sessionStorage.getItem(CONTRATSESSION));
        let origineNavigation =  JSON.parse(window.sessionStorage.getItem(OriginePageMonObjet));


        if (this.contrat == null) {
            this.utilsService.alertNotification("La session a expiré veuillez vous reconnecter");
            this.navCtrl.navigateRoot(LIENLOGIN);

        }

        this.utilsService.getDate().subscribe(
            dateFormatMobile => { this.date = new Date(dateFormatMobile.date) },
            error => {
                let message = "Oups le serveur est introuvable verfiez votre connexion et recommencez svp";
                this.utilsService.alertNotification(message);
                this.navCtrl.navigateRoot(LIENLOGIN);
            });
            if(origineNavigation == true){
                this.serviceContrat.getOperationByContrat(this.contrat.id)
            .subscribe(
                (data) => {
                    if (data) {
                        console.log("OperationsContrat", data);
                        if(data.length != 0){
                            this.contrat.operations = data;
                            this.initialiseitemsOperation();
                            // console.log("OperationsContrat 2", this.contrat.operations);
                            // this.contrat.operations.forEach((item) => {
                            //     this.TabOperationsContrat.push(item);
                            // })
                        }
                    }
                },
                (error) => {
                    console.log(error);
                }
            );
            }

            //A gerer quand les operations seront disponibles
        // if (this.contrat.operations[this.contrat.operations.length - 1].garanties[0] != null)
        //     this.ba = this.contrat.operations[this.contrat.operations.length - 1].garanties[0].ba;

        this.traitementDate();

        //A gerer quand les operations seront disponibles
        // this.contrat.operations.sort((a, b) => {
        //     let res = this.compare(a, b)
        //     console.log(res);

        //     return res



        // }
        // );


        //console.log(this.contrat.operations);
        //console.log(this.contrat.operations[this.contrat.operations.length-1].paiement.montant) ;


    }



    //A gerer quand sinitres s
    recupererlisteSinistres() {
        // let liste = " ";
        // this.contrat.operations[this.contrat.operations.length - 1].sinistre.forEach(element => {
        //     element.date = new Date(element.date);
        //     liste = liste.concat("Numero : " + element.id + "date : " + element.date.getDate() + "/" + (element.date.getMonth() + 1) + "/" + element.date.getFullYear() + "Type :" + element.typeSinistre.libelle + "<br/>");

        // });
        // this.listeSinistres = liste;

    }
    //ici on traite toutes les dates pour pouvoir les manipuler
    traitementDate() {
        console.log(this.contrat)
        this.contrat.dateEffet = new Date(this.contrat.dateEffet);
        this.contrat.dateCloture = new Date(this.contrat.dateCloture);

        // if (this.contrat.operations != null && this.contrat.operations.length != 0) {
        //     this.contrat.operations.forEach(element => {
        //         console.log(element)
        //         element.dateEffet = new Date(element.dateEffet);
        //         element.dateCloture = new Date(element.dateCloture);

        //     });
        //     this.contrat.operations.sort((a, b) => a.dateEffet - b.dateEffet)
        // }
        //A faire plus tard
        // if (this.contrat.operations[this.contrat.operations.length - 1].sinistres != null && this.contrat.operations[this.contrat.operations.length - 1].sinistres.length != 0) {
        //     this.contrat.operations[this.contrat.operations.length - 1].sinistres.forEach(element => {
        //         element.date = new Date(element.date);
        //         element.date = new Date(element.date);

        //     });

        // }





    }
    // async chargerControleTechnique(){
    //   const loader = await this.loadingCtrl.create({
    //     message: '',
    //   });

    //   loader.present() ;
    //   // To download the PDF file
    //   this.serviceCompte.download(CONTROLETECHNIQUEPATHNAME,this.objet.id,downloadControleTechnique) ;
    // }
    // async chargerCarteGrise(){
    //   const loader = await this.loadingCtrl.create({
    //     message: '',
    //   });

    //   loader.present() ;
    //   // To download the PDF file
    //   this.serviceCompte.download(CARTEGRISEPATHNAME,this.objet.id,downloadCarteGrise) ;
    // }
    recupererConditionsGarantie(garantie): ConditionGenerale {
        //on va recuperer les conditions de la garantie  proposé par la compagnie d'assurnace
        let conditionsGenerales: ConditionGenerale = {

            id: 1,

            conditionsSpeciales: ["Les garanties sont valables pour la traversée de la gambie", "les garanties sont valables pour la casamance", "les garanties sont valables pour la casamance", "les garanties sont valables pour la casamance", "les garanties sont valables pour la casamance", "les garanties sont valables pour la casamance", "les garanties sont valables pour la casamance", "les garanties sont valables pour la casamance"],
            garantie: null,

        }

        return conditionsGenerales;
    }
    async chargerConditionsGenerales(id) {
        const loader = await this.loadingCtrl.create({
            message: '',
        });

        loader.present();
        // To download the PDF file
        this.serviceCompte.download(CONDITIONSPATHNAME, id, downloadConditionsGenerales);
    }
    displayDate(date: Date): String {
        let jour, mois;
        let dd = date.getDate();

        let mm = date.getMonth() + 1;
        let yyyy = date.getFullYear();
        if (dd < 10) {
            jour = '0' + dd;
        }
        else
            jour = dd;

        if (mm < 10) {
            mois = '0' + mm;
        }
        else
            mois = mm;
        return jour + "/" + mois + "/" + yyyy;
    }
    renouveller() {

        this.utilsService.initialiserSessionRoutine();
        this.initialiserRoutineRenouvellement().then(
            () => {
                console.log(this.listeRoutineContrat[0])
                sessionStorage.setItem(contratActifenSession, JSON.stringify(this.listeRoutineContrat[0].contrat));
                sessionStorage.setItem(operationActifenSession, JSON.stringify(this.listeRoutineContrat[0].operation));
                sessionStorage.setItem(objetActifenSession, JSON.stringify(this.listeRoutineContrat[0].objet));
                sessionStorage.setItem(startRenouvellement, JSON.stringify(true));
                sessionStorage.setItem(typeRoutine, JSON.stringify(typeRoutineRenewContrat));
                sessionStorage.setItem(listeRoutineContratSession, JSON.stringify(this.listeRoutineContrat))
                this.navCtrl.navigateForward(LIENASSURINFOVEVEHICULEFLOTTE)

            }
        )




    }
    verifierIfColonneReseau() {
        //A gerer
        // if (this.contrat.reseau != null) {
        //     this.colonneReseau = true
        // }



    }
    initialiserSectiontoInvisible() {
        this.titreStatutVisible = true;
        this.infoClientVisible = false;
        this.condDispVisible = false;
        this.infoPropVisible = false;
        this.historiqueVisible = false;
        this.operationVisible = false;
        this.sinistreVisible = false;

    }
    affichageDetails(section, visible: boolean) {
        switch (section) {
            case this.titreStatutParam:
                this.titreStatutVisible = visible
                break;
            case this.infoClientParam:
                this.infoClientVisible = visible
                break;
            case this.condDispParam:
                this.condDispVisible = visible
                break;
            case this.infoPropParam:
                this.infoPropVisible = visible
                break;
            case this.historiqueParam:
                this.historiqueVisible = visible;
                break;
            case this.operationParam:
                this.operationVisible = visible;
                break;
            case this.sinistreParam:
                this.sinistreVisible = visible;
                break;
        }
    }
    initialiseitemsOperation() {
        let operationActifDeplie = JSON.parse(sessionStorage.getItem(OperationActifDeplie))
        this.itemsOperation = [];
        if (operationActifDeplie != null) {
            this.itemsOperation.push(
                {
                    operation: operationActifDeplie,
                    //zippedDetailOperation : true,
                    zippedDetailOperation: false,
                    zippedPeriode: false,
                    zippedConditionsSpeciales: false,
                    zippedPaiement: false,
                    zippedDocument: false,
                    zippedObjetsAssures: false,



                }
            )
            sessionStorage.setItem(OperationActifDeplie, JSON.stringify(null))

        }
        this.contrat.operations.forEach(operation => {
            if (operation != operationActifDeplie) {
                if (this.itemsOperation.length == 0)
                    this.itemsOperation.push(
                        {
                            operation: operation,
                            zippedDetailOperation: false,
                            zippedPeriode: false,
                            zippedConditionsSpeciales: false,
                            zippedPaiement: false,
                            zippedDocument: false,
                            zippedObjetsAssures: false,



                        }
                    )
                else
                    this.itemsOperation.push(
                        {
                            operation: operation,
                            zippedDetailOperation: false,
                            zippedPeriode: false,
                            zippedConditionsSpeciales: false,
                            zippedPaiement: false,
                            zippedDocument: false,
                            zippedObjetsAssures: false,



                        }
                    )

            }


        });
        console.log(this.itemsOperation)
    }
    affichageDetailsItemOperation(item, zippedItem, visible: boolean) {
        switch (zippedItem) {

            case this.zippedDetailOperation:
                item.zippedDetailOperation = visible
                break;
            case this.zippedPeriode: item.zippedPeriode = visible
                break;
            case this.zippedConditionsSpeciales: item.zippedConditionsSpeciales = visible
                break;
            case this.zippedPaiement: item.zippedPaiement = visible
                break;
            case this.zippedDocument: item.zippedDocument = visible
                break;
            case this.zippedObjetsAssures: item.zippedObjetsAssures = visible
                break;

        }
    }
    /**
         * ici on compare les operations d'abord par le statut puis par la date de cloture
         * on affecte à chaque operation un poids par rapport à un statut
         * une operation de  rang 1  est plus prioritaire qu'une operation de rang 2
         * @param a
         * @param b
         */
    compare(a, b) {

        let poidsStatutA, poidsStatutB
        let res;

        switch (a.statut) {
            case statutContratEnCoursDeValidation: poidsStatutA = 3

                break;
            case statutContratActif: poidsStatutA = 1
                break;
            case statutContratValideNonActif: poidsStatutA = 2
                break;
            case statutContratSuspendu: poidsStatutA = 4
                break;
            case statutContratResilie: poidsStatutA = 4
                break;
            case statutContratExpire: poidsStatutA = 4
                break;
            default: poidsStatutA = 4;

        }
        switch (b.statut) {
            case statutContratEnCoursDeValidation: poidsStatutB = 3

                break;
            case statutContratActif: poidsStatutB = 1
                break;
            case statutContratValideNonActif: poidsStatutB = 2
                break;
            case statutContratSuspendu: poidsStatutB = 4
                break;
            case statutContratResilie: poidsStatutB = 4
                break;
            case statutContratExpire: poidsStatutB = 4
                break;
            default: poidsStatutB = 4;

        }
        res = poidsStatutA - poidsStatutB
        if (res == 0) {
            if (a.dateCloture > b.dateCloture)
                return -1;
            else
                if (a.dateCloture < b.dateCloture)
                    return 1
                else
                    return 0;

        }
        console.log(res)
        return res

    }

    /**
     * Cette fonction nous permet d'initialiser la routine de renouvellement avec les donnee du contrat
     * origine donnee initialisation à modifier par rapport à  la base
     */
    async initialiserRoutineRenouvellement() {

        let operation = this.contrat.operations[this.contrat.operations.length - 1];
        operation.dateEffet = new Date();;
        operation.dateCloture = new Date();
        this.contrat.objetAssures.forEach(objet => {
            let routineContrat = new RoutineContrat();
            // routineContrat.contrat = this.contrat
            // routineContrat.objet = objet
            routineContrat.operation = operation

            this.listeRoutineContrat.push(routineContrat);
        });






    }


}

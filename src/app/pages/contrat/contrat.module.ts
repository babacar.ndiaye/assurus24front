import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';
import { ComponentsModule } from '../../components/components.module';

import { IonicModule } from '@ionic/angular';

import { ContratPage } from './contrat.page';
//import { AccordionListComponent } from './../../components/accordion-list/accordion-list.component';
// import { AccordionListComponentModule } from './../../components/accordion-list/accordion-list.component.module';
import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';


const routes: Routes = [
  {
    path: '',
    component: ContratPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes),
    
    ComponentsModule
  ],
  declarations: [ContratPage],
  schemas: [ CUSTOM_ELEMENTS_SCHEMA ]
})
export class ContratPageModule {}

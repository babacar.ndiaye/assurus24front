import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';
import { ComponentsModule } from '../../components/components.module';
import { IonicModule } from '@ionic/angular';

import { MesassurancesPage } from './mesassurances.page';
import {LazyLoadImageModule} from 'ng-lazyload-image';


const routes: Routes = [
  {
    path: '',
    component: MesassurancesPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    LazyLoadImageModule,
    RouterModule.forChild(routes),
    ComponentsModule,
  
  ],
  declarations: [MesassurancesPage]
})
export class MesassurancesPageModule {}

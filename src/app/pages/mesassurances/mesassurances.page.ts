import { Contrat2 } from 'src/app/models/contrat';
import { Component, OnInit, ViewChild } from '@angular/core';
import { UtilsService } from 'src/app/services/utils.service';
import { NavController, IonSlides } from '@ionic/angular';
import { AuthService } from 'src/app/services/auth.service';
import { banniereCom, UrlApi, LIENLOGIN, LIENEPOCNO } from 'src/environments/environment';

@Component({
  selector: 'app-mesassurances',
  templateUrl: './mesassurances.page.html',
  styleUrls: ['./mesassurances.page.scss'],
})
export class MesassurancesPage implements OnInit {
  urlApi =UrlApi ;
  @ViewChild('slideWithNav') slideWithNav: IonSlides;

  @ViewChild('slideWithNavPromo') slideWithNavPromo: IonSlides;
  sliderOne: any;



  //Configuration for each Slider
  slideOptsOne  = {
    initialSlide: 0,
    slidesPerView: 1,
    autoplay: true,
    speed : 0


  };
  slideChanged(slideView){
    switch(slideView){
      case "promos" : this.slideWithNavPromo.startAutoplay();break ;
      case "infos" : this.slideWithNav.startAutoplay();break ;
    }
  }
  contratsExpireBientot ;
 date :Date ;
  banniereComs: Array<banniereCom> ;
  constructor(private utilsService : UtilsService,private navCtrl : NavController,private authService: AuthService) {

  }

   ionViewWillEnter(){

     this.recupererContratdontFinProche() ;
     this.recupererBanieresCom() ;

   }
  ngOnInit() {
  }
  recupererContratdontFinProche(){

    this.utilsService.getDate().subscribe(
      dateFormatMobile => {this.date= new Date(dateFormatMobile.date)},
      error =>{
        let message ="Oups le serveur est introuvable verfiez votre connexion et recommencez svp" ;
        this.utilsService.alertNotification(message) ;
        this.navCtrl.navigateRoot(LIENLOGIN) ;
      }) ;
    let o2={
      id : 2 ,
      libelle :  "Tcamry"  ,
      contrats :[]  ,
      matricule	: "DK-2454-AB"	  ,
      statut : 1,infosValide : false ,
      numCarteGrise	:	"44445",
      marque	:	"Toyota"	,
      modele	:	"hilux"	,
      carburant	:	"gasoil"	,
      typeBA : null ,
      annee	:	2015	,
      puissanceFiscale	:"18cv"	,
      nombrePlaces	:	6	,
      valeurDeclarative :	2500000	,
      valeurInitiale :	1750000,
      controleTechnique: null,proprietaire:null
    }
    let o1 ={
      id : 2 ,
      libelle :  "mustang"  ,
      contrats :[]  ,
      matricule	: "DK-2454-AB"	  ,
      statut : 1,infosValide : false ,
      numCarteGrise	:	"44445",
      marque	:	"Toyota"	,
      modele	:	"hilux"	,
      carburant	:	"gasoil"	,
      typeBA : null ,
      annee	:	2015	,
      puissanceFiscale	:"18cv"	,
      nombrePlaces	:	6	,
      valeurDeclarative :	2500000	,
      valeurInitiale :	1750000,
      controleTechnique: null,proprietaire:null
    }
    let o3 ={
      id : 2 ,
      libelle :  "mford aveo"  ,
      contrats :[]  ,
      matricule	: "DK-2454-AB"	  ,
      statut : 1,infosValide : false ,
      numCarteGrise	:	"44445",
      marque	:	"Toyota"	,
      modele	:	"hilux"	,
      carburant	:	"gasoil"	,
      typeBA : null ,
      annee	:	2015	,
      puissanceFiscale	:"18cv"	,
      nombrePlaces	:	6	,
      valeurDeclarative :	2500000	,
      valeurInitiale :	1750000,
      controleTechnique: null,proprietaire:null
    }
    let o4 ={
      id : 2 ,
      libelle :  "dodge charger"  ,
      contrats :[]  ,
      matricule	: "DK-2454-AB"	  ,
      statut : 1,infosValide : false ,
      numCarteGrise	:	"44445",
      marque	:	"Toyota"	,
      modele	:	"hilux"	,
      carburant	:	"gasoil"	,
      typeBA : null ,
      annee	:	2015	,
      puissanceFiscale	:"18cv"	,
      nombrePlaces	:	6	,
      valeurDeclarative :	2500000	,
      valeurInitiale :	1750000,
      controleTechnique: null,proprietaire:null
    }
    // let contratsEssais:Array<Contrat2>  = [
    //   {
    //     id:   1,
    //     police :"488-4445-552",
    //     statut : nullfranchise:25000,capitauxGarantis:250000,
    //     objetAssures :[],
    //     client:null,
    //     reseau :  null ,
    //       ca:null,
    //     operations : [],
    //     dateEffet :new Date("05-02-2019"),
    //     dateCloture :new Date("05-05-2021"),

    //   },
    //   {
    //     id:   2,
    //     police :"488-4445-552",
    //     statut : null,franchise:25000,capitauxGarantis:250000,
    //     objetAssures :[o2,o1],
    //     client:null,
    //     reseau :  null ,
    //       ca:null,
    //     operations : [],
    //     dateEffet :new Date("05-02-2019"),
    //     dateCloture :new Date("05-05-2020"),

    //   },
    //   {
    //     id:   3,
    //     police :"488-4445-552",
    //     statut : null,franchise:25000,capitauxGarantis:250000,
    //     objetAssures :[o3,o4],
    //     client:null,
    //     reseau :  null ,
    //       ca:null,
    //     operations : [],
    //     dateEffet :new Date("05-02-2019"),
    //     dateCloture :new Date("05-05-2020"),

    //   },




    // ]

    //  this.contratsExpireBientot  = new Array<Contrat>() ;
    //  this.utilsService.getDate().subscribe(
    //   dateFormatMobile => {this.date= new Date(dateFormatMobile.date)
    //     contratsEssais.forEach(contrat => {

    //       let diffdate=this.diffdate(contrat.dateCloture,this.date,'jour') ;
    //       if(diffdate<=15)
    //         this.contratsExpireBientot.push(contrat) ;

    //     });
    //   },
    //   error =>{
    //     let message ="Oups le serveur est introuvable verfiez votre connexion et recommencez svp" ;
    //     this.utilsService.alertNotification(message) ;
    //     this.navCtrl.navigateRoot(LIENLOGIN) ;
    //   }) ;

    //apres preliminaire back ou front
  }
  diffdate(d1,d2,u){
    let div=1
    switch(u){
     case 'seconde': div=1000;
            break;
     case 'minute': div=1000*60
           break;
     case 'heure': div=1000*60*60
           break;
     case 'jour': div=1000*60*60*24
           break;
    }

    var Diff = d1.getTime() - d2.getTime() +1;
    return Math.ceil((Diff/div))
    }

    deconnecter(){
      this.authService.logout()
      this.navCtrl.navigateRoot(LIENEPOCNO); ;
    }
    recupererBanieresCom(){
      this.utilsService.recupererBanieresCom().then(
      liste =>{
        this.banniereComs =liste ;
      }

      )

      }
}

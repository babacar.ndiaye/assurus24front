import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MesassurancesPage } from './mesassurances.page';

describe('MesassurancesPage', () => {
  let component: MesassurancesPage;
  let fixture: ComponentFixture<MesassurancesPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MesassurancesPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MesassurancesPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

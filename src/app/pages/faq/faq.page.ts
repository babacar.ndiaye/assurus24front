import { Component, OnInit } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { ModalAllPage } from './modal-all/modal-all.page';
import { ModalOnePage } from './modal-one/modal-one.page';

export interface Questions {
    id: number;
    libelle: string,
    publish: boolean
};
@Component({
  selector: 'app-faq',
  templateUrl: './faq.page.html',
  styleUrls: ['./faq.page.scss'],
})
export class FaqPage implements OnInit {

    Categories: { id: number, libelle: string, publish: boolean, image: string , questions: Questions[] }[]=[
        {
            id: 1,
            libelle: "Général",
            publish: true,
            image: "../../assets/conversation.svg",
            questions: [
                {
                    id: 1,
                    libelle: "Téléchargement et installation",
                    publish: true
                },
                {
                    id: 2,
                    libelle: "Comment s'inscrire?",
                    publish: true
                },
                {
                    id: 3,
                    libelle: "Comment recharger mon crédit?",
                    publish: true
                }
            ]
        },
        {
            id: 2,
            libelle: "Simulation Auto",
            publish: true,
            image: "../../assets/car-insurance.svg",
            questions: [
                {
                    id: 1,
                    libelle: "Comment faire la simulation?",
                    publish: true
                }
            ]
        },
        {
            id: 3,
            libelle: "Simulation Sante",
            publish: true,
            image: "../../assets/heartbeat.svg",
            questions: [
                {
                    id: 1,
                    libelle: "Qu'est ce que fait cette simulation ?",
                    publish: true
                }
            ]
        },
        {
            id: 4,
            libelle: "Contrat Assurance",
            publish: true,
            image: "../../assets/check-mark.svg",
            questions: [
                {
                    id: 1,
                    libelle: "C'est quoi un contrat ?",
                    publish: true
                },
                {
                    id: 2,
                    libelle: "Etapes pour renouvellement contrat ?",
                    publish: true
                }
            ]
        }
    ];
    date = new Date().toLocaleString();
    mot;
    timer: any;
  constructor(public modalController: ModalController,) { }

  ngOnInit() {
    //Ceci permet d'avoir la date et l'heure en temps réel
    this.timer = setInterval(() => {
        this.date = new Date().toLocaleString();
      }, 500);
  }

  /**
   * Ceci détruit le timer quand on quite la page
   */
  ngOnDestroy() {
    if (this.timer) {
      clearInterval(this.timer);
    }
  }

  /**
   * * Cette fonction permet d'ouvir
   * * un modal pour afficher la liste
   * * des questions d'une categorie
   * @param categorie
   * @returns
   */
  async ModalAllQuestionByCategories(categorie) {
    const modal = await this.modalController.create({
      component: ModalAllPage,
      componentProps: {
        categoriePassed: categorie,
      },
    });
    modal.onDidDismiss().then((data) => {
      //this.updateAtelierDetails();
    });
    return await modal.present();
  }

  async ModalDetailsOneQuestion(question) {
    const modal = await this.modalController.create({
      component: ModalOnePage,
      componentProps: {
        questionPassed: question,
      },
      //cssClass: 'custom-modal-css'
    });
    modal.onDidDismiss().then((data) => {
      //this.updateAtelierDetails();
    });
    return await modal.present();
  }

}

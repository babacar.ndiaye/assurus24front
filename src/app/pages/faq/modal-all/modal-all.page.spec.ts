import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ModalAllPage } from './modal-all.page';

describe('ModalAllPage', () => {
  let component: ModalAllPage;
  let fixture: ComponentFixture<ModalAllPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ModalAllPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ModalAllPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

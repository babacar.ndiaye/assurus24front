import { Component, Input, OnInit } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { ModalOnePage } from '../modal-one/modal-one.page';

@Component({
  selector: 'app-modal-all',
  templateUrl: './modal-all.page.html',
  styleUrls: ['./modal-all.page.scss'],
})
export class ModalAllPage implements OnInit {
    @Input() categoriePassed;
  constructor(public modalController: ModalController) { }

  ngOnInit() {
    if (this.categoriePassed) {
        console.log("Categorie", this.categoriePassed);
      }
  }

  async ModalDetailsOneQuestion(question) {
    const modal = await this.modalController.create({
      component: ModalOnePage,
      componentProps: {
        questionPassed: question,
      },
    });
    modal.onDidDismiss().then((data) => {
      //this.updateAtelierDetails();
    });
    return await modal.present();
  }

  closeModal() {
    this.modalController.dismiss();
  }

}

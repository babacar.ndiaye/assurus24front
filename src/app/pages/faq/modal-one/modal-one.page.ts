import { Component, Input, OnInit } from '@angular/core';
import { ModalController } from '@ionic/angular';

@Component({
  selector: 'app-modal-one',
  templateUrl: './modal-one.page.html',
  styleUrls: ['./modal-one.page.scss'],
})
export class ModalOnePage implements OnInit {
    @Input() questionPassed;
  constructor(public modalController: ModalController) { }

  ngOnInit() {
  }

  closeModal() {
    this.modalController.dismiss();
  }

}

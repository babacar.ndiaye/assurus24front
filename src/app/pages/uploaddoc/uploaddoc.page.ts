import { Component, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { NavController, LoadingController, Platform, AlertController } from '@ionic/angular';

import { UrlApi, IDINSCRIPTION, typeRoutineProfil, CNIPATHNAME, downloadCNI, routineInscription, routineUpdateProfil } from 'src/environments/environment';
import { CompteService } from 'src/app/services/compte.service';
import { File, FileEntry } from '@ionic-native/file/ngx';
import { UtilsService } from 'src/app/services/utils.service';

@Component({
  selector: 'app-uploaddoc',
  templateUrl: './uploaddoc.page.html',
  styleUrls: ['./uploaddoc.page.scss'],
})
export class UploaddocPage implements OnInit {
  
  public onuploaddocForm: FormGroup;
  typeRoutineProfilLoc = JSON.parse(sessionStorage.getItem(typeRoutineProfil))
  public cni : any ; 
  @ViewChild('myFileInput') inputFile ;
  constructor(
    private formBuilder: FormBuilder,
    private file :File ,
    public navCtrl: NavController,
    private utilsService :UtilsService,
    
    private serviceCompte :CompteService,
    public loadingCtrl :LoadingController,
    private alertCtrl :AlertController) { }
    id : string ;
    ngOnInit() {
      this.onuploaddocForm = this.formBuilder.group({
        'cni': [null, Validators.compose([
          
        ])]
      });
    }
    ionViewWillEnter(){
      this.recupererId() ;
    }
    importer() {
      if(this.cni != null)
      {
        if(this.testExtension())
        { 
          let body = new FormData();
          
          body.append("file",this.cni);
          body.append("id",this.id) ;
          
          this.serviceCompte.upload(body).subscribe(
            res=>{
              if(res)
              {
                let message ="Enregistré avec succès" ;
                this.utilsService.alertNotification(message) ;
                this.goHome() ;
              }
              else
              {
                let message ="Erreur enregistrement Veuilez réessayer" ;
                this.utilsService.alertNotification(message) ;
              }
            },
            erreur =>{
              let message ="Erreur connexion Veuilez réessayer" ;
              this.utilsService.alertNotification(message) ;
              
              
            }
            )
            
          }
          else{
            
            this.AlertPasse() ;
            
          }
          
        }
        else{
              this.utilsService.alertNotification("Veuillez choisir un fichier pdf") ;
        }
      }
      // k
      
      
      
      
    
      
      onInputTime(event)
      {
        this.cni = event.target.files[0]   ;
        
      }
      //a redefinir suivant l'emplacement de la page 
      //soit cest apres l'inscription alors on atten la fin de cette etape pour valider l'inscription 
      //soit cest independant de l"inscription  à definir
      goHome(){
        console.log("aaa")
        this.typeRoutineProfilLoc = JSON.parse(sessionStorage.getItem(typeRoutineProfil))
        switch(this.typeRoutineProfilLoc)
        {
          case routineInscription :
              this.navCtrl.navigateRoot('/login');
            break ;
           case routineUpdateProfil :
              this.navCtrl.navigateRoot('/myprofile');
             break ; 
        }
        window.sessionStorage.removeItem(IDINSCRIPTION) ;
        window.sessionStorage.setItem(typeRoutineProfil,JSON.stringify(null))
       
        
      }
      recupererId()
      {
        //a definir selon le schema choisi apres
       this.id = window.sessionStorage.getItem(IDINSCRIPTION) ;
        if(this.id == null)
        {
          let message ="Erreur connexion Veuilez réessayer" ;
          this.utilsService.alertNotification(message) ;
          this.typeRoutineProfilLoc = JSON.parse(sessionStorage.getItem(typeRoutineProfil))
          switch(this.typeRoutineProfilLoc)
          {
            case routineInscription :
              this.navCtrl.navigateRoot("/contact") ;
              break ;
             case routineUpdateProfil :
                this.navCtrl.navigateRoot('/myprofile');
               break ; 
          }
          
          
        }
        
        
      }
      async AlertPasse() {
        let alert = await this.alertCtrl.create({
          header: 'Confirmation',
          message: "Vous n'avez pas choisi de fichier ,cliquez sur continuer pour confirmer ou sur annuler pour quitter",
          inputs: [
            
          ],
          buttons: [
            {
              text: 'Annuler',
              handler: data => {
                
              }
              
            },
            {
              text: 'Continuer',
              handler: data => {
                this.goHome() ;
              }
            }
            
          ]
        });
        alert.present();
      }
      testExtension(){
        let nom:String = this.inputFile.value ;
        let extension = nom.substring(nom.lastIndexOf(".")) ;
        if(extension == ".pdf")
        return true ;
        else 
        return false ;
      }
      async download(){
        const loader = await this.loadingCtrl.create({
          message: '',
        });
        
        loader.present() ;
        // To download the PDF file
        this.serviceCompte.download(CNIPATHNAME,this.id,downloadCNI) ;
      }
      
    }
    
import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UploaddocPage } from './uploaddoc.page';

describe('UploaddocPage', () => {
  let component: UploaddocPage;
  let fixture: ComponentFixture<UploaddocPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UploaddocPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UploaddocPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

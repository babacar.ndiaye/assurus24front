import { AssureContrat } from './../models/assure-contrat';
import { Contrat2 } from './../models/contrat';
import { ObjetAssures2 } from './../models/objet-assures';
import { Operation } from "../models/operation";
import { ObjetAssures } from "../models/objet-assures";

export class RoutineContrat {
    contrat :  Contrat2;
    operation : Operation;
    objet : ObjetAssures2;
    assureContrat: AssureContrat;
}

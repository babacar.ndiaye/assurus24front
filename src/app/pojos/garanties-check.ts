import { Garantie } from "../models/garantie";

export class GarantiesCheck {
    garantie : Garantie ;
    checked : boolean ;
    constructor(garantie:Garantie)
    {
        this.garantie=garantie ;
        this.checked = false ;
    }
}

import { CA } from "../models/ca";
import { CompagnieObjetPrix } from "./compagnie-objet-prix";

export class CompagniePrix {
    ca :CA ;
    prix : number ;
    caObjetPrix : Array<CompagnieObjetPrix>
    
    constructor(ca,prix,caObjetPrix)
    {
        this.ca = ca ;
        this.prix =prix ;
        this.caObjetPrix = caObjetPrix ;
    }

}

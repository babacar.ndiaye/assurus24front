import { CommonModule } from "@angular/common";
import { NgModule } from "@angular/core";
import { CustomTabBarComponent } from "./custom-tab-bar.component";

@NgModule({
  declarations: [CustomTabBarComponent],
  imports: [CommonModule],
  exports: [CustomTabBarComponent]
})
export class SharedModule {}

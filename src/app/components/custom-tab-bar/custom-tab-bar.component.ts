import { Component, OnInit } from "@angular/core";
import { NavController } from "@ionic/angular";
import { Router } from "@angular/router";
import { SessionCompte } from "src/environments/environment";

@Component({
  selector: "app-custom-tab-bar",
  templateUrl: "./custom-tab-bar.component.html",
  styleUrls: ["./custom-tab-bar.component.scss"]
})
export class CustomTabBarComponent implements OnInit {
  public home: any;
  public location: any;
  public birth: any;
  public misc: any;
  public phone: any;

  public homeShow: any;
  public locationShow: any;
  public birthShow: any;
  public miscShow: any;
  public phoneShow: any;
  routePath: string;

  constructor(public navCtrl: NavController, private router: Router) {}

  ngOnInit() {
    this.routePath = this.router.url;

    // if (this.router.url == "/home") {
    //   this.homeActive();
    // } else if (this.router.url == "/location") {
    //   this.locationActive();
    // } else if (this.router.url == "/birth") {
    //   this.birthActive();
    // } else if (this.router.url == "/misc") {
    //   this.miscActive();
    // }else if (this.router.url == "/phone") {
    //   this.phoneActive();
    // }else {
    //   this.deactivateAll();
    // }
  }
  gotoHome() {
    if(!sessionStorage.getItem(SessionCompte)){
      this.navCtrl.navigateRoot('/epocno');
    } else {
      this.navCtrl.navigateRoot("/accueil-client-logge");
    }
  }
  gotoLocation() {
    this.navCtrl.navigateRoot("/contactez-nous");
  }
  gotoBirth() {
    this.navCtrl.navigateRoot("/settings");
  }
  gotoMisc() {
    // this.navCtrl.navigateRoot("misc");
  }
  gotoPhone() {
    // this.navCtrl.navigateRoot("phone");
  }

  // public homeActive() {
  //   this.home = "assets/icon/home.svg";
  //   this.location = "assets/icon/msg.svg";
  //   this.birth = "assets/icon/tool.svg";
  //   this.misc = "assets/icon/scissor.svg";
  //   this.phone = "assets/icon/mobile.svg";


  //   this.homeShow = "";
  //   this.locationShow = "hideBlock";
  //   this.birthShow = "hideBlock";
  //   this.phoneShow = "hideBlock";
  //   console.log("gym");
  // }

  // public locationActive() {
  //   this.home = "assets/icon/home.svg";
  //   this.location = "assets/icon/msg.svg";
  //   this.birth = "assets/icon/tool.svg";
  //   this.misc = "assets/icon/scissor.svg";
  //   this.phone = "assets/icon/mobile.svg";


  //   this.homeShow = "hideBlock";
  //   this.locationShow = "";
  //   this.birthShow = "hideBlock";
  //   this.miscShow = "hideBlock";
  //   console.log("location");
  // }

  // public birthActive() {
  //   this.home = "assets/icon/home.svg";
  //   this.location = "assets/icon/msg.svg";
  //   this.birth = "assets/icon/tool.svg";
  //   this.misc = "assets/icon/scissor.svg";
  //   this.phone = "assets/icon/mobile.svg";


  //   this.homeShow = "hideBlock";
  //   this.locationShow = "hideBlock";
  //   this.birthShow = "";
  //   this.miscShow = "hideBlock";
  //   console.log("birth");
  // }

  // public miscActive() {
  //   this.home = "assets/icon/home.svg";
  //   this.location = "assets/icon/msg.svg";
  //   this.birth = "assets/icon/tool.svg";
  //   this.misc = "assets/icon/scissor.svg";
  //   this.phone = "assets/icon/mobile.svg";


  //   this.homeShow = "hideBlock";
  //   this.locationShow = "hideBlock";
  //   this.birthShow = "hideBlock";
  //   this.miscShow = "";
  //   console.log("misc");
  // }

  // public phoneActive() {
  //   this.home = "assets/icon/home.svg";
  //   this.location = "assets/icon/msg.svg";
  //   this.birth = "assets/icon/tool.svg";
  //   this.misc = "assets/icon/scissor.svg";
  //   this.phone = "assets/icon/mobile.svg";

  //   this.homeShow = "hideBlock";
  //   this.locationShow = "hideBlock";
  //   this.birthShow = "hideBlock";
  //   this.miscShow = "";
  //   console.log("misc");
  // }

  // public deactivateAll() {
  //   this.home = "assets/icon/home.svg";
  //   this.location = "assets/icon/msg.svg";
  //   this.birth = "assets/icon/tool.svg";
  //   this.misc = "assets/icon/scissor.svg";
  //   this.phone = "assets/icon/mobile.svg";

  //   this.homeShow = "hideBlock";
  //   this.locationShow = "hideBlock";
  //   this.birthShow = "hideBlock";
  //   this.miscShow = "hideBlock";
  //   console.log("deactivated");
  // }
}

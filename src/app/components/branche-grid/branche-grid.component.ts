import { Component, Input, OnChanges, OnInit, SimpleChanges } from '@angular/core';
import { UtilsService } from 'src/app/services/utils.service';
import { TranslateService } from '@ngx-translate/core';
import { AlertController, NavController, Platform } from '@ionic/angular';
import { Globalization } from '@ionic-native/globalization/ngx';
import { TypeBA2 } from 'src/app/models/type-ba';

@Component({
  selector: 'app-branche-grid',
  templateUrl: './branche-grid.component.html',
  styleUrls: ['./branche-grid.component.scss']
})
export class BrancheGridComponent implements OnChanges, OnInit {
  /**TRANSLATION VARIABLE */
  @Input() language: string;
  auto: string;
  maison: string;
  sante: string;
  bateau: any;
  voyage: any;
  depannage: any;
  idCar: number = 0;
  monbranches: TypeBA2[];
  branches: TypeBA2[] = [];

  constructor(
    public platform: Platform,
    public utilsService: UtilsService,
    private _translate: TranslateService,
    public navCtrl: NavController,
    private alertController: AlertController,
    private globalization: Globalization,
    private utilServie: UtilsService
  ) {

  }
  ngOnInit(): void {
    this.utilsService.getTypeBas().subscribe((data: TypeBA2[]) => {
      // console.log("data", data);
      this.branches = data;
    })

  }
  ngOnChanges(changes: SimpleChanges) {
    this.language = changes.language.currentValue;
    this._translateLanguage()
  }

  LunchSimulation(item) {
    const param = this._translate.instant(item.libelle) //
    console.log("param", item);
    if (item.publier == false) {
      this.AlertSimulationNotDisponible(param);
    } else {
      this.utilServie.getTypeBasAndSousBrancheAttributsDTO(item.id).subscribe((att) => {
        console.log("Attributs parents", att);
        sessionStorage.setItem("AttributsParent", JSON.stringify(att));
        this.lancerAuto(item.id);
      })
    }
  }

  lancerAuto(idBranche) {
    this.utilsService.lancerAuto(idBranche);

  }

  async AlertSimulationNotDisponible(param: any) {
    const alert = await this.alertController.create({
      header: this._translate.instant("alerte"),
      subHeader: "Simulation " + param,
      message: this._translate.instant("simulation_non_disponible"),
      buttons: [
        {
          text: "Ok",
          cssClass: "icon-color",
          handler: () => {
            //Call you API to remove Items here.
          },
        },
      ],
    });

    await alert.present();
  }

  /**GESTION LOGIQUE TRANSLATION DES LANGUES */
  ionViewDidEnter(): void {
    this._translateLanguage()
  }
  ionViewDidLoad() {
    this._translateLanguage();
  }

  public changeLanguage(): void {
    this._translateLanguage();
  }

  _translateLanguage(): void {
    this._translate.use(this.language);
    this._initialiseTranslation();
  }

  _initialiseTranslation(): void {
    setTimeout(() => {
      this.auto = this._translate.instant("auto");
      this.maison = this._translate.instant("maison");
      this.sante = this._translate.instant("sante");
      this.bateau = this._translate.instant("bateau");
      this.voyage = this._translate.instant("voyage");
      this.depannage = this._translate.instant("depannage");
    }, 250);
  }

  _initTranslate(language) {
    // Set the default language for translation strings, and the current language.
    this._translate.setDefaultLang('fr-FR');
    if (language) {
      this.language = language;
    }
    else {
      // Set your language here
      this.language = 'fr-FR';
    }
    this._translateLanguage();
  }

  // getDeviceLanguage() {
  //   if (!this.platform.is('android') || !this.platform.is('ios')) {
  //     //alert('Navigateur')
  //     console.log('Branche grid Navigateur langage', navigator.language)
  //     this._initTranslate(navigator.language);
  //   } else {
  //     this.globalization.getPreferredLanguage()
  //       .then(res => {
  //         this._initTranslate(res.value)
  //       })
  //       .catch(e => { console.log(e); });
  //   }
  // }
  /**END TRANSLATION */


}

import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BrancheGridComponent } from './branche-grid.component';

describe('BrancheGridComponent', () => {
  let component: BrancheGridComponent;
  let fixture: ComponentFixture<BrancheGridComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BrancheGridComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BrancheGridComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

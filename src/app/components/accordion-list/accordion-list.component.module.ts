import { NgModule} from '@angular/core';
import { AccordionListComponent } from './accordion-list.component';

@NgModule({
  declarations: [
	AccordionListComponent
  ],
  exports: [
	AccordionListComponent
  ]
})
export class AccordionListComponentModule {
}
import { Component, OnInit, Input, EventEmitter, Output } from '@angular/core';


@Component({
  selector: 'app-accordion-list',
  templateUrl: './accordion-list.component.html',
  styleUrls: ['./accordion-list.component.scss']
})
export class AccordionListComponent implements OnInit  {

  @Input()
  nameCompagnie : string;

  @Input()
  logoCompagnie : string;
  
  @Input()
  montant : number;

  @Input()
  details :string;

  
  @Output()
  change : EventEmitter<string> = new EventEmitter<string>();


  public isMenuOpen : boolean = false;



  constructor() { }



  ngOnInit() {
  }

   toggleAccordion(){
      this.isMenuOpen = !this.isMenuOpen;
  }


  /**
   * Allows the value for the <ion-button> element to be broadcast to the parent component
   * @public
   * @method broadcastName
   * @returns {none}
   */
  public broadcastName(name : string) : void
  {
     this.change.emit(name);
  }

}
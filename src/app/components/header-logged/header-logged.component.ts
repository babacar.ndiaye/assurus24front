import { Component, OnInit, Input } from '@angular/core';
import { AuthService } from 'src/app/services/auth.service';
import { NavController, Platform } from '@ionic/angular';
import { UtilsService } from 'src/app/services/utils.service';
import { Contact } from 'src/app/models/contact';
import { Compte } from 'src/app/models/compte';
import { LIENACCUEILCLIENTLOGGE } from 'src/environments/environment';
import { Globalization } from '@ionic-native/globalization/ngx';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-header-logged',
  templateUrl: './header-logged.component.html',
  styleUrls: ['./header-logged.component.scss']
})
export class HeaderLoggedComponent implements OnInit {
  /**
   * 1 pour dire que l'utilisateur s'est connecté et qui peut se deconnecter
   * 2 pour dire que l'utilisateur ne  s'est pas  connecté et  peut se connecter
   * 3 pour dire que l'utilisateur ne  s'est pas  connecté et ne peut pas se connecter
   */
  @Input('isLogged') isLogged : String ;
  @Input('pageOrigine') pageOrigine: String ;
  @Input('showBackButton') showBackButton: String;
  logged:number
  BackButton:number;
  compte:Compte;
  language: string;
  constructor(public authService: AuthService,
    public navCtrl: NavController,
    private platform: Platform,
    private globalization: Globalization,
    private _translate: TranslateService,
    public utilsService:UtilsService) { }

  ngOnInit() {
    this.logged=Number(this.isLogged);
    this.BackButton=Number(this.showBackButton);
    console.log("logged",this.logged);
    console.log("BackButton",this.BackButton);
  }
  deconnecter(){
    this.authService.logout()
    this.navCtrl.navigateRoot('/epocno'); ;
  }
  back(){
    this.navCtrl.back() ;
  }

  seConnecter(){
      if(this.pageOrigine != null)
    this.utilsService.recupererCompte(this.pageOrigine).then(
      compte=>{this.compte =compte}
      ) ;
      else
      this.utilsService.recupererCompte(LIENACCUEILCLIENTLOGGE).then(
        compte=>{this.compte =compte}
        ) ;
  }
  /**GESTION LOGIQUE TRANSLATION DES LANGUES */
  ionViewDidEnter(): void {
    this.getDeviceLanguage()
  }

  public changeLanguage(): void {
    this._translateLanguage();
  }

  _translateLanguage(): void {
    this._translate.use(this.language);
  }

  _initTranslate(language) {
    // Set the default language for translation strings, and the current language.
    this._translate.setDefaultLang('fr-FR');
    if (language) {
      this.language = language;
    }
    else {
      // Set your language here
      this.language = 'fr-FR';
    }
    this._translateLanguage();
  }

  getDeviceLanguage() {
    if (!this.platform.is('android') || !this.platform.is('ios')) {
      //alert('Navigateur')
      console.log('Navigateur langage', navigator.language)
      this._initTranslate(navigator.language);
    } else {
      this.globalization.getPreferredLanguage()
        .then(res => {
          this._initTranslate(res.value)
        })
        .catch(e => { console.log(e); });
    }
  }
  /**END TRANSLATION */
}

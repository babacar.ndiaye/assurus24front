import {NgModule,CUSTOM_ELEMENTS_SCHEMA} from '@angular/core';
import {TabsComponent,} from './tabs/tabs.component';
import {Routes} from '@angular/router';
import { ExpandableComponent } from './expandable/expandable.component';
import {AccordionListComponent} from './accordion-list/accordion-list.component';
import { ArianeComponent } from './ariane/ariane.component';
import { CustomTabBarComponent } from './custom-tab-bar/custom-tab-bar.component';
import { ComponentsComponent } from './components.component';
import { ArianeInscriptionComponent } from './ariane-inscription/ariane-inscription.component';

import { RefresherComponent } from './refresher/refresher.component';
import { HeaderLoggedModule } from './header-logged/header-logged.module';
import { baniereComModule } from './banniere-com/banniere-com.module';

import { BrancheGridComponent } from './branche-grid/branche-grid.component';
import { TranslateLoader, TranslateModule } from '@ngx-translate/core';
import { HttpLoaderFactory } from 'src/app/app.module';
import { HttpClient } from '@angular/common/http';
import { CommonModule } from '@angular/common';
//

const routes: Routes = [
    {
      path: '',
      component: TabsComponent,
    }
  ];


@NgModule({
    declarations : [TabsComponent,RefresherComponent, ExpandableComponent,AccordionListComponent,BrancheGridComponent, ArianeComponent,ArianeInscriptionComponent,CustomTabBarComponent, ComponentsComponent, RefresherComponent, BrancheGridComponent],
    exports : [TabsComponent,RefresherComponent,HeaderLoggedModule,baniereComModule,BrancheGridComponent, ExpandableComponent,AccordionListComponent,ArianeComponent,CustomTabBarComponent,ArianeInscriptionComponent],
    imports:[CommonModule,TranslateModule.forChild({
        loader: {
          provide: TranslateLoader,
          useFactory: HttpLoaderFactory,
          deps: [HttpClient]
        },
      }),],

    schemas: [
        CUSTOM_ELEMENTS_SCHEMA
    ],
})
export class ComponentsModule{}


// test1

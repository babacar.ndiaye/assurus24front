import { Component, OnInit, ViewChild, Input, Renderer2, ElementRef, ViewChildren } from '@angular/core';
import { createViewChildren } from '@angular/compiler/src/core';
import { Globalization } from '@ionic-native/globalization/ngx';
import { TranslateService } from '@ngx-translate/core';
import { Platform } from '@ionic/angular';

@Component({
  selector: 'app-ariane',
  templateUrl: './ariane.component.html',
  styleUrls: ['./ariane.component.scss']
})
export class ArianeComponent implements OnInit {
  iconeCheck: Array<string> = [null, null, null, null, null]
  listeCol: Array<string> = [null, null, null, null, null]
  // tslint:disable-next-line: no-input-rename
  @Input('etape') etape: number;
  actif: string = "actif";
  valide: string = "valide";
  inValide: string = "invalide";
  language: string;
  constructor(private globalization: Globalization,
    private _translate: TranslateService,
    private platform: Platform,) { }

  ngOnInit() {
    this.etape = Number(this.etape)
    console.log(this.etape);
    for (let index = this.etape - 1; index < this.listeCol.length; index++) {
      this.iconeCheck[index] = "hidden";


    }
    this.changerEtat(this.etape - 1)
    console.log(this.listeCol)
  }

  changerEtat(number) {
    for (let i = 0; i < this.listeCol.length; i++) {
      if (i == number)
        this.listeCol[i] = this.actif
      else
        if (i < number)
          this.listeCol[i] = this.valide
        else
          this.listeCol[i] = this.inValide

    }
  }
  /**GESTION LOGIQUE TRANSLATION DES LANGUES */
  ionViewDidEnter(): void {
    this.getDeviceLanguage()
  }

  public changeLanguage(): void {
    this._translateLanguage();
  }

  _translateLanguage(): void {
    this._translate.use(this.language);
  }

  _initTranslate(language) {
    // Set the default language for translation strings, and the current language.
    this._translate.setDefaultLang('fr-FR');
    if (language) {
      this.language = language;
    }
    else {
      // Set your language here
      this.language = 'fr-FR';
    }
    this._translateLanguage();
  }

  getDeviceLanguage() {
    if (!this.platform.is('android') || !this.platform.is('ios')) {
      //alert('Navigateur')
      console.log('Navigateur langage', navigator.language)
      this._initTranslate(navigator.language);
    } else {
      this.globalization.getPreferredLanguage()
        .then(res => {
          this._initTranslate(res.value)
        })
        .catch(e => { console.log(e); });
    }
  }
  /**END TRANSLATION */


}

import {
  Component,
  AfterViewInit,
  Input,
  ViewChild,
  ElementRef,
  Renderer2
} from '@angular/core';

@Component({
  selector: 'app-expandable',
  templateUrl: './expandable.component.html',
  styleUrls: ['./expandable.component.scss']
})
export class ExpandableComponent implements AfterViewInit {
  largeur = screen.width ;
  longueur = screen.height ;
  @ViewChild('expandWrapper', { read: ElementRef }) expandWrapper: ElementRef;
  @ViewChild('expandWrapper') expandWrapper2;
  // tslint:disable-next-line: no-input-rename
  @Input('expanded') expanded = false;
  // tslint:disable-next-line: no-input-rename
  @Input('name') name: string;
  // tslint:disable-next-line: no-input-rename
  @Input('src') img: string;
  // tslint:disable-next-line: no-input-rename
  @Input('content') content: string;
  // tslint:disable-next-line: no-input-rename
  @Input('id') id: string;
  // tslint:disable-next-line: no-input-rename
  @Input('expandHeight') expandHeight = '150px';
  class: string;

  constructor(public renderer: Renderer2) {
    this.class=this.largeur>500? 'expand-wrapper largeEcran' : 'expand-wrapper smallEcran'  ;
    
  }
 ngOnInit(): void {
  
  this.expandWrapper2.class = this.expandWrapper2.class+" "+this.class ;
  console.log(this.expandWrapper2.class)
 }
  ngAfterViewInit() {
    this.renderer.setStyle(
      this.expandWrapper.nativeElement,
      'max-height',
      this.expandHeight
    );
    this.renderer.setStyle(
      this.expandWrapper.nativeElement,
      'class',
      this.class
    );
    
   
}


}
// test1
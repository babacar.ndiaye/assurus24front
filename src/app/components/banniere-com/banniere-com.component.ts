import { Component, OnInit, ViewChild } from '@angular/core';
import { banniereCom, SessionCompte, UrlApi } from 'src/environments/environment';
import { IonSlides, NavController } from '@ionic/angular';
import { UtilsService } from 'src/app/services/utils.service';
import { ClientParticulier } from 'src/app/models/client-particulier';

@Component({
  selector: 'app-banniere-com',
  templateUrl: './banniere-com.component.html',
  styleUrls: ['./banniere-com.component.scss'],

})
export class BanniereComComponent implements OnInit {
 banniereComs=new Array ;
 urlApi =UrlApi ;
 @ViewChild('slideWithNav') slideWithNav: IonSlides;


 sliderOne: any;



 //Configuration for each Slider
 slideOptsOne  = {
   initialSlide: 0,
   slidesPerView: 1,
   autoplay: true,
   speed : 1000


 };
  client: ClientParticulier;
 slideChanged(slideView){
   switch(slideView){

     case "infos" : this.slideWithNav.startAutoplay();break ;
   }
 }

  constructor(private navCtrl : NavController,public utilsService : UtilsService) {
    this.recupererBanieresCom();
   }

  ngOnInit() {
    this.recupererBanieresCom() ;
    this.recupererCompte() ;
  }
 recupererBanieresCom(){
   console.log("baniere")
    let  liste : Array<banniereCom> =[
      {
        id : 1

      },
      {
       id : 2

     },

    ]
  this.banniereComs= liste ;



    }


    //apres on va recuperer contact au lieu de compte
    recupererCompte(){
      let c = JSON.parse(window.sessionStorage.getItem(SessionCompte));
      if(c!=null)
      this.client = c.client

      console.log(this.client) ;
      }

}

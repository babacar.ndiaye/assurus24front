import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BanniereComComponent } from './banniere-com.component';

describe('BanniereComComponent', () => {
  let component: BanniereComComponent;
  let fixture: ComponentFixture<BanniereComComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BanniereComComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BanniereComComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

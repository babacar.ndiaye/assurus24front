import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';
import { ComponentsModule } from '../../components/components.module';

import { IonicModule } from '@ionic/angular';
import { BanniereComComponent } from './banniere-com.component';
import {LazyLoadImageModule} from 'ng-lazyload-image';



@NgModule({
    declarations: [BanniereComComponent],
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    LazyLoadImageModule
 
  ],
  exports:[BanniereComComponent] ,
  
  schemas: [ CUSTOM_ELEMENTS_SCHEMA ]
})
export class baniereComModule {}

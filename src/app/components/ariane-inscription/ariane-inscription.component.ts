import { Component, OnInit, Input,  } from '@angular/core';

@Component({
  selector: 'app-ariane-inscription',
  templateUrl: './ariane-inscription.component.html',
  styleUrls: ['./ariane-inscription.component.scss']
})
export class ArianeInscriptionComponent implements OnInit {

  iconeCheck :Array<string> =[null,null,null,null,null]
  listeCol :Array<string> = [null,null,null,null,null]
  // tslint:disable-next-line: no-input-rename
  @Input('etape') etape : number ;
  actif: string="actif";
  valide: string="valide";
  inValide: string = "invalide";
  constructor() { }

  ngOnInit() {
    this.etape =Number(this.etape)
    console.log(this.etape) ;
    for (let index = this.etape-1; index < this.listeCol.length; index++) {
      this.iconeCheck[index] = "hidden" ;
      
      
    }
    this.changerEtat(this.etape-1)
    console.log(this.listeCol)
  }
  
  changerEtat(number){
    for (let i = 0; i < this.listeCol.length; i++) {
      if(i==number)
       this.listeCol[i]=this.actif
       else
       if(i<number)
       this.listeCol[i]=this.valide
       else
       this.listeCol[i] = this.inValide
      
    }
  }

 

}

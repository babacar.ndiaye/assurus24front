import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ArianeInscriptionComponent } from './ariane-inscription.component';

describe('ArianeInscriptionComponent', () => {
  let component: ArianeInscriptionComponent;
  let fixture: ComponentFixture<ArianeInscriptionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ArianeInscriptionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ArianeInscriptionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

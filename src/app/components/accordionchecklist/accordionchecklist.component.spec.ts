import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AccordionchecklistComponent } from './accordionchecklist.component';

describe('AccordionchecklistComponent', () => {
  let component: AccordionchecklistComponent;
  let fixture: ComponentFixture<AccordionchecklistComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AccordionchecklistComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AccordionchecklistComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ListeOperationsContratsPage } from './liste-operations-contrats.page';

describe('ListeOperationsContratsPage', () => {
  let component: ListeOperationsContratsPage;
  let fixture: ComponentFixture<ListeOperationsContratsPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ListeOperationsContratsPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListeOperationsContratsPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

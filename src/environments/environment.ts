import { UtilsService } from "src/app/services/utils.service";
import { InAppBrowserObject, InAppBrowser } from "@ionic-native/in-app-browser/ngx";
import { NavController } from "@ionic/angular";

export const environment = {
  production: true

};
//variable Session
export const UrlApi = 'https://groupeidev.net/Assurus24/api';
//export const UrlApi = 'http://localhost:8080/Assurus24/api' ;
export const UrlApiLogo =UrlApi+"/file/downloadLogo/"
export const UrlApiLogoReseau= UrlApi+"/file/downloadLogoReseau/"
export const downloadCNI = "downloadCNI"
export const downloadCarteGrise = "downloadCarteGrise"
export const downloadControleTechnique = "downloadControleTechnique"
export const downloadConditionsGenerales = "downloadConditionsGenerales"
export const downloadBaniere = "downloadBaniere"
export const idSessionUser = 'idSessionUser';
export const INDICATIFDEFAUT = "+221" ;
export const CONTACTSESSION ="contact" ;
export const CLIENTPARTICULIERSESSION="clientParticulierSession"
export const CLIENTPROFESSIONNELSESSION="clientProfessionnelSession"
export const LISTECONTACTCLIENTPROFESSIONNELSESSION="listecontactclientProfessionnelSession"
export const CONTACTDECLIENTPROFESSIONNELSESSION = "contactDeclientProfessionnelSession"
export const INDEXCONTACTDECLIENTPROFESSIONNELSESSION = "indexContactDeclientProfessionnelSession"
export const LOGIN = "login" ;
export const CNI ="cniUpload" ;
export const CNIPATHNAME = "cni.pdf" ;
export const BaniereName = "baniere.svg" ;
export const CARTEGRISEPATHNAME = "carteGrise.pdf" ;
export const CONTROLETECHNIQUEPATHNAME = "controleTechnique.pdf" ;
export const CONDITIONSPATHNAME = "conditionsGenerale.pdf" ;
export const CODE ="code" ;
export const NBESSAISAUTORISES = 3 ;
export const CODECONFIRMATION="codeConfirmation" ;
export const CODERESTORE = 'CODE';
export const IDINSCRIPTION = 'idInscription';
//Session Client Particulioer au lieu de Compte
export const SessionClient = "sessionClient" ;
export const SessionClientUpdate = "sessionClientUpdate" ;
export const SessionCompte = "sessionCompte" ;
export const SessionCompteUpdate = "sessionCompteUpdate" ;
export const numeroPivot ="numeroPivot"

//Code civilité home et femme
export const CodeHomme = "Mr" ;
export const codeFemme = "Mme" ;


//DESCRIPTIONS BRANCHES
export const DescAutos ="<h1>Informations générales</h1><h2>SECTION 1</h2><p>Quam ob rem circumspecta cautela observatum est deinceps </p> <p>et cum edita montium petere coeperint grassatores, loci iniquitati milites cedunt. ubi autem in planitie potuerint reperiri, quod contingit adsidue, nec exsertare lacertos nec crispare permissi tela, quae vehunt bina vel terna, pecudum ritu inertium trucidantur.Huic Arabia est conserta, ex alio latere Nabataeis contigua; opima varietate conmerciorum castrisque oppleta validis et castellis, quae ad repellendos gentium vicinarum excursus sollicitudo pervigil veterum per oportunos saltus erexit et cautos. haec quoque civitates habet inter oppida quaedam ingentes Bostram et Gerasam atque Philadelphiam murorum firmitate cautissimas. hanc provinciae inposito nomine rectoreque adtributo obtemperare legibus nostris Traianus conpulit imperator incolarum tumore saepe contunso cum glorioso marte Mediam urgeret et Partho.Utque proeliorum periti rectores primo catervas densas opponunt et fortes, deinde leves armaturas, post iaculatores ultimasque subsidiales acies, si fors adegerit, iuvaturas, ita praepositis urbanae familiae suspensae digerentibus sollicite,</p> <p> quos insignes faciunt virgae dexteris aptatae velut tessera data castrensi iuxta vehiculi frontem omne textrinum incedit: huic atratum coquinae iungitur ministerium, dein totum promiscue servitium cum otiosis plebeiis de vicinitate coniunctis: postrema multitudo spadonum a senibus in pueros desinens, obluridi distortaque lineamentorum conpage deformes, ut quaqua incesserit quisquam cernens mutilorum hominum agmina detestetur memoriam Samiramidis reginae illius veteris, quae teneros mares castravit omnium prima velut vim iniectans naturae, eandemque ab instituto cursu retorquens, quae inter ipsa oriundi crepundia per primigenios seminis fontes tacita quodam modo lege vias propagandae posteritatis ostendit.Qui cum venisset ob haec festinatis itineribus Antiochiam, praestrictis palatii ianuis, contempto Caesare, quem videri decuerat, ad praetorium cum pompa sollemni perrexit morbosque diu causatus nec regiam introiit nec processit in publicum, sed abditus multa in eius moliebatur exitium addens quaedam relationibus supervacua, quas subinde dimittebat ad principem.Cuius acerbitati uxor grave accesserat incentivum, germanitate Augusti turgida supra modum, quam Hannibaliano regi fratris filio antehac Constantinus iunxerat pater, Megaera quaedam mortalis, inflammatrix saevientis adsidua, humani cruoris avida nihil mitius quam maritus; qui paulatim eruditiores facti processu temporis ad nocendum per clandestinos versutosque rumigerulos conpertis leviter addere quaedam male suetos falsa et placentia sibi discentes, adfectati regni vel artium nefandarum calumnias insontibus adfligebant.</p>" ;
export const DescSante =DescAutos ;
export const DescMaison=DescAutos ;
export const DescMoto =DescAutos ;
export const DescBateau =DescAutos ;
export const DescVoyage=DescAutos ;
export const DescSinistre=DescAutos ;
export const DescDepannage=DescAutos ;
export const DescConstat=DescAutos ;
export const DescExpertise=DescAutos ;
//FIN DESCRIPTIONS BRANCHES
//noms branches
export const NomBrancheAuto ="Automobile" ;
export const NomBrancheSante ="Sante" ;
export const NomBrancheMaison="Maison" ;
export const NomBrancheMoto = "Moto";
export const NomBrancheBateau ="Bateau" ;
export const NomBrancheVoyage="Voyage" ;

//fin nom branches
//session contrat objet
export const CONTRATSESSION ="contrat" ;
export const OBJETSESSION ="objet" ;



//Fin session  contrat objet
//alertNotification

//
export const ConditionsSpeciale ="Quam ob rem circumspecta cautela observatum est deinceps";
export const ConditionsDisposition= "<p>Quam ob rem circumspecta cautela observatum est deinceps </p> <p>et cum edita montium petere coeperint grassatores, loci iniquitati milites cedunt. ubi autem in planitie potuerint reperiri, quod contingit adsidue, nec exsertare lacertos nec crispare permissi tela, quae vehunt bina vel terna, pecudum ritu inertium trucidantur.Huic Arabia est conserta, ex alio latere Nabataeis contigua; opima varietate conmerciorum castrisque oppleta validis et castellis, quae ad repellendos gentium vicinarum excursus sollicitudo pervigil veterum per oportunos saltus erexit et cautos. haec quoque civitates habet inter oppida quaedam ingentes Bostram et Gerasam atque Philadelphiam murorum firmitate cautissimas. hanc provinciae inposito nomine rectoreque adtributo obtemperare legibus nostris Traianus conpulit imperator incolarum tumore saepe contunso cum glorioso marte Mediam urgeret et Partho.Utque proeliorum periti rectores primo catervas densas opponunt et fortes, deinde leves armaturas, post iaculatores ultimasque subsidiales acies, si fors adegerit, iuvaturas, ita praepositis urbanae familiae suspensae digerentibus sollicite,</p> <p> quos insignes faciunt virgae dexteris aptatae velut tessera data castrensi iuxta vehiculi frontem omne textrinum incedit: huic atratum coquinae iungitur ministerium, dein totum promiscue servitium cum otiosis plebeiis de vicinitate coniunctis: postrema multitudo spadonum a senibus in pueros desinens, obluridi distortaque lineamentorum conpage deformes, ut quaqua incesserit quisquam cernens mutilorum hominum agmina detestetur memoriam Samiramidis reginae illius veteris, quae teneros mares castravit omnium prima velut vim iniectans naturae, eandemque ab instituto cursu retorquens, quae inter ipsa oriundi crepundia per primigenios seminis fontes tacita quodam modo lege vias propagandae posteritatis ostendit.Qui cum venisset ob haec festinatis itineribus Antiochiam, praestrictis palatii ianuis, contempto Caesare, quem videri decuerat, ad praetorium cum pompa sollemni perrexit morbosque diu causatus nec regiam introiit nec processit in publicum, sed abditus multa in eius moliebatur exitium addens quaedam relationibus supervacua, quas subinde dimittebat ad principem.Cuius acerbitati uxor grave accesserat incentivum, germanitate Augusti turgida supra modum, quam Hannibaliano regi fratris filio antehac Constantinus iunxerat pater, Megaera quaedam mortalis, inflammatrix saevientis adsidua, humani cruoris avida nihil mitius quam maritus; qui paulatim eruditiores facti processu temporis ad nocendum per clandestinos versutosque rumigerulos conpertis leviter addere quaedam male suetos falsa et placentia sibi discentes, adfectati regni vel artium nefandarum calumnias insontibus adfligebant.</p>" ;
export const messageContrat ="<p>Quam ob rem circumspecta cautela observatum est deinceps </p> <p>et cum edita montium petere coeperint grassatores, loci iniquitati milites cedunt. ubi autem in planitie potuerint reperiri, quod contingit adsidue, nec exsertare lacertos nec crispare permissi tela, quae vehunt bina vel terna, pecudum ritu inertium trucidantur.Huic Arabia est conserta, ex alio latere Nabataeis contigua; opima varietate conmerciorum castrisque oppleta validis et castellis, quae ad repellendos gentium vicinarum excursus sollicitudo pervigil veterum per oportunos saltus erexit et cautos. haec quoque civitates habet inter oppida quaedam ingentes Bostram et Gerasam atque Philadelphiam murorum firmitate cautissimas. hanc provinciae inposito nomine rectoreque adtributo obtemperare legibus nostris Traianus conpulit imperator incolarum tumore saepe contunso cum glorioso marte Mediam urgeret et Partho.Utque proeliorum periti rectores primo catervas densas opponunt et fortes, deinde leves armaturas, post iaculatores ultimasque subsidiales acies, si fors adegerit, iuvaturas, ita praepositis urbanae familiae suspensae digerentibus sollicite,</p> <p> quos insignes faciunt virgae dexteris aptatae velut tessera data castrensi iuxta vehiculi frontem omne textrinum incedit: huic atratum coquinae iungitur ministerium, dein totum promiscue servitium cum otiosis plebeiis de vicinitate coniunctis: postrema multitudo spadonum a senibus in pueros desinens, obluridi distortaque lineamentorum conpage deformes, ut quaqua incesserit quisquam cernens mutilorum hominum agmina detestetur memoriam Samiramidis reginae illius veteris, quae teneros mares castravit omnium prima velut vim iniectans naturae, eandemque ab instituto cursu retorquens, quae inter ipsa oriundi crepundia per primigenios seminis fontes tacita quodam modo lege vias propagandae posteritatis ostendit.Qui cum venisset ob haec festinatis itineribus Antiochiam, praestrictis palatii ianuis, contempto Caesare, quem videri decuerat, ad praetorium cum pompa sollemni perrexit morbosque diu causatus nec regiam introiit nec processit in publicum, sed abditus multa in eius moliebatur exitium addens quaedam relationibus supervacua, quas subinde dimittebat ad principem.Cuius acerbitati uxor grave accesserat incentivum, germanitate Augusti turgida supra modum, quam Hannibaliano regi fratris filio antehac Constantinus iunxerat pater, Megaera quaedam mortalis, inflammatrix saevientis adsidua, humani cruoris avida nihil mitius quam maritus; qui paulatim eruditiores facti processu temporis ad nocendum per clandestinos versutosque rumigerulos conpertis leviter addere quaedam male suetos falsa et placentia sibi discentes, adfectati regni vel artium nefandarum calumnias insontibus adfligebant.</p>" ;


//statut objet
export const statutValidationKo=1;
export const statutValidationEnCours =2;

export const statutValideHorsContrat=3;
export const statutValideEnCours=    4 ;
//fin statut objet

export const ListeSimulationEnSession="listeSimulation";
export const listeSimulationDansFlotte ="liste simulation dans flotte"
export const listeSimulationDansFlotteValide ="liste simulation dans flotte valide"
export const retourFlotte ="retoutFlotte"
export const listeOperationFlotte = "liste operation flotte" ;
export const listeObjetFlotte = "liste objet flotte" ;
export const  listeContratFlotte ="liste contrat flotte"
export const  listeAssureContratFlotte ="liste assure contrat flotte"
export const simulationActifenSession = "simulationActifEnSession" ;
export const contratActifenSession = "contratActifEnSession" ;
export const assureContratActifenSession = "assureContratActifenSession" ;
export const objetActifenSession = "objetActifEnSession" ;
export const operationActifenSession = "operationActifEnSession" ;
export const operationSimActifenSession = "operationSimActifEnSession" ;
//routine contrat variable session
export const listeRoutineContratSession ="ListeRoutineContratenSession"
export const indexRoutineContratEditingSession ="indexRoutineContratEditingSession"
//
//
export const debutRoutine ="debutRoutine" ;
export const OriginePageGesSim ="page de gestion mes simulations " ;
 export const flotte ="flotte ou only"
 export const editOptions ="edit options" ;
 export const editObjet = "edit objet"
export const OriginePageMonObjet="page mon objet" ;
export const OriginePageMesObjets="page mes objets" ;
export const OrigineClientNonLogue="origine client non logue" ;
export const OrigineMesContrats ="origine mes contrats" ;
export const OrigineMonContrat ="origine mon contrat" ;
export const OriginePageGesSimReseau ="origine page gestion simulation reseau" ;
//dispachement des routines et contrats
export const typeRoutine = "type de routine" ;
export const typeRoutineNewContrat = "type de routine nouveau contrat" ;
export const typeRoutineRenewContrat = "type de routine renouveller contrat" ;
export const startRenouvellement ="start renouvellement" ;
export const typeRoutineSimulation ="type de routine simulation"
export const typeRoutineNewObject ="type de routine nouveau  objet"
export const typeRoutineEditObject ="type de routine modifier objet"

export const typeRenouvellement = "type de renouvellement" ;
export const typeRenouvellementMemeCa = "type de renouvellement meme ca" ;
export const typeRenouvellementChangementDeCA = "type de renouvellement changement de CA" ;
export const simRoutine = "sim routine" ;
export const contRoutine = "cont routine" ;
export const checkParam= "check" ;
export const initParam = "init" ;
export const initialeCG ="cg" ;
export const initialeCT ="ct" ;
export const typeSousBranche ="typeSousBranche" ;
export const MARQUES:Marque[] =[

  { id : 0,
  name: 'Peugeot' ,
  listeModeles:[{id :0 ,name : "308 CC"}, {id:1,name :"6008 4x4"},{id :2,name :"504 II"},{id:3,name : "4007"}],
  path : "../assets/img/LogoMarque/peugeot.jpg"
  },
  { id : 1 ,
    name: 'Toyota' ,
    listeModeles:[{id :0 ,name : "Auris"},{id:1,name :"Avensis"},{id :2,name :"Corolla"},{id:3,name : "Prius"},{id:4,name:"Tacoma"}],
    path : "../assets/img/LogoMarque/toyota.jpg"} ,
  { id : 2 ,
    name: 'Renault' ,
    listeModeles:[{id :0 ,name : "Captur"},{id:1,name :"Clio"},{id :2,name :"Espace"},{id:3,name : "Kadjar"},{id:4,name:"Kangoo"}],
    path : "../assets/img/LogoMarque/renault.jpg"
  } ,

];
export class Marque {
  public id :number ;
  public name: string;
  public listeModeles : Array<Modele>;
  public path:string ;
}
export class Modele{
  public id :number ;
  public name: string;

}

///classe banniereCom provisoire
export class  banniereCom{
  public id :number ;


}
//sous usage

export const UsagepersonnelEtTourisme = "CATEGORIE 1:\nVéhicules Test  particuliers et de tourisme"
export const UsageTransportProduit = "CATEGORIE 2:\nVéhicules de transport privé de marchandises et de produits"
export const UsageTransportPayantProduit = "CATEGORIE 3:\nVéhicules de transport public de marchandises ou\nde produits"
export const UsageTransportVoyageur = "CATEGORIE 4:\nVéhicules de transport public de voyageurs "
export const UsageMotoPersonnelEtTourisme = "CATEGORIE 5:\nVéhicules 2 roues, 3 roues, et Véhicules légers à 4 roues"
export const catTransportPersonnel2Roues="catTransportPersonnel2Roues"
export const catTransportPersonneletTourisme2Roues="catTransportPersonneletTourisme2Roues"
export const catTransportProduitMateriel2Roues = "catTransportProduitMateriel2Roues"
export const cyclomoteur= "cyclomoteur"
export const scootVelomoteurLittleCat = "scootVelomoteurLittleCat"
export const scootVelomoteurBigCat = "scootVelomoteurBigCat"
export const sidecar="sidecar"
export const triporteur="triporteur"
export const moto4Roues ="moto4Roues"

 export const utilisationPrivee =  "utilisationPrivee"
export const  utilisationCommerciale =  "utilisationCommerciale"
export const carosserieTourisme ="carosserieTourisme"
export const autreCarroserieLittlePoids = "autreCarroserieLittlePoids"
export const autreCarroserieBigPoids = "autreCarroserieBigPoids"




export const statutContratEnCoursDeValidation="validation en cours"
export const statutContratActif ="actif"
export const  statutContratValideNonActif="valide non actif"
export const statutContratSuspendu=" suspendu"
export const statutContratResilie=" resilie"
export const statutContratExpire=" expire"

//export const statutPrestation
export const statutDemandeEnCoursDeTraitement = "En Cours De Traitement"
export const statutDemandeAffectee = "Affectee "
export const statutDemandePriseEnCharge = "Prise En Charge"
export const statutDemandeCloturee = "cloturee"
//type prestation
export const ServiceConstat = "serviceConstat"
export const ServiceDepannage = "serviceDepannage"
export const ServiceExpertise  ="serviceExpertise"
//serviceSession
export const PrestationServiceSession = "PrestationServiceSession"


//devise
export const devise ="FCFA"
//message

export const MessageChargementEnCours ="Chargement en cours veuillez patienter"
export const MessageVerificationReseau = "Erreur connexion veuillez vérifier votre réseau et recommencer"
export const MessageErreur = "Le chargement a  echoué veuillez réessayer"
export const MessageChangementClient = "Chargement du client veuillez patienter"

//reprise apres login
export const loginToPage ="loginToCreationObjet"
//minimum maximum
export const constMinNombrePlace: number = 1;
export const constMaxNombrePlace: number = 100;
export const constMaxAnnee: number = 2020;
export const constMinAnnee: number =1960;


//page de gestion simulation
export const resultatFlotteOrigineGesSimulation="resultatFlotteOrigineGesSimulation" ;
export const listeFlotteOrigineGestionSimulation="listeFlotteOrigineGestionSimulation" ;



//monObjetVersOperation

export const OperationActifDeplie="operationActfDeplie"


//ROutine INSCRIPTION OU UPDATE PROFIL
export const typeRoutineProfil ="typeRoutineProfil"

export const routineInscription ="routineInscription"
export const routineInscriptionPro ="routineInscriptionPro"


export const routineInscriptionClientReseau = "routineInscriptionClientReseau"
export const routineUpdateProfil="routineUpdateProfil"
export const routineUpdateProfilPro="routineUpdateProfilPro"
export const routineResetPassword="routineResetPassword"



//liens vers les pages
export const LIENLOGIN="/login"
export const LIENMESCONTRATS ="/mes-contrats"
export const LIENEPOCNO ="/epocno"
export const LIENCHOICEOPTION ="/choiceoption"
export const LIENASSURINFOVE ="/assurinfove"
export const LIENASSURINFOVEVEHICULEFLOTTE="/assurinfove-vehicule-flotte"
export const LIENSIMBRANAUTO ="/simbranauto"
export const LIENSIMDECLAOBJET="/sim-decla-objet"
export const LIENMONOBJET ="/monobjet"
export const LIENECRANPAGESIMULATION ="/ecranPageSimulation"
export const LIENGESTIONMESSIMULATION="/gestion-mes-simulations"
export const LIENCONTRAT = "/contrat"
export const LIENECRANRECAPITULATIF = "/ecran-recapitulatif"
export const LIENACCUEILCOMMERCIAL = "/accueil-commercial"
export const LIENMESCLIENTS ="/mes-clients"
export const LIENMONCOMPTECREDIT="/moncomptecredit"
export const LIENCONTACT ="/contact"
export const LIENSELECTIONPROFIL="/selection-profil"
export const LIENUPLOADDOC = "/uploaddoc"
export const LIENIDENTIFICATION = "/identification"
export const LIENPAGECLIENTCOMMERCIAL= "/page-client-commercial"
export const LIENACCUEILCLIENTLOGGE = "/accueil-client-logge"
export const LIENMYPROFILE="myprofile"
export const LIENMONSERVICE="/monservice"
export const LIENLISTECONTACTProfessionnel= "/liste-contact-professionel"
export const LIENFORMCONTACTSDEPROFESSIONEL = "/form-contacts-de-professionel"

//clients vendeur
export const CLIENTENSESSION="CLIENTENSESSION"
//Gestion Paiement paytech
export const MessagePaiementErreurConnexion="Oups Erreur!!!Veuillez vérifier votre connexion et recommencer"

export var utilsGenerale  : UtilsService ;
export var iab : InAppBrowser = new InAppBrowser ;
